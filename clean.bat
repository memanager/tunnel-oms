echo off & color 0A
set DIR="%cd%"
echo DIR=%DIR%
echo deleting non source files...
pause
echo delete target dirs...
for /r /d %%f in (target) do ( 
	if exist %%f (
		echo %%f
		rd /s /q %%f
	)
)
echo delete .svn dirs...
for /r /d %%f in (.git) do ( 
	if exist %%f (
		echo %%f
		rd /s /q %%f
	)
)
echo delete .idea dirs...
for /r /d %%f in (.idea) do ( 
	if exist %%f (
		echo %%f
		rd /s /q %%f
	)
)
echo delete .iml files...
for /r %%f in (*.iml) do ( 
	if exist %%f (
		echo %%f
		del %%f
	)
)
pause

