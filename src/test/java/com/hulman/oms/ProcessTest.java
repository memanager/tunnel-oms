package com.hulman.oms;

import com.hulman.oms.bean.ProcInst;
import com.hulman.oms.bean.Task;
import com.hulman.oms.service.ProcInstService;
import com.hulman.oms.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class ProcessTest
{
    @Autowired
    private ProcInstService procInstService;
    @Autowired
    private TaskService taskService;

    @Test
    public void testCheck()
    {
        Task task = new Task();
        String key = "check";
        ProcInst procInst = procInstService.startProcess(key);
        log.info("processInst:{}", procInst);
        task.setProcInstId(procInst.getId());
        task.setName("任务：" + procInst.getStateName());
        task.setOwnerId(38);
        task.setTag("点检");
        taskService.addTask(task);
        log.info("新建任务:{}", task);
        taskService.createAndCompleteTask(task.getId());
        log.info("完成任务ID：{}", task.getId());
        while (procInst.getState() > 0)
        {
            procInst = procInstService.next(procInst.getId());
            log.info("processInst:{}", procInst);
            if (procInst.getState() > 0)
            {
                task = new Task();
                task.setProcInstId(procInst.getId());
                task.setName("任务：" + procInst.getStateName());
                task.setOwnerId(38);
                task.setTag("点检");
                taskService.addTask(task);
                log.info("新建任务:{}", task);
                taskService.createAndCompleteTask(task.getId());
                log.info("完成任务ID：{}", task.getId());
            }
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }

    @Test
    public void testTasks()
    {
        List<Task> tasks = taskService.findOwnerTasks(38);
        log.info("show owner tasks:");
        for (Task task : tasks)
        {
            log.info("task：{}", task);
        }
    }

}
