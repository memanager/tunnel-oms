package com.hulman.oms;

import com.hulman.codegenerator.generator.GeneratorRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.sql.SQLException;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"classpath:spring-generator.xml"})
public class CodeGenerator
{
    @Autowired
    private ApplicationContext context;

    @Test
    public void generateCode() throws IOException, SQLException
    {
        GeneratorRunner runner = new GeneratorRunner(context);
        runner.run();
    }
}
