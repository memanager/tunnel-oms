package com.hulman.oms.util;

import java.util.Random;

/**
 * @Author: maxwellens
 */
public class CodeGenerator
{
    private static final String[] codeBase = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private static final String[] secretBase = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static String nextCode()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++)
        {
            Random random = new Random();
            int index = random.nextInt(codeBase.length);
            sb.append(codeBase[index]);
        }
        return sb.toString();
    }

    public static String nextSecret()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 24; i++)
        {
            Random random = new Random();
            int index = random.nextInt(secretBase.length);
            sb.append(secretBase[index]);
        }
        return sb.toString();
    }

    public static void main(String[] args)
    {
        for (int i = 0; i < 168; i++)
        {
            System.out.println(CodeGenerator.nextSecret());
        }
    }

}
