package com.hulman.oms.util;

import java.io.UnsupportedEncodingException;

/**
 * Base64编解码器
 */
public class Base64
{
    /**
     * 编码
     *
     * @param text
     * @return
     */
    public static String encode(String text)
    {
        try
        {
            return java.util.Base64.getEncoder().encodeToString(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解码
     *
     * @param text
     * @return
     */
    public static String decode(String text)
    {
        try
        {
            return new String(java.util.Base64.getDecoder().decode(text), "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
