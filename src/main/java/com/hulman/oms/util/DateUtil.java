package com.hulman.oms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: maxwellens
 */
public class DateUtil
{
    public static final String[] WEEK_DAYS = {"日", "一", "二", "三", "四", "五", "六"};

    public static Date addYears(Date date, Integer years)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }

    public static Date addMonths(Date date, Integer months)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static Date addDays(Date date, Integer days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date addHours(Date date, Integer hours)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }

    public static Date addMinutes(Date date, Integer minutes)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static String toDate(Date date)
    {
        if (date == null)
        {
            return "";
        } else
        {
            return new SimpleDateFormat("yyyy-MM-dd").format(date);
        }
    }

    public static String toChineseDate(Date date)
    {
        if (date == null)
        {
            return "";
        } else
        {
            return new SimpleDateFormat("yyyy年MM月dd日").format(date);
        }
    }

    public static String toDateTime(Date date)
    {
        return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(date);
    }

    public static String toTime(Date date)
    {
        return new SimpleDateFormat("hh:mm:ss").format(date);
    }

    public static String toSimpleTime(Date date)
    {
        if (date == null)
        {
            return "";
        }
        Date now = new Date();
        int days = 0;
        try
        {
            days = daysBetween(date, now);
        } catch (ParseException e)
        {
            e.printStackTrace();
            return null;
        }
        String dayStr = "";
        if (days == 0)
        {
            dayStr = "今天";
        } else if (days == 1)
        {
            dayStr = "昨天";
        } else if (days == 2)
        {
            dayStr = "前天";
        } else
        {
            dayStr = toDate(date);
        }
        return dayStr + " " + new SimpleDateFormat("HH:mm").format(date);
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param date1 较小的时间
     * @param date2 较大的时间
     * @return 相差天数
     * @throws ParseException
     */
    public static int daysBetween(Date date1, Date date2) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        date1 = sdf.parse(sdf.format(date1));
        date2 = sdf.parse(sdf.format(date2));
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long days = (time2 - time1) / (1000 * 3600 * 24);
        return (int) days;
    }

    public static Date parseDate(String date)
    {
        try
        {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e)
        {
            return null;
        }
    }

    public static Date parseDateTime(String date)
    {
        try
        {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
        } catch (ParseException e)
        {
            return null;
        }
    }

    public static String toChineseMinutes(Integer total)
    {
        if (total != null)
        {
            if (total == 0)
            {
                return "<1分钟";
            }
            int minutes = total;
            int days = minutes / (60 * 24);
            minutes = minutes - days * 60 * 24;
            int hours = minutes / 60;
            minutes = minutes - hours * 60;
            return (days > 0 ? (days + "天") : "") + (hours > 0 ? (hours + "小时") : "") + (minutes > 0 ? (minutes + "分钟") : "");
        } else
        {
            return "";
        }
    }

    public static String toChineseSeconds(Integer total)
    {
        if (total != null)
        {
            int seconds = total;
            int days = seconds / (3600 * 24);
            seconds = seconds - days * 3600 * 24;
            int hours = seconds / 3600;
            seconds = seconds - hours * 3600;
            int minutes = seconds / 60;
            seconds = seconds - minutes * 60;
            return (days > 0 ? (days + "天") : "") + (hours > 0 ? (hours + "小时") : "") + (minutes > 0 ? (minutes + "分") : "") + (seconds > 0 ? (seconds + "秒") : "");
        } else
        {
            return "";
        }
    }

    public static String toBriefChineseSeconds(Integer total)
    {
        if (total != null)
        {
            if (total < 60)
            {
                return "不到1分钟";
            } else
            {
                int seconds = total;
                int days = seconds / (3600 * 24);
                seconds = seconds - days * 3600 * 24;
                int hours = seconds / 3600;
                seconds = seconds - hours * 3600;
                int minutes = seconds / 60;
                return (days > 0 ? (days + "天") : "") + (hours > 0 ? (hours + "小时") : "") + (minutes > 0 ? (minutes + "分钟") : "");
            }
        } else
        {
            return "";
        }
    }

    public static String toChineseWeekDay(int day)
    {
        if (day >= 0 && day < 7)
        {
            return WEEK_DAYS[day];
        } else
        {
            return "";
        }
    }

    public static String getDaysText(Integer type, String days)
    {
        if (type == null)
        {
            return "";
        }
        if (type == 1)
        {
            return "每天";
        }
        if (type == 2)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            }
            try
            {
                Integer dayOfWeek = Integer.parseInt(days);
                return "每周" + DateUtil.toChineseWeekDay(dayOfWeek);
            } catch (Exception ex)
            {
                return "";
            }
        }
        if (type == 3)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            } else
            {
                return "每月" + days + "号";
            }
        }
        if (type == 4)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            } else
            {
                return "季初" + days + "号";
            }
        }
        if (type == 5)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            } else
            {
                return "季中" + days + "号";
            }
        }
        if (type == 6)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            } else
            {
                return "季末" + days + "号";
            }
        }
        if (type == 7)
        {
            if (StringUtil.isEmpty(days))
            {
                return "";
            } else
            {
                String[] strs = days.split("-");
                return "每年" + strs[1] + "月" + strs[2] + "日";
            }
        }
        return "";
    }
}
