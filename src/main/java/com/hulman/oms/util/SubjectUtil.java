package com.hulman.oms.util;

import com.hulman.oms.bean.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * @Author: maxwellens
 */
public class SubjectUtil
{
    public static User getUser()
    {
        Subject subject = SecurityUtils.getSubject();
        Object user = subject.getPrincipal();
        if (user == null)
        {
            return null;
        }
        if (user instanceof User)
        {
            return (User) user;
        } else
        {
            return null;
        }
    }

}
