package com.hulman.oms.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class HttpUtil
{
    public static String getCookieValue(HttpServletRequest request, String name)
    {
        Cookie[] cookies = request.getCookies();
        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if (cookie.getName().equals(name))
                {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static String getUrl(HttpServletRequest request)
    {
        String uri = request.getRequestURI();
        String query = request.getQueryString();
        if (StringUtil.isEmpty(query))
        {
            return uri;
        } else
        {
            return uri + "?" + query;
        }
    }
}
