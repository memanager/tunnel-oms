package com.hulman.oms.util;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class CipherUtil
{
    public static String encrypt(String account, String password)
    {
        ByteSource salt = ByteSource.Util.bytes(account);
        Object obj = new SimpleHash("MD5", password, salt, 2);
        return obj.toString();
    }

    public static void main(String[] args)
    {
        String password = CipherUtil.encrypt("dengyunyao", "12345678");
        System.out.println(password);
    }
}
