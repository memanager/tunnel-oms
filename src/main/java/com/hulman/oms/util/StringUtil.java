package com.hulman.oms.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @Author: maxwellens
 */
public class StringUtil
{
    public static boolean isEmpty(String text)
    {
        return text == null || "".equals(text);
    }

    public static String concat(List<String> list, String seperator)
    {
        if (list == null || list.size() == 0)
        {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String string : list)
        {
            if (string != null && string.length() > 0)
            {
                sb.append(string).append(seperator);
            }
        }
        if (sb.length() > 0)
        {
            return sb.substring(0, sb.length() - seperator.length());
        } else
        {
            return "";
        }
    }

    public static String concat(List<String> list)
    {
        return concat(list, ",");
    }

    public static String concat(String[] strings, String seperator)
    {
        return concat(Arrays.asList(strings), seperator);
    }

    public static String concat(String... strings)
    {
        return concat(Arrays.asList(strings), ",");
    }

    public static int[] splitInt(String text, String splitter)
    {
        String[] strs = text.split(splitter);
        int[] ints = new int[strs.length];
        for (int i = 0; i < strs.length; i++)
        {
            ints[i] = Integer.parseInt(strs[i]);
        }
        return ints;
    }

    public static int[] splitInt(String text)
    {
        return splitInt(text, ",");
    }

    public static String uuid()
    {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 汉语首字符
     *
     * @param chinese
     * @return
     */
    public static String getPinyinFirstLetters(String chinese)
    {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

        StringBuilder firstPinyin = new StringBuilder();
        char[] chnArr = chinese.trim().toCharArray();
        try
        {
            for (int i = 0, len = chnArr.length; i < len; i++)
            {
                if (Character.toString(chnArr[i]).matches("[\\u4E00-\\u9FA5]+"))
                {
                    String[] pys = PinyinHelper.toHanyuPinyinStringArray(chnArr[i], format);
                    firstPinyin.append(pys[0].charAt(0));
                } else
                {
                    firstPinyin.append(chnArr[i]);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination)
        {
            badHanyuPinyinOutputFormatCombination.printStackTrace();
        }
        return firstPinyin.toString().toUpperCase();
    }


    public static String getPinyin(String chinese)
    {
        //输出格式设置
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        /**
         * 输出大小写设置
         *
         * LOWERCASE:输出小写
         * UPPERCASE:输出大写
         */
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);

        /**
         * 输出音标设置
         *
         * WITH_TONE_MARK:直接用音标符（必须设置WITH_U_UNICODE，否则会抛出异常）
         * WITH_TONE_NUMBER：1-4数字表示音标
         * WITHOUT_TONE：没有音标
         */
        //format.setToneType(HanyuPinyinToneType.WITH_TONE_MARK);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

        /**
         * 特殊音标ü设置
         *
         * WITH_V：用v表示ü
         * WITH_U_AND_COLON：用"u:"表示ü
         * WITH_U_UNICODE：直接用ü
         */
        //format.setVCharType(HanyuPinyinVCharType.WITH_U_UNICODE);

        char[] hanYuArr = chinese.trim().toCharArray();
        StringBuilder pinYin = new StringBuilder();

        try
        {
            for (int i = 0, len = hanYuArr.length; i < len; i++)
            {
                //匹配是否是汉字
                if (Character.toString(hanYuArr[i]).matches("[\\u4E00-\\u9FA5]+"))
                {
                    //如果是多音字，返回多个拼音，这里只取第一个
                    String[] pys = PinyinHelper.toHanyuPinyinStringArray(hanYuArr[i], format);
                    pinYin.append(pys[0]);
                } else
                {
                    pinYin.append(hanYuArr[i]);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination)
        {
            badHanyuPinyinOutputFormatCombination.printStackTrace();
        }
        return pinYin.toString().toLowerCase();
    }


    /**
     * 姓名简写
     *
     * @param name
     * @return
     */
    public static String getAbbrName(String name)
    {
        if (StringUtil.isEmpty(name))
        {
            return "";
        } else if (name.length() <= 2)
        {
            return name;
        } else
        {
            return name.substring(name.length() - 2);
        }
    }

    public static String padRight(String src, char padding, int length)
    {
        int diff = length - src.length();
        if (diff <= 0)
        {
            return src;
        }
        char[] charr = new char[length];
        System.arraycopy(src.toCharArray(), 0, charr, 0, src.length());
        for (int i = src.length(); i < length; i++)
        {
            charr[i] = padding;
        }
        return new String(charr);
    }

    public static String padLeft(String src, char padding, int length)
    {
        int diff = length - src.length();
        if (diff <= 0)
        {
            return src;
        }

        char[] charr = new char[length];
        System.arraycopy(src.toCharArray(), 0, charr, diff, src.length());
        for (int i = 0; i < diff; i++)
        {
            charr[i] = padding;
        }
        return new String(charr);
    }

    public static void main(String[] args)
    {
        System.out.println(getPinyin("dengyunyao1"));
    }

}
