package com.hulman.oms.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil
{
    private static final ObjectMapper mapper = new ObjectMapper();

    public static Object parse(String text)
    {
        if (StringUtil.isEmpty(text))
        {
            return null;
        } else
        {
            try
            {
                return mapper.readValue(text, Object.class);
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String toJSONString(Object obj)
    {
        try
        {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
