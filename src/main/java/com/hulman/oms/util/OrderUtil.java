package com.hulman.oms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 工单号生成工具
 */
public class OrderUtil
{
    /**
     * 生成工单号
     * @param type
     * @return
     */
    public static String generate(String type)
    {
        return type + new SimpleDateFormat("yyMMddHHmmss").format(new Date());
    }

}
