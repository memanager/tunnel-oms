package com.hulman.oms.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NumberUtil
{
    public static boolean contains(int[] arrays, int value)
    {
        for (int i = 0; i < arrays.length; i++)
        {
            if (arrays[i] == value)
            {
                return true;
            }
        }
        return false;
    }

    public static int[] parseIntArray(String str)
    {
        if (StringUtil.isEmpty(str))
        {
            return new int[0];
        } else
        {
            String[] strs = str.split(",");
            int[] result = new int[strs.length];
            for (int i = 0; i < strs.length; i++)
            {
                result[i] = Integer.parseInt(strs[i]);
            }
            return result;
        }
    }

    public static boolean hasIntersection(int[] array1, int[] array2)
    {
        // 将数组转换为集合
        Set<Integer> set1 = new HashSet<>(Arrays.asList(Arrays.stream(array1).boxed().toArray(Integer[]::new)));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(Arrays.stream(array2).boxed().toArray(Integer[]::new)));
        // 检查两个集合是否有交集
        set1.retainAll(set2);
        // 如果交集不为空，则返回true，否则返回false
        return !set1.isEmpty();
    }
}
