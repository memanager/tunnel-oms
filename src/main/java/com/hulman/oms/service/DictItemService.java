package com.hulman.oms.service;

import com.hulman.oms.bean.DictItem;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.DictItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: maxwellens
 */
@Service
public class DictItemService
{
    @Autowired
    private DictItemDao dictItemDao;

    public List<DictItem> findDictItemsByDictId(Integer dictId)
    {
        return dictItemDao.findDictItemsByDictId(dictId);
    }

    public List<DictItem> findDictItemsByDictKey(String dictKey)
    {
        return dictItemDao.findDictItemsByDictKey(dictKey);
    }

    public Result findDictItemsResultByDictId(Integer dictId)
    {
        List<DictItem> items = dictItemDao.findDictItemsByDictId(dictId);
        return new Result(items, items.size());
    }

    public Result findDictItemsResultByDictKey(String dictKey)
    {
        List<DictItem> items = dictItemDao.findDictItemsByDictKey(dictKey);
        return new Result(items, items.size());
    }

    public DictItem findDictItemById(Integer id)
    {
        return dictItemDao.findDictItemById(id);
    }

    public void saveDictItem(DictItem dictItem)
    {
        if (dictItem.getId() != null && dictItem.getId() != 0)
        {
            dictItemDao.updateDictItem(dictItem);
        } else
        {
            dictItemDao.insertDictItem(dictItem);
        }
    }

    public void deleteDictItemById(Integer id)
    {
        dictItemDao.deleteDictItemById(id);
    }

    public void deleteDictItems(String ids)
    {
        dictItemDao.deleteDictItems(ids);
    }

}
