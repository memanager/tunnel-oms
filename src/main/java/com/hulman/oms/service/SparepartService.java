package com.hulman.oms.service;

import com.hulman.oms.bean.Sparepart;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.SparepartDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: maxwellens
 */
@Service
public class SparepartService
{
    @Autowired
    private SparepartDao sparepartDao;

    public List<Sparepart> findSpareparts(Map<String, Object> map)
    {
        return sparepartDao.findSpareparts(map);
    }

    public Integer findSparepartsCount(Map<String, Object> map)
    {
        return sparepartDao.findSparepartsCount(map);
    }

    public Result findSparepartsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartDao.findSparepartsCount(map);
        List<Sparepart> data = sparepartDao.findSpareparts(map);
        return new Result(data, count);
    }

    public Result findWarnSparepartsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        List<Sparepart> sparepartList = sparepartDao.findSpareparts(map);
        List<Sparepart> data = sparepartList.stream().filter(sparepart -> sparepart.getWarnType()!=0 && sparepart.getWarnType()!=null).collect(Collectors.toList());
        int count = data.size();
        return new Result(data, count);
    }

    public Sparepart findSparepartById(Integer id)
    {
        return sparepartDao.findSparepartById(id);
    }

    public void saveSparepart(Sparepart Sparepart)
    {
        if (Sparepart.getId() != null && Sparepart.getId() != 0)
        {
            sparepartDao.updateSparepart(Sparepart);
        } else
        {
            sparepartDao.insertSparepart(Sparepart);
        }
    }

    public void deleteSparepartById(Integer id)
    {
        sparepartDao.deleteSparepartById(id);
    }

    public void deleteSpareparts(int[] ids)
    {
        sparepartDao.deleteSpareparts(ids);
    }

}
