package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.LeaderShift;
import com.hulman.oms.dao.LeaderShiftDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class LeaderShiftService
{
    @Autowired
    private LeaderShiftDao leaderShiftDao;

    public List<LeaderShift> findLeaderShifts(Map<String, Object> map)
    {
        return leaderShiftDao.findLeaderShifts(map);
    }

    public Integer findLeaderShiftsCount(Map<String, Object> map)
    {
        return leaderShiftDao.findLeaderShiftsCount(map);
    }

    public Result findLeaderShiftsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = leaderShiftDao.findLeaderShiftsCount(map);
        List<LeaderShift> data = leaderShiftDao.findLeaderShifts(map);
        return new Result(data, count);
    }

    public LeaderShift findLeaderShiftById(Integer id)
    {
        return leaderShiftDao.findLeaderShiftById(id);
    }

    public void saveLeaderShift(LeaderShift leaderShift)
    {
        if (leaderShift.getId() != null && leaderShift.getId() != 0)
        {
            leaderShiftDao.updateLeaderShift(leaderShift);
        } else
        {
            leaderShiftDao.insertLeaderShift(leaderShift);
        }
    }

    public void deleteLeaderShiftById(Integer id)
    {
        leaderShiftDao.deleteLeaderShiftById(id);
    }

    public void deleteLeaderShifts(int[] ids)
    {
        leaderShiftDao.deleteLeaderShifts(ids);
    }

}
