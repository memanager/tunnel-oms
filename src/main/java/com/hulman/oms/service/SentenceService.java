package com.hulman.oms.service;

import com.hulman.oms.bean.Sentence;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.SentenceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class SentenceService
{
    @Autowired
    private SentenceDao sentenceDao;

    public List<Sentence> findSentences(Map<String, Object> map)
    {
        return sentenceDao.findSentences(map);
    }

    public Integer findSentencesCount(Map<String, Object> map)
    {
        return sentenceDao.findSentencesCount(map);
    }

    public Result findSentencesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sentenceDao.findSentencesCount(map);
        List<Sentence> data = sentenceDao.findSentences(map);
        return new Result(data, count);
    }

    public Sentence findSentenceById(Integer id)
    {
        return sentenceDao.findSentenceById(id);
    }

    public void saveSentence(Sentence sentence)
    {
        if (sentence.getId() != null && sentence.getId() != 0)
        {
            sentenceDao.updateSentence(sentence);
        } else
        {
            sentenceDao.insertSentence(sentence);
        }
    }

    public void deleteSentenceById(Integer id)
    {
        sentenceDao.deleteSentenceById(id);
    }

    public void deleteSentences(int[] ids)
    {
        sentenceDao.deleteSentences(ids);
    }

    public Sentence randomCheckInSentence()
    {
        return sentenceDao.randomSignInSentence();
    }

    public Sentence randomCheckOutSentence()
    {
        return sentenceDao.randomSignOutSentence();
    }

}
