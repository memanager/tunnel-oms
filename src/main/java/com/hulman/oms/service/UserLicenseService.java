package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.User;
import com.hulman.oms.bean.UserLicense;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.UserLicense;
import com.hulman.oms.dao.UserLicenseDao;

import java.util.List;
import java.util.Map;

@Service
public class UserLicenseService
{

    @Autowired
    private UserLicenseDao userLicenseDao;


    public int deleteByPrimaryKey(Integer id)
    {
        return userLicenseDao.deleteByPrimaryKey(id);
    }


    public int insert(UserLicense record)
    {
        if (record.getId() != null)
        {
            return userLicenseDao.updateByPrimaryKey(record);
        }
        else
        {
            return userLicenseDao.insert(record);
        }
    }


    public int insertSelective(UserLicense record)
    {
        return userLicenseDao.insertSelective(record);
    }


    public UserLicense selectByPrimaryKey(Integer id)
    {
        return userLicenseDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(UserLicense record)
    {
        return userLicenseDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(UserLicense record)
    {
        return userLicenseDao.updateByPrimaryKey(record);
    }

    public Result findUserLicensesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = userLicenseDao.findUserLicenseCount(map);
        List<UserLicense> data = userLicenseDao.findUserLicenses(map);
        return new Result(data, count);
    }

}
