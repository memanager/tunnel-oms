package com.hulman.oms.service;

import com.hulman.oms.bean.Notice;
import com.hulman.oms.dao.NoticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Author: maxwellens
 */
@Service
public class NoticeService
{
    @Autowired
    private NoticeDao noticeDao;

    public String findNotice()
    {
        Notice notice = noticeDao.findNotice();
        if (notice == null)
        {
            return null;
        } else
        {
            return notice.getContent();
        }
    }

    public void saveNotice(String content)
    {
        Notice notice = noticeDao.findNotice();
        if (notice != null)
        {
            notice.setContent(content);
            notice.setCreateTime(new Date());
            noticeDao.updateNotice(notice);
        } else
        {
            notice = new Notice();
            notice.setContent(content);
            notice.setCreateTime(new Date());
            noticeDao.insertNotice(notice);
        }
    }

}
