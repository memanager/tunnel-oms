package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolUseItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import com.hulman.oms.dao.ToolUseItemDao;

import java.util.List;
import java.util.Map;

@Service
public class ToolUseItemService{

    @Autowired
    private ToolUseItemDao toolUseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolUseItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolUseItem record) {
        return toolUseItemDao.insert(record);
    }

    
    public int insertSelective(ToolUseItem record) {
        return toolUseItemDao.insertSelective(record);
    }

    
    public ToolUseItem selectByPrimaryKey(Integer id) {
        return toolUseItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(ToolUseItem record) {
        return toolUseItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolUseItem record) {
        return toolUseItemDao.updateByPrimaryKey(record);
    }

    public Result findToolUseItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolUseItemDao.findToolUseItemsCount(map);
        List<ToolUseItem> data = toolUseItemDao.findToolUseItems(map);
        return new Result(data, count);
    }

    public List<ToolUseItem> selectByOrderId(Integer orderId) {
        List<ToolUseItem> toolUseItemList = toolUseItemDao.findToolUseByOrderId(orderId);
        return toolUseItemList;
    }

    public void saveToolUseItem(ToolUseItem toolUseItem)
    {
        if (toolUseItem.getId() != null && toolUseItem.getId() != 0)
        {
            toolUseItemDao.updateByPrimaryKey(toolUseItem);
        } else
        {
            toolUseItemDao.insert(toolUseItem);
        }
    }
}
