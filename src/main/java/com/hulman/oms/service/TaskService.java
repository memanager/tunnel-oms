package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Task;
import com.hulman.oms.dao.TaskDao;
import com.hulman.oms.exception.IoTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class TaskService
{
    @Autowired
    private TaskDao taskDao;
    @Autowired
    private MsgService msgService;

    public List<Task> findTasks(Map<String, Object> map)
    {
        return taskDao.findTasks(map);
    }

    public Integer findTasksCount(Map<String, Object> map)
    {
        return taskDao.findTasksCount(map);
    }

    public List<Task> findProcInstTasks(Integer procInstId)
    {
        return taskDao.findProcInstTasks(procInstId);
    }

    public List<Task> findTimeoutTasks()
    {
        return taskDao.findTimeoutTasks();
    }

    public List<Task> findOnGoingTasksByProcInstId(Integer procInstId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("procInstId", procInstId);
        map.put("state", Task.ONGOING);
        return taskDao.findTasks(map);
    }

    public Result findTasksResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = taskDao.findTasksCount(map);
        List<Task> data = taskDao.findTasks(map);
        return new Result(data, count);
    }

    public Task findTaskById(Integer id)
    {
        return taskDao.findTaskById(id);
    }

    public Task findTaskByTagAndFormKey(String tag, String formKey)
    {
        return taskDao.findTaskByTagAndFormKey(tag, formKey);
    }

    /**
     * 新建任务
     *
     * @param task
     */
    public Task addTask(Task task)
    {
        task.setCreateTime(new Date());
        task.setState(Task.ONGOING);
        //超时时间精确到分，忽略秒
        if (task.getDueTime() != null)
        {
            Date dueTime = task.getDueTime();
            dueTime.setSeconds(0);
            task.setDueTime(dueTime);
        }
        taskDao.insertTask(task);
        return task;
    }

    /**
     * 新建并立刻完成任务
     *
     * @param task
     * @return
     */
    public Task createAndCompleteTask(Task task)
    {
        task.setCreateTime(new Date());
        task.setCompleteTime(new Date());
        task.setState(Task.COMPLETED);
        taskDao.insertTask(task);
        return task;
    }

    public Task saveTask(Task task)
    {
        taskDao.updateTask(task);
        return task;
    }

    /**
     * 完成任务
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    public void createAndCompleteTask(Integer id)
    {
        Task task = taskDao.findTaskById(id);
        if (task.getState() != Task.ONGOING)
        {
            throw new IoTException("该任务已经完成，无法再次执行");
        }
        if (task != null)
        {
            task.setCompleteTime(new Date());
            task.setState(Task.COMPLETED);
            taskDao.updateTask(task);
        }
    }

    /**
     * 未完成任务
     *
     * @param id
     */
    public void uncompleteTask(Integer id)
    {
        Task task = taskDao.findTaskById(id);
        if (task != null)
        {
            task.setState(Task.UNCOMPLETED);
            taskDao.updateTask(task);
        }
    }

    /**
     * 用户待办任务
     *
     * @param ownerId
     * @return
     */
    public List<Task> findOwnerTasks(Integer ownerId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("ownerId", ownerId);
        map.put("state", Task.ONGOING);
        return taskDao.findTasks(map);
    }

    public void deleteTaskById(Integer id)
    {
        taskDao.deleteTaskById(id);
    }

}
