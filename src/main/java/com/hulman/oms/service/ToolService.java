package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Tool;
import com.hulman.oms.dao.ToolDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ToolService
{
    @Autowired
    private ToolDao toolDao;

    public List<Tool> findTools(Map<String, Object> map)
    {
        return toolDao.findTools(map);
    }

    public List<Tool> findAllTools()
    {
        Map<String, Object> map = new HashMap<>();
        List<Tool> list = toolDao.findTools(map);
        return list;
    }

    public Integer findToolsCount(Map<String, Object> map)
    {
        return toolDao.findToolsCount(map);
    }

    public Result findToolsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolDao.findToolsCount(map);
        List<Tool> data = toolDao.findTools(map);
        return new Result(data, count);
    }

    public Tool findToolById(Integer id)
    {
        return toolDao.findToolById(id);
    }

    public void saveTool(Tool tool)
    {
        if (tool.getId() != null && tool.getId() != 0)
        {
            toolDao.updateTool(tool);
        } else
        {
            toolDao.insertTool(tool);
        }
    }

    public void deleteToolById(Integer id)
    {
        toolDao.deleteToolById(id);
    }

    public void deleteTools(int[] ids)
    {
        toolDao.deleteTools(ids);
    }

}
