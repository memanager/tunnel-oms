package com.hulman.oms.service;

import com.hulman.oms.bean.Sign;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.User;
import com.hulman.oms.dao.SignDao;
import com.hulman.oms.dao.UserDao;
import com.hulman.oms.exception.IoTException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class SignService
{
    @Autowired
    private SignDao signDao;
    @Autowired
    private UserDao userDao;

    public List<Sign> findSigns(Map<String, Object> map)
    {
        return signDao.findSigns(map);
    }

    public Integer findSignsCount(Map<String, Object> map)
    {
        return signDao.findSignsCount(map);
    }

    public Result findSignsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = signDao.findSignsCount(map);
        List<Sign> data = signDao.findSigns(map);
        return new Result(data, count);
    }

    public Sign findSignById(Integer id)
    {
        return signDao.findSignById(id);
    }

    public void saveSign(Sign sign)
    {
        if (sign.getId() != null && sign.getId() != 0)
        {
            signDao.updateSign(sign);
        } else
        {
            signDao.insertSign(sign);
        }
    }

    public void deleteSignById(Integer id)
    {
        signDao.deleteSignById(id);
    }

    public void deleteSigns(int[] ids)
    {
        signDao.deleteSigns(ids);
    }

    /**
     * 签到
     *
     * @param userId
     * @param lng
     * @param lat
     */
    @Transactional(rollbackFor = Exception.class)
    public void signIn(Integer userId, String location, Double lng, Double lat)
    {
        User user = userDao.findUserById(userId);
        if (user != null)
        {
            if (user.getSignState() == User.SIGN_STATE_ON)
            {
                throw new IoTException("用户" + user.getName() + "当前处于在岗状态，请先签退");
            }
            userDao.changeSignState(userId, User.SIGN_STATE_ON);
            Sign sign = new Sign();
            sign.setUserId(userId);
            sign.setUserName(user.getName());
            sign.setLocationIn(location);
            sign.setLngIn(lng);
            sign.setLatIn(lat);
            sign.setState(Sign.STATE_CHECK_IN);
            sign.setTimeIn(new Date());
            signDao.insertSign(sign);
        } else
        {
            throw new IoTException("不存在用户Id为" + userId + "的用户");
        }
    }

    /**
     * 签退
     *
     * @param userId
     * @param location
     * @param note
     * @param lng
     * @param lat
     */
    @Transactional(rollbackFor = Exception.class)
    public void signOut(Integer userId, String location, String note, Double lng, Double lat)
    {
        User user = userDao.findUserById(userId);
        if (user != null)
        {
            if (user.getSignState() == User.SIGN_STATE_OFF)
            {
                throw new IoTException("用户" + user.getName() + "当前处于离岗状态，请先签到");
            }
            userDao.changeSignState(userId, User.SIGN_STATE_OFF);
            Sign sign = signDao.findLatestSignInSignByUserId(user.getId());
            if (sign != null)
            {
                sign.setLocationOut(location);
                sign.setLngOut(lng);
                sign.setLatOut(lat);
                sign.setNote(note);
                sign.setState(Sign.STATE_CHECK_OUT);
                sign.setTimeOut(new Date());
                Integer workingTime = (int) ((System.currentTimeMillis() - sign.getTimeIn().getTime()) / 1000);
                sign.setWorkingTime(workingTime);
                signDao.updateSign(sign);
            } else
            {
                throw new IoTException("找不用户" + user.getName() + "最近的签到记录");
            }
        } else
        {
            throw new IoTException("不存在用户Id为" + userId + "的用户");
        }
    }

    public Sign findLatestSignInSignByUserId(Integer userId)
    {
        return signDao.findLatestSignInSignByUserId(userId);
    }
}
