package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolWarehouse;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.ToolWarehouseDao;

import java.util.List;
import java.util.Map;

@Service
public class ToolWarehouseService
{

    @Autowired
    private ToolWarehouseDao toolWarehouseDao;


    public int deleteByPrimaryKey(Integer id)
    {
        return toolWarehouseDao.deleteByPrimaryKey(id);
    }


    public int insert(ToolWarehouse record)
    {
        return toolWarehouseDao.insert(record);
    }


    public int insertSelective(ToolWarehouse record)
    {
        return toolWarehouseDao.insertSelective(record);
    }


    public ToolWarehouse selectByPrimaryKey(Integer id)
    {
        return toolWarehouseDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(ToolWarehouse record)
    {
        return toolWarehouseDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(ToolWarehouse record)
    {
        return toolWarehouseDao.updateByPrimaryKey(record);
    }

    public Result findToolWarehousesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolWarehouseDao.findToolWarehouseCount(map);
        List<ToolWarehouse> data = toolWarehouseDao.findToolWareHouses(map);
        return new Result(data, count);
    }

    public int save(ToolWarehouse record)
    {
        if (record.getId() != null && record.getId() != 0)
        {
            return toolWarehouseDao.updateByPrimaryKey(record);
        } else
        {
            return toolWarehouseDao.insert(record);
        }
    }

    public ToolWarehouse selectByName(String name)
    {
        return toolWarehouseDao.selectByName(name);
    }

}
