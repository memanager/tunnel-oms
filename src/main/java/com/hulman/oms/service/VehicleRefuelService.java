package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.VehicleRefuelDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class VehicleRefuelService
{
    @Autowired
    private VehicleRefuelDao vehicleRefuelDao;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private MsgService msgService;

    public int deleteByPrimaryKey(Integer id)
    {
        return vehicleRefuelDao.deleteByPrimaryKey(id);
    }


    public int save(VehicleRefuel record)
    {
        if (record.getId() != null || record.getId() != 0)
        {
            return vehicleRefuelDao.updateByPrimaryKey(record);
        } else
        {
            return vehicleRefuelDao.insert(record);
        }
    }

    public int insertSelective(VehicleRefuel record)
    {
        return vehicleRefuelDao.insertSelective(record);
    }


    public VehicleRefuel selectByPrimaryKey(Integer id)
    {
        return vehicleRefuelDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(VehicleRefuel record)
    {
        return vehicleRefuelDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(VehicleRefuel record)
    {
        return vehicleRefuelDao.updateByPrimaryKey(record);
    }

    /**
     * 加油
     */
    @Transactional(rollbackFor = Exception.class)
    public void start(Integer vehicleId, Double mileage, String fuelCard, String fuelType, Double expense, Double lastBalance, Double amount)
    {
        //添加加油记录
        User user = SubjectUtil.getUser();
        VehicleRefuel vehicleRefuel = new VehicleRefuel();
        vehicleRefuel.setVehicleId(vehicleId);
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        vehicleRefuel.setVehicleName(vehicle.getName());
        vehicleRefuel.setDriverId(user.getId());
        vehicleRefuel.setDriverName(user.getName());
        vehicleRefuel.setMileage(mileage);
        vehicleRefuel.setFuelCard(fuelCard);
        vehicleRefuel.setFuelType(fuelType);
        vehicleRefuel.setExpense(expense);
        vehicleRefuel.setLastBalance(lastBalance);
        vehicleRefuel.setThisBalance(lastBalance - expense);
        vehicleRefuel.setAmount(amount);
        vehicleRefuel.setCreateTime(new Date());
        //记录状态待审核
        vehicleRefuel.setState(1);
        vehicleRefuelDao.insert(vehicleRefuel);
    }

    /**
     * 审核
     */
    @Transactional(rollbackFor = Exception.class)
    public void audit(Integer vehicleRefuelId, Integer auditResult, String auditComment)
    {
        User user = SubjectUtil.getUser();
        VehicleRefuel vehicleRefuel = selectByPrimaryKey(vehicleRefuelId);
        vehicleRefuel.setAuditById(user.getId());
        vehicleRefuel.setAuditByName(user.getName());
        vehicleRefuel.setAuditResult(auditResult);
        vehicleRefuel.setAuditComment(auditComment);
        vehicleRefuel.setAuditTime(new Date());
        vehicleRefuel.setState(2);
        save(vehicleRefuel);

        Vehicle vehicle = vehicleService.findVehicleById(vehicleRefuel.getVehicleId());
        //记录状态已审核
        if (auditResult == 1)
        {
            //更改车辆基础信息
            vehicle.setMileage(vehicleRefuel.getMileage());
            vehicle.setFuelCard(vehicleRefuel.getFuelCard());
            vehicle.setBalance(vehicleRefuel.getThisBalance());
            vehicleService.saveVehicle(vehicle);
        } else
        {
            Msg msg = new Msg();
            msg.setType(Msg.TYPE_SYS);
            msg.setTitle("加油记录审核未通过，请重新提交");
            msg.setState(0);
            msg.setOwnerId(vehicleRefuel.getDriverId());
            msg.setOwnerName(vehicleRefuel.getDriverName());
            msg.addItem("审核意见", vehicleRefuel.getAuditComment());
            msgService.sendMsg(msg);
        }
    }

    public void deleteVehicleRefuelById(Integer id)
    {
        vehicleRefuelDao.deleteByPrimaryKey(id);
    }

    public Integer findVehicleRefuelsCount(Map<String, Object> map)
    {
        return vehicleRefuelDao.findVehicleRefuelsCount(map);
    }

    public Result findVehicleRefuelsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = vehicleRefuelDao.findVehicleRefuelsCount(map);
        List<VehicleRefuel> data = vehicleRefuelDao.findVehicleRefuels(map);
        return new Result(data, count);
    }
}
