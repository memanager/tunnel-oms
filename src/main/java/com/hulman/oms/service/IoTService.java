package com.hulman.oms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class IoTService
{
    @Value("${tunnel-cloud.context}")
    private String context;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 所有站点
     *
     * @return
     */
    public Object findStations()
    {
        String url = context + "/dev/stations";
        return restTemplate.getForObject(url, Object.class);
    }

    /**
     * 站点下的物联设备
     * @param stationId
     * @return
     */
    public Object findIotDevicesByStationId(Integer stationId)
    {
        String url = context + "/dev/devices?stationId=" + stationId;
        return restTemplate.getForObject(url, Object.class);
    }
}
