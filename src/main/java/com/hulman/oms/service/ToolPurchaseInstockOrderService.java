package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.ToolDao;
import com.hulman.oms.dao.ToolInoutStockDao;
import com.hulman.oms.dao.ToolPurchaseInstockItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.ToolPurchaseInstockOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ToolPurchaseInstockOrderService{

    private static final String ORDER_PREFIX = "GJCGRK";

    @Autowired
    private ToolPurchaseInstockOrderDao toolPurchaseInstockOrderDao;

    @Autowired
    private ToolPurchaseInstockItemDao toolPurchaseInstockItemDao;

    @Autowired
    private ToolInoutStockDao toolInoutStockDao;

    @Autowired
    private ToolDao toolDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolPurchaseInstockOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolPurchaseInstockOrder record) {
        return toolPurchaseInstockOrderDao.insert(record);
    }

    
    public int insertSelective(ToolPurchaseInstockOrder record) {
        return toolPurchaseInstockOrderDao.insertSelective(record);
    }

    
    public ToolPurchaseInstockOrder selectByPrimaryKey(Integer id) {
        ToolPurchaseInstockOrder toolPurchaseInstockOrder = toolPurchaseInstockOrderDao.selectByPrimaryKey(id);
        List<ToolPurchaseInstockItem> items = toolPurchaseInstockItemDao.findToolPurchaseInstockItemsByOrderId(id);
        toolPurchaseInstockOrder.setItems(items);
        return toolPurchaseInstockOrder;
    }

    
    public int updateByPrimaryKeySelective(ToolPurchaseInstockOrder record) {
        return toolPurchaseInstockOrderDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolPurchaseInstockOrder record) {
        return toolPurchaseInstockOrderDao.updateByPrimaryKey(record);
    }

    public Result findToolPurchaseInstockOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolPurchaseInstockOrderDao.findToolPurchaseInstockOrdersCount(map);
        List<ToolPurchaseInstockOrder> data = toolPurchaseInstockOrderDao.findToolPurchaseInstockOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveToolPurchaseInstockOrder(ToolPurchaseInstockOrder toolPurchaseInstockOrder)
    {
        if (toolPurchaseInstockOrder.getId() != null && toolPurchaseInstockOrder.getId() != 0)
        {
            toolPurchaseInstockOrderDao.updateByPrimaryKey(toolPurchaseInstockOrder);
        } else
        {
            toolPurchaseInstockOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            toolPurchaseInstockOrder.setCreateBy(user.getName());
            toolPurchaseInstockOrder.setCreateTime(new Date());
            toolPurchaseInstockOrderDao.insert(toolPurchaseInstockOrder);
            for (ToolPurchaseInstockItem item : toolPurchaseInstockOrder.getItems())
            {
                item.setToolPurchaseOrderId(toolPurchaseInstockOrder.getId());
                //插入订单明细
                toolPurchaseInstockItemDao.insert(item);
                //出入库记录
                ToolInoutStock toolInoutStock = item.toToolInoutStock();
                toolInoutStock.setOrderNo(toolPurchaseInstockOrder.getNo());
                toolInoutStockDao.insert(toolInoutStock);
                //更新库存数量
                toolDao.addQuantity(item.getToolId(), item.getQuantity());
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteToolPurchaseInstockOrderById(Integer id)
    {
        ToolPurchaseInstockOrder toolPurchaseInstockOrder = toolPurchaseInstockOrderDao.selectByPrimaryKey(id);
        //删除单据
        toolPurchaseInstockOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        toolPurchaseInstockItemDao.deleteByOrderId(id);
        //删除出入库记录
        String orderNo = toolPurchaseInstockOrder.getNo();
        toolInoutStockDao.deleteToolInoutStockByOrderNo(orderNo);
    }

    public ToolPurchaseInstockOrder selectByNo(String no) {
        ToolPurchaseInstockOrder toolPurchaseInstockOrder = toolPurchaseInstockOrderDao.selectByNo(no);
        List<ToolPurchaseInstockItem> items = toolPurchaseInstockItemDao.findToolPurchaseInstockItemsByOrderId(toolPurchaseInstockOrder.getId());
        toolPurchaseInstockOrder.setItems(items);
        return toolPurchaseInstockOrder;
    }

}
