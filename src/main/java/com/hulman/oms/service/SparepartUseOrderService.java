package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.SparepartDao;
import com.hulman.oms.dao.SparepartInoutStockDao;
import com.hulman.oms.dao.SparepartUseItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.SparepartUseOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SparepartUseOrderService
{

    private static final String ORDER_PREFIX = "BJLY";

    @Autowired
    private SparepartUseOrderDao sparepartUseOrderDao;

    @Autowired
    private SparepartUseItemDao sparepartUseItemDao;

    @Autowired
    private SparepartInoutStockDao sparepartInoutStockDao;

    @Autowired
    private SparepartDao sparepartDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartUseOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartUseOrder record) {
        return sparepartUseOrderDao.insert(record);
    }

    
    public int insertSelective(SparepartUseOrder record) {
        return sparepartUseOrderDao.insertSelective(record);
    }

    public int updateByPrimaryKeySelective(SparepartUseOrder record) {
        return sparepartUseOrderDao.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(SparepartUseOrder record) {
        return sparepartUseOrderDao.updateByPrimaryKey(record);
    }

    public SparepartUseOrder selectByPrimaryKey(Integer id) {
        SparepartUseOrder sparepartUseOrder = sparepartUseOrderDao.selectByPrimaryKey(id);
        List<SparepartUseItem> sparepartUseItemList = sparepartUseItemDao.findSparepartUseByOrderId(id);
        sparepartUseOrder.setItems(sparepartUseItemList);
        return sparepartUseOrder;
    }

    public SparepartUseOrder selectByNo(String no) {
        SparepartUseOrder sparepartUseOrder = sparepartUseOrderDao.selectByNo(no);
        List<SparepartUseItem> sparepartUseItemList = sparepartUseItemDao.findSparepartUseByOrderId(sparepartUseOrder.getId());
        sparepartUseOrder.setItems(sparepartUseItemList);
        return sparepartUseOrder;
    }



    public Result findSparepartUseOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartUseOrderDao.findSparepartUserOrdersCount(map);
        List<SparepartUseOrder> data = sparepartUseOrderDao.findSparepartUserOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveSparepartUseOrder(SparepartUseOrder sparepartUseOrder)
    {
        if (sparepartUseOrder.getId() != null && sparepartUseOrder.getId() != 0)
        {
            sparepartUseOrderDao.updateByPrimaryKey(sparepartUseOrder);
        } else
        {
            sparepartUseOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            sparepartUseOrder.setCreateBy(user.getName());
            sparepartUseOrder.setCreateTime(new Date());
            sparepartUseOrderDao.insert(sparepartUseOrder);
            for (SparepartUseItem item : sparepartUseOrder.getItems())
            {
                item.setSparepartUseOrderId(sparepartUseOrder.getId());
                //插入订单明细
                sparepartUseItemDao.insert(item);
                //出入库记录
                SparepartInoutStock sparepartInoutStock = item.toSparepartInoutStock();
                sparepartInoutStock.setOrderNo(sparepartUseOrder.getNo());
                sparepartInoutStockDao.insertSparepartInoutStock(sparepartInoutStock);
                //更新库存数量
                sparepartDao.addQuantity(item.getSparepartId(), 0-item.getQuantity());
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteSparepartUseOrderById(Integer id)
    {
        SparepartUseOrder sparepartUseOrder = sparepartUseOrderDao.selectByPrimaryKey(id);
        //删除单据
        sparepartUseOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        sparepartUseItemDao.deleteByOrderId(id);
        //删除出入库记录
        String orderNo = sparepartUseOrder.getNo();
        sparepartInoutStockDao.deleteSparepartInoutStockByOrderNo(orderNo);
    }

}
