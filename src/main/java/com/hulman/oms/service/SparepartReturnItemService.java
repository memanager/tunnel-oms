package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartUseItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.SparepartReturnItem;
import com.hulman.oms.dao.SparepartReturnItemDao;

import java.util.List;
import java.util.Map;

@Service
public class SparepartReturnItemService
{

    @Autowired
    private SparepartReturnItemDao sparepartReturnItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartReturnItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartReturnItem record) {
        return sparepartReturnItemDao.insert(record);
    }

    
    public int insertSelective(SparepartReturnItem record) {
        return sparepartReturnItemDao.insertSelective(record);
    }

    
    public SparepartReturnItem selectByPrimaryKey(Integer id) {
        return sparepartReturnItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(SparepartReturnItem record)
    {
        return sparepartReturnItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(SparepartReturnItem record)
    {
        return sparepartReturnItemDao.updateByPrimaryKey(record);
    }

    public Result findSparepartReturnItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartReturnItemDao.findSparepartReturnItemsCount(map);
        List<SparepartReturnItem> data = sparepartReturnItemDao.findSparepartReturnItems(map);
        return new Result(data, count);
    }

    public void saveSparepartReturnItem(SparepartReturnItem sparepartReturnItem)
    {
        if (sparepartReturnItem.getId() != null && sparepartReturnItem.getId() != 0)
        {
            sparepartReturnItemDao.updateByPrimaryKey(sparepartReturnItem);
        } else
        {
            sparepartReturnItemDao.insert(sparepartReturnItem);
        }
    }

}
