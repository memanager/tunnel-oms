package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.ToolPurchaseItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.ToolPurchaseOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ToolPurchaseOrderService{

    private static final String ORDER_PREFIX = "GJCG";

    @Autowired
    private ToolPurchaseOrderDao toolPurchaseOrderDao;

    @Autowired
    private ToolPurchaseItemDao toolPurchaseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolPurchaseOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolPurchaseOrder record) {
        return toolPurchaseOrderDao.insert(record);
    }

    
    public int insertSelective(ToolPurchaseOrder record) {
        return toolPurchaseOrderDao.insertSelective(record);
    }
    
    public int updateByPrimaryKeySelective(ToolPurchaseOrder record) {
        return toolPurchaseOrderDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolPurchaseOrder record) {
        return toolPurchaseOrderDao.updateByPrimaryKey(record);
    }

    public ToolPurchaseOrder selectByPrimaryKey(Integer id) {
        ToolPurchaseOrder toolPurchaseOrder = toolPurchaseOrderDao.selectByPrimaryKey(id);
        List<ToolPurchaseItem> toolPurchaseItemList = toolPurchaseItemDao.findToolPurchaseByOrderId(toolPurchaseOrder.getId());
        toolPurchaseOrder.setItems(toolPurchaseItemList);
        return toolPurchaseOrder;
    }

    public Result findToolPurchaseOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolPurchaseOrderDao.findToolPuchaseOrdersCount(map);
        List<ToolPurchaseOrder> data = toolPurchaseOrderDao.findToolPuchaseOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveToolPurchaseOrder(ToolPurchaseOrder toolPurchaseOrder)
    {
        if (toolPurchaseOrder.getId() != null && toolPurchaseOrder.getId() != 0)
        {
            toolPurchaseOrderDao.updateByPrimaryKey(toolPurchaseOrder);
        } else
        {
            toolPurchaseOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            toolPurchaseOrder.setCreateBy(user.getName());
            toolPurchaseOrder.setCreateTime(new Date());
            toolPurchaseOrderDao.insert(toolPurchaseOrder);
            for (ToolPurchaseItem item : toolPurchaseOrder.getItems())
            {
                item.setToolPurchaseOrderId(toolPurchaseOrder.getId());
                //插入订单明细
                toolPurchaseItemDao.insert(item);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteToolPurchaseOrderById(Integer id)
    {
        ToolPurchaseOrder toolPurchaseOrder = toolPurchaseOrderDao.selectByPrimaryKey(id);
        //删除单据
        toolPurchaseOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        toolPurchaseItemDao.deleteByOrderId(id);
    }
}
