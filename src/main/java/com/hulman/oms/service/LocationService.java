package com.hulman.oms.service;

import com.alibaba.excel.EasyExcel;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Location;
import com.hulman.oms.bean.Tunnel;
import com.hulman.oms.dao.LocationDao;
import com.hulman.oms.util.CodeGenerator;
import com.hulman.oms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class LocationService
{
    @Autowired
    private LocationDao locationDao;
    @Autowired
    private TunnelService tunnelService;

    public List<Location> findLocations(Map<String, Object> map)
    {
        return locationDao.findLocations(map);
    }

    public List<Location> findLocationsByTunnelId(Integer tunnelId)
    {
        return locationDao.findLocationsByTunnelId(tunnelId);
    }

    public Integer findLocationsCount(Map<String, Object> map)
    {
        return locationDao.findLocationsCount(map);
    }

    public Result findLocationsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = locationDao.findLocationsCount(map);
        List<Location> data = locationDao.findLocations(map);
        return new Result(data, count);
    }

    public Location findLocationById(Integer id)
    {
        return locationDao.findLocationById(id);
    }

    public Location findLocationByCode(String code)
    {
        return locationDao.findLocationByCode(code);
    }

    public Location findLocationByName(String name)
    {
        return locationDao.findLocationByName(name);
    }

    public Location findLocationByTunnelIdAndAbbr(Integer tunnelId, String abbr)
    {
        return locationDao.findLocationByTunnelIdAndAbbr(tunnelId, abbr);
    }

    public void saveLocation(Location location)
    {
        if (StringUtil.isEmpty(location.getCode()))
        {
            location.setCode(CodeGenerator.nextCode());
        }
        if (location.getId() != null && location.getId() != 0)
        {
            locationDao.updateLocation(location);
        } else
        {
            locationDao.insertLocation(location);
        }
    }

    public void deleteLocationById(Integer id)
    {
        locationDao.deleteLocationById(id);
    }

    public void deleteLocations(int[] ids)
    {
        locationDao.deleteLocations(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    public void importLocations(List<Location> locations) throws IOException
    {
        for (Location location : locations)
        {
            Tunnel tunnel = tunnelService.findTunnelByName(location.getTunnelName());
            if (tunnel == null)
            {
                throw new IOException("不存在隧道：" + location.getTunnelName());
            }
            location.setTunnelId(tunnel.getId());
            locationDao.insertLocation(location);
        }
    }

}
