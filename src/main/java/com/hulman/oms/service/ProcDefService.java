package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ProcDef;
import com.hulman.oms.dao.ProcDefDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ProcDefService
{
    @Autowired
    private ProcDefDao procDefDao;

    public List<ProcDef> findProcDefs(Map<String, Object> map)
    {
        return procDefDao.findProcDefs(map);
    }

    public Integer findProcDefsCount(Map<String, Object> map)
    {
        return procDefDao.findProcDefsCount(map);
    }

    public Result findProcDefsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = procDefDao.findProcDefsCount(map);
        List<ProcDef> data = procDefDao.findProcDefs(map);
        return new Result(data, count);
    }

    public ProcDef findProcDefById(Integer id)
    {
        return procDefDao.findProcDefById(id);
    }

    public ProcDef findProcDefByKey(String key)
    {
        return procDefDao.findProcDefByKey(key);
    }

    public void saveProcDef(ProcDef procDef)
    {
        if (procDef.getId() != null && procDef.getId() != 0)
        {
            procDefDao.updateProcDef(procDef);
        } else
        {
            procDefDao.insertProcDef(procDef);
        }
    }

    public void deleteProcDefById(Integer id)
    {
        procDefDao.deleteProcDefById(id);
    }

}
