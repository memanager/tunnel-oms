package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Material;
import com.hulman.oms.dao.MaterialDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class MaterialService
{
    @Autowired
    private MaterialDao materialDao;

    public List<Material> findMaterials(Map<String, Object> map)
    {
        return materialDao.findMaterials(map);
    }

    public List<Material> findAllMaterials()
    {
        Map<String, Object> map = new HashMap<>();
        List<Material> list = materialDao.findMaterials(map);
        return list;
    }

    public Integer findMaterialsCount(Map<String, Object> map)
    {
        return materialDao.findMaterialsCount(map);
    }

    public Result findMaterialsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = materialDao.findMaterialsCount(map);
        List<Material> data = materialDao.findMaterials(map);
        return new Result(data, count);
    }

    public Material findMaterialById(Integer id)
    {
        return materialDao.findMaterialById(id);
    }

    public void saveMaterial(Material material)
    {
        if (material.getId() != null && material.getId() != 0)
        {
            materialDao.updateMaterial(material);
        } else
        {
            materialDao.insertMaterial(material);
        }
    }

    public void deleteMaterialById(Integer id)
    {
        materialDao.deleteMaterialById(id);
    }

    public void deleteMaterials(int[] ids)
    {
        materialDao.deleteMaterials(ids);
    }

}
