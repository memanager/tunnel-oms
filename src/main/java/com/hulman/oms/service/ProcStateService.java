package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ProcState;
import com.hulman.oms.dao.ProcStateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ProcStateService
{
    @Autowired
    private ProcStateDao procStateDao;

    public List<ProcState> findProcStates(Map<String, Object> map)
    {
        return procStateDao.findProcStates(map);
    }

    public Integer findProcStatesCount(Map<String, Object> map)
    {
        return procStateDao.findProcStatesCount(map);
    }

    public Result findProcStatesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = procStateDao.findProcStatesCount(map);
        List<ProcState> data = procStateDao.findProcStates(map);
        return new Result(data, count);
    }

    public ProcState findProcStateById(Integer id)
    {
        return procStateDao.findProcStateById(id);
    }

    public void saveProcState(ProcState procState)
    {
        if (procState.getId() != null && procState.getId() != 0)
        {
            procStateDao.updateProcState(procState);
        } else
        {
            procStateDao.insertProcState(procState);
        }
    }

    public void deleteProcStateById(Integer id)
    {
        procStateDao.deleteProcStateById(id);
    }

    public void deleteProcStates(int[] ids)
    {
        procStateDao.deleteProcStates(ids);
    }

}
