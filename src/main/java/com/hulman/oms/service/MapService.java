package com.hulman.oms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class MapService
{

    @Value("${qq.map.key}")
    private String key;

    @Value("${qq.map.sk}")
    private String sk;

    @Autowired
    private RestTemplate restTemplate;

    public Map geocoder(Double lat, Double lng)
    {
        String url = "/ws/geocoder/v1?key=" + key + "&location=" + lat + "," + lng + sk;
        String sig = DigestUtils.md5DigestAsHex(url.getBytes());
        String apiUrl = "https://apis.map.qq.com/ws/geocoder/v1?key=" + key + "&location=" + lat + "," + lng + "&sig=" + sig;
        Map map = restTemplate.getForObject(apiUrl, HashMap.class);
        return map;
    }

}
