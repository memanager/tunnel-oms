package com.hulman.oms.service;

import com.hulman.oms.bean.AccessPolicy;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.AccessPolicyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class AccessPolicyService
{
    @Autowired
    private AccessPolicyDao accessPolicyDao;

    public List<AccessPolicy> findAllAccessPolicies()
    {
        Map<String, Object> map = new HashMap<>();
        return accessPolicyDao.findAccessPolicies(map);
    }

    public List<AccessPolicy> findAccessPolicies(Map<String, Object> map)
    {
        return accessPolicyDao.findAccessPolicies(map);
    }

    public Integer findAccessPoliciesCount(Map<String, Object> map)
    {
        return accessPolicyDao.findAccessPoliciesCount(map);
    }

    public Result findAccessPoliciesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = accessPolicyDao.findAccessPoliciesCount(map);
        List<AccessPolicy> data = accessPolicyDao.findAccessPolicies(map);
        return new Result(data, count);
    }

    public AccessPolicy findAccessPolicyById(Integer id)
    {
        return accessPolicyDao.findAccessPolicyById(id);
    }

    public void saveAccessPolicy(AccessPolicy accessPolicy)
    {
        if (accessPolicy.getId() != null && accessPolicy.getId() != 0)
        {
            accessPolicyDao.updateAccessPolicy(accessPolicy);
        } else
        {
            accessPolicyDao.insertAccessPolicy(accessPolicy);
        }
    }

    public void deleteAccessPolicyById(Integer id)
    {
        accessPolicyDao.deleteAccessPolicyById(id);
    }

    public void deleteAccessPolicies(String ids)
    {
        accessPolicyDao.deleteAccessPolicies(ids);
    }

}
