package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.VehicleUseDao;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class VehicleUseService
{
    @Autowired
    private VehicleUseDao vehicleUseDao;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private MsgService msgService;

    public List<VehicleUse> findVehicleUses(Map<String, Object> map)
    {
        return vehicleUseDao.findVehicleUses(map);
    }

    public Integer findVehicleUsesCount(Map<String, Object> map)
    {
        return vehicleUseDao.findVehicleUsesCount(map);
    }

    public Result findVehicleUsesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = vehicleUseDao.findVehicleUsesCount(map);
        List<VehicleUse> data = vehicleUseDao.findVehicleUses(map);
        return new Result(data, count);
    }

    public VehicleUse findVehicleUseById(Integer id)
    {
        return vehicleUseDao.findVehicleUseById(id);
    }

    public void saveVehicleUse(VehicleUse vehicleUse)
    {
        if (vehicleUse.getId() != null && vehicleUse.getId() != 0)
        {
            vehicleUseDao.updateVehicleUse(vehicleUse);
        } else
        {
            vehicleUseDao.insertVehicleUse(vehicleUse);
        }
    }

    public void deleteVehicleUseById(Integer id)
    {
        vehicleUseDao.deleteVehicleUseById(id);
    }

    public void deleteVehicleUses(int[] ids)
    {
        vehicleUseDao.deleteVehicleUses(ids);
    }

    /**
     * 出车
     */
    @Transactional(rollbackFor = Exception.class)
    public void start(Integer vehicleId, Double startMile)
    {
        //添加出车记录
        User user = SubjectUtil.getUser();
        VehicleUse vehicleUse = new VehicleUse();
        vehicleUse.setVehicleId(vehicleId);
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        vehicleUse.setVehicleName(vehicle.getName());
        vehicleUse.setStartTime(new Date());
        vehicleUse.setStartMile(startMile);
        vehicleUse.setDriverId(user.getId());
        vehicleUse.setDriverName(user.getName());
        //记录状态出车中
        vehicleUse.setState(1);
        vehicleUseDao.insertVehicleUse(vehicleUse);
        //更改车辆状态为外出
        vehicle.setState(2);
        vehicle.setMileage(startMile);
        vehicleService.saveVehicle(vehicle);
    }

    /**
     * 结束
     */
    @Transactional(rollbackFor = Exception.class)
    public void end(Integer vehicleUseId, Double endMile)
    {
        VehicleUse vehicleUse = findVehicleUseById(vehicleUseId);
        Vehicle vehicle = vehicleService.findVehicleById(vehicleUse.getVehicleId());
        Date endTime = new Date();
        vehicleUse.setEndTime(endTime);
        vehicleUse.setEndMile(endMile);
        vehicleUse.setDriveMile(endMile - vehicleUse.getStartMile());
        //使用时间精确到分钟
        vehicleUse.setUseTime((int) ((endTime.getTime() - vehicleUse.getStartTime().getTime()) / 60_000));
        //记录状态待审核
        vehicleUse.setState(2);
        saveVehicleUse(vehicleUse);
        //更改车辆状态为在库
        vehicle.setState(1);
        vehicle.setMileage(endMile);
        vehicleService.saveVehicle(vehicle);
    }

    /**
     * 审核
     */
    @Transactional(rollbackFor = Exception.class)
    public void audit(Integer vehicleUseId, Integer auditResult, String auditComment)
    {
        User user = SubjectUtil.getUser();
        VehicleUse vehicleUse = findVehicleUseById(vehicleUseId);
        vehicleUse.setAuditById(user.getId());
        vehicleUse.setAuditByName(user.getName());
        vehicleUse.setAuditResult(auditResult);
        vehicleUse.setAuditComment(auditComment);
        vehicleUse.setAuditTime(new Date());
        if (auditResult == 1)
        {
            //记录状态已审核
            vehicleUse.setState(3);
        } else
        {
            //回到出车状态
            vehicleUse.setState(1);
            Msg msg = new Msg();
            msg.setType(Msg.TYPE_SYS);
            msg.setTitle("出车记录审核未通过，请重新提交");
            msg.setState(0);
            msg.setOwnerId(vehicleUse.getDriverId());
            msg.setOwnerName(vehicleUse.getDriverName());
            msg.addItem("审核意见", vehicleUse.getAuditComment());
            msgService.sendMsg(msg);
        }
        saveVehicleUse(vehicleUse);
    }
}
