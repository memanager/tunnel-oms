package com.hulman.oms.service;

import com.hulman.oms.bean.Device;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.DeviceRelation;
import com.hulman.oms.dao.DeviceDao;
import com.hulman.oms.dao.DeviceRelationDao;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DeviceRelationService
{
    @Autowired
    private DeviceRelationDao deviceRelationDao;
    @Autowired
    private DeviceDao deviceDao;

    public List<DeviceRelation> findDeviceRelations(Map<String, Object> map)
    {
        return deviceRelationDao.findDeviceRelations(map);
    }

    public Integer findDeviceRelationsCount(Map<String, Object> map)
    {
        return deviceRelationDao.findDeviceRelationsCount(map);
    }

    public Result findDeviceRelationsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = deviceRelationDao.findDeviceRelationsCount(map);
        List<DeviceRelation> data = deviceRelationDao.findDeviceRelations(map);
        return new Result(data, count);
    }

    public void saveDeviceRelation(DeviceRelation deviceRelation)
    {
        if (deviceRelation.getId() != null && deviceRelation.getId() != 0)
        {
            deviceRelationDao.updateDeviceRelation(deviceRelation);
        } else
        {
            deviceRelationDao.insertDeviceRelation(deviceRelation);
        }
    }

    public void deleteDeviceRelationById(Integer id)
    {
        deviceRelationDao.deleteDeviceRelationById(id);
    }

    public void clearDeviceRelationsByDeviceId(Integer deviceId)
    {
        deviceRelationDao.clearDeviceRelationsByDeviceId(deviceId);
    }

    public void addDeviceRelations(Integer deviceId, String relatedDeviceCodes, Integer type)
    {
        if (! StringUtil.isEmpty(relatedDeviceCodes))
        {
            String[] codes = relatedDeviceCodes.split(",");
            for (String code : codes)
            {
                Device relatedDevice = deviceDao.findDeviceByCode(code);
                if (relatedDevice != null)
                {
                    DeviceRelation deviceRelation = new DeviceRelation();
                    deviceRelation.setDeviceId(deviceId);
                    deviceRelation.setRelatedDeviceId(relatedDevice.getId());
                    deviceRelation.setType(type);
                    deviceRelationDao.insertDeviceRelation(deviceRelation);
                } else
                {
                    throw new IoTException("不存在设备编码：" + code);
                }
            }
        }
    }

    public void importDeviceRelations(Device device)
    {
        clearDeviceRelationsByDeviceId(device.getId());
        addDeviceRelations(device.getId(), device.getSupCodes(), DeviceRelation.SUP);
        addDeviceRelations(device.getId(), device.getSubCodes(), DeviceRelation.SUB);
    }

}
