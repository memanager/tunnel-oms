package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.SparepartUseItem;
import com.hulman.oms.dao.SparepartUseItemDao;

import java.util.List;
import java.util.Map;

@Service
public class SparepartUseItemService{

    @Autowired
    private SparepartUseItemDao sparepartUseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartUseItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartUseItem record) {
        return sparepartUseItemDao.insert(record);
    }

    
    public int insertSelective(SparepartUseItem record) {
        return sparepartUseItemDao.insertSelective(record);
    }

    
    public SparepartUseItem selectByPrimaryKey(Integer id) {
        return sparepartUseItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(SparepartUseItem record) {
        return sparepartUseItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(SparepartUseItem record) {
        return sparepartUseItemDao.updateByPrimaryKey(record);
    }

    public Result findSparepartUseItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartUseItemDao.findSparepartUseItemsCount(map);
        List<SparepartUseItem> data = sparepartUseItemDao.findSparepartUseItems(map);
        return new Result(data, count);
    }

    public List<SparepartUseItem> selectByOrderId(Integer orderId) {
        List<SparepartUseItem> sparepartUseItemList = sparepartUseItemDao.findSparepartUseByOrderId(orderId);
        return sparepartUseItemList;
    }

    public void saveSparepartUseItem(SparepartUseItem sparepartUseItem)
    {
        if (sparepartUseItem.getId() != null && sparepartUseItem.getId() != 0)
        {
            sparepartUseItemDao.updateByPrimaryKey(sparepartUseItem);
        } else
        {
            sparepartUseItemDao.insert(sparepartUseItem);
        }
    }
}
