package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.SparepartDao;
import com.hulman.oms.dao.SparepartInoutStockDao;
import com.hulman.oms.dao.SparepartReturnItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.SparepartReturnOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SparepartReturnOrderService
{

    private static final String ORDER_PREFIX = "BJGH";

    @Autowired
    private SparepartReturnOrderDao sparepartReturnOrderDao;

    @Autowired
    private SparepartReturnItemDao sparepartReturnItemDao;

    @Autowired
    private SparepartInoutStockDao sparepartInoutStockDao;

    @Autowired
    private SparepartDao sparepartDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartReturnOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartReturnOrder record) {
        return sparepartReturnOrderDao.insert(record);
    }

    
    public int insertSelective(SparepartReturnOrder record) {
        return sparepartReturnOrderDao.insertSelective(record);
    }
    
    public int updateByPrimaryKeySelective(SparepartReturnOrder record) {
        return sparepartReturnOrderDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(SparepartReturnOrder record) {
        return sparepartReturnOrderDao.updateByPrimaryKey(record);
    }

    public SparepartReturnOrder selectByPrimaryKey(Integer id) {
        SparepartReturnOrder sparepartReturnOrder = sparepartReturnOrderDao.selectByPrimaryKey(id);
        List<SparepartReturnItem> sparepartReturnItemList = sparepartReturnItemDao.findSparepartReturnByOrderId(id);
        sparepartReturnOrder.setItems(sparepartReturnItemList);
        return sparepartReturnOrder;
    }

    public SparepartReturnOrder selectByNo(String no) {
        SparepartReturnOrder sparepartReturnOrder = sparepartReturnOrderDao.selectByNo(no);
        List<SparepartReturnItem> sparepartReturnItemList = sparepartReturnItemDao.findSparepartReturnByOrderId(sparepartReturnOrder.getId());
        sparepartReturnOrder.setItems(sparepartReturnItemList);
        return sparepartReturnOrder;
    }

    public Result findSparepartReturnOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartReturnOrderDao.findSparepartReturnOrdersCount(map);
        List<SparepartReturnOrder> data = sparepartReturnOrderDao.findSparepartReturnOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveSparepartReturnOrder(SparepartReturnOrder sparepartReturnOrder)
    {
        if (sparepartReturnOrder.getId() != null && sparepartReturnOrder.getId() != 0)
        {
            sparepartReturnOrderDao.updateByPrimaryKey(sparepartReturnOrder);
        } else
        {
            sparepartReturnOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            sparepartReturnOrder.setCreateBy(user.getName());
            sparepartReturnOrder.setCreateTime(new Date());
            sparepartReturnOrderDao.insert(sparepartReturnOrder);
            for (SparepartReturnItem item : sparepartReturnOrder.getItems())
            {
                item.setSparepartUseOrderId(sparepartReturnOrder.getId());
                //插入订单明细
                sparepartReturnItemDao.insert(item);
                //出入库记录
                SparepartInoutStock sparepartInoutStock = item.toSparepartInoutStock();
                sparepartInoutStock.setOrderNo(sparepartReturnOrder.getNo());
                sparepartInoutStockDao.insertSparepartInoutStock(sparepartInoutStock);
                //更新库存数量
                sparepartDao.addQuantity(item.getSparepartId(), item.getQuantity());
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteSparepartReturnOrderById(Integer id)
    {
        SparepartReturnOrder sparepartReturnOrder = sparepartReturnOrderDao.selectByPrimaryKey(id);
        //删除单据
        sparepartReturnOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        sparepartReturnItemDao.deleteByOrderId(id);
        //删除出入库记录
        String orderNo = sparepartReturnOrder.getNo();
        sparepartInoutStockDao.deleteSparepartInoutStockByOrderNo(orderNo);
    }

}
