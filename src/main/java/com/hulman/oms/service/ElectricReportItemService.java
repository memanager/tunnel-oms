package com.hulman.oms.service;

import com.hulman.oms.bean.ElectricMeter;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ElectricReportItem;
import com.hulman.oms.dao.ElectricReportItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ElectricReportItemService
{
    @Autowired
    private ElectricReportItemDao electricReportItemDao;
    @Autowired
    private ElectricMeterService electricMeterService;

    public List<ElectricReportItem> findElectricReportItems(Map<String, Object> map)
    {
        return electricReportItemDao.findElectricReportItems(map);
    }

    public List<ElectricReportItem> findElectricReportItemsByReportId(Integer reportId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("reportId",reportId);
        return electricReportItemDao.findElectricReportItems(map);
    }

    public Integer findElectricReportItemsCount(Map<String, Object> map)
    {
        return electricReportItemDao.findElectricReportItemsCount(map);
    }

    public Result findElectricReportItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = electricReportItemDao.findElectricReportItemsCount(map);
        List<ElectricReportItem> data = electricReportItemDao.findElectricReportItems(map);
        return new Result(data, count);
    }

    public ElectricReportItem findElectricReportItemById(Integer id)
    {
        return electricReportItemDao.findElectricReportItemById(id);
    }

    public void saveElectricReportItem(ElectricReportItem electricReportItem)
    {
        if (electricReportItem.getId() != null && electricReportItem.getId() != 0)
        {
            electricReportItemDao.updateElectricReportItem(electricReportItem);
        } else
        {
            electricReportItemDao.insertElectricReportItem(electricReportItem);
        }
    }

    public void deleteElectricReportItemById(Integer id)
    {
        electricReportItemDao.deleteElectricReportItemById(id);
    }

    public void deleteElectricReportItems(int[] ids)
    {
        electricReportItemDao.deleteElectricReportItems(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    public void read(ElectricReportItem electricReportItem)
    {
        //更新明细
        electricReportItem.setState(1);
        electricReportItem.setReadTime(new Date());
        saveElectricReportItem(electricReportItem);
        //更新电表信息
        ElectricMeter meter = electricMeterService.findElectricMeterById(electricReportItem.getMeterId());
        if (meter != null)
        {
            meter.setAmount(electricReportItem.getThisAmount());
            meter.setBalance(electricReportItem.getThisBalance());
            meter.setReadTime(new Date());
            electricMeterService.saveElectricMeter(meter);
        }
    }

}
