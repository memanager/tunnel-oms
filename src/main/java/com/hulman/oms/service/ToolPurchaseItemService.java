package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolPurchaseItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.ToolPurchaseItem;
import com.hulman.oms.dao.ToolPurchaseItemDao;

import java.util.List;
import java.util.Map;

@Service
public class ToolPurchaseItemService{

    @Autowired
    private ToolPurchaseItemDao toolPurchaseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolPurchaseItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolPurchaseItem record) {
        return toolPurchaseItemDao.insert(record);
    }

    
    public int insertSelective(ToolPurchaseItem record) {
        return toolPurchaseItemDao.insertSelective(record);
    }

    
    public ToolPurchaseItem selectByPrimaryKey(Integer id) {
        return toolPurchaseItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(ToolPurchaseItem record) {
        return toolPurchaseItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolPurchaseItem record) {
        return toolPurchaseItemDao.updateByPrimaryKey(record);
    }

    public Result findToolPurchaseItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolPurchaseItemDao.findToolPurchaseItemsCount(map);
        List<ToolPurchaseItem> data = toolPurchaseItemDao.findToolPurchaseItems(map);
        return new Result(data, count);
    }

    public void saveToolPurchaseItem(ToolPurchaseItem toolPurchaseItem)
    {
        if (toolPurchaseItem.getId() != null && toolPurchaseItem.getId() != 0)
        {
            toolPurchaseItemDao.updateByPrimaryKey(toolPurchaseItem);
        } else
        {
            toolPurchaseItemDao.insert(toolPurchaseItem);
        }
    }

    public List<ToolPurchaseItem> selectByOrderId(Integer orderId) {
        return toolPurchaseItemDao.findToolPurchaseByOrderId(orderId);
    }

}
