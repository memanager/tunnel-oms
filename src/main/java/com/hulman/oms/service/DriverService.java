package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Driver;
import com.hulman.oms.dao.DriverDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DriverService
{
    @Autowired
    private DriverDao driverDao;

    public List<Driver> findDrivers(Map<String, Object> map)
    {
        return driverDao.findDrivers(map);
    }

    public Integer findDriversCount(Map<String, Object> map)
    {
        return driverDao.findDriversCount(map);
    }

    public Result findDriversResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = driverDao.findDriversCount(map);
        List<Driver> data = driverDao.findDrivers(map);
        return new Result(data, count);
    }

    public Driver findDriverById(Integer id)
    {
        return driverDao.findDriverById(id);
    }

    public void saveDriver(Driver driver)
    {
        if (driver.getId() != null && driver.getId() != 0)
        {
            driverDao.updateDriver(driver);
        } else
        {
            driverDao.insertDriver(driver);
        }
    }

    public void deleteDriverById(Integer id)
    {
        driverDao.deleteDriverById(id);
    }

    public void deleteDrivers(int[] ids)
    {
        driverDao.deleteDrivers(ids);
    }

}
