package com.hulman.oms.service;

import com.hulman.oms.bean.AccessPolicy;
import com.hulman.oms.bean.Menu;
import com.hulman.oms.bean.Workbench;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Role;
import com.hulman.oms.dao.RoleDao;
import com.hulman.oms.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @Author: maxwellens
 */
@Service
public class RoleService
{
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private AccessPolicyService accessPolicyService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private WorkbenchService workbenchService;
    @Autowired
    private UserDao userDao;

    public List<Role> findAllRoles()
    {
        Map<String, Object> map = new HashMap<>();
        return roleDao.findRoles(map);
    }

    public List<Role> findRoles(Map<String, Object> map)
    {
        return roleDao.findRoles(map);
    }

    public List<Role> findRolesByIds(String ids)
    {
        return roleDao.findRolesByIds(ids);
    }

    public String findRoleNamesByIds(String ids)
    {
        StringBuilder sb = new StringBuilder();
        List<Role> roles = roleDao.findRolesByIds(ids);
        for (Role role : roles)
        {
            sb.append(role.getName()).append(",");
        }
        if (sb.length() > 0)
        {
            return sb.substring(0, sb.length() - 1);
        } else
        {
            return "";
        }
    }

    public String findRoleIdsByNames(String names)
    {
        StringBuilder sb = new StringBuilder();
        String[] roleNames = names.split(",");
        for (String roleName : roleNames)
        {
            Role role = roleDao.findRoleByName(roleName);
            if (role != null)
            {
                sb.append(role.getId()).append(",");
            }
        }
        if (sb.length() > 0)
        {
            return sb.substring(0, sb.length() - 1);
        } else
        {
            return "";
        }
    }

    public Integer findRolesCount(Map<String, Object> map)
    {
        return roleDao.findRolesCount(map);
    }

    public Result findRolesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = roleDao.findRolesCount(map);
        List<Role> data = roleDao.findRoles(map);
        return new Result(data, count);
    }

    public Role findRoleById(Integer id)
    {
        return roleDao.findRoleById(id);
    }

    public void saveRole(Role role)
    {
        if (role.getId() != null && role.getId() != 0)
        {
            roleDao.updateRole(role);
        } else
        {
            roleDao.insertRole(role);
        }
    }

    public void deleteRoleById(Integer id)
    {
        roleDao.deleteRoleById(id);
    }

    public void deleteRoles(String ids)
    {
        roleDao.deleteRoles(ids);
    }

    public void updateMenus(Integer id, String menus)
    {
        roleDao.updateMenus(id, menus);
    }

    public void updateAppWorks(Integer id, String modules)
    {
        roleDao.updateWorkbenches(id, modules);
    }

    public void updateAccessPolicies(Integer id, String accessPolicies)
    {
        roleDao.updateAccessPolicies(id, accessPolicies);
    }

    public Result findAccessPoliciesResultByRoleId(Integer roleId)
    {
        Role role = findRoleById(roleId);
        String policies = role.getAccessPolicies();
        List<AccessPolicy> list = accessPolicyService.findAllAccessPolicies();
        if (!StringUtils.isEmpty(policies))
        {
            List<String> pols = Arrays.asList(policies.split(","));
            for (AccessPolicy item : list)
            {
                if (pols.contains(item.getId().toString()))
                {
                    item.setChecked(true);
                } else
                {
                    item.setChecked(false);
                }
            }
        }
        return new Result(list, list.size());
    }

    public Result findMenuResultByRoleId(Integer roleId)
    {
        List<Map<String, Object>> nodes = new ArrayList<>();
        List<Menu> rootMenus = menuService.findRootMenus();
        Role role = findRoleById(roleId);
        String menus = role.getMenus();
        List<String> menuIds;
        if (StringUtils.isEmpty(menus))
        {
            menuIds = new ArrayList<>();
        } else
        {
            menuIds = Arrays.asList(menus.split(","));
        }
        for (Menu menu : rootMenus)
        {
            Map<String, Object> node = convertMenuToTreeNode(menu, menuIds);
            nodes.add(node);
        }
        return new Result(nodes);
    }

    public Result findAppWorkResultByRoleId(Integer roleId)
    {
        List<Map<String, Object>> nodes = new ArrayList<>();
        List<Workbench> rootWorkbenches = workbenchService.findRootWorkbenches();
        Role role = findRoleById(roleId);
        String works = role.getWorkbenches();
        List<String> moduleIds;
        if (StringUtils.isEmpty(works))
        {
            moduleIds = new ArrayList<>();
        } else
        {
            moduleIds = Arrays.asList(works.split(","));
        }
        for (Workbench module : rootWorkbenches)
        {
            Map<String, Object> node = convertAppWorkToTreeNode(module, moduleIds);
            nodes.add(node);
        }
        return new Result(nodes);
    }

    private Map<String, Object> convertMenuToTreeNode(Menu menu, List<String> menuIds)
    {
        Map<String, Object> node = new HashMap<>();
        Integer id = menu.getId();
        node.put("id", id);
        node.put("label", menu.getName());
        List<Menu> childMenus = menuService.findMenusByParentId(id);
        if (childMenus.size() > 0)
        {
            List<Map<String, Object>> children = new ArrayList<>();
            for (Menu child : childMenus)
            {
                Map<String, Object> childNode = convertMenuToTreeNode(child, menuIds);
                children.add(childNode);
            }
            node.put("children", children);
        } else
        {
            node.put("checked", menuIds.contains(id.toString()));
        }
        return node;
    }

    private Map<String, Object> convertAppWorkToTreeNode(Workbench workbench, List<String> moduleIds)
    {
        Map<String, Object> node = new HashMap<>();
        Integer id = workbench.getId();
        node.put("id", id);
        node.put("label", workbench.getTitle());
        List<Workbench> childWorkbenches = workbenchService.findWorkbenchesByParentId(id);
        if (childWorkbenches.size() > 0)
        {
            List<Map<String, Object>> children = new ArrayList<>();
            for (Workbench child : childWorkbenches)
            {
                Map<String, Object> childNode = convertAppWorkToTreeNode(child, moduleIds);
                children.add(childNode);
            }
            node.put("children", children);
        } else
        {
            node.put("checked", moduleIds.contains(id.toString()));
        }
        return node;
    }

}
