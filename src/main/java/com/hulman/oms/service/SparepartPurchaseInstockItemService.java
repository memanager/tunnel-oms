package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import com.hulman.oms.dao.SparepartPurchaseInstockItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class SparepartPurchaseInstockItemService
{
    @Autowired
    private SparepartPurchaseInstockItemDao sparepartPurchaseInstockItemDao;

    public List<SparepartPurchaseInstockItem> findSparepartPurchaseInstockItems(Map<String, Object> map)
    {
        return sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItems(map);
    }

    public Integer findSparepartPurchaseInstockItemsCount(Map<String, Object> map)
    {
        return sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItemsCount(map);
    }

    public Result findSparepartPurchaseInstockItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItemsCount(map);
        List<SparepartPurchaseInstockItem> data = sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItems(map);
        return new Result(data, count);
    }

    public SparepartPurchaseInstockItem findSparepartPurchaseInstockItemById(Integer id)
    {
        return sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItemById(id);
    }

    public void saveSparepartPurchaseInstockItem(SparepartPurchaseInstockItem sparepartPurchaseInstockItem)
    {
        if (sparepartPurchaseInstockItem.getId() != null && sparepartPurchaseInstockItem.getId() != 0)
        {
            sparepartPurchaseInstockItemDao.updateSparepartPurchaseInstockItem(sparepartPurchaseInstockItem);
        } else
        {
            sparepartPurchaseInstockItemDao.insertSparepartPurchaseInstockItem(sparepartPurchaseInstockItem);
        }
    }

    public void deleteSparepartPurchaseInstockItemById(Integer id)
    {
        sparepartPurchaseInstockItemDao.deleteSparepartPurchaseInstockItemById(id);
    }

    public void deleteSparepartPurchaseInstockItems(int[] ids)
    {
        sparepartPurchaseInstockItemDao.deleteSparepartPurchaseInstockItems(ids);
    }

}
