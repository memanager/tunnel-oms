package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.SparepartPurchaseItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.SparepartPurchaseOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SparepartPurchaseOrderService{

    private static final String ORDER_PREFIX = "BJCG";

    @Autowired
    private SparepartPurchaseOrderDao sparepartPurchaseOrderDao;

    @Autowired
    private SparepartPurchaseItemDao sparepartPurchaseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartPurchaseOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartPurchaseOrder sparepartPurchaseOrder) {
        return sparepartPurchaseOrderDao.insert(sparepartPurchaseOrder);
    }

    
    public int insertSelective(SparepartPurchaseOrder record) {
        return sparepartPurchaseOrderDao.insertSelective(record);
    }
    
    public int updateByPrimaryKeySelective(SparepartPurchaseOrder record) {
        return sparepartPurchaseOrderDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(SparepartPurchaseOrder record) {
        return sparepartPurchaseOrderDao.updateByPrimaryKey(record);
    }

    public SparepartPurchaseOrder selectByPrimaryKey(Integer id) {
        SparepartPurchaseOrder sparepartPurchaseOrder = sparepartPurchaseOrderDao.selectByPrimaryKey(id);
        List<SparepartPurchaseItem> sparepartPurchaseItemList = sparepartPurchaseItemDao.findSparepartPurchaseByOrderId(sparepartPurchaseOrder.getId());
        sparepartPurchaseOrder.setItems(sparepartPurchaseItemList);
        return sparepartPurchaseOrder;
    }

    public Result findSparepartPurchaseOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartPurchaseOrderDao.findSparepartPuchaseOrdersCount(map);
        List<SparepartPurchaseOrder> data = sparepartPurchaseOrderDao.findSparepartPuchaseOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveSparepartPurchaseOrder(SparepartPurchaseOrder sparepartPurchaseOrder)
    {
        if (sparepartPurchaseOrder.getId() != null && sparepartPurchaseOrder.getId() != 0)
        {
            sparepartPurchaseOrderDao.updateByPrimaryKey(sparepartPurchaseOrder);
        } else
        {
            sparepartPurchaseOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            sparepartPurchaseOrder.setCreateBy(user.getName());
            sparepartPurchaseOrder.setCreateTime(new Date());
            sparepartPurchaseOrderDao.insert(sparepartPurchaseOrder);
            for (SparepartPurchaseItem item : sparepartPurchaseOrder.getItems())
            {
                item.setSparepartPurchaseOrderId(sparepartPurchaseOrder.getId());
                //插入订单明细
                sparepartPurchaseItemDao.insert(item);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteSparepartPurchaseOrderById(Integer id)
    {
        SparepartPurchaseOrder sparepartPurchaseOrder = sparepartPurchaseOrderDao.selectByPrimaryKey(id);
        //删除单据
        sparepartPurchaseOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        sparepartPurchaseItemDao.deleteByOrderId(id);
    }

}
