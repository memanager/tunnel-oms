package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolInoutStock;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.hulman.oms.dao.ToolInoutStockDao;

import java.util.List;
import java.util.Map;

@Service
public class ToolInoutStockService{

    @Autowired
    private ToolInoutStockDao toolInoutStockDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolInoutStockDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolInoutStock record) {
        return toolInoutStockDao.insert(record);
    }

    
    public int insertSelective(ToolInoutStock record) {
        return toolInoutStockDao.insertSelective(record);
    }

    
    public ToolInoutStock selectByPrimaryKey(Integer id) {
        return toolInoutStockDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(ToolInoutStock record) {
        return toolInoutStockDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolInoutStock record) {
        return toolInoutStockDao.updateByPrimaryKey(record);
    }

    public Result findToolInoutStocksResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolInoutStockDao.findToolInoutStocksCount(map);
        List<ToolInoutStock> data = toolInoutStockDao.findToolInoutStocks(map);
        return new Result(data, count);
    }

    public void saveToolInoutStock(ToolInoutStock toolInoutStock)
    {
        if (toolInoutStock.getId() != null && toolInoutStock.getId() != 0)
        {
            toolInoutStockDao.updateByPrimaryKey(toolInoutStock);
        } else
        {
            toolInoutStockDao.insert(toolInoutStock);
        }
    }
}
