package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Vehicle;
import com.hulman.oms.dao.VehicleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class VehicleService
{
    @Autowired
    private VehicleDao vehicleDao;

    public List<Vehicle> findVehicles(Map<String, Object> map)
    {
        return vehicleDao.findVehicles(map);
    }

    public Integer findVehiclesCount(Map<String, Object> map)
    {
        return vehicleDao.findVehiclesCount(map);
    }

    public Result findVehiclesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = vehicleDao.findVehiclesCount(map);
        List<Vehicle> data = vehicleDao.findVehicles(map);
        return new Result(data, count);
    }

    public Vehicle findVehicleById(Integer id)
    {
        return vehicleDao.findVehicleById(id);
    }

    public void saveVehicle(Vehicle vehicle)
    {
        if (vehicle.getId() != null && vehicle.getId() != 0)
        {
            vehicleDao.updateVehicle(vehicle);
        } else
        {
            vehicleDao.insertVehicle(vehicle);
        }
    }

    public void deleteVehicleById(Integer id)
    {
        vehicleDao.deleteVehicleById(id);
    }

    public void deleteVehicles(int[] ids)
    {
        vehicleDao.deleteVehicles(ids);
    }

}
