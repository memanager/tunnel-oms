package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.VehicleRepairDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class VehicleRepairService
{
    @Autowired
    private VehicleRepairDao vehicleRepairDao;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private MsgService msgService;

    public int deleteByPrimaryKey(Integer id)
    {
        return vehicleRepairDao.deleteByPrimaryKey(id);
    }

    
    public int insert(VehicleRepair record)
    {
        if(record.getId()!=0 || record.getId()!=null)
        {
            return vehicleRepairDao.updateByPrimaryKey(record);
        }else {
            return vehicleRepairDao.insert(record);
        }
    }

    
    public int insertSelective(VehicleRepair record)
    {
        return vehicleRepairDao.insertSelective(record);
    }

    
    public VehicleRepair selectByPrimaryKey(Integer id)
    {
        return vehicleRepairDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(VehicleRepair record)
    {
        return vehicleRepairDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(VehicleRepair record)
    {
        return vehicleRepairDao.updateByPrimaryKey(record);
    }

    /**
     * 送修
     */
    @Transactional(rollbackFor = Exception.class)
    public void send(Integer vehicleId,String location,String content)
    {
        //添加出车记录
        User user = SubjectUtil.getUser();
        VehicleRepair vehicleRepair = new VehicleRepair();
        vehicleRepair.setVehicleId(vehicleId);
        Vehicle vehicle = vehicleService.findVehicleById(vehicleId);
        vehicleRepair.setVehicleName(vehicle.getName());
        vehicleRepair.setSendTime(new Date());
        vehicleRepair.setLocation(location);
        vehicleRepair.setContent(content);
        vehicleRepair.setDriverId(user.getId());
        vehicleRepair.setDriverName(user.getName());
        //记录状态出车中
        vehicleRepair.setState(1);
        vehicleRepairDao.insert(vehicleRepair);
        //更改车辆状态为维修
        vehicle.setState(3);
        vehicleService.saveVehicle(vehicle);
    }

    /**
     * 接回
     */
    @Transactional(rollbackFor = Exception.class)
    public void retrieve(Integer vehicleRepairId, Double mileage)
    {
        VehicleRepair vehicleRepair = selectByPrimaryKey(vehicleRepairId);
        Vehicle vehicle = vehicleService.findVehicleById(vehicleRepair.getVehicleId());
        Date retrieveTime = new Date();
        vehicleRepair.setRetrieveTime(retrieveTime);
        vehicleRepair.setMileage(mileage);
        //记录状态待审核
        vehicleRepair.setState(2);
        insert(vehicleRepair);
        //更改车辆状态为在库
        vehicle.setState(1);
        vehicle.setMileage(mileage);
        vehicleService.saveVehicle(vehicle);
    }

    /**
     * 审核
     */
    @Transactional(rollbackFor = Exception.class)
    public void audit(Integer vehicleRepairId, Integer auditResult, String auditComment)
    {
        User user = SubjectUtil.getUser();
        VehicleRepair vehicleRepair = selectByPrimaryKey(vehicleRepairId);
        vehicleRepair.setAuditById(user.getId());
        vehicleRepair.setAuditByName(user.getName());
        vehicleRepair.setAuditResult(auditResult);
        vehicleRepair.setAuditComment(auditComment);
        vehicleRepair.setAuditTime(new Date());
        if (auditResult == 1)
        {
            //记录状态已审核
            vehicleRepair.setState(3);
        } else
        {
            vehicleRepair.setState(1);
            Msg msg = new Msg();
            msg.setType(Msg.TYPE_SYS);
            msg.setTitle("维修记录审核未通过，请重新提交");
            msg.setState(0);
            msg.setOwnerId(vehicleRepair.getDriverId());
            msg.setOwnerName(vehicleRepair.getDriverName());
            msg.addItem("审核意见", vehicleRepair.getAuditComment());
            msgService.sendMsg(msg);
        }
        insert(vehicleRepair);
    }

    public void deleteVehicleRepairById(Integer id)
    {
        vehicleRepairDao.deleteByPrimaryKey(id);
    }

    public Integer findVehicleRepairsCount(Map<String, Object> map)
    {
        return vehicleRepairDao.findVehicleRepairsCount(map);
    }

    public Result findVehicleRepairsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = vehicleRepairDao.findVehicleRepairsCount(map);
        List<VehicleRepair> data = vehicleRepairDao.findVehicleRepairs(map);
        return new Result(data, count);
    }

    public List<VehicleRepair> selectByVehicleId(Integer id)
    {
        return vehicleRepairDao.selectByVehicleId(id);
    }

}
