package com.hulman.oms.service;

import com.hulman.oms.bean.Workbench;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.WorkbenchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: maxwellens
 */
@Service
public class WorkbenchService
{
    @Autowired
    private WorkbenchDao workbenchDao;
    @Autowired
    private UserService userService;

    public List<Workbench> findRootWorkbenches()
    {
        return workbenchDao.findRootWorkbenches();
    }

    public List<Workbench> findWorkbenchesByParentId(Integer id)
    {
        return workbenchDao.findWorkbenchesByParentId(id);
    }

    public List<Workbench> findOwnedTreeWorkbenches(Integer userId)
    {
        List<Integer> Workbenchs = userService.findOwnedWorkbenches(userId);
        List<Workbench> rootWorkbenches = filterWorkbenches(findRootWorkbenches(), Workbenchs);
        for (Workbench workbench : rootWorkbenches)
        {
            List<Workbench> children = findWorkbenchesByParentId(workbench.getId());
            workbench.setChildren(filterWorkbenches(children, Workbenchs));
        }
        return rootWorkbenches;
    }

    public List<Workbench> findWorkbenches()
    {
        return workbenchDao.findWorkbenches();
    }

    public Integer findWorkbenchesCount()
    {
        return workbenchDao.findWorkbenchesCount();
    }

    public Result findWorkbenchesResult()
    {
        int count = workbenchDao.findWorkbenchesCount();
        List<Workbench> data = workbenchDao.findWorkbenches();
        return new Result(data, count);
    }

    public Workbench findWorkbenchById(Integer id)
    {
        return workbenchDao.findWorkbenchById(id);
    }

    public void saveWorkbench(Workbench module)
    {
        if (module.getId() != null && module.getId() != 0)
        {
            workbenchDao.updateWorkbench(module);
        } else
        {
            workbenchDao.insertWorkbench(module);
        }
    }

    public void deleteWorkbenchById(Integer id)
    {
        workbenchDao.deleteWorkbenchById(id);
    }

    public void deleteWorkbenchesByIdCascade(Integer id)
    {
        workbenchDao.deleteWorkbenchById(id);
        List<Workbench> childWorkbenches = workbenchDao.findWorkbenchesByParentId(id);
        for (Workbench module : childWorkbenches)
        {
            deleteWorkbenchesByIdCascade(module.getId());
        }
    }

    /**
     * @param workbenches     待过滤的模块列表
     * @param filterIds 需要保留的WorkbenchIds
     * @return 已过滤的菜模块列表
     */
    private List<Workbench> filterWorkbenches(List<Workbench> workbenches, List<Integer> filterIds)
    {
        List<Workbench> result = new ArrayList<>();
        for (Workbench workbench : workbenches)
        {
            if (filterIds.contains(workbench.getId()))
            {
                result.add(workbench);
            }
        }
        return result;
    }

}
