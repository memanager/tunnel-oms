package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.DeviceType;
import com.hulman.oms.dao.DeviceTypeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DeviceTypeService
{
    @Autowired
    private DeviceTypeDao deviceTypeDao;

    public List<DeviceType> findDeviceTypes()
    {
        return deviceTypeDao.findDeviceTypes();
    }

    public void saveDeviceType(DeviceType deviceType)
    {
        if (deviceType.getId() != null && deviceType.getId() != 0)
        {
            deviceTypeDao.updateDeviceType(deviceType);
        } else
        {
            deviceTypeDao.insertDeviceType(deviceType);
        }
    }

    /**
     * 如果不存在名称则保存
     *
     * @param name
     */
    public void checkAndSave(String name)
    {
        DeviceType deviceType = deviceTypeDao.findDeviceTypeByName(name);
        if (deviceType == null)
        {
            deviceType = new DeviceType();
            deviceType.setName(name);
            deviceTypeDao.insertDeviceType(deviceType);
        }
    }

}
