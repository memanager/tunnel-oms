package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Sparepart;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.SparepartWarehouseDao;
import com.hulman.oms.bean.SparepartWarehouse;

import java.util.List;
import java.util.Map;

@Service
public class SparepartWarehouseService
{

    @Autowired
    private SparepartWarehouseDao sparepartWarehouseDao;


    public int deleteByPrimaryKey(Integer id)
    {
        return sparepartWarehouseDao.deleteByPrimaryKey(id);
    }


    public int insert(SparepartWarehouse record)
    {
        return sparepartWarehouseDao.insert(record);
    }


    public int insertSelective(SparepartWarehouse record)
    {
        return sparepartWarehouseDao.insertSelective(record);
    }


    public SparepartWarehouse selectByPrimaryKey(Integer id)
    {
        return sparepartWarehouseDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(SparepartWarehouse record)
    {
        return sparepartWarehouseDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(SparepartWarehouse record)
    {
        return sparepartWarehouseDao.updateByPrimaryKey(record);
    }

    public Result findSparepartWarehousesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartWarehouseDao.findSparepartWarehouseCount(map);
        List<SparepartWarehouse> data = sparepartWarehouseDao.findSparepartWareHouses(map);
        return new Result(data, count);
    }

    public int save(SparepartWarehouse record)
    {
        if (record.getId() != null && record.getId() != 0)
        {
            return sparepartWarehouseDao.updateByPrimaryKey(record);
        } else
        {
            return sparepartWarehouseDao.insert(record);
        }
    }

    public SparepartWarehouse selectByName(String name)
    {
        return sparepartWarehouseDao.selectByName(name);
    }

}
