package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.DeviceReplaceDao;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DeviceReplaceService
{
    private static final String PROCESS_KEY = "device-replace";

    @Autowired
    private DeviceReplaceDao deviceReplaceDao;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private UserService userService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProcInstService procInstService;

    public List<DeviceReplace> findDeviceReplaces(Map<String, Object> map)
    {
        return deviceReplaceDao.findDeviceReplaces(map);
    }

    public Integer findDeviceReplacesCount(Map<String, Object> map)
    {
        return deviceReplaceDao.findDeviceReplacesCount(map);
    }

    public Result findDeviceReplacesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = deviceReplaceDao.findDeviceReplacesCount(map);
        List<DeviceReplace> data = deviceReplaceDao.findDeviceReplaces(map);
        return new Result(data, count);
    }

    public DeviceReplace findDeviceReplaceById(Integer id)
    {
        return deviceReplaceDao.findDeviceReplaceById(id);
    }

    public void saveDeviceReplace(DeviceReplace deviceReplace)
    {
        Device device = deviceService.findDeviceById(deviceReplace.getDeviceId());
        device.setBrand(deviceReplace.getBrand2());
        device.setModel(deviceReplace.getModel2());
        device.setSpec(deviceReplace.getSpec2());
        device.setProductNo(deviceReplace.getProductNo2());
        device.setProductDate(deviceReplace.getProductDate2());
        deviceService.saveDevice(device);
        deviceReplace.setReplaceTime(new Date());
        deviceReplaceDao.insertDeviceReplace(deviceReplace);
    }

    public void deleteDeviceReplaceById(Integer id)
    {
        deviceReplaceDao.deleteDeviceReplaceById(id);
    }

    public void deleteDeviceReplaces(int[] ids)
    {
        deviceReplaceDao.deleteDeviceReplaces(ids);
    }

    /**
     * 创建设备更换工单
     *
     * @param deviceId
     * @param brand1
     * @param model1
     * @param spec1
     * @param productNo1
     * @param productDate1
     * @param note1
     * @param replaceById
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Task create(Integer deviceId, String brand1, String model1, String spec1, String productNo1, Date productDate1, String note1, Integer replaceById, String replaceByName)
    {
        User user = SubjectUtil.getUser();
        if (user == null)
        {
            throw new IoTException("用户未登陆");
        }
        //创建工单
        ProcInst procInst = procInstService.startProcess(PROCESS_KEY);
        Device device = deviceService.findDeviceById(deviceId);
        DeviceReplace deviceReplace = new DeviceReplace();
        deviceReplace.setProcInstId(procInst.getId());
        deviceReplace.setNo(OrderUtil.generate("SBGH"));
        deviceReplace.setDeviceId(deviceId);
        deviceReplace.setDeviceName(device.getName());
        deviceReplace.setBrand1(brand1);
        deviceReplace.setModel1(model1);
        deviceReplace.setSpec1(spec1);
        deviceReplace.setProductNo1(productNo1);
        deviceReplace.setProductDate1(productDate1);
        deviceReplace.setNote1(note1);
        deviceReplace.setCreateById(user.getId());
        deviceReplace.setCreateByName(user.getName());
        deviceReplace.setCreateTime(new Date());
        User replaceBy = userService.findUserByIdOrName(replaceById, replaceByName);
        deviceReplace.setReplaceById(replaceById);
        deviceReplace.setReplaceByName(replaceBy.getName());
        deviceReplace.setState(DeviceReplace.STATE_REPLACING);
        deviceReplaceDao.insertDeviceReplace(deviceReplace);

        //创建任务
        Task createTask = new Task();
        createTask.setProcInstId(procInst.getId());
        createTask.setOwnerId(user.getId());
        createTask.setOwnerName(user.getName());
        createTask.setName(procInst.getStateName());
        createTask.setTag(PROCESS_KEY);
        createTask.setFormKey(deviceReplace.getId().toString());
        createTask.addExtra("工单号", deviceReplace.getNo());
        createTask.addExtra("更换设备", deviceReplace.getDeviceName());
        taskService.createAndCompleteTask(createTask);

        //创建更换任务
        procInst = procInstService.next(procInst.getId());
        Task replaceTask = new Task();
        replaceTask.setProcInstId(procInst.getId());
        replaceTask.setOwnerId(replaceBy.getId());
        replaceTask.setOwnerName(replaceBy.getName());
        replaceTask.setName(procInst.getStateName());
        replaceTask.setTag(PROCESS_KEY);
        replaceTask.setFormKey(deviceReplace.getId().toString());
        replaceTask.addExtra("工单号", deviceReplace.getNo());
        replaceTask.addExtra("更换设备", deviceReplace.getDeviceName());
        taskService.addTask(replaceTask);
        replaceTask.setPath("/pages/device-replace/detail?taskId=" + replaceTask.getId());
        replaceTask.setPage("/device-replace-info.html?taskId=" + replaceTask.getId());
        taskService.saveTask(replaceTask);
        return replaceTask;
    }

    /**
     * 设备更换业务
     *
     * @param taskId
     * @param id
     * @param brand2
     * @param model2
     * @param spec2
     * @param productNo2
     * @param productDate2
     * @param note2
     */
    @Transactional(rollbackFor = Exception.class)
    public void complete(Integer taskId, Integer id, String brand2, String model2, String spec2, String productNo2, Date productDate2, String note2)
    {
        DeviceReplace deviceReplace = deviceReplaceDao.findDeviceReplaceById(id);
        deviceReplace.setBrand2(brand2);
        deviceReplace.setModel2(model2);
        deviceReplace.setSpec2(spec2);
        deviceReplace.setProductNo2(productNo2);
        deviceReplace.setProductDate2(productDate2);
        deviceReplace.setNote2(note2);
        deviceReplace.setReplaceTime(new Date());
        deviceReplace.setState(DeviceReplace.STATE_REPLACED);
        deviceReplaceDao.updateDeviceReplace(deviceReplace);
        //完成本次任务
        taskService.createAndCompleteTask(taskId);
    }

    /**
     * 撤销工单
     *
     * @param id 工单ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void terminate(Integer id)
    {
        //修改工单
        DeviceReplace deviceReplace = deviceReplaceDao.findDeviceReplaceById(id);
        deviceReplace.setState(DeviceReplace.STATE_TERMINATED);
        deviceReplaceDao.updateDeviceReplace(deviceReplace);
        //终止流程
        procInstService.terminate(deviceReplace.getProcInstId());
    }
}
