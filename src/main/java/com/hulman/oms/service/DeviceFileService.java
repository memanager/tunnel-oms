package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.UserFile;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.DeviceFileDao;
import com.hulman.oms.bean.DeviceFile;

import java.util.List;
import java.util.Map;

@Service
public class DeviceFileService
{

    @Autowired
    private DeviceFileDao deviceFileDao;


    public int deleteByPrimaryKey(Integer id)
    {
        return deviceFileDao.deleteByPrimaryKey(id);
    }


    public int insert(DeviceFile record)
    {
        return deviceFileDao.insert(record);
    }


    public int insertSelective(DeviceFile record)
    {
        return deviceFileDao.insertSelective(record);
    }


    public DeviceFile selectByPrimaryKey(Integer id)
    {
        return deviceFileDao.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(DeviceFile record)
    {
        return deviceFileDao.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(DeviceFile record)
    {
        return deviceFileDao.updateByPrimaryKey(record);
    }

    public Result findDeviceFilesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = deviceFileDao.findDeviceFilesCount(map);
        List<DeviceFile> data = deviceFileDao.findDeviceFiles(map);
        return new Result(data, count);
    }

    public DeviceFile findDeviceFilesByUuid(String uuid)
    {
        return deviceFileDao.findDeviceFilesByUuid(uuid);
    }

    public void saveDeviceFile(DeviceFile deviceFile)
    {
        if (deviceFile.getId() != null && deviceFile.getId() != 0)
        {
            deviceFileDao.updateByPrimaryKey(deviceFile);
        } else
        {
            deviceFileDao.insert(deviceFile);
        }
    }


}
