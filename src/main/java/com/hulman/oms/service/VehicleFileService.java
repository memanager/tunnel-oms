package com.hulman.oms.service;

import com.hulman.oms.bean.VehicleFile;
import com.hulman.oms.bean.Result;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.VehicleFileDao;
import com.hulman.oms.bean.VehicleFile;

import java.util.List;
import java.util.Map;

@Service
public class VehicleFileService{

    @Autowired
    private VehicleFileDao vehicleFileDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return vehicleFileDao.deleteByPrimaryKey(id);
    }

    
    public int insert(VehicleFile record) {
        return vehicleFileDao.insert(record);
    }

    
    public int insertSelective(VehicleFile record) {
        return vehicleFileDao.insertSelective(record);
    }

    
    public VehicleFile selectByPrimaryKey(Integer id) {
        return vehicleFileDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(VehicleFile record) {
        return vehicleFileDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(VehicleFile record) {
        return vehicleFileDao.updateByPrimaryKey(record);
    }

    public Result findVehicleFilesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = vehicleFileDao.findVehicleFilesCount(map);
        List<VehicleFile> data = vehicleFileDao.findVehicleFiles(map);
        return new Result(data, count);
    }

    public VehicleFile findVehicleFilesByUuid(String uuid)
    {
        return vehicleFileDao.findVehicleFilesByUuid(uuid);
    }

    public void saveVehicleFile(VehicleFile vehicleFile)
    {
        if (vehicleFile.getId() != null && vehicleFile.getId() != 0)
        {
            vehicleFileDao.updateByPrimaryKey(vehicleFile);
        } else
        {
            vehicleFileDao.insert(vehicleFile);
        }
    }

}
