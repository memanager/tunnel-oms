package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.MaintainPlanItem;
import com.hulman.oms.dao.MaintainPlanItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class MaintainPlanItemService
{
    @Autowired
    private MaintainPlanItemDao maintainPlanItemDao;

    public List<MaintainPlanItem> findMaintainPlanItems(Map<String, Object> map)
    {
        return maintainPlanItemDao.findMaintainPlanItems(map);
    }

    public Integer findMaintainPlanItemsCount(Map<String, Object> map)
    {
        return maintainPlanItemDao.findMaintainPlanItemsCount(map);
    }

    public List<MaintainPlanItem> findMaintainPlanItemsByMaintainPlanId(Integer maintainPlanId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("maintainPlanId", maintainPlanId);
        return maintainPlanItemDao.findMaintainPlanItems(map);
    }

    public List<MaintainPlanItem> findMaintainPlanItemsByMaintainPlanIdAndItem(Integer maintainPlanId, String item)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("maintainPlanId", maintainPlanId);
        map.put("item", item);
        return maintainPlanItemDao.findMaintainPlanItems(map);
    }

    public Result findMaintainPlanItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = maintainPlanItemDao.findMaintainPlanItemsCount(map);
        List<MaintainPlanItem> data = maintainPlanItemDao.findMaintainPlanItems(map);
        return new Result(data, count);
    }

    public MaintainPlanItem findMaintainPlanItemById(Integer id)
    {
        return maintainPlanItemDao.findMaintainPlanItemById(id);
    }

    public void saveMaintainPlanItem(MaintainPlanItem maintainPlanItem)
    {
        if (maintainPlanItem.getId() != null && maintainPlanItem.getId() != 0)
        {
            maintainPlanItemDao.updateMaintainPlanItem(maintainPlanItem);
        } else
        {
            maintainPlanItemDao.insertMaintainPlanItem(maintainPlanItem);
        }
    }

    public void deleteMaintainPlanItemById(Integer id)
    {
        maintainPlanItemDao.deleteMaintainPlanItemById(id);
    }

    public void deleteMaintainPlanItems(int[] ids)
    {
        maintainPlanItemDao.deleteMaintainPlanItems(ids);
    }

}
