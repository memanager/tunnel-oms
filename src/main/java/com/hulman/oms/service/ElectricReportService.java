package com.hulman.oms.service;

import com.hulman.oms.bean.ElectricMeter;
import com.hulman.oms.bean.ElectricReportItem;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ElectricReport;
import com.hulman.oms.dao.ElectricMeterDao;
import com.hulman.oms.dao.ElectricReportDao;
import com.hulman.oms.dao.ElectricReportItemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ElectricReportService
{
    @Autowired
    private ElectricReportDao electricReportDao;
    @Autowired
    private ElectricMeterService electricMeterService;
    @Autowired
    private ElectricReportItemDao electricReportItemDao;

    public List<ElectricReport> findElectricReports(Map<String, Object> map)
    {
        return electricReportDao.findElectricReports(map);
    }

    public Integer findElectricReportsCount(Map<String, Object> map)
    {
        return electricReportDao.findElectricReportsCount(map);
    }

    public Result findElectricReportsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = electricReportDao.findElectricReportsCount(map);
        List<ElectricReport> data = electricReportDao.findElectricReports(map);
        return new Result(data, count);
    }

    public ElectricReport findElectricReportById(Integer id)
    {
        return electricReportDao.findElectricReportById(id);
    }

    public void saveElectricReport(ElectricReport electricReport)
    {
        if (electricReport.getId() != null && electricReport.getId() != 0)
        {
            electricReportDao.updateElectricReport(electricReport);
        } else
        {
            electricReportDao.insertElectricReport(electricReport);
        }
    }

    public void deleteElectricReportById(Integer id)
    {
        electricReportDao.deleteElectricReportById(id);
    }

    public void deleteElectricReports(int[] ids)
    {
        electricReportDao.deleteElectricReports(ids);
    }

    /**
     * 创建报表
     * @param report
     */
    @Transactional(rollbackFor = Exception.class)
    public void createReport(ElectricReport report)
    {
        electricReportDao.insertElectricReport(report);
        List<ElectricMeter> meters = electricMeterService.findAllElectricMeters();
        for (ElectricMeter meter : meters)
        {
            ElectricReportItem item = new ElectricReportItem();
            item.setReportId(report.getId());
            item.setMeterId(meter.getId());
            item.setMeterName(meter.getName());
            item.setMeterNo(meter.getNo());
            item.setRatio(meter.getRatio());
            item.setLastAmount(meter.getAmount());
            item.setLastBalance(meter.getBalance());
            item.setState(0);
            electricReportItemDao.insertElectricReportItem(item);
        }
    }

}
