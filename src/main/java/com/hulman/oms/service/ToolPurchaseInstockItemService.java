package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.ToolPurchaseInstockItem;
import com.hulman.oms.dao.ToolPurchaseInstockItemDao;

import java.util.List;
import java.util.Map;

@Service
public class ToolPurchaseInstockItemService{

    @Autowired
    private ToolPurchaseInstockItemDao toolPurchaseInstockItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolPurchaseInstockItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolPurchaseInstockItem record) {
        return toolPurchaseInstockItemDao.insert(record);
    }

    
    public int insertSelective(ToolPurchaseInstockItem record) {
        return toolPurchaseInstockItemDao.insertSelective(record);
    }

    
    public ToolPurchaseInstockItem selectByPrimaryKey(Integer id) {
        return toolPurchaseInstockItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(ToolPurchaseInstockItem record) {
        return toolPurchaseInstockItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolPurchaseInstockItem record) {
        return toolPurchaseInstockItemDao.updateByPrimaryKey(record);
    }

    public Result findToolPurchaseInstockItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolPurchaseInstockItemDao.findToolPurchaseInstockItemsCount(map);
        List<ToolPurchaseInstockItem> data = toolPurchaseInstockItemDao.findToolPurchaseInstockItems(map);
        return new Result(data, count);
    }

    public void saveToolPurchaseInstockItem(ToolPurchaseInstockItem toolPurchaseInstockItem)
    {
        if (toolPurchaseInstockItem.getId() != null && toolPurchaseInstockItem.getId() != 0)
        {
            toolPurchaseInstockItemDao.updateByPrimaryKey(toolPurchaseInstockItem);
        } else
        {
            toolPurchaseInstockItemDao.insert(toolPurchaseInstockItem);
        }
    }

    public void deleteToolPurchaseInstockItemById(Integer id)
    {
        toolPurchaseInstockItemDao.deleteByPrimaryKey(id);
    }

    public ToolPurchaseInstockItem findToolPurchaseInstockItemById(Integer id)
    {
        return toolPurchaseInstockItemDao.selectByPrimaryKey(id);
    }

}
