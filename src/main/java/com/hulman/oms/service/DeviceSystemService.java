package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.DeviceSystem;
import com.hulman.oms.dao.DeviceSystemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DeviceSystemService
{
    @Autowired
    private DeviceSystemDao deviceSystemDao;

    public List<DeviceSystem> findDeviceSystems()
    {
        return deviceSystemDao.findDeviceSystems();
    }

    public DeviceSystem findDeviceSystemByName(String name)
    {
        return deviceSystemDao.findDeviceSystemByName(name);
    }

    public DeviceSystem findDeviceSystemById(Integer id)
    {
        return deviceSystemDao.findDeviceSystemById(id);
    }

    public void saveDeviceSystem(DeviceSystem deviceSystem)
    {
        if (deviceSystem.getId() != null && deviceSystem.getId() != 0)
        {
            deviceSystemDao.updateDeviceSystem(deviceSystem);
        } else
        {
            deviceSystemDao.insertDeviceSystem(deviceSystem);
        }
    }

    public void deleteDeviceSystemById(Integer id)
    {
        deviceSystemDao.deleteDeviceSystemById(id);
    }

}
