package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.MaintainTaskDao;
import com.hulman.oms.dao.MaintainTaskItemDao;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class MaintainTaskItemService
{
    private static final String PROCESS_KEY = "maintain";
    /**
     * 审核通过
     */
    private static final Integer AUDIT_OK = 1;

    /**
     * 审核不通过
     */
    private static final Integer AUDIT_NG = 2;
    @Autowired
    private MaintainTaskItemDao maintainTaskItemDao;
    @Autowired
    private MaintainTaskDao maintainTaskDao;
    @Autowired
    private MsgService msgService;

    public List<MaintainTaskItem> findMaintainTaskItems(Map<String, Object> map)
    {
        return maintainTaskItemDao.findMaintainTaskItems(map);
    }

    public Integer findMaintainTaskItemsCount(Map<String, Object> map)
    {
        return maintainTaskItemDao.findMaintainTaskItemsCount(map);
    }

    public Result findMaintainTaskItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = maintainTaskItemDao.findMaintainTaskItemsCount(map);
        List<MaintainTaskItem> data = maintainTaskItemDao.findMaintainTaskItems(map);
        return new Result(data, count);
    }

    public MaintainTaskItem findMaintainTaskItemById(Integer id)
    {
        return maintainTaskItemDao.findMaintainTaskItemById(id);
    }

    public MaintainTaskItem findMaintainTaskItemByDeviceCode(Integer maintainTaskId, String deviceCode)
    {
        return maintainTaskItemDao.findMaintainTaskItemByDeviceCode(maintainTaskId, deviceCode);
    }

    public MaintainTaskItem findMaintainTaskItemByArea(Integer maintainTaskId, String area)
    {
        return maintainTaskItemDao.findMaintainTaskItemByArea(maintainTaskId, area);
    }

    public void saveMaintainTaskItem(MaintainTaskItem maintainTaskItem)
    {
        if (maintainTaskItem.getId() != null && maintainTaskItem.getId() != 0)
        {
            maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
        } else
        {
            maintainTaskItemDao.insertMaintainTaskItem(maintainTaskItem);
        }
    }

    public void deleteMaintainTaskItemById(Integer id)
    {
        maintainTaskItemDao.deleteMaintainTaskItemById(id);
    }

    public void deleteMaintainTaskItems(int[] ids)
    {
        maintainTaskItemDao.deleteMaintainTaskItems(ids);
    }

    public void start(Integer maintainTaskItemId)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemDao.findMaintainTaskItemById(maintainTaskItemId);
        maintainTaskItem.setStartTime(new Date());
        maintainTaskItem.setState(MaintainTaskItem.STATE_EXECUTING);
        maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
    }

    public void save(Integer maintainTaskItemId, String content, String imgs, String completion)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemDao.findMaintainTaskItemById(maintainTaskItemId);
        maintainTaskItem.setContent(content);
        maintainTaskItem.setImgs(imgs);
        maintainTaskItem.setCompletion(completion);
        maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
    }

    public void complete(Integer maintainTaskItemId, String content, Integer result, String imgs, String completion)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemDao.findMaintainTaskItemById(maintainTaskItemId);
        maintainTaskItem.setContent(content);
        maintainTaskItem.setResult(result);
        maintainTaskItem.setImgs(imgs);
        maintainTaskItem.setCompletion(completion);
        maintainTaskItem.setEndTime(new Date());
        maintainTaskItem.setState(MaintainTaskItem.STATE_AUDITING);
        maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
    }

    @Transactional(rollbackFor = Exception.class)
    public void audit(Integer maintainTaskItemId, Integer auditResult, String auditComment)
    {
        User user = SubjectUtil.getUser();
        if (user == null)
        {
            throw new IoTException("用户未登陆");
        }
        MaintainTaskItem maintainTaskItem = maintainTaskItemDao.findMaintainTaskItemById(maintainTaskItemId);
        maintainTaskItem.setAuditResult(auditResult);
        maintainTaskItem.setAuditComment(auditComment);
        maintainTaskItem.setAuditTimes(maintainTaskItem.getAuditTimes() + 1);
        MaintainTask maintainTask = maintainTaskDao.findMaintainTaskById(maintainTaskItem.getMaintainTaskId());
        //给执行人发通知
        Msg msg = new Msg();
        msg.setType(Msg.TYPE_OM);
        //审核通过
        if (auditResult == AUDIT_OK)
        {
            maintainTaskItem.setState(MaintainTaskItem.STATE_AUDITED);
            maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
            msg.setTitle("专项任务子项审核通过");
        }
        //审核不通过
        else if (auditResult == AUDIT_NG)
        {
            maintainTaskItem.setState(MaintainTaskItem.STATE_EXECUTING);
            maintainTaskItemDao.updateMaintainTaskItem(maintainTaskItem);
            msg.setTitle("专项任务子项未通过");
        }
        msg.addItem("审核意见", auditComment);
        msg.setOwnerId(maintainTask.getExecuteById());
        msg.setOwnerName(maintainTask.getExecuteByName());
        msgService.sendMsg(msg);
    }

}
