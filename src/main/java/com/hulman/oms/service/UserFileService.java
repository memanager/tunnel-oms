package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.UserFile;
import com.hulman.oms.dao.UserFileDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class UserFileService
{
    @Autowired
    private UserFileDao userFileDao;

    public List<UserFile> findUserFiles(Map<String, Object> map)
    {
        return userFileDao.findUserFiles(map);
    }

    public Integer findUserFilesCount(Map<String, Object> map)
    {
        return userFileDao.findUserFilesCount(map);
    }

    public Result findUserFilesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = userFileDao.findUserFilesCount(map);
        List<UserFile> data = userFileDao.findUserFiles(map);
        return new Result(data, count);
    }

    public UserFile findUserFileById(Integer id)
    {
        return userFileDao.findUserFileById(id);
    }

    public UserFile findUserFileByUuid(String uuid)
    {
        return userFileDao.findUserFileByUuid(uuid);
    }

    public void saveUserFile(UserFile UserFile)
    {
        if (UserFile.getId() != null && UserFile.getId() != 0)
        {
            userFileDao.updateUserFile(UserFile);
        } else
        {
            userFileDao.insertUserFile(UserFile);
        }
    }

    public void deleteUserFileById(Integer id)
    {
        userFileDao.deleteUserFileById(id);
    }

    public void deleteUserFiles(int[] ids)
    {
        userFileDao.deleteUserFiles(ids);
    }

}
