package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartInoutStock;
import com.hulman.oms.dao.SparepartInoutStockDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class SparepartInoutStockService
{
    @Autowired
    private SparepartInoutStockDao sparepartInoutStockDao;

    public List<SparepartInoutStock> findSparepartInoutStocks(Map<String, Object> map)
    {
        return sparepartInoutStockDao.findSparepartInoutStocks(map);
    }

    public Integer findSparepartInoutStocksCount(Map<String, Object> map)
    {
        return sparepartInoutStockDao.findSparepartInoutStocksCount(map);
    }

    public Result findSparepartInoutStocksResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartInoutStockDao.findSparepartInoutStocksCount(map);
        List<SparepartInoutStock> data = sparepartInoutStockDao.findSparepartInoutStocks(map);
        return new Result(data, count);
    }

    public SparepartInoutStock findSparepartInoutStockById(Integer id)
    {
        return sparepartInoutStockDao.findSparepartInoutStockById(id);
    }

    public void saveSparepartInoutStock(SparepartInoutStock sparepartInoutStock)
    {
        if (sparepartInoutStock.getId() != null && sparepartInoutStock.getId() != 0)
        {
            sparepartInoutStockDao.updateSparepartInoutStock(sparepartInoutStock);
        } else
        {
            sparepartInoutStockDao.insertSparepartInoutStock(sparepartInoutStock);
        }
    }

    public void deleteSparepartInoutStockById(Integer id)
    {
        sparepartInoutStockDao.deleteSparepartInoutStockById(id);
    }

    public void deleteSparepartInoutStocks(int[] ids)
    {
        sparepartInoutStockDao.deleteSparepartInoutStocks(ids);
    }

}
