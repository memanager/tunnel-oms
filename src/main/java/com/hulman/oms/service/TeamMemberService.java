package com.hulman.oms.service;

import com.hulman.oms.bean.Team;
import com.hulman.oms.bean.TeamMember;
import com.hulman.oms.dao.TeamDao;
import com.hulman.oms.dao.TeamMemberDao;
import com.hulman.oms.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: maxwellens
 */
@Service
public class TeamMemberService
{
    @Autowired
    private TeamMemberDao teamMemberDao;
    @Autowired
    private TeamDao teamDao;

    public List<TeamMember> findTeamMembers()
    {
        return teamMemberDao.findTeamMembers();
    }

    public void addMember(Integer teamId, Integer userId)
    {
        teamMemberDao.insertTeamMember(teamId, userId);
    }

    public void removeMember(Integer teamId, Integer userId)
    {
        teamMemberDao.deleteTeamMember(teamId, userId);
    }

    /**
     * 人员所在组
     *
     * @param userId
     * @return
     */
    public Team findTeamByMemberId(Integer userId)
    {
        Integer teamId = teamMemberDao.findTeamId(userId);
        if (teamId == null)
        {
            return null;
        } else
        {
            return teamDao.findTeamById(teamId);
        }
    }

}
