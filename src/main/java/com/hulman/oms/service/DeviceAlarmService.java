package com.hulman.oms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class DeviceAlarmService
{
    @Value("${tunnel-cloud.context}")
    private String context;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 告警列表
     * @param page
     * @param limit
     * @param deviceName
     * @param state
     * @return
     */
    public Object findDeviceAlarms(Integer page, Integer limit, String deviceName, Integer state)
    {
        String url = context + "/device-alarms?page={page}&limit={limit}&deviceName={deviceName}&state={state}";
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceName", deviceName);
        map.put("state", state);
        Object result = restTemplate.getForObject(url, Object.class, map);
        return result;
    }

    public Object findDeviceAlarmById(Integer id)
    {
        String url = context + "/device-alarms/" + id;
        return restTemplate.getForObject(url, Object.class);
    }

    public Object findUnconfirmedDeviceAlarmsCount()
    {
        String url = context + "/device-alarms/unconfirmed-device-alarms-count";
        return restTemplate.getForObject(url, Object.class);
    }

    /**
     * 确认告警
     *
     * @param id
     * @return
     */
    public Object confirmDeviceAlarm(Integer id)
    {
        String url = context + "/device-alarms/" + id + "/confirm";
        return restTemplate.getForObject(url, Object.class);
    }

    /**
     * 处理告警
     *
     * @param id
     * @return
     */
    public Object handleDeviceAlarm(Integer id)
    {
        String url = context + "/device-alarms/" + id + "/handle";
        return restTemplate.getForObject(url, Object.class);
    }


    public void deleteDeviceAlarm(Integer id)
    {
        String url = context + "/device-alarms/" + id;
        restTemplate.delete(url);
    }
}
