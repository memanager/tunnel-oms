package com.hulman.oms.service;

import com.hulman.oms.bean.Param;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.ParamDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class ParamService
{
    @Autowired
    private ParamDao paramDao;

    public List<Param> findParams(Map<String, Object> map)
    {
        return paramDao.findParams(map);
    }

    public Integer findParamsCount(Map<String, Object> map)
    {
        return paramDao.findParamsCount(map);
    }

    public Result findParamsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = paramDao.findParamsCount(map);
        List<Param> data = paramDao.findParams(map);
        return new Result(data, count);
    }

    public Param findParamById(Integer id)
    {
        return paramDao.findParamById(id);
    }

    public void saveParam(Param param)
    {
        if (param.getId() != null && param.getId() != 0)
        {
            paramDao.updateParam(param);
        } else
        {
            paramDao.insertParam(param);
        }
    }

    public void deleteParamById(Integer id)
    {
        paramDao.deleteParamById(id);
    }

    public void deleteParams(String ids)
    {
        paramDao.deleteParams(ids);
    }

}
