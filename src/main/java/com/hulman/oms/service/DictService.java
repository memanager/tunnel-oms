package com.hulman.oms.service;

import com.hulman.oms.bean.Dict;
import com.hulman.oms.bean.Result;
import com.hulman.oms.dao.DictDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class DictService
{
    @Autowired
    private DictDao dictDao;

    public List<Dict> findDicts(Map<String, Object> map)
    {
        return dictDao.findDicts(map);
    }

    public Integer findDictsCount(Map<String, Object> map)
    {
        return dictDao.findDictsCount(map);
    }

    public Result findDictsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = dictDao.findDictsCount(map);
        List<Dict> data = dictDao.findDicts(map);
        return new Result(data, count);
    }

    public Dict findDictById(Integer id)
    {
        return dictDao.findDictById(id);
    }

    public Dict findDictByKey(String key)
    {
        return dictDao.findDictByKey(key);
    }

    public void saveDict(Dict dict)
    {
        if (dict.getId() != null && dict.getId() != 0)
        {
            dictDao.updateDict(dict);
        } else
        {
            dictDao.insertDict(dict);
        }
    }

    public void deleteDictById(Integer id)
    {
        dictDao.deleteDictById(id);
    }

    public void deleteDicts(String ids)
    {
        dictDao.deleteDicts(ids);
    }

}
