package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.ToolDao;
import com.hulman.oms.dao.ToolInoutStockDao;
import com.hulman.oms.dao.ToolUseItemDao;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.dao.ToolUseOrderDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ToolUseOrderService{

    private static final String ORDER_PREFIX = "GJLY";

    @Autowired
    private ToolUseOrderDao toolUseOrderDao;

    @Autowired
    private ToolUseItemDao toolUseItemDao;

    @Autowired
    private ToolInoutStockDao toolInoutStockDao;

    @Autowired
    private ToolDao toolDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return toolUseOrderDao.deleteByPrimaryKey(id);
    }

    
    public int insert(ToolUseOrder record) {
        return toolUseOrderDao.insert(record);
    }

    
    public int insertSelective(ToolUseOrder record) {
        return toolUseOrderDao.insertSelective(record);
    }

    
    public ToolUseOrder selectByPrimaryKey(Integer id) {
        ToolUseOrder toolUseOrder = toolUseOrderDao.selectByPrimaryKey(id);
        List<ToolUseItem> toolUseItemList = toolUseItemDao.findToolUseByOrderId(id);
        toolUseOrder.setItems(toolUseItemList);
        return toolUseOrder;
    }

    
    public int updateByPrimaryKeySelective(ToolUseOrder record) {
        return toolUseOrderDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(ToolUseOrder record) {
        return toolUseOrderDao.updateByPrimaryKey(record);
    }


    public ToolUseOrder selectByNo(String no) {
        ToolUseOrder toolUseOrder = toolUseOrderDao.selectByNo(no);
        List<ToolUseItem> toolUseItemList = toolUseItemDao.findToolUseByOrderId(toolUseOrder.getId());
        toolUseOrder.setItems(toolUseItemList);
        return toolUseOrder;
    }



    public Result findToolUseOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = toolUseOrderDao.findToolUserOrdersCount(map);
        List<ToolUseOrder> data = toolUseOrderDao.findToolUserOrders(map);
        return new Result(data, count);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveToolUseOrder(ToolUseOrder toolUseOrder)
    {
        User user = SubjectUtil.getUser();
        ToolInoutStock toolInoutStock = new ToolInoutStock();
        if (toolUseOrder.getId() != null && toolUseOrder.getId() != 0)
        {
            ToolUseOrder toolUseOrderHistory = toolUseOrderDao.selectByPrimaryKey(toolUseOrder.getId());
            toolUseOrder.setState(2);
            toolUseOrder.setUseBy(toolUseOrderHistory.getUseBy());
            toolUseOrder.setUseTime(toolUseOrderHistory.getUseTime());
            toolUseOrder.setUseCreateBy(toolUseOrderHistory.getUseCreateBy());
            toolUseOrder.setReturnTime(new Date());
            toolUseOrder.setReturnCreateBy(user.getName());
            toolUseOrder.setNo(toolUseOrderHistory.getNo());
            toolUseOrderDao.updateByPrimaryKey(toolUseOrder);
            for (ToolUseItem item : toolUseOrder.getItems())
            {
                item.setToolUseOrderId(toolUseOrder.getId());
                //插入订单明细
                toolUseItemDao.updateByPrimaryKey(item);
                //出库记录
                toolInoutStock = item.toToolReturnInoutStock();
                toolInoutStock.setOrderNo(toolUseOrder.getNo());
                toolInoutStockDao.insert(toolInoutStock);
                //更新库存数量
                toolDao.addQuantity(item.getToolId(),  item.getReturnQuantity());

            }
        } else
        {
            toolUseOrder.setState(1);
            toolUseOrder.setUseTime(new Date());
            toolUseOrder.setUseCreateBy(user.getName());
            toolUseOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            toolUseOrderDao.insert(toolUseOrder);
            for (ToolUseItem item : toolUseOrder.getItems())
            {
                item.setToolUseOrderId(toolUseOrder.getId());
                //插入订单明细
                toolUseItemDao.insert(item);
                //入库记录
                toolInoutStock = item.toToolUseInoutStock();
                toolInoutStock.setOrderNo(toolUseOrder.getNo());
                toolInoutStockDao.insert(toolInoutStock);
                //更新库存数量
                toolDao.addQuantity(item.getToolId(), 0 - item.getUseQuantity());

            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteToolUseOrderById(Integer id)
    {
        ToolUseOrder toolUseOrder = toolUseOrderDao.selectByPrimaryKey(id);
        //删除单据
        toolUseOrderDao.deleteByPrimaryKey(id);
        //删除单据明细
        toolUseItemDao.deleteByOrderId(id);
        //删除出入库记录
        String orderNo = toolUseOrder.getNo();
        toolInoutStockDao.deleteToolInoutStockByOrderNo(orderNo);
    }
}
