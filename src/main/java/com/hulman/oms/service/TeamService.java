package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Team;
import com.hulman.oms.bean.User;
import com.hulman.oms.dao.TeamDao;
import com.hulman.oms.dao.UserDao;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class TeamService
{
    @Autowired
    private TeamDao teamDao;
    @Autowired
    private UserDao userDao;

    public List<Team> findTeams(Map<String, Object> map)
    {
        return teamDao.findTeams(map);
    }

    public Integer findTeamsCount(Map<String, Object> map)
    {
        return teamDao.findTeamsCount(map);
    }

    public Result findTeamsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = teamDao.findTeamsCount(map);
        List<Team> data = teamDao.findTeams(map);
        return new Result(data, count);
    }

    public Team findTeamById(Integer id)
    {
        return teamDao.findTeamById(id);
    }

    public Team findTeamByUser(User user)
    {
        int[] roles = NumberUtil.parseIntArray(user.getRoles());
        //组长
        if (NumberUtil.contains(roles, 6) || NumberUtil.contains(roles, 7))
        {
            return teamDao.findTeamByLeaderId(user.getId());
        }
        //组员
        else if (NumberUtil.contains(roles, 8) || NumberUtil.contains(roles, 9))
        {
            return teamDao.findTeamByUserId(user.getId());
        }
        //其他
        else
        {
            return null;
        }
    }

    public Team findTeamByLeaderId(Integer leaderId)
    {
        return teamDao.findTeamByLeaderId(leaderId);
    }

    public Team findTeamByUserId(Integer userId)
    {
        return teamDao.findTeamByUserId(userId);
    }

    public List<User> findMembersByTeamId(Integer teamId)
    {
        return teamDao.findMembersByTeamId(teamId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveTeam(Team team)
    {
        if (team.getId() != null && team.getId() != 0)
        {
            Team t = teamDao.findTeamByLeaderId(team.getLeaderId());
            if (t != null && !t.getId().equals(team.getId()))
            {
                throw new IoTException(team.getLeaderName() + "已经是" + t.getName() + "组长");
            }
            teamDao.updateTeam(team);
        } else
        {
            Team t = teamDao.findTeamByLeaderId(team.getLeaderId());
            if (t != null)
            {
                throw new IoTException(team.getLeaderName() + "已经是" + t.getName() + "组长");
            }
            teamDao.insertTeam(team);
        }
    }

    public void deleteTeamById(Integer id)
    {
        teamDao.deleteTeamById(id);
    }

    public User findTeamLeader(Integer teamId)
    {
        Team team = teamDao.findTeamById(teamId);
        if (team == null)
        {
            return null;
        }
        Integer leaderId = team.getLeaderId();
        return userDao.findUserById(leaderId);
    }

}
