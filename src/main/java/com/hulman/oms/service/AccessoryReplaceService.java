package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.AccessoryDao;
import com.hulman.oms.dao.AccessoryReplaceDao;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class AccessoryReplaceService
{
    private static final String PROCESS_KEY = "accessory-replace";

    @Autowired
    private AccessoryReplaceDao accessoryReplaceDao;
    @Autowired
    private AccessoryDao accessoryDao;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private UserService userService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProcInstService procInstService;

    public List<AccessoryReplace> findAccessoryReplaces(Map<String, Object> map)
    {
        return accessoryReplaceDao.findAccessoryReplaces(map);
    }

    public Integer findAccessoryReplacesCount(Map<String, Object> map)
    {
        return accessoryReplaceDao.findAccessoryReplacesCount(map);
    }

    public Result findAccessoryReplacesResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = accessoryReplaceDao.findAccessoryReplacesCount(map);
        List<AccessoryReplace> data = accessoryReplaceDao.findAccessoryReplaces(map);
        return new Result(data, count);
    }

    public AccessoryReplace findAccessoryReplaceById(Integer id)
    {
        return accessoryReplaceDao.findAccessoryReplaceById(id);
    }

    public void deleteAccessoryReplaceById(Integer id)
    {
        accessoryReplaceDao.deleteAccessoryReplaceById(id);
    }

    public void deleteAccessoryReplaces(int[] ids)
    {
        accessoryReplaceDao.deleteAccessoryReplaces(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    public Task create(Integer deviceId, Integer accessoryId, String brand1, String model1, String spec1, String productNo1, Date productDate1, Integer quantity1, String note1, Integer replaceById, String replaceByName)
    {
        User user = SubjectUtil.getUser();
        if (user == null)
        {
            throw new IoTException("用户未登陆");
        }
        //创建工单
        ProcInst procInst = procInstService.startProcess(PROCESS_KEY);
        Device device = deviceService.findDeviceById(deviceId);
        Accessory accessory = accessoryDao.findAccessoryById(accessoryId);
        AccessoryReplace accessoryReplace = new AccessoryReplace();
        accessoryReplace.setProcInstId(procInst.getId());
        accessoryReplace.setNo(OrderUtil.generate("FJGH"));
        accessoryReplace.setDeviceId(deviceId);
        accessoryReplace.setDeviceName(device.getName());
        accessoryReplace.setAccessoryId(accessoryId);
        accessoryReplace.setAccessoryName(accessory.getName());
        accessoryReplace.setBrand1(brand1);
        accessoryReplace.setModel1(model1);
        accessoryReplace.setSpec1(spec1);
        accessoryReplace.setProductNo1(productNo1);
        accessoryReplace.setProductDate1(productDate1);
        accessoryReplace.setQuantity1(quantity1);
        accessoryReplace.setNote1(note1);
        accessoryReplace.setCreateById(user.getId());
        accessoryReplace.setCreateByName(user.getName());
        accessoryReplace.setCreateTime(new Date());
        User replaceBy = userService.findUserByIdOrName(replaceById, replaceByName);
        accessoryReplace.setReplaceById(replaceById);
        accessoryReplace.setReplaceByName(replaceBy.getName());
        accessoryReplace.setState(DeviceReplace.STATE_REPLACING);
        accessoryReplaceDao.insertAccessoryReplace(accessoryReplace);

        //创建任务
        Task createTask = new Task();
        createTask.setProcInstId(procInst.getId());
        createTask.setOwnerId(user.getId());
        createTask.setOwnerName(user.getName());
        createTask.setName(procInst.getStateName());
        createTask.setTag(PROCESS_KEY);
        createTask.setFormKey(accessoryReplace.getId().toString());
        createTask.addExtra("工单号", accessoryReplace.getNo());
        createTask.addExtra("更换附件", accessoryReplace.getAccessoryName());
        taskService.createAndCompleteTask(createTask);

        //创建更换任务
        procInst = procInstService.next(procInst.getId());
        Task replaceTask = new Task();
        replaceTask.setProcInstId(procInst.getId());
        replaceTask.setOwnerId(replaceBy.getId());
        replaceTask.setOwnerName(replaceBy.getName());
        replaceTask.setName(procInst.getStateName());
        replaceTask.setTag(PROCESS_KEY);
        replaceTask.setFormKey(accessoryReplace.getId().toString());
        replaceTask.addExtra("工单号", accessoryReplace.getNo());
        replaceTask.addExtra("更换附件", accessoryReplace.getAccessoryName());
        taskService.addTask(replaceTask);
        replaceTask.setPath("/pages/accessory-replace/detail?taskId=" + replaceTask.getId());
        replaceTask.setPage("/accessory-replace-info1.html?taskId=" + replaceTask.getId());
        taskService.saveTask(replaceTask);
        return replaceTask;
    }

    @Transactional(rollbackFor = Exception.class)
    public void complete(Integer taskId, Integer id, String brand2, String model2, String spec2, String productNo2, Date productDate2, Integer quantity2, String note2)
    {
        AccessoryReplace accessoryReplace = accessoryReplaceDao.findAccessoryReplaceById(id);
        accessoryReplace.setBrand2(brand2);
        accessoryReplace.setModel2(model2);
        accessoryReplace.setSpec2(spec2);
        accessoryReplace.setProductNo2(productNo2);
        accessoryReplace.setProductDate2(productDate2);
        accessoryReplace.setQuantity2(quantity2);
        accessoryReplace.setNote2(note2);
        accessoryReplace.setReplaceTime(new Date());
        accessoryReplace.setState(DeviceReplace.STATE_REPLACED);
        accessoryReplaceDao.updateAccessoryReplace(accessoryReplace);
        //完成本次任务
        taskService.createAndCompleteTask(taskId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void terminate(Integer id)
    {
        //修改工单
        AccessoryReplace accessoryReplace = accessoryReplaceDao.findAccessoryReplaceById(id);
        accessoryReplace.setState(DeviceReplace.STATE_TERMINATED);
        accessoryReplaceDao.updateAccessoryReplace(accessoryReplace);
        //终止流程
        procInstService.terminate(accessoryReplace.getProcInstId());
    }
}
