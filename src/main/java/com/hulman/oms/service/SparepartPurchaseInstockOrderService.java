package com.hulman.oms.service;

import com.hulman.oms.bean.*;
import com.hulman.oms.dao.*;
import com.hulman.oms.util.OrderUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Service
public class SparepartPurchaseInstockOrderService
{
    private static final String ORDER_PREFIX = "BJCGRK";
    @Autowired
    private SparepartPurchaseInstockOrderDao sparepartPurchaseInstockOrderDao;
    @Autowired
    private SparepartPurchaseInstockItemDao sparepartPurchaseInstockItemDao;
    @Autowired
    private SparepartInoutStockDao sparepartInoutStockDao;
    @Autowired
    private SparepartDao sparepartDao;

    public List<SparepartPurchaseInstockOrder> findSparepartPurchaseInstockOrders(Map<String, Object> map)
    {
        return sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrders(map);
    }

    public Integer findSparepartPurchaseInstockOrdersCount(Map<String, Object> map)
    {
        return sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrdersCount(map);
    }

    public Result findSparepartPurchaseInstockOrdersResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrdersCount(map);
        List<SparepartPurchaseInstockOrder> data = sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrders(map);
        return new Result(data, count);
    }

    public SparepartPurchaseInstockOrder findSparepartPurchaseInstockOrderById(Integer id)
    {
        SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder = sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrderById(id);
        List<SparepartPurchaseInstockItem> items = sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItemsByOrderId(id);
        sparepartPurchaseInstockOrder.setItems(items);
        return sparepartPurchaseInstockOrder;
    }

    public SparepartPurchaseInstockOrder findSparepartPurchaseInstockOrderByNo(String no)
    {
        SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder = sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrderByNo(no);
        List<SparepartPurchaseInstockItem> items = sparepartPurchaseInstockItemDao.findSparepartPurchaseInstockItemsByOrderId(sparepartPurchaseInstockOrder.getId());
        sparepartPurchaseInstockOrder.setItems(items);
        return sparepartPurchaseInstockOrder;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveSparepartPurchaseInstockOrder(SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder)
    {
        if (sparepartPurchaseInstockOrder.getId() != null && sparepartPurchaseInstockOrder.getId() != 0)
        {
            sparepartPurchaseInstockOrderDao.updateSparepartPurchaseInstockOrder(sparepartPurchaseInstockOrder);
        } else
        {
            sparepartPurchaseInstockOrder.setNo(OrderUtil.generate(ORDER_PREFIX));
            User user = SubjectUtil.getUser();
            sparepartPurchaseInstockOrder.setCreateBy(user.getName());
            sparepartPurchaseInstockOrder.setCreateTime(new Date());
            sparepartPurchaseInstockOrderDao.insertSparepartPurchaseInstockOrder(sparepartPurchaseInstockOrder);
            for (SparepartPurchaseInstockItem item : sparepartPurchaseInstockOrder.getItems())
            {
                item.setSparepartPurchaseOrderId(sparepartPurchaseInstockOrder.getId());
                //插入订单明细
                sparepartPurchaseInstockItemDao.insertSparepartPurchaseInstockItem(item);
                //出入库记录
                SparepartInoutStock sparepartInoutStock = item.toSparepartInoutStock();
                sparepartInoutStock.setOrderNo(sparepartPurchaseInstockOrder.getNo());
                sparepartInoutStockDao.insertSparepartInoutStock(sparepartInoutStock);
                //更新库存数量
                sparepartDao.addQuantity(item.getSparepartId(), item.getQuantity());
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteSparepartPurchaseInstockOrderById(Integer id)
    {
        SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder = sparepartPurchaseInstockOrderDao.findSparepartPurchaseInstockOrderById(id);
        //删除单据
        sparepartPurchaseInstockOrderDao.deleteSparepartPurchaseInstockOrderById(id);
        //删除单据明细
        sparepartPurchaseInstockItemDao.deleteSparepartPurchaseInstockItemByOrderId(id);
        //删除出入库记录
        String orderNo = sparepartPurchaseInstockOrder.getNo();
        sparepartInoutStockDao.deleteSparepartInoutStockByOrderNo(orderNo);
    }

    public void deleteSparepartPurchaseInstockOrders(int[] ids)
    {
        sparepartPurchaseInstockOrderDao.deleteSparepartPurchaseInstockOrders(ids);
    }

}
