package com.hulman.oms.service;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartReturnItem;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hulman.oms.bean.SparepartPurchaseItem;
import com.hulman.oms.dao.SparepartPurchaseItemDao;

import java.util.List;
import java.util.Map;

@Service
public class SparepartPurchaseItemService{

    @Autowired
    private SparepartPurchaseItemDao sparepartPurchaseItemDao;

    
    public int deleteByPrimaryKey(Integer id) {
        return sparepartPurchaseItemDao.deleteByPrimaryKey(id);
    }

    
    public int insert(SparepartPurchaseItem record) {
        return sparepartPurchaseItemDao.insert(record);
    }

    
    public int insertSelective(SparepartPurchaseItem record) {
        return sparepartPurchaseItemDao.insertSelective(record);
    }

    
    public SparepartPurchaseItem selectByPrimaryKey(Integer id) {
        return sparepartPurchaseItemDao.selectByPrimaryKey(id);
    }

    
    public int updateByPrimaryKeySelective(SparepartPurchaseItem record) {
        return sparepartPurchaseItemDao.updateByPrimaryKeySelective(record);
    }

    
    public int updateByPrimaryKey(SparepartPurchaseItem record) {
        return sparepartPurchaseItemDao.updateByPrimaryKey(record);
    }

    public Result findSparepartPurchaseItemsResult(Map<String, Object> map)
    {
        Integer page = (Integer) map.get("page");
        Integer limit = (Integer) map.get("limit");
        if (page != null && limit != null)
        {
            map.put("start", (page - 1) * limit);
            map.put("length", limit);
        }
        int count = sparepartPurchaseItemDao.findSparepartPurchaseItemsCount(map);
        List<SparepartPurchaseItem> data = sparepartPurchaseItemDao.findSparepartPurchaseItems(map);
        return new Result(data, count);
    }

    public void saveSparepartPurchaseItem(SparepartPurchaseItem sparepartPurchaseItem)
    {
        if (sparepartPurchaseItem.getId() != null && sparepartPurchaseItem.getId() != 0)
        {
            sparepartPurchaseItemDao.updateByPrimaryKey(sparepartPurchaseItem);
        } else
        {
            sparepartPurchaseItemDao.insert(sparepartPurchaseItem);
        }
    }

    public List<SparepartPurchaseItem> selectByOrderId(Integer orderId) {
        return sparepartPurchaseItemDao.findSparepartPurchaseByOrderId(orderId);
    }

}
