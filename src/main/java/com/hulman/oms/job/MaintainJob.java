package com.hulman.oms.job;

import com.hulman.oms.bean.Maintain;
import com.hulman.oms.bean.MaintainPlan;
import com.hulman.oms.bean.Msg;
import com.hulman.oms.bean.User;
import com.hulman.oms.service.MaintainPlanService;
import com.hulman.oms.service.MaintainService;
import com.hulman.oms.service.MsgService;
import com.hulman.oms.service.UserService;
import com.hulman.oms.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class MaintainJob
{
    private static final String PROCESS_KEY = "maintain";

    @Autowired
    private MaintainService maintainService;
    @Autowired
    private MaintainPlanService maintainPlanService;
    @Autowired
    private MsgService msgService;
    @Autowired
    private UserService userService;

    /**
     * 计划完成日期到期提醒
     * 说明：检查状态为执行中（3）的保养工单，计划完成时间超期提醒，每天上午8点30分提醒
     */
    @Scheduled(cron = "0 30 8 * * ? ")
    public void planDateNotify()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("state", Maintain.STATE_EXECUTING);
        List<Maintain> maintainList = maintainService.findMaintains(map);
        for (Maintain maintain : maintainList)
        {
            Date planEndDate = maintain.getPlanEndDate();
            if (planEndDate != null)
            {
                Date today = new Date();
                try
                {
                    int diffDays = DateUtil.daysBetween(today, planEndDate);
                    //已到计划完成日期通知负责人
                    if (diffDays == 0)
                    {
                        Msg msg = new Msg();
                        msg.setType(Msg.TYPE_OM);
                        msg.setOwnerId(maintain.getDirectorId());
                        msg.setOwnerName(maintain.getDirectorName());
                        msg.setTitle("计划结束时间到期提醒");
                        msg.addItem("专项保养工单", maintain.getNo());
                        msgService.sendMsg(msg);
                    }
                    //超过计划完成日期通知负责人和分配人（常务经理）
                    if (diffDays < 0)
                    {
                        Msg msg = new Msg();
                        msg.setType(Msg.TYPE_OM);
                        msg.setTitle("计划结束时间过期提醒");
                        msg.addItem("专项保养工单", maintain.getNo());
                        msg.setOwnerId(maintain.getDirectorId());
                        msg.setOwnerName(maintain.getDirectorName());
                        msgService.sendMsg(msg);
                        msg.addItem("备注", "计划结束日期已过，请督促完成");
                        msg.setOwnerId(maintain.getCreateById());
                        msg.setOwnerName(maintain.getCreateByName());
                        msgService.sendMsg(msg);
                    }
                } catch (ParseException e)
                {
                    log.error("plan end date notify error:", e);
                }
            }
        }
    }

    /**
     * 根据专项保养计划的下次保养日期提醒
     * 说明：8点30分生成
     */
    @Scheduled(cron = "0 30 8 * * ? ")
    public void generateMaintain()
    {
        User standingManager = userService.findStandingManager();
        if (standingManager == null)
        {
            return;
        }
        //提前一周通知维保
        List<MaintainPlan> weekPlans = maintainPlanService.findLatestMaintainPlans();
        for (MaintainPlan maintainPlan : weekPlans)
        {
            Msg msg = new Msg();
            msg.setType(Msg.TYPE_OM);
            msg.setTitle("专项保养计划提醒");
            msg.addItem("提醒内容", "还有一周就要执行保养计划");
            msg.addItem("保养名称", maintainPlan.getName());
            msg.setOwnerId(standingManager.getId());
            msg.setOwnerName(standingManager.getName());
            msgService.sendMsg(msg);
        }
        //当天要维保但还没执行的，再通知
        List<MaintainPlan> todayPlans = maintainPlanService.findTodayMaintainPlans();
        for (MaintainPlan maintainPlan : todayPlans)
        {
            Msg msg = new Msg();
            msg.setType(Msg.TYPE_OM);
            msg.setTitle("专项保养计划提醒");
            msg.addItem("提醒内容", "今天有保养计划要执行");
            msg.addItem("保养名称", maintainPlan.getName());
            msg.setOwnerId(standingManager.getId());
            msg.setOwnerName(standingManager.getName());
            msgService.sendMsg(msg);
        }
    }

}
