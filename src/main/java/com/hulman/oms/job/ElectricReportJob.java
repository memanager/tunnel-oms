package com.hulman.oms.job;

import com.hulman.oms.bean.ElectricReport;
import com.hulman.oms.service.ElectricReportService;
import com.hulman.oms.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Date;

@Component
@Slf4j
public class ElectricReportJob
{
    @Autowired
    private ElectricReportService electricReportService;

    /**
     * 每周一早晨6点生成本周的用电周报
     */
    @Scheduled(cron = "0 0 6 ? * MON")
    public void generateElectricReport()
    {
        ElectricReport electricReport = new ElectricReport();
        log.info("生成用电周报");
        LocalDate now =  LocalDate.now();
        int year = now.getYear();
        int week = now.get(WeekFields.ISO.weekOfWeekBasedYear());
        electricReport.setYear(year);
        electricReport.setWeek(week);
        electricReport.setStartDate(new Date());
        electricReport.setCreateTime(new Date());
        electricReport.setEndDate(DateUtil.addDays(new Date(),6));
        electricReportService.createReport(electricReport);
    }

}
