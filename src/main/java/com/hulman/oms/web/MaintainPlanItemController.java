package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.MaintainPlanItem;
import com.hulman.oms.service.MaintainPlanItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class MaintainPlanItemController
{
	@Autowired
	private MaintainPlanItemService maintainPlanItemService;

	@GetMapping("/maintain-plan-items/{id}")
	public Result findMaintainPlanItemById(@PathVariable Integer id)
	{
		MaintainPlanItem maintainPlanItem = maintainPlanItemService.findMaintainPlanItemById(id);
		return new Result(maintainPlanItem);
	}

	@GetMapping("/maintain-plan-items")
	public Result findMaintainPlanItems(Integer page, Integer limit, Integer maintainPlanId)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("maintainPlanId", maintainPlanId);
		return maintainPlanItemService.findMaintainPlanItemsResult(map);
	}

	@DeleteMapping("/maintain-plan-items/{id}")
	public Result deleteMaintainPlanItemById(@PathVariable Integer id)
	{
		maintainPlanItemService.deleteMaintainPlanItemById(id);
		return Result.SUCCESS;
	}

	@PutMapping("/maintain-plan-items")
	public Result saveMaintainPlanItem(MaintainPlanItem maintainPlanItem)
	{
		maintainPlanItemService.saveMaintainPlanItem(maintainPlanItem);
		return Result.SUCCESS;
	}

}
