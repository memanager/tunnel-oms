package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.VehicleUse;
import com.hulman.oms.service.VehicleUseService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class VehicleUseController
{
    @Autowired
    private VehicleUseService vehicleUseService;

    @GetMapping("/vehicle-uses/{id}")
    public Result findVehicleUseById(@PathVariable Integer id)
    {
        VehicleUse vehicleUse = vehicleUseService.findVehicleUseById(id);
        return new Result(vehicleUse);
    }

    @GetMapping("/vehicle-uses/in-use-count")
    public Result findVehicleInUseCount()
    {
        Map<String, Object> map = new HashMap<>();
        Integer driverId = SubjectUtil.getUser().getId();
        map.put("state", 1);
        map.put("driverId", driverId);
        Integer count = vehicleUseService.findVehicleUsesCount(map);
        return new Result(count);
    }

    @GetMapping("/vehicle-uses")
    public Result findVehicleUses(Integer page, Integer limit, String vehicleName, String startDate, String endDate, String startDateRange, Integer driverId, String driverName, Integer state, Integer vehicleId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("vehicleName", vehicleName);
        if (!StringUtil.isEmpty(startDate))
        {
            map.put("startDate", startDate);
        }
        if (!StringUtil.isEmpty(endDate))
        {
            map.put("endDate", endDate);
        }
        if (!StringUtil.isEmpty(startDateRange))
        {
            String[] dates = startDateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        map.put("driverId", driverId);
        map.put("driverName", driverName);
        map.put("state", state);
        map.put("vehicleId", vehicleId);
        return vehicleUseService.findVehicleUsesResult(map);
    }

    @DeleteMapping("/vehicle-uses/{ids}")
    public Result deleteVehicleUseById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            vehicleUseService.deleteVehicleUseById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/vehicle-uses")
    public Result saveVehicleUse(VehicleUse vehicleUse)
    {
        vehicleUseService.saveVehicleUse(vehicleUse);
        return Result.SUCCESS;
    }

    /**
     * 开始出车
     *
     * @return
     */
    @PostMapping("/vehicle-uses/start")
    public Result startVehicleUse(Integer vehicleId, Double startMile)
    {
        vehicleUseService.start(vehicleId, startMile);
        return Result.SUCCESS;
    }

    /**
     * 结束用车
     *
     * @return
     */
    @PostMapping("/vehicle-uses/end")
    public Result endVehicleUse(Integer vehicleUseId, Double endMile)
    {
        vehicleUseService.end(vehicleUseId, endMile);
        return Result.SUCCESS;
    }

    /**
     * 审核用车
     *
     * @return
     */
    @PostMapping("/vehicle-uses/audit")
    public Result auditVehicleUse(Integer vehicleUseId, Integer auditResult, String auditComment)
    {
        vehicleUseService.audit(vehicleUseId, auditResult, auditComment);
        return Result.SUCCESS;
    }

}
