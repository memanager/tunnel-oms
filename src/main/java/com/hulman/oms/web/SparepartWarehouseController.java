package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Sparepart;
import com.hulman.oms.bean.SparepartWarehouse;
import com.hulman.oms.service.SparepartWarehouseService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 备件仓库(sparepart_warehouse)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartWarehouseController {
/**
* 服务对象
*/
    @Autowired
    private SparepartWarehouseService sparepartWarehouseService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/sparepartWarehouses/{id}")
    public Result findSpareparttWarehouseById(@PathVariable Integer id)
    {
        SparepartWarehouse sparepartWarehouse = sparepartWarehouseService.selectByPrimaryKey(id);
        return new Result(sparepartWarehouse);
    }

    @GetMapping("/sparepartWarehouses")
    public Result findSparepartWarehouses(Integer page, Integer limit, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        return sparepartWarehouseService.findSparepartWarehousesResult(map);
    }

    @DeleteMapping("/sparepartWarehouses/{ids}")
    public Result deleteSparepartWarehouseById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sparepartWarehouseService.deleteByPrimaryKey(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/sparepartWarehouses")
    public Result saveSparepartWarehouse(SparepartWarehouse SparepartWarehouse)
    {
        sparepartWarehouseService.save(SparepartWarehouse);
        return Result.SUCCESS;
    }

}
