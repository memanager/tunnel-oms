package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartUseOrder;
import com.hulman.oms.bean.ToolPurchaseInstockOrder;
import com.hulman.oms.service.ToolPurchaseInstockOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具采购入库单(tool_purchase_instock_order)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolPurchaseInstockOrderController {
/**
* 服务对象
*/
    @Autowired
    private ToolPurchaseInstockOrderService toolPurchaseInstockOrderService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/tool-purchase-instock-orders/{id}")
    public Result findToolPurchaseInstockOrderById(@PathVariable Integer id)
    {
     ToolPurchaseInstockOrder toolPurchaseInstockOrder = toolPurchaseInstockOrderService.selectByPrimaryKey(id);
    return new Result(toolPurchaseInstockOrder);
    }

    @GetMapping("/tool-purchase-instock-orders/no/{no}")
    public Result findToolPurchaseInstockOrderByNo(@PathVariable String no)
    {
        ToolPurchaseInstockOrder toolPurchaseInstockOrder = toolPurchaseInstockOrderService.selectByNo(no);
        return new Result(toolPurchaseInstockOrder);
    }

    @GetMapping("/tool-purchase-instock-orders")
    public Result findToolPurchaseInstockOrders(Integer page, Integer limit, String no, String purchaseNo, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        map.put("purchaseNo", purchaseNo);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return toolPurchaseInstockOrderService.findToolPurchaseInstockOrdersResult(map);
    }

    @PutMapping("/tool-purchase-instock-orders")
    public Result saveToolPurchaseInstockOrders(@RequestBody ToolPurchaseInstockOrder toolPurchaseInstockOrder)
    {
        toolPurchaseInstockOrderService.saveToolPurchaseInstockOrder(toolPurchaseInstockOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/tool-purchase-instock-orders/{ids}")
    public Result deleteToolPurchaseInstockOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            toolPurchaseInstockOrderService.deleteToolPurchaseInstockOrderById(id);
        }
        return Result.SUCCESS;
    }



}
