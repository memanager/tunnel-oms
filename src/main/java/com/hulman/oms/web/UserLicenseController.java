package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.UserLicense;
import com.hulman.oms.service.UserLicenseService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * 人员证件(user_license)表控制层
 *
 * @author xxxxx
 */
@RestController
public class UserLicenseController
{
    /**
     * 服务对象
     */
    @Autowired
    private UserLicenseService userLicenseService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/user-licenses/{id}")
    public Result findUserLicenseById(@PathVariable Integer id)
    {
        UserLicense userLicense = userLicenseService.selectByPrimaryKey(id);
        return new Result(userLicense);
    }

    @GetMapping("/user-licenses")
    public Result findUserLicenses(Integer page, Integer limit, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        return userLicenseService.findUserLicensesResult(map);
    }

    @DeleteMapping("/user-licenses/{ids}")
    public Result deleteUserLicenseById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            userLicenseService.deleteByPrimaryKey(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/user-licenses")
    public Result saveUserLicense(UserLicense UserLicense)
    {
        userLicenseService.insert(UserLicense);
        return Result.SUCCESS;
    }

}
