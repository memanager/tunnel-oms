package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.VehicleRefuel;
import com.hulman.oms.bean.VehicleRefuel;
import com.hulman.oms.service.VehicleRefuelService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * 车辆加油(vehicle_refuel)表控制层
 *
 * @author xxxxx
 */
@RestController
public class VehicleRefuelController
{
    /**
     * 服务对象
     */
    @Autowired
    private VehicleRefuelService vehicleRefuelService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/vehicle-refuels/{id}")
    public Result findVehicleUseById(@PathVariable Integer id)
    {
        VehicleRefuel vehicleRefuel = vehicleRefuelService.selectByPrimaryKey(id);
        return new Result(vehicleRefuel);
    }

    /**
     * 车辆加油
     *
     * @return
     */
    @PostMapping("/vehicle-refuels/start")
    public Result startVehicleRefuel(Integer vehicleId, Double mileage, String fuelCard, String fuelType, Double expense, Double lastBalance, Double amount)
    {
        vehicleRefuelService.start(vehicleId, mileage, fuelCard, fuelType, expense, lastBalance, amount);
        return Result.SUCCESS;
    }

    /**
     * 审核加油
     *
     * @return
     */
    @PostMapping("/vehicle-refuels/audit")
    public Result auditVehicleRefuel(Integer vehicleRefuelId, Integer auditResult, String auditComment)
    {
        vehicleRefuelService.audit(vehicleRefuelId, auditResult, auditComment);
        return Result.SUCCESS;
    }

    @DeleteMapping("/vehicle-refuels/{ids}")
    public Result deleteVehicleRefuelById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            vehicleRefuelService.deleteVehicleRefuelById(id);
        }
        return Result.SUCCESS;
    }

    @GetMapping("/vehicle-refuels/in-repair-count")
    public Result findVehicleInUseCount()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("state", 1);
        Integer count = vehicleRefuelService.findVehicleRefuelsCount(map);
        return new Result(count);
    }

    @GetMapping("/vehicle-refuels")
    public Result findVehicleRefuels(Integer page, Integer limit, String vehicleName, String startDate, String endDate, String startDateRange, Integer driverId, String driverName, Integer state, Integer vehicleId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("vehicleName", vehicleName);
        if (!StringUtil.isEmpty(startDate))
        {
            map.put("startDate", startDate);
        }
        if (!StringUtil.isEmpty(endDate))
        {
            map.put("endDate", endDate);
        }
        if (!StringUtil.isEmpty(startDateRange))
        {
            String[] dates = startDateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        map.put("driverId", driverId);
        map.put("driverName", driverName);
        map.put("state", state);
        map.put("vehicleId", vehicleId);
        return vehicleRefuelService.findVehicleRefuelsResult(map);
    }
}


