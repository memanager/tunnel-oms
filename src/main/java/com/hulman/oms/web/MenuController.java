package com.hulman.oms.web;

import com.hulman.oms.bean.Menu;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.MenuService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: maxwellens
 */
@RestController
public class MenuController
{
    @Autowired
    private MenuService menuService;

    /**
     * 返回所有菜单项
     *
     * @return
     */
    @GetMapping("/menus")
    public Result findMenus()
    {
        return menuService.findMenusResult();
    }

    /**
     * 根据ID查找菜单项
     *
     * @param id
     * @return
     */
    @GetMapping("/menus/{id}")
    public Result findMenuById(@PathVariable Integer id)
    {
        return new Result(menuService.findMenuById(id));
    }

    /**
     * 删除菜单项
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/menus/{ids}")
    public Result deleteMenuById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            menuService.deleteMenuById(id);
        }
        return Result.SUCCESS;
    }

    /**
     * 更新或添加菜单项
     *
     * @param menu
     * @return
     */
    @PutMapping("/menus")
    public Result saveMenu(Menu menu)
    {
        menuService.saveMenu(menu);
        return Result.SUCCESS;
    }

}
