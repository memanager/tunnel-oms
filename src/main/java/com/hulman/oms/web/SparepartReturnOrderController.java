package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartReturnOrder;
import com.hulman.oms.service.SparepartReturnOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 备件归还单(sparepart_return_order)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartReturnOrderController
{
/**
* 服务对象
*/
    @Autowired
    private SparepartReturnOrderService sparepartReturnOrderService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/sparepart-return-orders/{id}")
    public Result findSparepartReturnOrderById(@PathVariable Integer id)
    {
        SparepartReturnOrder sparepartReturnOrder = sparepartReturnOrderService.selectByPrimaryKey(id);
        return new Result(sparepartReturnOrder);
    }

    @GetMapping("/sparepart-return-orders/no/{no}")
    public Result findSparepartReturnOrderByNo(@PathVariable String no)
    {
        SparepartReturnOrder sparepartReturnOrder = sparepartReturnOrderService.selectByNo(no);
        return new Result(sparepartReturnOrder);
    }

    @GetMapping("/sparepart-return-orders")
    public Result findSparepartReturnOrders(Integer page, Integer limit, String no, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return sparepartReturnOrderService.findSparepartReturnOrdersResult(map);
    }

    @PutMapping("/sparepart-return-orders")
    public Result saveSparepartReturnOrders(@RequestBody SparepartReturnOrder sparepartReturnOrder)
    {
        sparepartReturnOrderService.saveSparepartReturnOrder(sparepartReturnOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/sparepart-return-orders/{ids}")
    public Result deleteSparepartReturnOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sparepartReturnOrderService.deleteSparepartReturnOrderById(id);
        }
        return Result.SUCCESS;
    }

}
