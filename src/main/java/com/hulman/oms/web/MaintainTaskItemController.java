package com.hulman.oms.web;

import com.hulman.oms.bean.MaintainTask;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.MaintainTaskItem;
import com.hulman.oms.service.MaintainTaskItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class MaintainTaskItemController
{
    @Autowired
    private MaintainTaskItemService maintainTaskItemService;

    @GetMapping("/maintain-task-items/{id}")
    public Result findMaintainTaskItemById(@PathVariable Integer id)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemService.findMaintainTaskItemById(id);
        return new Result(maintainTaskItem);
    }

    @GetMapping("/maintain-task-items")
    public Result findMaintainTaskItems(Integer maintainTaskId, Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("maintainTaskId", maintainTaskId);
        map.put("page", page);
        map.put("limit", limit);
        return maintainTaskItemService.findMaintainTaskItemsResult(map);
    }

    @GetMapping("/maintain-task-items/device-code")
    public Result findMaintainTaskItemByDeviceCode(Integer maintainTaskId, String deviceCode)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemService.findMaintainTaskItemByDeviceCode(maintainTaskId, deviceCode);
        return new Result(maintainTaskItem);
    }

    @GetMapping("/maintain-task-items/area")
    public Result findMaintainTaskItemByArea(Integer maintainTaskId, String area)
    {
        MaintainTaskItem maintainTaskItem = maintainTaskItemService.findMaintainTaskItemByArea(maintainTaskId, area);
        return new Result(maintainTaskItem);
    }

    @DeleteMapping("/maintain-task-items/{id}")
    public Result deleteMaintainTaskItemById(@PathVariable Integer id)
    {
        maintainTaskItemService.deleteMaintainTaskItemById(id);
        return Result.SUCCESS;
    }

    @PutMapping("/maintain-task-items")
    public Result saveMaintainTaskItem(MaintainTaskItem maintainTaskItem)
    {
        maintainTaskItemService.saveMaintainTaskItem(maintainTaskItem);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-task-items/start")
    public Result start(Integer maintainTaskItemId)
    {
        maintainTaskItemService.start(maintainTaskItemId);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-task-items/save")
    public Result save(Integer maintainTaskItemId, String content, String imgs, String completion)
    {
        maintainTaskItemService.save(maintainTaskItemId, content, imgs, completion);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-task-items/complete")
    public Result complete(Integer maintainTaskItemId, String content, Integer result, String imgs, String completion)
    {
        maintainTaskItemService.complete(maintainTaskItemId, content, result, imgs, completion);
        return Result.SUCCESS;

    }

    @PostMapping("/maintain-task-items/audit")
    public Result audit(Integer maintainTaskItemId, Integer auditResult, String auditComment)
    {
        maintainTaskItemService.audit(maintainTaskItemId, auditResult, auditComment);
        return Result.SUCCESS;
    }

}
