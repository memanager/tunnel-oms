package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.DeviceReplace;
import com.hulman.oms.service.DeviceReplaceService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class DeviceReplaceController
{
    @Autowired
    private DeviceReplaceService deviceReplaceService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder)
    {
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @GetMapping("/device-replaces/{id}")
    public Result findDeviceReplaceById(@PathVariable Integer id)
    {
        DeviceReplace deviceReplace = deviceReplaceService.findDeviceReplaceById(id);
        return new Result(deviceReplace);
    }

    @GetMapping("/device-replaces")
    public Result findDeviceReplaces(Integer page, Integer limit, Integer deviceId, String deviceName, Integer replaceById, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        map.put("deviceName", deviceName);
        map.put("replaceById", replaceById);
        map.put("state", state);
        return deviceReplaceService.findDeviceReplacesResult(map);
    }

    @DeleteMapping("/device-replaces/{ids}")
    public Result deleteDeviceReplaceById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            deviceReplaceService.deleteDeviceReplaceById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/device-replaces")
    public Result saveDeviceReplace(DeviceReplace deviceReplace)
    {
        deviceReplaceService.saveDeviceReplace(deviceReplace);
        return Result.SUCCESS;
    }

    /**
     * 创建设备更换工单
     *
     * @param deviceId
     * @param brand1
     * @param model1
     * @param spec1
     * @param productNo1
     * @param productDate1
     * @param note1
     * @param replaceById
     * @return
     */
    @PostMapping("/device-replaces/create")
    public Result create(Integer deviceId, String brand1, String model1, String spec1, String productNo1, Date productDate1, String note1, Integer replaceById, String replaceByName)
    {
        deviceReplaceService.create(deviceId, brand1, model1, spec1, productNo1, productDate1, note1, replaceById, replaceByName);
        return Result.SUCCESS;
    }

    /**
     * 设备更换
     *
     * @param taskId
     * @param id
     * @param brand2
     * @param model2
     * @param spec2
     * @param productNo2
     * @param productDate2
     * @param note2
     * @return
     */
    @PostMapping("/device-replaces/complete")
    public Result complete(Integer taskId, Integer id, String brand2, String model2, String spec2, String productNo2, Date productDate2, String note2)
    {
        deviceReplaceService.complete(taskId, id, brand2, model2, spec2, productNo2, productDate2, note2);
        return Result.SUCCESS;
    }

    /**
     * 工单撤销
     *
     * @param id
     * @return
     */
    @PostMapping("/device-replaces/terminate")
    public Result terminate(Integer id)
    {
        deviceReplaceService.terminate(id);
        return Result.SUCCESS;
    }

}
