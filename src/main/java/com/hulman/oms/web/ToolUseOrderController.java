package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolUseOrder;
import com.hulman.oms.service.ToolUseOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具领用单(tool_use_order)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolUseOrderController {
/**
* 服务对象
*/
    @Autowired
    private ToolUseOrderService toolUseOrderService;

    @GetMapping("/tool-use-orders/{id}")
    public Result findToolUseOrderById(@PathVariable Integer id)
    {
        ToolUseOrder toolUseOrder = toolUseOrderService.selectByPrimaryKey(id);
        return new Result(toolUseOrder);
    }

    @GetMapping("/tool-use-orders/no/{no}")
    public Result findToolUseOrderByNo(@PathVariable String no)
    {
        ToolUseOrder toolUseOrder = toolUseOrderService.selectByNo(no);
        return new Result(toolUseOrder);
    }



    @GetMapping("/tool-use-orders")
    public Result findToolUseOrders(Integer page, Integer limit, String no, String useDateRange,String returnDateRange,Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        map.put("state", state);
        if (!StringUtil.isEmpty(useDateRange))
        {
            String[] dates = useDateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        if (!StringUtil.isEmpty(returnDateRange))
        {
            String[] dates = returnDateRange.split(" - ");
            map.put("returnStartDate", dates[0]);
            map.put("returnEndDate", dates[1]);
        }
        return toolUseOrderService.findToolUseOrdersResult(map);
    }

    @PutMapping("/tool-use-orders")
    public Result saveToolUseOrders(@RequestBody ToolUseOrder toolUseOrder)
    {
        toolUseOrderService.saveToolUseOrder(toolUseOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/tool-use-orders/{ids}")
    public Result deleteToolUseOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            toolUseOrderService.deleteToolUseOrderById(id);
        }
        return Result.SUCCESS;
    }

}
