package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartUseItem;
import com.hulman.oms.bean.SparepartUseItem;
import com.hulman.oms.service.SparepartUseItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 备件领用明细(sparepart_use_item)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartUseItemController {
/**
* 服务对象
*/
    @Autowired
    private SparepartUseItemService sparepartUseItemService;

    @GetMapping("/sparepart-use-items/{id}")
    public Result findSparepartUseItemById(@PathVariable Integer id)
    {
        SparepartUseItem sparepartUseItem = sparepartUseItemService.selectByPrimaryKey(id);
        return new Result(sparepartUseItem);
    }

    @GetMapping("/sparepart-use-items")
    public Result findSparepartUseItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return sparepartUseItemService.findSparepartUseItemsResult(map);
    }

    @GetMapping("/sparepart-use-items/orderId/{orderId}")
    public Result findSparepartUseOrderId(@PathVariable Integer orderId)
    {
        List<SparepartUseItem> sparepartUseItemList = sparepartUseItemService.selectByOrderId(orderId);
        return new Result(sparepartUseItemList);
    }

    @DeleteMapping("/sparepart-use-items/{id}")
    public Result deleteSparepartUseItemById(@PathVariable Integer id)
    {
        sparepartUseItemService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @PutMapping("/sparepart-use-items")
    public Result saveSparepartUseItem(SparepartUseItem SparepartUseItem)
    {
        sparepartUseItemService.saveSparepartUseItem(SparepartUseItem);
        return Result.SUCCESS;
    }

}
