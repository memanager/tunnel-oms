package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Role;
import com.hulman.oms.service.RoleService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class RoleController
{
    @Autowired
    private RoleService roleService;

    @GetMapping("/roles/{id}")
    public Result findRoleById(@PathVariable Integer id)
    {
        Role role = roleService.findRoleById(id);
        return new Result(role);
    }

    @GetMapping("/roles")
    public Result findRoles(Integer page, Integer limit, Integer level, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("level", level);
        map.put("name", name);
        return roleService.findRolesResult(map);
    }

    @DeleteMapping("/roles/{ids}")
    public Result deleteRoleById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            roleService.deleteRoleById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/roles")
    public Result saveRole(Role role)
    {
        roleService.saveRole(role);
        return Result.SUCCESS;
    }

    @GetMapping("/roles/{id}/menus")
    public Result findMenusByRoleId(@PathVariable Integer id)
    {
        return roleService.findMenuResultByRoleId(id);
    }

    @PutMapping("/roles/{id}/menus")
    public Result saveMenus(@PathVariable Integer id, String menus)
    {
        roleService.updateMenus(id, menus);
        return Result.SUCCESS;
    }

    @GetMapping("/roles/{id}/modules")
    public Result findModulesByRoleId(@PathVariable Integer id)
    {
        return roleService.findAppWorkResultByRoleId(id);
    }

    @PutMapping("/roles/{id}/modules")
    public Result saveModules(@PathVariable Integer id, String modules)
    {
        roleService.updateAppWorks(id, modules);
        return Result.SUCCESS;
    }

    @GetMapping("/roles/{id}/access-policies")
    public Result findAccessPolicies(@PathVariable Integer id)
    {
        return roleService.findAccessPoliciesResultByRoleId(id);
    }

    @PutMapping("/roles/{id}/access-policies")
    public Result saveAccessPolicies(@PathVariable Integer id, String accessPolicies)
    {
        roleService.updateAccessPolicies(id, accessPolicies);
        return Result.SUCCESS;
    }

}
