package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.service.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MapController
{
    @Autowired
    private MapService mapService;

    @GetMapping("/maps/geocoder")
    public Result geocoder(Double lat, Double lng)
    {
        Map data = mapService.geocoder(lat, lng);
        return new Result(data);
    }

}
