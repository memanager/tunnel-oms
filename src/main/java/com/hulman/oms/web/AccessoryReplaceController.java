package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.AccessoryReplace;
import com.hulman.oms.service.AccessoryReplaceService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class AccessoryReplaceController
{
    @Autowired
    private AccessoryReplaceService accessoryReplaceService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder)
    {
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @GetMapping("/accessory-replaces/{id}")
    public Result findAccessoryReplaceById(@PathVariable Integer id)
    {
        AccessoryReplace accessoryReplace = accessoryReplaceService.findAccessoryReplaceById(id);
        return new Result(accessoryReplace);
    }

    @GetMapping("/accessory-replaces")
    public Result findAccessoryReplaces(Integer page, Integer limit, Integer deviceId, String deviceName, Integer accessoryId, String accessoryName, Integer replaceById, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        map.put("deviceName", deviceName);
        map.put("accessoryId", accessoryId);
        map.put("accessoryName", accessoryName);
        map.put("replaceById", replaceById);
        map.put("state", state);
        return accessoryReplaceService.findAccessoryReplacesResult(map);
    }

    @DeleteMapping("/accessory-replaces/{ids}")
    public Result deleteAccessoryReplaceById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            accessoryReplaceService.deleteAccessoryReplaceById(id);
        }
        return Result.SUCCESS;
    }

    @PostMapping("/accessory-replaces/create")
    public Result create(Integer deviceId, Integer accessoryId, String brand1, String model1, String spec1, String productNo1, Date productDate1, Integer quantity1, String note1, Integer replaceById, String replaceByName)
    {
        accessoryReplaceService.create(deviceId, accessoryId, brand1, model1, spec1, productNo1, productDate1, quantity1, note1, replaceById, replaceByName);
        return Result.SUCCESS;
    }

    @PostMapping("/accessory-replaces/complete")
    public Result complete(Integer taskId, Integer id, String brand2, String model2, String spec2, String productNo2, Date productDate2, Integer quantity2, String note2)
    {
        accessoryReplaceService.complete(taskId, id, brand2, model2, spec2, productNo2, productDate2, quantity2, note2);
        return Result.SUCCESS;
    }

    @PostMapping("/accessory-replaces/terminate")
    public Result terminate(Integer id)
    {
        accessoryReplaceService.terminate(id);
        return Result.SUCCESS;
    }

}
