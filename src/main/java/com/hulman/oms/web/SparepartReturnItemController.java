package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartReturnItem;
import com.hulman.oms.service.SparepartReturnItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 备件归还明细(sparepart_return_item)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartReturnItemController
{
/**
* 服务对象
*/
    @Autowired
    private SparepartReturnItemService sparepartReturnItemService;
    
    @GetMapping("/sparepart-return-items/{id}")
    public Result findSparepartReturnItemById(@PathVariable Integer id)
    {
        SparepartReturnItem sparepartReturnItem = sparepartReturnItemService.selectByPrimaryKey(id);
        return new Result(sparepartReturnItem);
    }

    @GetMapping("/sparepart-return-items")
    public Result findSparepartReturnItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return sparepartReturnItemService.findSparepartReturnItemsResult(map);
    }

    @DeleteMapping("/sparepart-return-items/{id}")
    public Result deleteSparepartReturnItemById(@PathVariable Integer id)
    {
        sparepartReturnItemService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @PutMapping("/sparepart-return-items")
    public Result saveSparepartReturnItem(SparepartReturnItem sparepartReturnItem)
    {
        sparepartReturnItemService.saveSparepartReturnItem(sparepartReturnItem);
        return Result.SUCCESS;
    }

}
