package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartUseItem;
import com.hulman.oms.bean.SparepartUseOrder;
import com.hulman.oms.service.SparepartUseOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 备件领用单(sparepart_use_order)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartUseOrderController
{
/**
* 服务对象
*/
    @Autowired
    private SparepartUseOrderService sparepartUseOrderService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/sparepart-use-orders/{id}")
    public Result findSparepartUseOrderById(@PathVariable Integer id)
    {
        SparepartUseOrder sparepartUseOrder = sparepartUseOrderService.selectByPrimaryKey(id);
        return new Result(sparepartUseOrder);
    }

    @GetMapping("/sparepart-use-orders/no/{no}")
    public Result findSparepartUseOrderByNo(@PathVariable String no)
    {
        SparepartUseOrder sparepartUseOrder = sparepartUseOrderService.selectByNo(no);
        return new Result(sparepartUseOrder);
    }



    @GetMapping("/sparepart-use-orders")
    public Result findSparepartUseOrders(Integer page, Integer limit, String no, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return sparepartUseOrderService.findSparepartUseOrdersResult(map);
    }

    @PutMapping("/sparepart-use-orders")
    public Result saveSparepartUseOrders(@RequestBody SparepartUseOrder sparepartUseOrder)
    {
        sparepartUseOrderService.saveSparepartUseOrder(sparepartUseOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/sparepart-use-orders/{ids}")
    public Result deleteSparepartUseOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sparepartUseOrderService.deleteSparepartUseOrderById(id);
        }
        return Result.SUCCESS;
    }
}
