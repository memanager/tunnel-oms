package com.hulman.oms.web;

import com.hulman.oms.bean.AccessPolicy;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.AccessPolicyService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class AccessPolicyController
{
	@Autowired
	private AccessPolicyService accessPolicyService;

	@GetMapping("/access-policies/{id}")
	public Result findAccessPolicyById(@PathVariable int id)
	{
		AccessPolicy accessPolicy = accessPolicyService.findAccessPolicyById(id);
		return new Result(accessPolicy);
	}

	@GetMapping("/access-policies")
	public Result findAccessPolicies(Integer page, Integer limit, String name, String note)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("name", name);
        map.put("note", note);
		return accessPolicyService.findAccessPoliciesResult(map);
	}

	@DeleteMapping("/access-policies/{ids}")
	public Result deleteAccessPolicyById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			accessPolicyService.deleteAccessPolicyById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/access-policies")
	public Result saveAccessPolicy(AccessPolicy accessPolicy)
	{
		accessPolicyService.saveAccessPolicy(accessPolicy);
		return Result.SUCCESS;
	}

}
