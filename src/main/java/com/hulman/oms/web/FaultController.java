package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Fault;
import com.hulman.oms.service.FaultService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class FaultController
{
    @Autowired
    private FaultService faultService;

    @GetMapping("/faults/{id}")
    public Result findFaultById(@PathVariable Integer id)
    {
        Fault fault = faultService.findFaultById(id);
        return new Result(fault);
    }

    @GetMapping("/faults")
    public Result findFaults(Integer page, Integer limit, Integer deviceId, Integer source, String no, String content, String reportByName, Integer auditTimes, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        map.put("source", source);
        map.put("no", no);
        map.put("content", content);
        map.put("reportByName", reportByName);
        map.put("auditTimes", auditTimes);
        map.put("state", state);
        return faultService.findFaultsResult(map);
    }

    @DeleteMapping("/faults/{ids}")
    public Result deleteFaultById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            faultService.deleteFaultById(id);
        }
        return Result.SUCCESS;
    }

    /**
     * 上报缺陷
     *
     * @param content
     * @param deviceId
     * @param deviceName
     * @param faultHandle
     * @param faultImgs
     * @return
     */
    @PostMapping("/faults/report")
    public Result report(Integer source, Integer alarmId, String content, Integer deviceId, String deviceName, String deviceLocation, Integer teamId, Integer faultHandle, String faultImgs)
    {
        faultService.report(source, alarmId, content, deviceId, deviceName, deviceLocation, teamId, faultHandle, faultImgs);
        return Result.SUCCESS;
    }

    /**
     * 维修处置
     *
     * @param id
     * @param level
     * @param repairHandle
     * @return
     */
    @PostMapping("/faults/repair")
    public Result repair(Integer taskId, Integer id, Integer level, Integer repairHandle)
    {
        faultService.repair(taskId, id, level, repairHandle);
        return Result.SUCCESS;
    }

    /**
     * 审核缺陷
     *
     * @param taskId
     * @param id
     * @param auditResult
     * @param auditComment
     * @param auditLocation
     * @param auditImgs
     * @return
     */
    @PostMapping("/faults/audit")
    public Result audit(Integer taskId, Integer id, Integer level, Integer auditResult, String auditComment, String auditLocation, String auditImgs)
    {
        faultService.audit(taskId, id, level, auditResult, auditComment, auditLocation, auditImgs);
        return Result.SUCCESS;
    }

    /**
     * 撤销缺陷
     *
     * @param id
     * @return
     */
    @PostMapping("/faults/terminate")
    public Result terminate(Integer id)
    {
        faultService.terminate(id);
        return Result.SUCCESS;
    }

}
