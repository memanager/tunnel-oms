package com.hulman.oms.web;

import com.hulman.oms.bean.DeviceType;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.DeviceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: maxwellens
 */
@RestController
public class DeviceTypeController
{
    @Autowired
    private DeviceTypeService deviceTypeService;

    @GetMapping("/device-types")
    public Result findDeviceTypes()
    {
        List<DeviceType> deviceTypes = deviceTypeService.findDeviceTypes();
        return new Result(deviceTypes, deviceTypes.size());
    }

}
