package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseOrder;
import com.hulman.oms.service.SparepartPurchaseOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 备件采购订单(sparepart_purchase_order)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartPurchaseOrderController {
/**
* 服务对象
*/
    @Autowired
    private SparepartPurchaseOrderService sparepartPurchaseOrderService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/sparepart-purchase-orders/{id}")
    public Result findSparepartPurchaseOrderById(@PathVariable Integer id)
    {
        SparepartPurchaseOrder sparepartPurchaseOrder = sparepartPurchaseOrderService.selectByPrimaryKey(id);
        return new Result(sparepartPurchaseOrder);
    }

    @GetMapping("/sparepart-purchase-orders")
    public Result findSparepartPurchaseOrders(Integer page, Integer limit, String no, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return sparepartPurchaseOrderService.findSparepartPurchaseOrdersResult(map);
    }

    @PutMapping("/sparepart-purchase-orders")
    public Result saveSparepartPuchaseOrders(@RequestBody SparepartPurchaseOrder sparepartPuchaseOrder)
    {
        sparepartPurchaseOrderService.saveSparepartPurchaseOrder(sparepartPuchaseOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/sparepart-purchase-orders/{ids}")
    public Result deleteSparepartPuchaseOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sparepartPurchaseOrderService.deleteSparepartPurchaseOrderById(id);
        }
        return Result.SUCCESS;
    }

}
