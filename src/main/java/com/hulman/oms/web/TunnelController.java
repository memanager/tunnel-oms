package com.hulman.oms.web;

import com.hulman.oms.bean.Device;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Tunnel;
import com.hulman.oms.service.TunnelService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Author: maxwellens
 */
@RestController
public class TunnelController
{
    @Autowired
    private TunnelService tunnelService;

    @GetMapping("/tunnels/{id}")
    public Result findTunnelById(@PathVariable Integer id)
    {
        Tunnel tunnel = tunnelService.findTunnelById(id);
        return new Result(tunnel);
    }

    @GetMapping("/tunnels")
    public Result findTunnels(Integer page, Integer limit, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        return tunnelService.findTunnelsResult(map);
    }

    /**
     * 删除隧道（支持批量）
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/tunnels/{ids}")
    public Result deleteTunnelByIds(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (int id : idArrays)
        {
            tunnelService.deleteTunnelById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/tunnels")
    public Result saveTunnel(Tunnel tunnel)
    {
        tunnelService.saveTunnel(tunnel);
        return Result.SUCCESS;
    }

    @GetMapping("/tunnels/select")
    public Result findTunnelByName(String name)
    {
        Tunnel tunnel = tunnelService.findTunnelByName(name);
        return new Result(tunnel);
    }

    @RequestMapping("/tunnels/index")
    public Result findTunnelsIndex(String name, String ids)
    {
        Map<String, Object> condition = new HashMap<>();
        condition.put("name", name);
        if (!StringUtil.isEmpty(ids))
        {
            condition.put("ids", ids.split(","));
        }
        List<Tunnel> tunnels = tunnelService.findTunnels(condition);
        Map<String, List<String>> map = new HashMap<>();
        for (Tunnel tunnel : tunnels)
        {
            String letter = StringUtil.getPinyinFirstLetters(tunnel.getName().substring(0, 1));
            if (!map.containsKey(letter))
            {
                map.put(letter, new ArrayList<>());
            }
            map.get(letter).add(tunnel.getName());
        }
        //对字母进行排序
        Object[] keys = map.keySet().toArray();
        Arrays.sort(keys);
        List<Object> result = new ArrayList<>();
        for (Object key : keys)
        {
            Map<String, Object> item = new HashMap<>();
            item.put("letter", key);
            item.put("data", map.get(key));
            result.add(item);
        }
        return new Result(result);
    }

    @PostMapping("/tunnels/parse4ids")
    public Result parseTunnelsNames(String names)
    {
        return new Result(tunnelService.parseTunnelIds(names));
    }

}
