package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.DeviceRelation;
import com.hulman.oms.service.DeviceRelationService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class DeviceRelationController
{
    @Autowired
    private DeviceRelationService deviceRelationService;

    @GetMapping("/device-relations")
    public Result findDeviceRelations(Integer page, Integer limit, Integer deviceId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        return deviceRelationService.findDeviceRelationsResult(map);
    }

    @DeleteMapping("/device-relations/{ids}")
    public Result deleteDeviceRelationById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            deviceRelationService.deleteDeviceRelationById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/device-relations")
    public Result saveDeviceRelation(DeviceRelation deviceRelation)
    {
        deviceRelationService.saveDeviceRelation(deviceRelation);
        return Result.SUCCESS;
    }

	@DeleteMapping("/device-relations")
	public Result clearDeviceRelationsByDeviceId(Integer deviceId)
	{
		deviceRelationService.clearDeviceRelationsByDeviceId(deviceId);
		return Result.SUCCESS;
	}

}
