package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.MaintainTask;
import com.hulman.oms.service.MaintainTaskService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class MaintainTaskController
{
    @Autowired
    private MaintainTaskService maintainTaskService;

    @GetMapping("/maintain-tasks/{id}")
    public Result findMaintainTaskById(@PathVariable Integer id)
    {
        MaintainTask maintainTask = maintainTaskService.findMaintainTaskById(id);
        return new Result(maintainTask);
    }

    @GetMapping("/maintain-tasks")
    public Result findMaintainTasks(Integer page, Integer limit, Integer maintainId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("maintainId", maintainId);
        map.put("page", page);
        map.put("limit", limit);
        return maintainTaskService.findMaintainTasksResult(map);
    }

    @DeleteMapping("/maintain-tasks/{ids}")
    public Result deleteMaintainTaskById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            maintainTaskService.deleteMaintainTaskById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/maintain-tasks")
    public Result saveMaintainTask(MaintainTask maintainTask)
    {
        maintainTaskService.saveMaintainTask(maintainTask);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-tasks/create")
    public Result create(MaintainTask maintainTask)
    {
        maintainTaskService.create(maintainTask);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-tasks/complete")
    public Result complete(Integer taskId, Integer maintainTaskId, String vehicle, String driver)
    {
        maintainTaskService.complete(taskId, maintainTaskId, vehicle, driver);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-tasks/cancel")
    public Result cancel(Integer id)
    {
        maintainTaskService.cancel(id);
        return Result.SUCCESS;
    }

}
