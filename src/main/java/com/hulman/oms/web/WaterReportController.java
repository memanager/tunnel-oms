package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.WaterReport;
import com.hulman.oms.bean.WaterReportItem;
import com.hulman.oms.service.WaterReportItemService;
import com.hulman.oms.service.WaterReportService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class WaterReportController
{
	@Autowired
	private WaterReportService waterReportService;
	@Autowired
	private WaterReportItemService waterReportItemService;
	@Autowired
	private HttpServletResponse response;

	@GetMapping("/water-reports/{id}")
	public Result findWaterReportById(@PathVariable Integer id)
	{
		WaterReport waterReport = waterReportService.findWaterReportById(id);
		return new Result(waterReport);
	}

	@GetMapping("/water-reports")
	public Result findWaterReports(Integer page, Integer limit, Integer year, Integer month)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
		map.put("year", year);
		map.put("month", month);
		return waterReportService.findWaterReportsResult(map);
	}

	@DeleteMapping("/water-reports/{ids}")
	public Result deleteWaterReportById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			waterReportService.deleteWaterReportById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/water-reports")
	public Result saveWaterReport(WaterReport waterReport)
	{
		waterReportService.saveWaterReport(waterReport);
		return Result.SUCCESS;
	}

	@PostMapping("/water-reports/create")
	public Result createReport(WaterReport report)
	{
		report.setCreateTime(new Date());
		waterReportService.createReport(report);
		return Result.SUCCESS;
	}

	@GetMapping("/water-reports/export")
	public void findWaterReports(Integer id) throws IOException
	{
		response.setContentType("multipart/form-data");
		response.setCharacterEncoding("utf-8");
		response.setHeader("Content-disposition", "attachment;filename=water-report.xlsx");
		WaterReport report = waterReportService.findWaterReportById(id);
		List<WaterReportItem> items = waterReportItemService.findWaterReportItemsByReportId(id);
		Integer totalAmount = 0;
		Integer sn = 1;
		for (WaterReportItem item : items)
		{
			item.setSn(sn++);
			if (item.getUseAmount() != null)
			{
				totalAmount += item.getUseAmount();
			}
		}
		report.setTotalAmount(totalAmount);
		InputStream inputStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("template/water-report.xlsx");
		ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).withTemplate(inputStream).build();
		WriteSheet writeSheet = EasyExcel.writerSheet().build();
		FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
		excelWriter.fill(items, fillConfig, writeSheet);
		excelWriter.fill(report, writeSheet);
		excelWriter.finish();
	}

}
