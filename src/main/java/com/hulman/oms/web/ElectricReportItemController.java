package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ElectricReportItem;
import com.hulman.oms.service.ElectricReportItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ElectricReportItemController
{
    @Autowired
    private ElectricReportItemService electricReportItemService;

    @GetMapping("/electric-report-items/{id}")
    public Result findElectricReportItemById(@PathVariable Integer id)
    {
        ElectricReportItem electricReportItem = electricReportItemService.findElectricReportItemById(id);
        return new Result(electricReportItem);
    }

    @GetMapping("/electric-report-items")
    public Result findElectricReportItems(Integer page, Integer limit, Integer reportId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("reportId", reportId);
        return electricReportItemService.findElectricReportItemsResult(map);
    }

    @DeleteMapping("/electric-report-items/{id}")
    public Result deleteElectricReportItemById(@PathVariable Integer id)
    {
        electricReportItemService.deleteElectricReportItemById(id);
        return Result.SUCCESS;
    }

    /**
     * 抄表业务
     *
     * @param electricReportItem
     * @return
     */
    @PostMapping("/electric-report-items/read")
    public Result readElectricReportItem(ElectricReportItem electricReportItem)
    {
        electricReportItemService.read(electricReportItem);
        return Result.SUCCESS;
    }

}
