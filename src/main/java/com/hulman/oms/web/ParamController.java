package com.hulman.oms.web;

import com.hulman.oms.bean.Param;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.ParamService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ParamController
{
	@Autowired
	private ParamService paramService;

	@GetMapping("/params/{id}")
	public Result findParamById(@PathVariable Integer id)
	{
		Param param = paramService.findParamById(id);
		return new Result(param);
	}

	@GetMapping("/params")
	public Result findParams(Integer page, Integer limit, String name)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("name", name);
		return paramService.findParamsResult(map);
	}

	@DeleteMapping("/params/{ids}")
	public Result deleteParamById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			paramService.deleteParamById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/params")
	public Result saveParam(Param param)
	{
		paramService.saveParam(param);
		return Result.SUCCESS;
	}

}
