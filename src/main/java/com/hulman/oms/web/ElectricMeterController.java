package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.hulman.oms.bean.ElectricMeter;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.ElectricMeterService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ElectricMeterController
{
	@Autowired
	private ElectricMeterService electricMeterService;
	@Autowired
	private HttpServletResponse response;

	@GetMapping("/electric-meters/{id}")
	public Result findElectricMeterById(@PathVariable Integer id)
	{
		ElectricMeter electricMeter = electricMeterService.findElectricMeterById(id);
		return new Result(electricMeter);
	}

	@GetMapping("/electric-meters")
	public Result findElectricMeters(Integer page, Integer limit, String name, String no)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("name", name);
        map.put("no", no);
		return electricMeterService.findElectricMetersResult(map);
	}

	@DeleteMapping("/electric-meters/{ids}")
	public Result deleteElectricMeterById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			electricMeterService.deleteElectricMeterById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/electric-meters")
	public Result saveElectricMeter(ElectricMeter electricMeter)
	{
		electricMeterService.saveElectricMeter(electricMeter);
		return Result.SUCCESS;
	}

	@PostMapping("/electric-meters/import")
	public Result importElectricMeters(MultipartFile file) throws IOException
	{
		File tmp = new File(System.getProperty("java.io.tmpdir") + "/" + System.currentTimeMillis() + ".xlsx");
		file.transferTo(tmp);
		List<ElectricMeter> meters = EasyExcel.read(tmp).head(ElectricMeter.class).sheet(0).doReadSync();
		electricMeterService.importElectricMeters(meters);
		return Result.SUCCESS;
	}

	@GetMapping("/electric-meters/export")
	public void exportElectricMeters( String name, String no) throws IOException
	{
		response.setContentType("multipart/form-data");
		response.setCharacterEncoding("utf-8");
		response.setHeader("Content-disposition", "attachment;filename=electric-meter.xlsx");

		Map<String, Object> map = new HashMap<>();
		map.put("name", name);
		map.put("no", no);

		List<ElectricMeter> meters = electricMeterService.findElectricMeters(map);
		ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
		WriteSheet sheet = EasyExcel.writerSheet(0, "电表").head(ElectricMeter.class).build();
		excelWriter.write(meters, sheet);
		excelWriter.finish();
	}

}
