package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.util.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@RestController
public class ImageController
{
    @Value("${img-home}")
    private String imgHome;

    @PostMapping("/imgs/upload")
    public Result uploadImages(MultipartFile file) throws IOException
    {
        String filePath = imgHome + file.getOriginalFilename();
        file.transferTo(new File(filePath));
        return Result.SUCCESS;
    }

    @DeleteMapping("/imgs/{file}")
    public Result deleteImage(@PathVariable String file) throws IOException
    {
        String filePath = imgHome + file;
        boolean result = FileUtil.deleteFile(new File(filePath));
        if (result)
        {
            return Result.SUCCESS;
        } else
        {
            throw new IOException("文件删除失败");
        }

    }
}
