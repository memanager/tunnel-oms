package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ScadaReport;
import com.hulman.oms.service.ScadaReportService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ScadaReportController
{
	@Autowired
	private ScadaReportService scadaReportService;

	@GetMapping("/scada-reports/{id}")
	public Result findScadaReportById(@PathVariable Integer id)
	{
		ScadaReport scadaReport = scadaReportService.findScadaReportById(id);
		return new Result(scadaReport);
	}

	@GetMapping("/scada-reports")
	public Result findScadaReports(Integer page, Integer limit, String identifier, String name)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("identifier", identifier);
        map.put("name", name);
		return scadaReportService.findScadaReportsResult(map);
	}

	@DeleteMapping("/scada-reports/{ids}")
	public Result deleteScadaReportById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			scadaReportService.deleteScadaReportById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/scada-reports")
	public Result saveScadaReport(ScadaReport scadaReport)
	{
		scadaReportService.saveScadaReport(scadaReport);
		return Result.SUCCESS;
	}

}
