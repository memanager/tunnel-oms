package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolUseItem;
import com.hulman.oms.service.ToolUseItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 工具领用明细(tool_use_item)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolUseItemController {
/**
* 服务对象
*/
    @Autowired
    private ToolUseItemService toolUseItemService;

    @GetMapping("/tool-use-items/{id}")
    public Result findToolUseItemById(@PathVariable Integer id)
    {
        ToolUseItem toolUseItem = toolUseItemService.selectByPrimaryKey(id);
        return new Result(toolUseItem);
    }

    @GetMapping("/tool-use-items")
    public Result findToolUseItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return toolUseItemService.findToolUseItemsResult(map);
    }

    @GetMapping("/tool-use-items/orderId/{orderId}")
    public Result findToolUseOrderId(@PathVariable Integer orderId)
    {
        List<ToolUseItem> toolUseItemList = toolUseItemService.selectByOrderId(orderId);
        return new Result(toolUseItemList);
    }

    @DeleteMapping("/tool-use-items/{id}")
    public Result deleteToolUseItemById(@PathVariable Integer id)
    {
        toolUseItemService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @PutMapping("/tool-use-items")
    public Result saveToolUseItem(ToolUseItem ToolUseItem)
    {
        toolUseItemService.saveToolUseItem(ToolUseItem);
        return Result.SUCCESS;
    }

}
