package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolWarehouse;
import com.hulman.oms.bean.ToolWarehouse;
import com.hulman.oms.service.ToolWarehouseService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具仓库(tool_warehouse)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolWarehouseController {
/**
* 服务对象
*/
    @Autowired
    private ToolWarehouseService toolWarehouseService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/tool-warehouses/{id}")
    public Result findTooltWarehouseById(@PathVariable Integer id)
    {
        ToolWarehouse toolWarehouse = toolWarehouseService.selectByPrimaryKey(id);
        return new Result(toolWarehouse);
    }

    @GetMapping("/tool-warehouses")
    public Result findToolWarehouses(Integer page, Integer limit, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        return toolWarehouseService.findToolWarehousesResult(map);
    }

    @DeleteMapping("/tool-warehouses/delete/{ids}")
    public Result deleteToolWarehouseById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            toolWarehouseService.deleteByPrimaryKey(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/tool-warehouses")
    public Result saveToolWarehouse(ToolWarehouse ToolWarehouse)
    {
        toolWarehouseService.save(ToolWarehouse);
        return Result.SUCCESS;
    }

}
