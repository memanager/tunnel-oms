package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartInoutStock;
import com.hulman.oms.service.SparepartInoutStockService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class SparepartInoutStockController
{
	@Autowired
	private SparepartInoutStockService sparepartInoutStockService;

	@GetMapping("/sparepart-inout-stocks/{id}")
	public Result findSparepartInoutStockById(@PathVariable Integer id)
	{
		SparepartInoutStock sparepartInoutStock = sparepartInoutStockService.findSparepartInoutStockById(id);
		return new Result(sparepartInoutStock);
	}

	@GetMapping("/sparepart-inout-stocks")
	public Result findSparepartInoutStocks(Integer page, Integer limit, Integer sparepartId, String name, String brand, String model, Integer orderType, String dateRange)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("sparepartId", sparepartId);
        map.put("name", name);
        map.put("brand", brand);
        map.put("model", model);
        map.put("orderType", orderType);
		if (!StringUtil.isEmpty(dateRange))
		{
			String[] dates = dateRange.split(" - ");
			map.put("startDate", dates[0]);
			map.put("endDate", dates[1]);
		}
		return sparepartInoutStockService.findSparepartInoutStocksResult(map);
	}

	@DeleteMapping("/sparepart-inout-stocks/{ids}")
	public Result deleteSparepartInoutStockById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			sparepartInoutStockService.deleteSparepartInoutStockById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/sparepart-inout-stocks")
	public Result saveSparepartInoutStock(SparepartInoutStock sparepartInoutStock)
	{
		sparepartInoutStockService.saveSparepartInoutStock(sparepartInoutStock);
		return Result.SUCCESS;
	}

}
