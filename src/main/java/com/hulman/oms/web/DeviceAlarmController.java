package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.service.DeviceAlarmService;
import com.hulman.oms.util.NumberUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DeviceAlarmController
{
    @Autowired
    private DeviceAlarmService deviceAlarmService;

    @GetMapping("/device-alarms")
    public Object findDeviceAlarms(Integer page, Integer limit, String deviceName, Integer state)
    {
        return deviceAlarmService.findDeviceAlarms(page, limit, deviceName, state);
    }

    @GetMapping("/device-alarms/{id}")
    public Object findDeviceAlarm(@PathVariable Integer id)
    {
        return deviceAlarmService.findDeviceAlarmById(id);
    }

    @GetMapping("/device-alarms/unconfirmed-device-alarms-count")
    public Object findUnconfirmedDeviceAlarmsCount()
    {
        try
        {
            return deviceAlarmService.findUnconfirmedDeviceAlarmsCount();
        } catch (Exception ex)
        {
            log.error("findUnconfirmedDeviceAlarmsCount", ex);
            return Result.SUCCESS;
        }
    }

    @GetMapping("/device-alarms/{id}/confirm")
    public Object confirmDeviceAlarm(@PathVariable Integer id)
    {
        return deviceAlarmService.confirmDeviceAlarm(id);
    }

    @GetMapping("/device-alarms/{id}/handle")
    public Object handleDeviceAlarm(@PathVariable Integer id)
    {
        return deviceAlarmService.handleDeviceAlarm(id);
    }

    @DeleteMapping("/device-alarms/{ids}")
    public Object deleteDeviceAlarm(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            deviceAlarmService.deleteDeviceAlarm(id);
        }
        return Result.SUCCESS;
    }
}
