package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Maintain;
import com.hulman.oms.service.MaintainService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class MaintainController
{
    @Autowired
    private MaintainService maintainService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder)
    {
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @GetMapping("/maintains/{id}")
    public Result findMaintainById(@PathVariable Integer id)
    {
        Maintain maintain = maintainService.findMaintainById(id);
        return new Result(maintain);
    }

    @GetMapping("/maintains")
    public Result findMaintains(Integer page, Integer limit, Integer no, String name, String createByName, String directorName, Integer auditTimes, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        map.put("name", name);
        map.put("createByName", createByName);
        map.put("directorName", directorName);
        map.put("auditTimes", auditTimes);
        map.put("state", state);
        return maintainService.findMaintainsResult(map);
    }

    @DeleteMapping("/maintains/{ids}")
    public Result deleteMaintainById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            maintainService.deleteMaintainById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/maintains")
    public Result saveMaintain(Maintain maintain)
    {
        maintainService.saveMaintain(maintain);
        return Result.SUCCESS;
    }

    /**
     * 创建专项保养工单
     */
    @PostMapping("/maintains/create")
    public Result create(Maintain maintain)
    {
        maintainService.create(maintain);
        return Result.SUCCESS;
    }

    /**
     * 完成专项保养工单
     *
     * @param taskId
     * @param id
     * @param completion
     * @return
     */
    @PostMapping("/maintains/complete")
    public Result complete(Integer taskId, Integer id, String completion)
    {
        maintainService.complete(taskId, id, completion);
        return Result.SUCCESS;
    }

    /**
     * 审核保养工单
     *
     * @param taskId
     * @param id
     * @param auditResult
     * @param auditComment
     * @param auditLocation
     * @param auditImgs
     * @return
     */
    @PostMapping("/maintains/audit")
    public Result audit(Integer taskId, Integer id, Integer auditResult, String auditComment, String auditLocation, String auditImgs)
    {
        maintainService.audit(taskId, id, auditResult, auditComment, auditLocation, auditImgs);
        return Result.SUCCESS;
    }

    @PostMapping("/maintains/terminate")
    public Result terminate(Integer id)
    {
        maintainService.terminate(id);
        return Result.SUCCESS;
    }

}
