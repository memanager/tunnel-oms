package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import com.hulman.oms.service.SparepartPurchaseInstockItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class SparepartPurchaseInstockItemController
{
	@Autowired
	private SparepartPurchaseInstockItemService sparepartPurchaseInstockItemService;

	@GetMapping("/sparepart-purchase-instock-items/{id}")
	public Result findSparepartPurchaseInstockItemById(@PathVariable Integer id)
	{
		SparepartPurchaseInstockItem sparepartPurchaseInstockItem = sparepartPurchaseInstockItemService.findSparepartPurchaseInstockItemById(id);
		return new Result(sparepartPurchaseInstockItem);
	}

	@GetMapping("/sparepart-purchase-instock-items")
	public Result findSparepartPurchaseInstockItems(Integer page, Integer limit)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
		return sparepartPurchaseInstockItemService.findSparepartPurchaseInstockItemsResult(map);
	}

	@DeleteMapping("/sparepart-purchase-instock-items/{id}")
	public Result deleteSparepartPurchaseInstockItemById(@PathVariable Integer id)
	{
		sparepartPurchaseInstockItemService.deleteSparepartPurchaseInstockItemById(id);
		return Result.SUCCESS;
	}

	@PutMapping("/sparepart-purchase-instock-items")
	public Result saveSparepartPurchaseInstockItem(SparepartPurchaseInstockItem sparepartPurchaseInstockItem)
	{
		sparepartPurchaseInstockItemService.saveSparepartPurchaseInstockItem(sparepartPurchaseInstockItem);
		return Result.SUCCESS;
	}

}
