package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseItem;
import com.hulman.oms.service.SparepartPurchaseItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 备件采购明细(sparepart_purchase_item)表控制层
*
* @author xxxxx
*/
@RestController
public class SparepartPurchaseItemController {
/**
* 服务对象
*/
    @Autowired
    private SparepartPurchaseItemService sparepartPurchaseItemService;

    @GetMapping("/sparepart-purchase-items/{id}")
    public Result findSparepartPuchaseItemById(@PathVariable Integer id)
    {
        SparepartPurchaseItem sparepartPuchaseItem = sparepartPurchaseItemService.selectByPrimaryKey(id);
        return new Result(sparepartPuchaseItem);
    }

    @GetMapping("/sparepart-purchase-items")
    public Result findSparepartPuchaseItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return sparepartPurchaseItemService.findSparepartPurchaseItemsResult(map);
    }

    @DeleteMapping("/sparepart-purchase-items/{id}")
    public Result deleteSparepartPuchaseItemById(@PathVariable Integer id)
    {
        sparepartPurchaseItemService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @PutMapping("/sparepart-purchase-items")
    public Result saveSparepartPuchaseItem(SparepartPurchaseItem sparepartPuchaseItem)
    {
        sparepartPurchaseItemService.saveSparepartPurchaseItem(sparepartPuchaseItem);
        return Result.SUCCESS;
    }

    @GetMapping("/sparepart-purchase-items/orderId/{orderId}")
    public Result findSparepartPuchaseItemByOrderId(@PathVariable Integer orderId)
    {
        List<SparepartPurchaseItem> sparepartPuchaseItem = sparepartPurchaseItemService.selectByOrderId(orderId);
        return new Result(sparepartPuchaseItem);
    }

}
