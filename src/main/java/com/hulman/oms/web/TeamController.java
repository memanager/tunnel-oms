package com.hulman.oms.web;

import com.hulman.oms.bean.*;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.service.TeamMemberService;
import com.hulman.oms.service.TeamService;
import com.hulman.oms.service.UserService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Author: maxwellens
 */
@RestController
public class TeamController
{
    @Autowired
    private TeamService teamService;
    @Autowired
    private TeamMemberService teamMemberService;
    @Autowired
    private UserService userService;

    @GetMapping("/teams/{id}")
    public Result findTeamById(@PathVariable Integer id)
    {
        Team team = teamService.findTeamById(id);
        return new Result(team);
    }

    @GetMapping("/teams")
    public Result findTeams(Integer page, Integer limit, String name, Integer type)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        map.put("type", type);
        return teamService.findTeamsResult(map);
    }

    @DeleteMapping("/teams/{ids}")
    public Result deleteTeamById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            teamService.deleteTeamById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/teams")
    public Result saveTeam(Team team)
    {
        User leader = userService.findUserById(team.getLeaderId());
        team.setLeaderName(leader.getName());
        teamService.saveTeam(team);
        return Result.SUCCESS;
    }

    /*@GetMapping("/teams/{id}/members")
    public Result findTeamMembersById(@PathVariable Integer id)
    {
        List<TeamMember> data = new ArrayList<>();
        Team team = teamService.findTeamById(id);
        if (team == null)
        {
            throw new IoTException("不存在该班组");
        }
        List<TeamMember> teamMembers;
        //点检组
        if (team.getType() == Team.TYPE_PATROL)
        {
            teamMembers = teamMemberService.findTeamMembersByRoleId(Role.CHECKER);
        }
        //检修组
        else if (team.getType() == Team.TYPE_REPAIR)
        {
            teamMembers = teamMemberService.findTeamMembersByRoleId(Role.REPAIRER);
        } else
        {
            teamMembers = Collections.emptyList();
        }
        for (TeamMember teamMember : teamMembers)
        {
            if (teamMember.getTeamId() == team.getId())
            {
                data.add(teamMember);
            }
        }
        return new Result(data);
    }*/

}
