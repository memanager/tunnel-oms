package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolPurchaseOrder;
import com.hulman.oms.bean.ToolPurchaseOrder;
import com.hulman.oms.service.ToolPurchaseOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具采购订单(tool_purchase_order)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolPurchaseOrderController {
/**
* 服务对象
*/
    @Autowired
    private ToolPurchaseOrderService toolPurchaseOrderService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/tool-purchase-orders/{id}")
    public Result findToolPurchaseOrderById(@PathVariable Integer id)
    {
        ToolPurchaseOrder toolPurchaseOrder = toolPurchaseOrderService.selectByPrimaryKey(id);
        return new Result(toolPurchaseOrder);
    }

    @GetMapping("/tool-purchase-orders")
    public Result findToolPurchaseOrders(Integer page, Integer limit, String no, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return toolPurchaseOrderService.findToolPurchaseOrdersResult(map);
    }

    @PutMapping("/tool-purchase-orders")
    public Result saveToolPuchaseOrders(@RequestBody ToolPurchaseOrder toolPuchaseOrder)
    {
        toolPurchaseOrderService.saveToolPurchaseOrder(toolPuchaseOrder);
        return Result.SUCCESS;
    }

    @DeleteMapping("/tool-purchase-orders/{ids}")
    public Result deleteToolPuchaseOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            toolPurchaseOrderService.deleteToolPurchaseOrderById(id);
        }
        return Result.SUCCESS;
    }

}
