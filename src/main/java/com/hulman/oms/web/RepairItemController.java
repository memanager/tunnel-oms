package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.RepairItem;
import com.hulman.oms.bean.WorkTaskItem;
import com.hulman.oms.service.RepairItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class RepairItemController
{
    @Autowired
    private RepairItemService repairItemService;

    @GetMapping("/repair-items/{id}")
    public Result findRepairItemById(@PathVariable Integer id)
    {
        RepairItem repairItem = repairItemService.findRepairItemById(id);
        return new Result(repairItem);
    }

    @GetMapping("/repair-items")
    public Result findRepairItems(Integer page, Integer limit, Integer repairId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("repairId", repairId);
        return repairItemService.findRepairItemsResult(map);
    }

    /**
     * 是否有未审核的维修子项
     *
     * @param repairId
     * @return
     */
    @GetMapping("/repair-items/{repairId}/has-unaudited-items")
    public Result hasUnAuditedItems(@PathVariable Integer repairId)
    {
        boolean data = false;
        Map<String, Object> map = new HashMap<>();
        map.put("repairId", repairId);
        List<RepairItem> repairItems = repairItemService.findRepairItems(map);
        for (RepairItem repairItem : repairItems)
        {
            if (repairItem.getState() != RepairItem.STATE_AUDITED)
            {
                data = true;
                break;
            }
        }
        return new Result(data);
    }

    @DeleteMapping("/repair-items/{id}")
    public Result deleteRepairItemById(@PathVariable Integer id)
    {
        repairItemService.deleteRepairItemById(id);
        return Result.SUCCESS;
    }

    @PutMapping("/repair-items")
    public Result saveRepairItem(RepairItem repairItem)
    {
        repairItemService.saveRepairItem(repairItem);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/create")
    public Result create(Integer repairId, Integer deviceId, String deviceName, String deviceLocation, String name, String note, String executeByName, String notifyNames)
    {
        repairItemService.create(repairId, deviceId, deviceName, deviceLocation, name, note, executeByName, notifyNames);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/execute")
    public Result execute(Integer id)
    {
        repairItemService.execute(id);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/save")
    public Result save(Integer id, String vehicle, String driver, String tools, String materials, String deviceName, String deviceLocation, String imgs, String completion)
    {
        repairItemService.save(id, vehicle, driver, tools, materials, deviceName, deviceLocation, imgs, completion);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/complete")
    public Result complete(Integer taskId, Integer id, String vehicle, String driver, String tools, String materials, String deviceName, String deviceLocation, String imgs, String completion)
    {
        repairItemService.complete(taskId, id, vehicle, driver, tools, materials, deviceName, deviceLocation, imgs, completion);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/audit")
    public Result audit(Integer id, Integer auditResult, String auditComment)
    {
        repairItemService.audit(id, auditResult, auditComment);
        return Result.SUCCESS;
    }

    @PostMapping("/repair-items/cancel")
    public Result cancel(Integer id)
    {
        repairItemService.cancel(id);
        return Result.SUCCESS;
    }

}
