package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Sentence;
import com.hulman.oms.service.SentenceService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class SentenceController
{
    @Autowired
    private SentenceService sentenceService;

    @GetMapping("/sentences/{id}")
    public Result findSentenceById(@PathVariable Integer id)
    {
        Sentence attendanceSentence = sentenceService.findSentenceById(id);
        return new Result(attendanceSentence);
    }

    @GetMapping("/sentences")
    public Result findSentences(Integer page, Integer limit, String sentence, Integer type)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("type", type);
        map.put("sentence", sentence);
        return sentenceService.findSentencesResult(map);
    }

    @DeleteMapping("/sentences/{ids}")
    public Result deleteSentenceById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sentenceService.deleteSentenceById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/sentences")
    public Result saveSentence(Sentence attendanceSentence)
    {
        sentenceService.saveSentence(attendanceSentence);
        return Result.SUCCESS;
    }

}
