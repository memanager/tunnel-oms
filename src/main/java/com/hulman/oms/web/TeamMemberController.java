package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Role;
import com.hulman.oms.bean.Team;
import com.hulman.oms.bean.TeamMember;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.service.TeamMemberService;
import com.hulman.oms.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Author: maxwellens
 */
@RestController
public class TeamMemberController
{
    @Autowired
    private TeamMemberService teamMemberService;
    @Autowired
    private TeamService teamService;

    @GetMapping("/team-members")
    public Result findTeamMembers(Integer teamId)
    {
        List<Map<String, Object>> data = new ArrayList<>();
        Team team = teamService.findTeamById(teamId);
        if (team == null)
        {
            throw new IoTException("不存在该班组");
        }
        List<TeamMember> teamMembers = teamMemberService.findTeamMembers();
        for (TeamMember teamMember : teamMembers)
        {
            Map<String, Object> item = new HashMap<>();
            item.put("value", teamMember.getUserId());
            item.put("title", teamMember.getUserName());
            if (teamMember.getTeamId() != null)
            {
                if (teamMember.getTeamId() == teamId)
                {
                    item.put("selected", true);
                } else
                {
                    item.put("disabled", true);
                }
            }
            data.add(item);
        }
        return new Result(data);
    }

    @PostMapping("/team-members")
    public Result addMember(Integer teamId, Integer userId)
    {
        teamMemberService.addMember(teamId, userId);
        return Result.SUCCESS;
    }

    @DeleteMapping("/team-members")
    public Result removeMember(Integer teamId, Integer userId)
    {
        teamMemberService.removeMember(teamId, userId);
        return Result.SUCCESS;
    }

}
