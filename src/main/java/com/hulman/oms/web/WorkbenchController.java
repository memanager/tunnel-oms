package com.hulman.oms.web;

import com.hulman.oms.bean.Workbench;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.WorkbenchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: maxwellens
 */
@RestController
public class WorkbenchController
{
    @Autowired
    private WorkbenchService workbenchService;

    /**
     * 返回所有模块
     *
     * @return
     */
    @GetMapping("/workbenches")
    public Result findAppWorks()
    {
        return workbenchService.findWorkbenchesResult();
    }

    /**
     * 根据ID查找模块
     *
     * @param id
     * @return
     */
    @GetMapping("/workbenches/{id}")
    public Result findAppWorkById(@PathVariable Integer id)
    {
        return new Result(workbenchService.findWorkbenchById(id));
    }

    /**
     * 删除模块
     *
     * @param id
     * @return
     */
    @DeleteMapping("/workbenches/{id}")
    public Result deleteAppWorkById(@PathVariable Integer id)
    {
        workbenchService.deleteWorkbenchesByIdCascade(id);
        return Result.SUCCESS;
    }

    /**
     * 更新或添加模块
     *
     * @param module
     * @return
     */
    @PutMapping("/workbenches")
    public Result saveAppWork(Workbench module)
    {
        workbenchService.saveWorkbench(module);
        return Result.SUCCESS;
    }

}
