package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ProcDef;
import com.hulman.oms.service.ProcDefService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ProcDefController
{
	@Autowired
	private ProcDefService procDefService;

	@GetMapping("/proc-defs/{id}")
	public Result findProcDefById(@PathVariable Integer id)
	{
		ProcDef procDef = procDefService.findProcDefById(id);
		return new Result(procDef);
	}

	@GetMapping("/proc-defs")
	public Result findProcDefs(Integer page, Integer limit, String key, String name)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("key", key);
        map.put("name", name);
		return procDefService.findProcDefsResult(map);
	}

	@DeleteMapping("/proc-defs/{ids}")
	public Result deleteProcDefById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			procDefService.deleteProcDefById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/proc-defs")
	public Result saveProcDef(ProcDef procDef)
	{
		procDefService.saveProcDef(procDef);
		return Result.SUCCESS;
	}

}
