package com.hulman.oms.web;
import com.hulman.oms.bean.VehicleFile;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.VehicleFile;
import com.hulman.oms.service.VehicleFileService;
import com.hulman.oms.service.VehicleFileService;
import com.hulman.oms.util.FileUtil;
import com.hulman.oms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
* 车辆文件(vehicle_file)表控制层
*
* @author xxxxx
*/
@RestController
@Slf4j
public class VehicleFileController {
/**
* 服务对象
*/
    @Autowired
    private VehicleFileService vehicleFileService;
    
    @Value("${file-home}")
    private String fileHome;

    @Autowired
    private HttpServletResponse response;

    @PostConstruct
    public void init()
    {
        File fileDir = new File(fileHome);
        if (!fileDir.exists())
        {
            fileDir.mkdirs();
            log.info("Create file home:{}", fileHome);
        }
    }

    @GetMapping("/vehicle-files/{id}")
    public Result findVehicleFileById(@PathVariable Integer id)
    {
        VehicleFile vehicleFile = vehicleFileService.selectByPrimaryKey(id);
        return new Result(vehicleFile);
    }

    @GetMapping("/vehicle-files")
    public Result findVehicleFiles(Integer page, Integer limit, String name, Integer vehicleId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("vehicleId", vehicleId);
        map.put("name", name);
        return vehicleFileService.findVehicleFilesResult(map);
    }

    @DeleteMapping("/vehicle-files/{id}")
    public Result deleteVehicleFileById(@PathVariable Integer id)
    {
        VehicleFile vehicleFile = vehicleFileService.selectByPrimaryKey(id);
        if (vehicleFile != null)
        {
            String fileName = fileHome + vehicleFile.getStorageName();
            File file = new File(fileName);
            file.delete();
        }
        vehicleFileService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @GetMapping("/vehicle-files/download/{uuid}")
    public void downloadFile(@PathVariable String uuid) throws IOException
    {
        VehicleFile fileInfo = vehicleFileService.findVehicleFilesByUuid(uuid);
        InputStream in = Files.newInputStream(new File(fileHome + fileInfo.getStorageName()).toPath());
        response.setHeader("Content-Disposition", "attachment;Filename=" + URLEncoder.encode(fileInfo.getName(), "UTF-8"));
        OutputStream os = response.getOutputStream();
        byte[] b = new byte[1024];
        int length;
        while ((length = in.read(b)) > 0)
        {
            os.write(b, 0, length);
        }
        os.close();
        in.close();
    }

    /**
     * 设备目录文件上传
     *
     * @param vehicleId
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/vehicle-files/upload/{vehicleId}")
    public Result uploadUserFile(@PathVariable Integer vehicleId, MultipartFile file) throws Exception
    {
        String name = file.getOriginalFilename();
        String ext = FileUtil.parseExtName(name);
        String uuid = StringUtil.uuid();
        VehicleFile vehicleFile = new VehicleFile();
        vehicleFile.setVehicleId(vehicleId);
        vehicleFile.setUuid(uuid);
        vehicleFile.setName(name);
        vehicleFile.setExt(ext);
        vehicleFile.setCreateTime(new Date());
        String storageName = vehicleFile.getStorageName();
        File fileToUpload = new File(fileHome + storageName);
        file.transferTo(fileToUpload);
        vehicleFileService.saveVehicleFile(vehicleFile);
        return Result.SUCCESS;
    }

}
