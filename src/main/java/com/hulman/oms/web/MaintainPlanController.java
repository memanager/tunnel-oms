package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.hulman.oms.bean.*;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.service.MaintainPlanItemService;
import com.hulman.oms.service.MaintainPlanService;
import com.hulman.oms.service.MaintainService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class MaintainPlanController
{
    @Autowired
    private MaintainPlanService maintainPlanService;
    @Autowired
    private MaintainPlanItemService maintainPlanItemService;
    @Autowired
    private MaintainService maintainService;
    @Autowired
    private HttpServletResponse response;

    @GetMapping("/maintain-plans/{id}")
    public Result findMaintainPlanById(@PathVariable Integer id)
    {
        MaintainPlan maintainPlan = maintainPlanService.findMaintainPlanById(id);
        return new Result(maintainPlan);
    }

    @GetMapping("/maintain-plans/{id}/detail")
    public Result findMaintainPlanDetail(@PathVariable Integer id)
    {
        List<Map> result = new ArrayList<>();
        Map<String, List> map = maintainPlanService.findMaintainPlanDetail(id);
        for (Map.Entry<String, List> entry : map.entrySet())
        {
            Map<String, Object> item = new HashMap<>();
            item.put("item", entry.getKey());
            item.put("entries", entry.getValue());
            result.add(item);
        }
        return new Result(result, result.size());
    }

    @GetMapping("/maintain-plans")
    public Result findMaintainPlans(Integer page, Integer limit, String name, Integer period, Integer unit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        map.put("period", period);
        map.put("unit", unit);
        return maintainPlanService.findMaintainPlansResult(map);
    }

    @DeleteMapping("/maintain-plans/{ids}")
    public Result deleteMaintainPlanById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            maintainPlanService.deleteMaintainPlanById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/maintain-plans")
    public Result saveMaintainPlan(MaintainPlan maintainPlan)
    {
        maintainPlanService.saveMaintainPlan(maintainPlan);
        return Result.SUCCESS;
    }

    @PostMapping("/maintain-plans/import")
    public Result importMaintainPlans(MultipartFile file) throws IOException
    {
        File tmp = new File(System.getProperty("java.io.tmpdir") + "/" + System.currentTimeMillis() + ".xlsx");
        file.transferTo(tmp);
        try
        {
            maintainPlanService.importMaintainPlan(tmp);
            return Result.SUCCESS;
        } catch (Exception ex)
        {
            ex.printStackTrace();
            return new Result(ex.getMessage(), null);
        }
    }

    @GetMapping("/maintain-plans/export")
    public void exportMaintainPlans() throws IOException
    {
        String fileName = "专项保养计划.xlsx";
        response.setContentType("application/octet-stream;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        maintainPlanService.exportMaintainPlan(excelWriter);
    }

}
