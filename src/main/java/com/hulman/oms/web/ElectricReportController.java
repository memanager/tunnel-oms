package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.hulman.oms.bean.ElectricReport;
import com.hulman.oms.bean.ElectricReportItem;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.ElectricReportItemService;
import com.hulman.oms.service.ElectricReportService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ElectricReportController
{
    @Autowired
    private ElectricReportService electricReportService;
    @Autowired
    private ElectricReportItemService electricReportItemService;
    @Autowired
    private HttpServletResponse response;

    @GetMapping("/electric-reports/{id}")
    public Result findElectricReportById(@PathVariable Integer id)
    {
        ElectricReport electricReport = electricReportService.findElectricReportById(id);
        return new Result(electricReport);
    }

    @GetMapping("/electric-reports")
    public Result findElectricReports(Integer page, Integer limit, Integer year, Integer week)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("year", year);
        map.put("week", week);
        return electricReportService.findElectricReportsResult(map);
    }

    @DeleteMapping("/electric-reports/{ids}")
    public Result deleteElectricReportById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            electricReportService.deleteElectricReportById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/electric-reports")
    public Result saveElectricReport(ElectricReport electricReport)
    {
        electricReportService.saveElectricReport(electricReport);
        return Result.SUCCESS;
    }

    @GetMapping("/electric-reports/export")
    public void findElectricReports(Integer id) throws IOException
    {
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=electric-report.xlsx");
        ElectricReport report = electricReportService.findElectricReportById(id);
        List<ElectricReportItem> items = electricReportItemService.findElectricReportItemsByReportId(id);
        Integer totalAmount = 0;
        Integer totalCost = 0;
        Integer sn = 1;
        for (ElectricReportItem item : items)
        {
            item.setSn(sn++);
            if (item.getWeeklyAmount() != null)
            {
                totalAmount += item.getWeeklyAmount();
            }
            if (item.getWeeklyCost() != null)
            {
                totalCost += item.getWeeklyCost();
            }
        }
        report.setTotalAmount(totalAmount);
        report.setTotalCost(totalCost);
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("template/electric-report.xlsx");
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).withTemplate(inputStream).build();
        WriteSheet writeSheet = EasyExcel.writerSheet().build();
        FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
        excelWriter.fill(items, fillConfig, writeSheet);
        excelWriter.fill(report, writeSheet);
        excelWriter.finish();
    }

}
