package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.hulman.oms.bean.Driver;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.DriverService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class DriverController
{
	@Autowired
	private DriverService driverService;

	@Autowired
	private HttpServletResponse response;

	@GetMapping("/drivers/{id}")
	public Result findDriverById(@PathVariable Integer id)
	{
		Driver driver = driverService.findDriverById(id);
		return new Result(driver);
	}

	@GetMapping("/drivers")
	public Result findDrivers(Integer page, Integer limit, String name)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("name", name);
		return driverService.findDriversResult(map);
	}

	@DeleteMapping("/drivers/{ids}")
	public Result deleteDriverById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			driverService.deleteDriverById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/drivers")
	public Result saveDriver(Driver driver)
	{
		driverService.saveDriver(driver);
		return Result.SUCCESS;
	}

	/**
	 * 通过Excel批量导入司机信息
	 *
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/drivers/import")
	public Result importDrivers(MultipartFile file) throws IOException
	{
		File tmp = new File(System.getProperty("java.io.tmpdir") + "/" + System.currentTimeMillis() + ".xlsx");
		file.transferTo(tmp);
		try
		{
			List<Driver> drivers = EasyExcel.read(tmp).head(Driver.class).sheet(0).doReadSync();
			for (Driver driver : drivers)
			{
				driverService.saveDriver(driver);
			}
			return Result.SUCCESS;
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return new Result(ex.getMessage(), null);
		}
	}

	/**
	 * 导出司机列表
	 *
	 * @throws IOException
	 */
	@GetMapping("/drivers/export")
	public void exportDrivers() throws IOException
	{
		String fileName = "司机清单.xlsx";
		response.setContentType("application/octet-stream;charset=utf-8");
		response.setCharacterEncoding("utf-8");
		response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
		List<Driver> drivers = driverService.findDrivers(new HashMap<>());
		ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
		WriteSheet sheet0 = EasyExcel.writerSheet(0, "司机").head(Driver.class).build();
		excelWriter.write(drivers, sheet0);
		excelWriter.finish();
	}

}
