package com.hulman.oms.web;

import com.hulman.oms.bean.Device;
import com.hulman.oms.bean.DeviceSystem;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.DeviceService;
import com.hulman.oms.service.DeviceSystemService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class DeviceSystemController
{
    @Autowired
    private DeviceSystemService deviceSystemService;
    @Autowired
    private DeviceService deviceService;

    @GetMapping({"/device-systems","/devices/systems"})
    public Result findDeviceSystems()
    {
        List<DeviceSystem> deviceSystems = deviceSystemService.findDeviceSystems();
        return new Result(deviceSystems, deviceSystems.size());
    }

    @GetMapping({"/device-systems/devices"})
    public Result findSystemDevices(Integer tunnelSystem)
    {
        Integer tunnelId = tunnelSystem / 100;
        Integer systemId = tunnelSystem % 100;
        DeviceSystem deviceSystem = deviceSystemService.findDeviceSystemById(systemId);
        Map<String,Object> map = new HashMap<>();
        map.put("tunnelId",tunnelId);
        map.put("system",deviceSystem.getName());
        List<Device> devices = deviceService.findDevices(map);
        return new Result(devices, devices.size());
    }

    @DeleteMapping("/device-systems/{ids}")
    public Result deleteDeviceSystemById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            deviceSystemService.deleteDeviceSystemById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/device-systems")
    public Result saveDeviceSystem(DeviceSystem deviceSystem)
    {
        deviceSystemService.saveDeviceSystem(deviceSystem);
        return Result.SUCCESS;
    }

}
