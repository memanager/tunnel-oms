package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.WorkTaskItem;
import com.hulman.oms.service.WorkTaskItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class WorkTaskItemController
{
    @Autowired
    private WorkTaskItemService workTaskItemService;

    @GetMapping("/work-task-items/{id}")
    public Result findWorkTaskItemById(@PathVariable Integer id)
    {
        WorkTaskItem workTaskItem = workTaskItemService.findWorkTaskItemById(id);
        return new Result(workTaskItem);
    }

    /**
     * 是否有未审核的任务子项
     * @param workTaskId
     * @return
     */
    @GetMapping("/work-task-items/{workTaskId}/has-unaudited-items")
    public Result hasUnAuditedItems(@PathVariable Integer workTaskId)
    {
        boolean data = false;
        Map<String, Object> map = new HashMap<>();
        map.put("workTaskId", workTaskId);
        List<WorkTaskItem> workTaskItems = workTaskItemService.findWorkTaskItems(map);
        for (WorkTaskItem workTaskItem : workTaskItems)
        {
            if (workTaskItem.getState() != WorkTaskItem.STATE_AUDITED)
            {
                data = true;
                break;
            }
        }
        return new Result(data);
    }

    @GetMapping("/work-task-items")
    public Result findWorkTaskItems(Integer page, Integer limit, Integer workTaskId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("workTaskId", workTaskId);
        return workTaskItemService.findWorkTaskItemsResult(map);
    }

    @DeleteMapping("/work-task-items/{id}")
    public Result deleteWorkTaskItemById(@PathVariable Integer id)
    {
        workTaskItemService.deleteWorkTaskItemById(id);
        return Result.SUCCESS;
    }

    @PutMapping("/work-task-items")
    public Result saveWorkTaskItem(WorkTaskItem workTaskItem)
    {
        workTaskItemService.saveWorkTaskItem(workTaskItem);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/create")
    public Result create(Integer workTaskId, String workTaskNo, String name, String deviceName, String deviceLocation, String note, String executeByName, String notifyNames)
    {
        workTaskItemService.create(workTaskId, workTaskNo, name, deviceName, deviceLocation, note, executeByName, notifyNames);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/execute")
    public Result execute(Integer id)
    {
        workTaskItemService.execute(id);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/save")
    public Result save(Integer id, String vehicle, String driver, String tools, String materials, String imgs, String completion)
    {
        workTaskItemService.save(id, vehicle, driver, tools, materials, imgs, completion);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/complete")
    public Result complete(Integer taskId, Integer id, String vehicle, String driver, String tools, String materials, String deviceName, String deviceLocation, String imgs, String completion)
    {
        workTaskItemService.complete(taskId, id, vehicle, driver, tools, materials, deviceName, deviceLocation, imgs, completion);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/audit")
    public Result audit(Integer id, Integer auditResult, String auditComment)
    {
        workTaskItemService.audit(id, auditResult, auditComment);
        return Result.SUCCESS;
    }

    @PostMapping("/work-task-items/cancel")
    public Result cancel(Integer id)
    {
        workTaskItemService.cancel(id);
        return Result.SUCCESS;
    }

}
