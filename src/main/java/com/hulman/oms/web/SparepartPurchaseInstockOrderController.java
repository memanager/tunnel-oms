package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockOrder;
import com.hulman.oms.service.SparepartPurchaseInstockOrderService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class SparepartPurchaseInstockOrderController
{
    @Autowired
    private SparepartPurchaseInstockOrderService sparepartPurchaseInstockOrderService;

    @GetMapping("/sparepart-purchase-instock-orders/{id}")
    public Result findSparepartPurchaseInstockOrderById(@PathVariable Integer id)
    {
        SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder = sparepartPurchaseInstockOrderService.findSparepartPurchaseInstockOrderById(id);
        return new Result(sparepartPurchaseInstockOrder);
    }

    @GetMapping("/sparepart-purchase-instock-orders/no/{no}")
    public Result findSparepartPurchaseInstockOrderByNo(@PathVariable String no)
    {
        SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder = sparepartPurchaseInstockOrderService.findSparepartPurchaseInstockOrderByNo(no);
        return new Result(sparepartPurchaseInstockOrder);
    }

    @GetMapping("/sparepart-purchase-instock-orders")
    public Result findSparepartPurchaseInstockOrders(Integer page, Integer limit, String no, String purchaseNo, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("no", no);
        map.put("purchaseNo", purchaseNo);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return sparepartPurchaseInstockOrderService.findSparepartPurchaseInstockOrdersResult(map);
    }

    @DeleteMapping("/sparepart-purchase-instock-orders/{ids}")
    public Result deleteSparepartPurchaseInstockOrderById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            sparepartPurchaseInstockOrderService.deleteSparepartPurchaseInstockOrderById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/sparepart-purchase-instock-orders")
    public Result saveSparepartPurchaseInstockOrder(@RequestBody SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder)
    {
        sparepartPurchaseInstockOrderService.saveSparepartPurchaseInstockOrder(sparepartPurchaseInstockOrder);
        return Result.SUCCESS;
    }

}
