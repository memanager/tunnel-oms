package com.hulman.oms.web;

import com.hulman.oms.bean.Patrol;
import com.hulman.oms.bean.Result;
import com.hulman.oms.service.PatrolService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class PatrolController
{
    @Autowired
    private PatrolService patrolService;

    @GetMapping("/patrols/{id}")
    public Result findPatrolById(@PathVariable Integer id)
    {
        Patrol patrol = patrolService.findPatrolById(id);
        return new Result(patrol);
    }

    @GetMapping("/patrols")
    public Result findPatrols(Integer page, Integer limit, Integer patrolById, String patrolByName, String content, String startTime, Integer auditTimes, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("patrolById", patrolById);
        map.put("patrolByName", patrolByName);
        map.put("content", content);
        map.put("startTime", startTime);
        map.put("auditTimes", auditTimes);
        map.put("state", state);
        return patrolService.findPatrolsResult(map);
    }

    @DeleteMapping("/patrols/{ids}")
    public Result deletePatrolByIds(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            patrolService.deletePatrolById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/patrols")
    public Result savePatrol(Patrol patrol)
    {
        patrolService.savePatrol(patrol);
        return Result.SUCCESS;
    }

    @PostMapping("/patrols/create")
    public Result create(Integer shift, String vehicle, String driver)
    {
        patrolService.create(shift, vehicle, driver);
        return Result.SUCCESS;
    }

    @PostMapping("/patrols/save")
    public Result save(Integer id, String content, String routes, String imgs)
    {
        patrolService.save(id, content, routes, imgs);
        return Result.SUCCESS;
    }

    @PostMapping("/patrols/complete")
    public Result complete(Integer taskId, Integer id, String content, String routes, String imgs)
    {
        patrolService.complete(taskId, id, content, routes, imgs);
        return Result.SUCCESS;
    }

    @PostMapping("/patrols/audit")
    public Result audit(Integer taskId, Integer id, Integer auditResult, String auditComment)
    {
        patrolService.audit(taskId, id, auditResult, auditComment);
        return Result.SUCCESS;
    }

    @PostMapping("/patrols/terminate")
    public Result terminate(Integer id)
    {
        patrolService.terminate(id);
        return Result.SUCCESS;
    }

}
