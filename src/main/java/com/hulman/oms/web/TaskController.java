package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Task;
import com.hulman.oms.bean.User;
import com.hulman.oms.service.TaskService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.SubjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class TaskController
{
    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks/{id}")
    public Result findTaskById(@PathVariable Integer id)
    {
        Task task = taskService.findTaskById(id);
        return new Result(task);
    }

    @GetMapping("/tasks")
    public Result findTasks(Integer page, Integer limit, Integer procInstId, Integer ownerId, String tag, String name, Integer state, Integer desc)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("procInstId", procInstId);
        map.put("ownerId", ownerId);
        map.put("tag", tag);
        map.put("name", name);
        map.put("state", state);
        map.put("desc", desc);
        return taskService.findTasksResult(map);
    }

    @GetMapping("/my-tasks")
    public Result findMyTasks(Integer page, Integer limit, Integer ownerId, String tag, Integer state)
    {
        User user = SubjectUtil.getUser();
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("ownerId", user.getId());
        map.put("tag", tag);
        map.put("state", state);
        map.put("desc", 1);
        return taskService.findTasksResult(map);
    }

    @GetMapping("/tasks/pendings/{tag}")
    public Result findPendingTasksCount(@PathVariable String tag)
    {
        User user = SubjectUtil.getUser();
        Map<String, Object> map = new HashMap<>();
        map.put("tag", tag);
        map.put("ownerId", user.getId());
        map.put("state", 1);
        return new Result(taskService.findTasksCount(map));
    }

    @GetMapping("/tasks/pendings-count")
    public Result findAllPendingTasksCount()
    {
        User user = SubjectUtil.getUser();
        Map<String, Object> map = new HashMap<>();
        map.put("ownerId", user.getId());
        map.put("state", 1);
        return new Result(taskService.findTasksCount(map));
    }

    @GetMapping("/tasks/proc-inst-tasks")
    public Result findProcInstTasks(Integer procInstId)
    {
        List<Task> list = taskService.findProcInstTasks(procInstId);
        return new Result(list);
    }

    @DeleteMapping("/tasks/{ids}")
    public Result deleteTaskById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            taskService.deleteTaskById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/tasks")
    public Result saveTask(Task task)
    {
        taskService.saveTask(task);
        return Result.SUCCESS;
    }

}
