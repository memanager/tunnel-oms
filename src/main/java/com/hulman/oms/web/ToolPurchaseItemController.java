package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolPurchaseItem;
import com.hulman.oms.service.ToolPurchaseItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 工具采购明细(tool_purchase_item)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolPurchaseItemController {
/**
* 服务对象
*/
    @Autowired
    private ToolPurchaseItemService toolPurchaseItemService;

    @GetMapping("/tool-purchase-items/{id}")
    public Result findToolPuchaseItemById(@PathVariable Integer id)
    {
        ToolPurchaseItem toolPuchaseItem = toolPurchaseItemService.selectByPrimaryKey(id);
        return new Result(toolPuchaseItem);
    }

    @GetMapping("/tool-purchase-items")
    public Result findToolPuchaseItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return toolPurchaseItemService.findToolPurchaseItemsResult(map);
    }

    @DeleteMapping("/tool-purchase-items/{id}")
    public Result deleteToolPuchaseItemById(@PathVariable Integer id)
    {
        toolPurchaseItemService.deleteByPrimaryKey(id);
        return Result.SUCCESS;
    }

    @PutMapping("/tool-purchase-items")
    public Result saveToolPuchaseItem(ToolPurchaseItem toolPuchaseItem)
    {
        toolPurchaseItemService.saveToolPurchaseItem(toolPuchaseItem);
        return Result.SUCCESS;
    }

    @GetMapping("/tool-purchase-items/orderId/{orderId}")
    public Result findToolPuchaseItemByOrderId(@PathVariable Integer orderId)
    {
        List<ToolPurchaseItem> toolPuchaseItem = toolPurchaseItemService.selectByOrderId(orderId);
        return new Result(toolPuchaseItem);
    }

}
