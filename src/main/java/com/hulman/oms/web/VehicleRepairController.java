package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.VehicleRepair;
import com.hulman.oms.bean.VehicleUse;
import com.hulman.oms.service.VehicleRepairService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 车辆维修(vehicle_repair)表控制层
 *
 * @author xxxxx
 */
@RestController
public class VehicleRepairController
{
    /**
     * 服务对象
     */
    @Autowired
    private VehicleRepairService vehicleRepairService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/vehicle-repairs/{id}")
    public Result findVehicleRepairById(@PathVariable Integer id)
    {
        VehicleRepair vehicleRepair = vehicleRepairService.selectByPrimaryKey(id);
        return new Result(vehicleRepair);
    }

    /**
     * 送修车辆
     *
     * @return
     */
    @PostMapping("/vehicle-repairs/start")
    public Result startVehicleRepair(Integer vehicleId, String location, String content)
    {
        vehicleRepairService.send(vehicleId, location, content);
        return Result.SUCCESS;
    }

    /**
     * 接回车辆
     *
     * @return
     */
    @PostMapping("/vehicle-repairs/end")
    public Result endVehicleRepair(Integer vehicleRepairId, Double mileage)
    {
        vehicleRepairService.retrieve(vehicleRepairId, mileage);
        return Result.SUCCESS;
    }

    /**
     * 审核维修
     *
     * @return
     */
    @PostMapping("/vehicle-repairs/audit")
    public Result auditVehicleRepair(Integer vehicleRepairId, Integer auditResult, String auditComment)
    {
        vehicleRepairService.audit(vehicleRepairId, auditResult, auditComment);
        return Result.SUCCESS;
    }

    @DeleteMapping("/vehicle-repairs/{ids}")
    public Result deleteVehicleRepairById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            vehicleRepairService.deleteVehicleRepairById(id);
        }
        return Result.SUCCESS;
    }

    @GetMapping("/vehicle-repairs/in-repair-count")
    public Result findVehicleInUseCount()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("state", 1);
        Integer count = vehicleRepairService.findVehicleRepairsCount(map);
        return new Result(count);
    }

    @GetMapping("/vehicle-repairs")
    public Result findVehicleRepairs(Integer page, Integer limit, String vehicleName, String startDate, String endDate, String startDateRange, Integer driverId, String driverName, Integer state, Integer vehicleId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("vehicleName", vehicleName);
        if (!StringUtil.isEmpty(startDate))
        {
            map.put("startDate", startDate);
        }
        if (!StringUtil.isEmpty(endDate))
        {
            map.put("endDate", endDate);
        }
        if (!StringUtil.isEmpty(startDateRange))
        {
            String[] dates = startDateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        map.put("vehicleId", vehicleId);
        map.put("driverId", driverId);
        map.put("driverName", driverName);
        map.put("state", state);
        return vehicleRepairService.findVehicleRepairsResult(map);
    }

}
