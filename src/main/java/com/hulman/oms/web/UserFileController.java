package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.UserFile;
import com.hulman.oms.service.UserFileService;
import com.hulman.oms.util.FileUtil;
import com.hulman.oms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
@Slf4j
public class UserFileController
{
    @Value("${file-home}")
    private String fileHome;
    @Autowired
    private UserFileService userFileService;
    @Autowired
    private HttpServletResponse response;

    @PostConstruct
    public void init()
    {
        File fileDir = new File(fileHome);
        if (!fileDir.exists())
        {
            fileDir.mkdirs();
            log.info("Create file home:{}", fileHome);
        }
    }

    @GetMapping("/user-files/{id}")
    public Result findUserFileById(@PathVariable Integer id)
    {
        UserFile userFile = userFileService.findUserFileById(id);
        return new Result(userFile);
    }

    @GetMapping("/user-files")
    public Result findUserFiles(Integer page, Integer limit, String name, Integer userId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("userId", userId);
        map.put("name", name);
        return userFileService.findUserFilesResult(map);
    }

    @DeleteMapping("/user-files/{id}")
    public Result deleteUserFileById(@PathVariable Integer id)
    {
        UserFile userFile = userFileService.findUserFileById(id);
        if (userFile != null)
        {
            String fileName = fileHome + userFile.getStorageName();
            File file = new File(fileName);
            file.delete();
        }
        userFileService.deleteUserFileById(id);
        return Result.SUCCESS;
    }

    @GetMapping("/user-files/download/{uuid}")
    public void downloadFile(@PathVariable String uuid) throws IOException
    {
        UserFile fileInfo = userFileService.findUserFileByUuid(uuid);
        InputStream in = Files.newInputStream(new File(fileHome + fileInfo.getStorageName()).toPath());
        response.setHeader("Content-Disposition", "attachment;Filename=" + URLEncoder.encode(fileInfo.getName(), "UTF-8"));
        OutputStream os = response.getOutputStream();
        byte[] b = new byte[1024];
        int length;
        while ((length = in.read(b)) > 0)
        {
            os.write(b, 0, length);
        }
        os.close();
        in.close();
    }

    /**
     * 设备目录文件上传
     *
     * @param userId
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/user-files/upload/{userId}")
    public Result uploadUserFile(@PathVariable Integer userId, MultipartFile file) throws Exception
    {
        String name = file.getOriginalFilename();
        String ext = FileUtil.parseExtName(name);
        String uuid = StringUtil.uuid();
        UserFile userFile = new UserFile();
        userFile.setUserId(userId);
        userFile.setUuid(uuid);
        userFile.setName(name);
        userFile.setExt(ext);
        userFile.setCreateTime(new Date());
        String storageName = userFile.getStorageName();
        File fileToUpload = new File(fileHome + storageName);
        file.transferTo(fileToUpload);
        userFileService.saveUserFile(userFile);
        return Result.SUCCESS;
    }

}
