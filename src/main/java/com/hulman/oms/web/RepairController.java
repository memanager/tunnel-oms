package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.Repair;
import com.hulman.oms.service.RepairService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class RepairController
{
    @Autowired
    private RepairService repairService;

    @GetMapping("/repairs/{id}")
    public Result findRepairById(@PathVariable Integer id)
    {
        Repair repair = repairService.findRepairById(id);
        return new Result(repair);
    }

    @GetMapping("/repairs/fault/{faultId}")
    public Result findRepairByFaultId(@PathVariable Integer faultId)
    {
        Repair repair = repairService.findRepairByFaultId(faultId);
        return new Result(repair);
    }

    @GetMapping("/repairs")
    public Result findRepairs(Integer page, Integer limit, Integer deviceId, String no, String content, String createByName, String executeByName, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        map.put("no", no);
        map.put("content", content);
        map.put("createByName", createByName);
        map.put("executeByName", executeByName);
        map.put("state", state);
        return repairService.findRepairsResult(map);
    }

    @DeleteMapping("/repairs/{ids}")
    public Result deleteRepairById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            repairService.deleteRepairById(id);
        }
        return Result.SUCCESS;
    }

    /**
     * 创建维修工单
     *
     * @param faultId
     * @param content
     * @param deviceId
     * @param deviceName
     * @param directorId
     * @return
     */
    @PostMapping("/repairs/create")
    public Result create(Integer faultId, String content, Integer deviceId, String deviceName, String deviceLocation, String directorName)
    {
        repairService.create(faultId, content, deviceId, deviceName, deviceLocation, directorName);
        return Result.SUCCESS;
    }

    /**
     * 完成维修
     *
     * @param taskId
     * @param id
     * @param completion
     * @return
     */
    @PostMapping("/repairs/complete")
    public Result complete(Integer taskId, Integer id, String completion)
    {
        repairService.complete(taskId, id, completion);
        return Result.SUCCESS;
    }

    /**
     * 撤销工单
     *
     * @param id
     * @return
     */
    @PostMapping("/repairs/terminate")
    public Result terminate(Integer id)
    {
        repairService.terminate(id);
        return Result.SUCCESS;
    }

}
