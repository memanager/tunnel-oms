package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ToolInoutStock;
import com.hulman.oms.service.ToolInoutStockService;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具出入库记录(tool_inout_stock)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolInoutStockController {
/**
* 服务对象
*/
    @Autowired
    private ToolInoutStockService toolInoutStockService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/tool-inout-stocks/{id}")
    public ToolInoutStock findToolInoutStockById(Integer id) {
    return toolInoutStockService.selectByPrimaryKey(id);
    }

    @GetMapping("/tool-inout-stocks")
    public Result findToolInoutStocks(Integer page, Integer limit, Integer toolId, String name, String brand, String model, Integer orderType, String dateRange)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("toolId", toolId);
        map.put("name", name);
        map.put("brand", brand);
        map.put("model", model);
        map.put("orderType", orderType);
        if (!StringUtil.isEmpty(dateRange))
        {
            String[] dates = dateRange.split(" - ");
            map.put("startDate", dates[0]);
            map.put("endDate", dates[1]);
        }
        return toolInoutStockService.findToolInoutStocksResult(map);
    }

    @DeleteMapping("/tool-inout-stocks/{ids}")
    public Result deleteToolInoutStockById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            toolInoutStockService.deleteByPrimaryKey(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/tool-inout-stocks")
    public Result saveToolInoutStock(ToolInoutStock toolInoutStock)
    {
        toolInoutStockService.saveToolInoutStock(toolInoutStock);
        return Result.SUCCESS;
    }



}
