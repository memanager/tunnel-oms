package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.LeaderShift;
import com.hulman.oms.service.LeaderShiftService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class LeaderShiftController
{
	@Autowired
	private LeaderShiftService leaderShiftService;

	@GetMapping("/leader-shifts/{id}")
	public Result findLeaderShiftById(@PathVariable Integer id)
	{
		LeaderShift leaderShift = leaderShiftService.findLeaderShiftById(id);
		return new Result(leaderShift);
	}

	@GetMapping("/leader-shifts")
	public Result findLeaderShifts(Integer page, Integer limit, Integer leaderId1, String leaderName1, Integer leaderId2, String leaderName2)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("leaderId1", leaderId1);
        map.put("leaderName1", leaderName1);
        map.put("leaderId2", leaderId2);
        map.put("leaderName2", leaderName2);
		return leaderShiftService.findLeaderShiftsResult(map);
	}

	@DeleteMapping("/leader-shifts/{ids}")
	public Result deleteLeaderShiftById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			leaderShiftService.deleteLeaderShiftById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/leader-shifts")
	public Result saveLeaderShift(LeaderShift leaderShift)
	{
		leaderShiftService.saveLeaderShift(leaderShift);
		return Result.SUCCESS;
	}

}
