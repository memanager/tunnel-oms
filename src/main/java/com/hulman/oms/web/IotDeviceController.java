package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.IotDevice;
import com.hulman.oms.service.IoTService;
import com.hulman.oms.service.IotDeviceService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class IotDeviceController
{
    @Autowired
    private IotDeviceService iotDeviceService;
    @Autowired
    private IoTService ioTService;

    @GetMapping("/iot-devices/{id}")
    public Result findIotDeviceById(@PathVariable Integer id)
    {
        IotDevice iotDevice = iotDeviceService.findIotDeviceById(id);
        return new Result(iotDevice);
    }

    @GetMapping("/iot-devices")
    public Result findIotDevices(Integer page, Integer limit, Integer deviceId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("deviceId", deviceId);
        return iotDeviceService.findIotDevicesResult(map);
    }

    @DeleteMapping("/iot-devices/{ids}")
    public Result deleteIotDeviceById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            iotDeviceService.deleteIotDeviceById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/iot-devices")
    public Result saveIotDevice(IotDevice iotDevice)
    {
        iotDeviceService.saveIotDevice(iotDevice);
        return Result.SUCCESS;
    }

    @DeleteMapping("/iot-devices")
    public Result clearIotDevicesByDeviceId(Integer deviceId)
    {
        iotDeviceService.clearIotDevicesByDeviceId(deviceId);
        return Result.SUCCESS;
    }

    @GetMapping("/iot-stations")
    public Result findIotStations()
    {
        return new Result(ioTService.findStations());
    }

    @GetMapping("/remote-iot-devices")
    public Result findRemoteIoTDevices(Integer stationId)
    {
        return new Result(ioTService.findIotDevicesByStationId(stationId));
    }

}
