package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: maxwellens
 */
@RestController
public class NoticeController
{
    @Autowired
    private NoticeService noticeService;

    /**
     * 获取公告
     *
     * @return
     */
    @GetMapping("/notice")
    public Result findNotice()
    {
        String content = noticeService.findNotice();
        return new Result(content);
    }

    /**
     * 保存公告
     *
     * @param content
     * @return
     */
    @PostMapping("/notice")
    public Result saveNotice(String content)
    {
        noticeService.saveNotice(content);
        return Result.SUCCESS;
    }

}
