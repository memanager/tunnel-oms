package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ProcInst;
import com.hulman.oms.bean.Task;
import com.hulman.oms.service.ProcInstService;
import com.hulman.oms.service.TaskService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ProcInstController
{
    @Autowired
    private ProcInstService procInstService;
    @Autowired
    private TaskService taskService;

    @GetMapping("/proc-insts/{id}")
    public Result findProcInstById(@PathVariable Integer id)
    {
        ProcInst procInst = procInstService.findProcInstById(id);
        return new Result(procInst);
    }

    @GetMapping("/proc-insts/{id}/tasks")
    public Result findProcInstTasks(@PathVariable Integer id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("procInstId", id);
        List<Task> tasks = taskService.findTasks(map);
        Collections.reverse(tasks);
        return new Result(tasks);
    }

    @GetMapping("/proc-insts")
    public Result findProcInsts(Integer page, Integer limit, String key, String name, Integer state)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("key", key);
        map.put("name", name);
        map.put("state", state);
        return procInstService.findProcInstsResult(map);
    }

    @DeleteMapping("/proc-insts/{ids}")
    public Result deleteProcInstById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            procInstService.deleteProcInstById(id);
        }
        return Result.SUCCESS;
    }

}
