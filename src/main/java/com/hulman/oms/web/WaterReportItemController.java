package com.hulman.oms.web;

import com.hulman.oms.bean.ElectricReportItem;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.WaterReportItem;
import com.hulman.oms.service.WaterReportItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class WaterReportItemController
{
	@Autowired
	private WaterReportItemService waterReportItemService;

	@GetMapping("/water-report-items/{id}")
	public Result findWaterReportItemById(@PathVariable Integer id)
	{
		WaterReportItem waterReportItem = waterReportItemService.findWaterReportItemById(id);
		return new Result(waterReportItem);
	}

	@GetMapping("/water-report-items")
	public Result findWaterReportItems(Integer page, Integer limit, Integer reportId)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("reportId", reportId);
		return waterReportItemService.findWaterReportItemsResult(map);
	}

	@DeleteMapping("/water-report-items/{id}")
	public Result deleteWaterReportItemById(@PathVariable Integer id)
	{
		waterReportItemService.deleteWaterReportItemById(id);
		return Result.SUCCESS;
	}

	@PutMapping("/water-report-items")
	public Result saveWaterReportItem(WaterReportItem waterReportItem)
	{
		waterReportItemService.saveWaterReportItem(waterReportItem);
		return Result.SUCCESS;
	}

	@PostMapping("/water-report-items/read")
	public Result readWaterReportItem(WaterReportItem waterReportItem)
	{
		waterReportItemService.read(waterReportItem);
		return Result.SUCCESS;
	}

}
