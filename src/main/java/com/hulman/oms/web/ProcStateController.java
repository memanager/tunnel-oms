package com.hulman.oms.web;

import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.ProcState;
import com.hulman.oms.service.ProcStateService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class ProcStateController
{
	@Autowired
	private ProcStateService procStateService;

	@GetMapping("/proc-states/{id}")
	public Result findProcStateById(@PathVariable Integer id)
	{
		ProcState procState = procStateService.findProcStateById(id);
		return new Result(procState);
	}

	@GetMapping("/proc-states")
	public Result findProcStates(Integer page, Integer limit, Integer procDefId, Integer state, String name)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
        map.put("procDefId", procDefId);
        map.put("state", state);
        map.put("name", name);
		return procStateService.findProcStatesResult(map);
	}

	@DeleteMapping("/proc-states/{ids}")
	public Result deleteProcStateById(@PathVariable String ids)
	{
		int[] idArrays = NumberUtil.parseIntArray(ids);
		for (Integer id : idArrays)
		{
			procStateService.deleteProcStateById(id);
		}
		return Result.SUCCESS;
	}

	@PutMapping("/proc-states")
	public Result saveProcState(ProcState procState)
	{
		procStateService.saveProcState(procState);
		return Result.SUCCESS;
	}

}
