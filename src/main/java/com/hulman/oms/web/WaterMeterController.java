package com.hulman.oms.web;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.WaterMeter;
import com.hulman.oms.service.WaterMeterService;
import com.hulman.oms.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@RestController
public class WaterMeterController
{
    @Autowired
    private WaterMeterService waterMeterService;
    @Autowired
    private HttpServletResponse response;

    @GetMapping("/water-meters/{id}")
    public Result findWaterMeterById(@PathVariable Integer id)
    {
        WaterMeter waterMeter = waterMeterService.findWaterMeterById(id);
        return new Result(waterMeter);
    }

    @GetMapping("/water-meters")
    public Result findWaterMeters(Integer page, Integer limit, String name)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        map.put("name", name);
        return waterMeterService.findWaterMetersResult(map);
    }

    @DeleteMapping("/water-meters/{ids}")
    public Result deleteWaterMeterById(@PathVariable String ids)
    {
        int[] idArrays = NumberUtil.parseIntArray(ids);
        for (Integer id : idArrays)
        {
            waterMeterService.deleteWaterMeterById(id);
        }
        return Result.SUCCESS;
    }

    @PutMapping("/water-meters")
    public Result saveWaterMeter(WaterMeter waterMeter)
    {
        waterMeterService.saveWaterMeter(waterMeter);
        return Result.SUCCESS;
    }

    @PostMapping("/water-meters/import")
    public Result importWaterMeters(MultipartFile file) throws IOException
    {
        File tmp = new File(System.getProperty("java.io.tmpdir") + "/" + System.currentTimeMillis() + ".xlsx");
        file.transferTo(tmp);
        List<WaterMeter> meters = EasyExcel.read(tmp).head(WaterMeter.class).sheet(0).doReadSync();
        waterMeterService.importWaterMeters(meters);
        return Result.SUCCESS;
    }

    @GetMapping("/water-meters/export")
    public void exportWaterMeters(String name) throws IOException
    {
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=water-meter.xlsx");

        Map<String, Object> map = new HashMap<>();
        map.put("name", name);

        List<WaterMeter> meters = waterMeterService.findWaterMeters(map);
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        WriteSheet sheet = EasyExcel.writerSheet(0, "水表").head(WaterMeter.class).build();
        excelWriter.write(meters, sheet);
        excelWriter.finish();
    }

}
