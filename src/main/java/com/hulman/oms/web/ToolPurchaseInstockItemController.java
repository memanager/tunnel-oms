package com.hulman.oms.web;
import com.hulman.oms.bean.Result;
import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import com.hulman.oms.bean.ToolPurchaseInstockItem;
import com.hulman.oms.bean.ToolPurchaseInstockOrder;
import com.hulman.oms.service.ToolPurchaseInstockItemService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
* 工具采购入库明细(tool_purchase_instock_item)表控制层
*
* @author xxxxx
*/
@RestController
public class ToolPurchaseInstockItemController {
/**
* 服务对象
*/
    @Autowired
    private ToolPurchaseInstockItemService toolPurchaseInstockItemService;

    /**
    * 通过主键查询单条数据
    *
    * @param id 主键
    * @return 单条数据
    */
    @GetMapping("/tool-purchase-instock-item/{id}")
    public Result findToolPurchaseInstockItemById(Integer id)
    {
        ToolPurchaseInstockItem toolPurchaseInstockItem = toolPurchaseInstockItemService.selectByPrimaryKey(id);
        return new Result(toolPurchaseInstockItem);
    }

    @GetMapping("/tool-purchase-instock-items")
    public Result findToolPurchaseInstockItems(Integer page, Integer limit)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("limit", limit);
        return toolPurchaseInstockItemService.findToolPurchaseInstockItemsResult(map);
    }

    @PutMapping("/tool-purchase-instock-items")
    public Result saveToolPurchaseInstockItem(ToolPurchaseInstockItem toolPurchaseInstockItem)
    {
        toolPurchaseInstockItemService.saveToolPurchaseInstockItem(toolPurchaseInstockItem);
        return Result.SUCCESS;
    }

    @DeleteMapping("/tool-purchase-instock-items/{id}")
    public Result deleteToolPurchaseInstockItemById(@PathVariable Integer id)
    {
        toolPurchaseInstockItemService.deleteToolPurchaseInstockItemById(id);
        return Result.SUCCESS;
    }

}
