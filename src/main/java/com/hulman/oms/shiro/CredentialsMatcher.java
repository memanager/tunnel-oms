package com.hulman.oms.shiro;

import com.hulman.oms.util.JwtUtil;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * @Author: maxwellens
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher
{
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info)
    {
        JwtToken jwtToken = (JwtToken) token;
        String username = JwtUtil.getUsername(jwtToken.getToken());
        return JwtUtil.verify(jwtToken.getToken(), username, (String) info.getCredentials());
    }
}
