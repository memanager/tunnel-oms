package com.hulman.oms.shiro;

import com.hulman.oms.bean.Role;
import com.hulman.oms.bean.User;
import com.hulman.oms.service.RoleService;
import com.hulman.oms.service.UserService;
import com.hulman.oms.util.JwtUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 普通登录验证
 *
 * @Author: maxwellens
 */
public class UserRealm extends AuthorizingRealm
{
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @Override
    public boolean supports(AuthenticationToken token)
    {
        if (token instanceof JwtToken)
        {
            return true;
        } else
        {
            return false;
        }
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException
    {
        JwtToken jwtToken = (JwtToken) token;
        String username = JwtUtil.getUsername(jwtToken.getToken());
        User user = userService.findUserByAccount(username);
        return new SimpleAuthenticationInfo(user, user.getPassword(), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection)
    {
        //从凭证中获得用户名
        String username = (String) SecurityUtils.getSubject().getPrincipal();
        //根据用户名查询用户对象
        User user = userService.findUserByMobile(username);
        //查询用户拥有的角色
        List<Role> roles = roleService.findRolesByIds(user.getRoles());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        for (Role role : roles)
        {
            info.addStringPermission(role.getName());
        }
        return info;
    }

}
