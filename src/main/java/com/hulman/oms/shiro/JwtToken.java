package com.hulman.oms.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author: maxwellens
 */
public class JwtToken implements AuthenticationToken
{
    public static final String NAME = "token";

    private String token;

    public JwtToken(String token)
    {
        this.token = token;
    }

    @Override
    public Object getPrincipal()
    {
        return token;
    }

    @Override
    public Object getCredentials()
    {
        return token;
    }

    public String getToken()
    {
        return token;
    }
}