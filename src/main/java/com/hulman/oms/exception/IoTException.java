package com.hulman.oms.exception;

import com.hulman.oms.bean.Result;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统自定义异常
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IoTException extends RuntimeException
{
    /**
     * 异常编码
     */
    public Integer code;
    /**
     * 额外数据
     */
    public Object extra;

    public IoTException(String msg)
    {
        super(msg);
        this.code = Result.CODE_SERVER_ERROR;
    }

    public IoTException(Integer code, String msg)
    {
        super(msg);
        this.code = code;
    }

    public IoTException(Integer code, String msg, Object extra)
    {
        super(msg);
        this.code = code;
        this.extra = extra;
    }
}
