package com.hulman.oms.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface TaskTimeout
{
    String name() default "";

    String tag() default "";
}
