package com.hulman.oms.bean;

import lombok.Data;

/**
 * 工具采购明细
 */
@Data
public class ToolPurchaseItem {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 工具采购订单ID
    */
    private Integer toolPurchaseOrderId;

    /**
    * 工具ID
    */
    private Integer toolId;

    /**
    * 工具名称
    */
    private String name;

    /**
    * 品牌
    */
    private String brand;

    /**
    * 型号/规格
    */
    private String model;

    /**
    * 数量
    */
    private Double quantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 备注
    */
    private String note;
}