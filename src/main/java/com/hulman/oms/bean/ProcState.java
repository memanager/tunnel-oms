package com.hulman.oms.bean;

import lombok.Data;


/**
 * 流程状态
 *
 * @Author: maxwellens
 */
@Data
public class ProcState
{
    /**
     * 正常归档
     */
    public static final Integer STATE_DOCUMENTED = 0;

    /**
     * 异常结束
     */
    public static final Integer STATE_TERMINATED = -1;

    public static final String DOCUMENTED = "工单已归档";

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 流程定义ID
     */
    private Integer procDefId;

    /**
     * 状态值
     */
    private Integer state;

    /**
     * 状态名称
     */
    private String name;

}
