package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hulman.oms.util.DateUtil;
import com.hulman.oms.util.JsonUtil;
import com.hulman.oms.util.StringUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 巡检工单
 *
 * @Author: maxwellens
 */
@Data
public class Patrol
{
    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 1;

    /**
     * 审核中
     */
    public static final Integer STATE_AUDITING = 2;

    /**
     * 已归档
     */
    public static final Integer STATE_DOCUMENTED = 3;

    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 4;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 工单号
     */
    private String no;

    /**
     * 巡检人ID
     */
    private Integer patrolById;

    /**
     * 巡检人
     */
    private String patrolByName;

    /**
     * 岗位区域
     */
    private String area;

    /**
     * 班次（1、早班（7:30-15:30)、2、中班(15:30-23:30)、3、晚班(23:30-次日7:30)）
     */
    private Integer shift;

    /**
     * 班次名称
     *
     * @return
     */
    public String getShiftText()
    {
        if (shift == null)
        {
            return "";
        }
        if (shift == 1)
        {
            return "早班";
        }
        if (shift == 2)
        {
            return "中班";
        }
        if (shift == 3)
        {
            return "晚班";
        }
        return "";
    }

    /**
     * 随行车辆
     */
    private String vehicle;

    /**
     * 随行司机
     */
    private String driver;

    /**
     * 巡检内容
     */
    private String content;

    /**
     * 巡检路线
     */
    @JsonIgnore
    private String routes;

    /**
     * 现场照片
     */

    private String imgs;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        } else if (auditResult == 1)
        {
            return "合格";
        } else if (auditResult == 2)
        {
            return "不合格";
        } else
        {
            return "";
        }
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_EXECUTING)
        {
            return "执行中";
        }
        if (state == STATE_AUDITING)
        {
            return "审核中";
        }
        if (state == STATE_DOCUMENTED)
        {
            return "已归档";
        }
        if (state == STATE_TERMINATED)
        {
            return "已撤销";
        }
        return "";
    }

    public Object getRouteList()
    {
        if (StringUtil.isEmpty(routes))
        {
            routes = "[]";
        }
        return JsonUtil.parse(routes);
    }

    public String getSimpleStartTime()
    {
        return DateUtil.toSimpleTime(this.startTime);
    }

}
