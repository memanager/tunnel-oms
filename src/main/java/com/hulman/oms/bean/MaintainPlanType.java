package com.hulman.oms.bean;

import lombok.Data;


/**
 * 保养周期类型
 *
 * @Author: maxwellens
 */
@Data
public class MaintainPlanType
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 类型名称
     */
    private String name;

    /**
     * 生成月份
     */
    private String months;

}
