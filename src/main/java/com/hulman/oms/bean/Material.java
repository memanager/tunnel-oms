package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 材料
 *
 * @Author: maxwellens
 */
@Data
public class Material
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 大项
     */
    @ExcelProperty(index = 0, value = "大项")
    private String category;

    /**
     * 分项
     */
    @ExcelProperty(index = 1, value = "分项")
    private String item;

    /**
     * 名称
     */
    @ExcelProperty(index = 2, value = "备注")
    private String name;

    /**
     * 品牌
     */
    @ExcelProperty(index = 3, value = "品牌")
    private String brand;

    /**
     * 规格
     */
    @ExcelProperty(index = 4, value = "规格")
    private String spec;

    /**
     * 单位
     */
    @ExcelProperty(index = 5, value = "单位")
    private String unit;

}
