package com.hulman.oms.bean;

import lombok.Data;

import java.util.Date;

/**
 * 备件领用明细
 */
@Data
public class SparepartUseItem {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 备件领用单ID
    */
    private Integer sparepartUseOrderId;

    /**
    * 备件ID
    */
    private Integer sparepartId;

    /**
    * 备件名称
    */
    private String name;

    /**
    * 品牌
    */
    private String brand;

    /**
    * 型号/规格
    */
    private String model;

    /**
    * 数量
    */
    private Integer quantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 备注
    */
    private String note;

    /**
     * 生成出入库记录
     *
     * @return
     */
    public SparepartInoutStock toSparepartInoutStock()
    {
        SparepartInoutStock sparepartInoutStock = new SparepartInoutStock();
        sparepartInoutStock.setSparepartId(sparepartId);
        sparepartInoutStock.setName(name);
        sparepartInoutStock.setBrand(brand);
        sparepartInoutStock.setModel(model);
        sparepartInoutStock.setUnit(unit);
        sparepartInoutStock.setQuantity(0-quantity);
        sparepartInoutStock.setCreateTime(new Date());
        sparepartInoutStock.setOrderType(SparepartOrderType.USE.getType());
        return sparepartInoutStock;
    }
}