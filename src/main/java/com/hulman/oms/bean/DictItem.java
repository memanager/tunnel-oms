package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


/**
 * 字典项
 *
 * @Author: maxwellens
 */
@Data
public class DictItem
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 字典ID
     */
    @JsonIgnore
    private Integer dictId;

    /**
     * 字典键名
     */
    @JsonIgnore
    private String dictKey;

    /**
     * 字典项健
     */
    private String key;

    /**
     * 字典项值
     */
    private String value;

    /**
     * 序号
     */
    private Integer sn;

}
