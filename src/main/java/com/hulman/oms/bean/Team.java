package com.hulman.oms.bean;

import lombok.Data;


/**
 * 运维组
 *
 * @Author: maxwellens
 */
@Data
public class Team
{
    /**
     * 巡检班
     */
    public static final Integer TYPE_PATROL = 1;

    /**
     * 维修班
     */
    public static final Integer TYPE_REPAIR = 2;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 班组名
     */
    private String name;

    /**
     * 组别
     */
    private Integer type;

    /**
     * 组别显示值
     */
    public String getTypeText()
    {
        if (type == null)
        {
            return "";
        }
        if (type == TYPE_PATROL)
        {
            return "巡检班";
        }
        if (type == TYPE_REPAIR)
        {
            return "维修班";
        }
        return "";
    }

    /**
     * 组长ID
     */
    private Integer leaderId;

    /**
     * 组长姓名
     */
    private String leaderName;

}
