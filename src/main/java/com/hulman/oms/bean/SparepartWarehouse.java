package com.hulman.oms.bean;

import lombok.Data;

/**
 * 备件仓库
 */
@Data
public class SparepartWarehouse {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 仓库名称
    */
    private String name;

    /**
    * 备注
    */
    private String note;
}