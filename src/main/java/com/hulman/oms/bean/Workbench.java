package com.hulman.oms.bean;

import lombok.Data;

import java.util.List;


/**
 * 工作台
 *
 * @Author: maxwellens
 */
@Data
public class Workbench
{
    /**
     * ID
     */
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 图标
     */
    private String img;

    /**
     * 上级模块ID
     */
    private Integer parentId;

    /**
     * 页面
     */
    private String page;

    /**
     * 数字接口地址
     */
    private String badgeUrl;

    /**
     * 排序
     */
    private Integer sn;

    private Integer count;

    /**
     * 子菜单项
     */
    private List<Workbench> children;

}
