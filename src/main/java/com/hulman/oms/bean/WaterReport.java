package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 用水报表
 *
 * @Author: maxwellens
 */
@Data
public class WaterReport
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 年
     */
    private Integer year;

    /**
     * 月
     */
    private Integer month;

    /**
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 总用水量
     */
    private Integer totalAmount;

    public String getPeriod()
    {
        return year + "年" + month + "月";
    }

    public String getChineseStartDate()
    {
        return DateUtil.toChineseDate(startDate);
    }

    public String getChineseEndDate()
    {
        return DateUtil.toChineseDate(endDate);
    }

}
