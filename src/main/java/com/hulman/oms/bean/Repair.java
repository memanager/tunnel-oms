
package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 维修工单
 *
 * @Author: maxwellens
 */
@Data
public class Repair
{
    /**
     * 待执行
     */
    public static final Integer STATE_EXECUTING = 1;

    /**
     * 已归档
     */
    public static final Integer STATE_DOCUMENTED = 2;

    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 3;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 工单号
     */
    private String no;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 缺陷ID
     */
    private Integer faultId;

    /**
     * 缺陷单号
     */
    private String faultNo;

    /**
     * 维修内容
     */
    private String content;

    /**
     * 维修设备ID
     */
    private Integer deviceId;

    /**
     * 维修设备
     */
    private String deviceName;

    /**
     * 设备位置
     */
    private String deviceLocation;

    /**
     * 创建人ID
     */
    private Integer createById;

    /**
     * 创建人
     */
    private String createByName;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 负责人ID
     */
    private Integer directorId;

    /**
     * 负责人
     */
    private String directorName;

    /**
     * 完成情况
     */
    private String completion;

    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeTime;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 建筑群
     */
    private String buildingGroup;

    /**
     * 设备系统
     */
    private String deviceSystem;

    /**
     * 区域
     */
    private String area;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备简称
     */
    private String deviceShortName;

    /**
     * 设备编号
     */
    private String deviceCode;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        } else if (state == STATE_EXECUTING)
        {
            return "待执行";
        } else if (state == STATE_DOCUMENTED)
        {
            return "已归档";
        } else if (state == STATE_TERMINATED)
        {
            return "已撤销";
        }
        return "";
    }

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }
}