package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 保养工单
 *
 * @Author: maxwellens
 */
@Data
public class Maintain
{

    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 1;

    /**
     * 待审核
     */
    public static final Integer STATE_AUDITING = 2;

    /**
     * 已归档
     */
    public static final Integer STATE_DOCUMENTED = 3;

    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 4;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 工单号
     */
    private String no;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 保养计划ID
     */
    private Integer maintainPlanId;

    /**
     * 专项名称
     */
    private String name;

    /**
     * 保养单元
     */
    private Integer unit;

    /**
     * 创建人ID
     */
    private Integer createById;

    /**
     * 创建人
     */
    private String createByName;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 负责人ID
     */
    private Integer directorId;

    /**
     * 负责人
     */
    private String directorName;

    /**
     * 计划开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date planStartDate;

    /**
     * 计划结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date planEndDate;

    /**
     * 完成情况
     */
    private String completion;

    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeTime;

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核位置
     */
    private String auditLocation;

    /**
     * 审核图片
     */
    private String auditImgs;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "合格";
        }
        if (auditResult == 2)
        {
            return "不合格";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_EXECUTING)
        {
            return "执行中";
        }
        if (state == STATE_AUDITING)
        {
            return "待审核";
        }
        if (state == STATE_DOCUMENTED)
        {
            return "已归档";
        }
        if (state == STATE_TERMINATED)
        {
            return "已撤销";
        }
        return "";
    }

    public String getUnitName()
    {
        return MaintainPlan.getUnitName(unit);
    }

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }
}
