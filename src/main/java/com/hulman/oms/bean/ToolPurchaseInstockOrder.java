package com.hulman.oms.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 工具采购入库单
 */
@Data
public class ToolPurchaseInstockOrder {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 单据号 -S
    */
    private String no;

    /**
    * 采购订单号 -S
    */
    private String purchaseNo;

    /**
    * 供应商
    */
    private String supplier;

    /**
    * 入库人
    */
    private String createBy;

    /**
    * 入库时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 采购单据
     */
    private List<ToolPurchaseInstockItem> items;
}