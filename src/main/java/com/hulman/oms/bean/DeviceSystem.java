package com.hulman.oms.bean;

import lombok.Data;


/**
 * 设备系统
 *
 * @Author: maxwellens
 */
@Data
public class DeviceSystem
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 设备系统名称
     */
    private String name;

}
