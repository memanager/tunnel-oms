package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


/**
 * 备品备件
 *
 * @Author: maxwellens
 */
@Data
public class Sparepart
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 备件类型
     */
    @ExcelIgnore
    private Integer type;

    @ExcelProperty(index = 0, value = "备件类型")
    @JsonIgnore
    private String excelTypeText;

    /**
     * 备件名称
     */
    @ExcelProperty(index = 1, value = "备件名称")
    private String name;

    /**
     * 品牌
     */
    @ExcelProperty(index = 2, value = "品牌")
    private String brand;

    /**
     * 型号/规格
     */
    @ExcelProperty(index = 3, value = "型号/规格")
    private String model;

    /**
     * 数量
     */
    @ExcelProperty(index = 4, value = "数量")
    private Integer quantity;

    /**
     * 单位
     */
    @ExcelProperty(index = 5, value = "单位")
    private String unit;

    /**
     * 备注
     */
    @ExcelProperty(index = 6, value = "备注")
    private String note;

    public String getTypeText()
    {
        if (type == 1)
        {
            return "电气";
        } else if (type == 2)
        {
            return "管道";
        } else if (type == 3)
        {
            return "消防";
        } else if (type == 4)
        {
            return "五金";
        } else if (type == 5)
        {
            return "其它";
        } else
        {
            return "";
        }
    }

    public static Integer parseType(String text)
    {
        if ("电气".equals(text))
        {
            return 1;
        } else if ("管道".equals(text))
        {
            return 2;
        } else if ("消防".equals(text))
        {
            return 3;
        } else if ("五金".equals(text))
        {
            return 4;
        } else if ("其它".equals(text))
        {
            return 5;
        } else
        {
            return 5;
        }
    }

    @ExcelProperty(index = 7, value = "仓库")
    private String warehouseName;

    private Integer warehouseId;

    /**
     * 上限
     */
    private Integer upper;

    /**
     * 下限
     */
    private Integer lower;

    /**
     * 告警动态计算字段
     */
    public Integer getWarnType()
    {
        if(upper!=null && upper<quantity)
        {
            return 1;
        }
        if (lower!=null && lower>quantity)
        {
            return 2;
        }
        return 0;
    }

    public String getWarnTypeText()
    {
        if(getWarnType()!=null && getWarnType() != 0)
        {
            if (getWarnType() == 1)
            {
                return "超过上限";
            }
            if (getWarnType() == 2)
            {
                return "低于下限";
            }
        }
        return null;
    }
}
