package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UserStat
{
    private String name;

    private String roles;

    @JsonIgnore
    private Integer signState;

    private String onDuty;

    private Integer count1;

    private Integer count2;

    private Integer count3;

    private Integer count4;
}
