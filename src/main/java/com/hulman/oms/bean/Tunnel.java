package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 隧道
 *
 * @Author: maxwellens
 */
@Data
public class Tunnel
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 隧道名称
     */
    private String name;

    /**
     * 隧道地址
     */
    private String address;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 通车日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date openDate;

    /**
     * 隧道里程（单洞）
     */
    private Integer distance;

    /**
     * 面积
     */
    private Integer area;

    /**
     * 隧道长度
     */
    private Integer length;

    /**
     * 运行时间
     */
    public Long getRuntime()
    {
        if (openDate != null)
        {
            return ((new Date()).getTime() - openDate.getTime()) / (1000 * 60 * 60 * 24);
        } else
        {
            return null;
        }
    }

}
