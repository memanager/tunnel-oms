package com.hulman.oms.bean;

import lombok.Data;

@Data
public class NameValue
{
    private String name;
    private Object value;
}
