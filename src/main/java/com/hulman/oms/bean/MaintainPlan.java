package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 专项保养计划
 *
 * @Author: maxwellens
 */
@Data
public class MaintainPlan
{
    public static final Integer PERIOD_MONTH = 1;
    public static final Integer PERIOD_SEASON = 2;
    public static final Integer PERIOD_HALF_YEAR = 3;
    public static final Integer PERIOD_YEAR = 4;
    /**
     * 隧道
     */
    public static final Integer UNIT_TUNNEL = 1;
    /**
     * 区域
     */
    public static final Integer UNIT_AREA = 2;
    /**
     * 设备
     */
    public static final Integer UNIT_DEVICE = 3;

    /**
     *
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 编号
     */
    @ExcelProperty(index = 0, value = "编号")
    private String no;

    /**
     * 专项保养计划名称
     */
    @ExcelProperty(index = 1, value = "专项保养计划名称")
    private String name;

    /**
     * 保养周期
     */
    @ExcelIgnore
    private Integer period;


    /**
     * 保养周期文本
     */
    @ExcelProperty(index = 2, value = "保养周期")
    @JsonIgnore
    private String periodText;

    /**
     * 保养单元
     */
    @ExcelIgnore
    private Integer unit;

    /**
     * 保养单元文本
     */
    @ExcelProperty(index = 3, value = "保养单元")
    @JsonIgnore
    private String unitText;

    public String getPeriodName()
    {
        if (period == PERIOD_MONTH)
        {
            return "月";
        } else if (period == PERIOD_SEASON)
        {
            return "季";
        } else if (period == PERIOD_HALF_YEAR)
        {
            return "半年";
        } else if (period == PERIOD_YEAR)
        {
            return "年";
        } else
        {
            return "";
        }
    }

    public Date calcNextMaintainDate(Date date)
    {
        if (period == PERIOD_MONTH)
        {
            return DateUtil.addMonths(date, 1);
        } else if (period == PERIOD_SEASON)
        {
            return DateUtil.addMonths(date, 3);
        } else if (period == PERIOD_HALF_YEAR)
        {
            return DateUtil.addMonths(date, 6);
        } else if (period == PERIOD_YEAR)
        {
            return DateUtil.addMonths(date, 12);
        } else
        {
            return null;
        }
    }

    public static String getUnitName(Integer unit)
    {
        if (unit == UNIT_TUNNEL)
        {
            return "建筑群";
        } else if (unit == UNIT_AREA)
        {
            return "区域";
        } else if (unit == UNIT_DEVICE)
        {
            return "设备";
        } else
        {
            return "";
        }
    }

    public String getUnitName()
    {
        return getUnitName(unit);
    }

    /**
     * 根据文本解析保养周期
     *
     * @param text
     * @return
     */
    public static Integer parsePeriod(String text)
    {
        if ("月".equals(text))
        {
            return PERIOD_MONTH;
        } else if ("季".equals(text))
        {
            return PERIOD_SEASON;
        } else if ("半年".equals(text))
        {
            return PERIOD_HALF_YEAR;
        } else if ("年".equals(text))
        {
            return PERIOD_YEAR;
        } else
        {
            throw new IoTException("无法识别保养周期:" + text);
        }
    }

    public static Integer parseUnit(String text)
    {
        if ("建筑群".equals(text))
        {
            return UNIT_TUNNEL;
        } else if ("区域".equals(text))
        {
            return UNIT_AREA;
        } else if ("设备".equals(text))
        {
            return UNIT_DEVICE;
        } else
        {
            throw new IoTException("无法识别养护单元:" + text);
        }
    }

    /**
     * 下次保养
     */
    @ExcelProperty(index = 4, value = "下次保养")
    @com.alibaba.excel.annotation.format.DateTimeFormat(value = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd ")
    private Date nextMaintainDate;

    /**
     * 执行状态
     */
    @ExcelIgnore
    private Integer state;

    public String getStateText()
    {
        if (state == 0)
        {
            return "待执行";
        } else if (state == 1)
        {
            return "执行中";
        } else
        {
            return "";
        }
    }

}
