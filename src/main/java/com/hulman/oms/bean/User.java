package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hulman.oms.util.DateUtil;
import com.hulman.oms.util.NumberUtil;
import com.hulman.oms.util.StringUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


/**
 * 用户
 *
 * @Author: maxwellens
 */
@Data
public class User
{
    public static final String DEFAULT_PASSWORD = "wxaz@123456";

    public static SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MM");

    /**
     * 签到状态
     */
    public static final Integer SIGN_STATE_ON = 1;

    /**
     * 签退状态
     */
    public static final Integer SIGN_STATE_OFF = 0;

    /**
     * 启用状态
     */
    public static final Integer STATE_ENABLED = 1;

    /**
     * 禁用状态
     */
    public static final Integer STATE_DISENABLED = 0;


    /**
     *
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 姓名
     */
    @ExcelProperty(index = 0, value = "姓名")
    private String name;

    /**
     * 账号
     */
    @ExcelProperty(index = 1, value = "账号")
    private String account;

    /**
     * 字母
     */
    @ExcelIgnore
    private String letter;

    /**
     * 用户编码
     */
    @ExcelIgnore
    private String code;

    /**
     * 手机号
     */
    @ExcelProperty(index = 2, value = "手机号")
    private String mobile;

    /**
     * 密码
     */
    @JsonIgnore
    @ExcelIgnore
    private String password;

    /**
     * 角色
     */
    @ExcelIgnore
    private String roles;

    /**
     * 角色名称
     */
    @ExcelProperty(value = "角色")
    private String roleNames;

    @ExcelProperty(value = "岗位区域")
    private String area;

    /**
     * 状态
     */
    @ExcelIgnore
    private Integer isEnabled;

    /**
     * 是否值班长
     */
    @ExcelIgnore
    private Integer isShiftLeader;

    /**
     * 签到状态
     */
    @ExcelIgnore
    private Integer signState;

    /**
     * 班组ID
     */
    @ExcelIgnore
    private Integer teamId;

    /**
     * 班组姓名
     */
    @ExcelIgnore
    private String teamName;

    /**
     * 同步状态
     */
    @ExcelIgnore
    private Integer sync;

    /**
     * 性别
     */
    @ExcelProperty(value = "性别")
    private String gender;

    /**
     * 出生年月
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelProperty(value = "出生年月")
    private String birthday;

    /**
     * 学历
     */
    @ExcelProperty(value = "学历")
    private String education;

    /**
     * 状态显示值
     */
    public String getIsEnabledText()
    {
        if (isEnabled == null)
        {
            return "";
        }
        if (isEnabled == 1)
        {
            return "启用";
        }
        if (isEnabled == 0)
        {
            return "禁用";
        }
        return "";
    }

    /**
     * 姓名简称
     *
     * @return
     */
    public String getAbbrName()
    {
        return StringUtil.getAbbrName(name);
    }

    public String getLetter()
    {
        String letters = StringUtil.getPinyinFirstLetters(name);
        char l = letters.toCharArray()[0];
        String letter;
        if (l >= 65 && l <= 90)
        {
            letter = l + "";
        } else
        {
            letter = "#";
        }
        return letter;
    }

    /**
     * 是否项目经理（一级）
     *
     * @return
     */
    public boolean isManager()
    {
        if (StringUtil.isEmpty(roles))
        {
            return false;
        } else
        {
            int[] roleList = StringUtil.splitInt(roles);
            int[] targetList = new int[]{1, 2};
            return NumberUtil.hasIntersection(roleList, targetList);
        }
    }

    /**
     * 是否负责人（二级）
     *
     * @return
     */
    public boolean isDirector()
    {
        if (StringUtil.isEmpty(roles))
        {
            return false;
        } else
        {
            int[] roleList = StringUtil.splitInt(roles);
            int[] targetList = new int[]{1, 2, 3, 4, 5, 13, 14};
            return NumberUtil.hasIntersection(roleList, targetList);
        }
    }

    /**
     * 是否管理员
     *
     * @return
     */
    public boolean isAdmin()
    {
        if (StringUtil.isEmpty(roles))
        {
            return false;
        } else
        {
            int[] roleList = StringUtil.splitInt(roles);
            return NumberUtil.contains(roleList, 10);
        }
    }

    public int[] getRoleIds()
    {
        if (StringUtil.isEmpty(roles))
        {
            return new int[0];
        } else
        {
            return StringUtil.splitInt(roles);
        }
    }

    public String getSyncText()
    {
        if (sync == null)
        {
            return "";
        } else if (sync == 0)
        {
            return "未同步";
        } else if (sync == 1)
        {
            return "已同步";
        } else if (sync == 2)
        {
            return "同步失败";
        } else
        {
            return "";
        }
    }

    public Integer getAge()
    {
        if (!StringUtils.isEmpty(birthday))
        {
            try
            {
                LocalDate birthdate = yearMonthFormat.parse(birthday).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate now = LocalDate.now();
                return (now.getYear() - birthdate.getYear()) + (birthdate.getMonthValue() <= now.getMonthValue() ? 1 : 0);
            } catch (ParseException e)
            {
                return null;
            }
        } else
        {
            return null;
        }
    }

}
