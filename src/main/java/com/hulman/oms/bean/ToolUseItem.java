package com.hulman.oms.bean;

import lombok.Data;

import java.util.Date;

/**
 * 工具领用明细
 */
@Data
public class ToolUseItem {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 工具领用单ID
    */
    private Integer toolUseOrderId;

    /**
    * 工具ID
    */
    private Integer toolId;

    /**
    * 工具名称
    */
    private String name;

    /**
    * 品牌
    */
    private String brand;

    /**
    * 型号/规格
    */
    private String model;

    /**
    * 领用数量
    */
    private Integer useQuantity;

    /**
    * 归还数量
    */
    private Integer returnQuantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 备注
    */
    private String note;

    public ToolInoutStock toToolUseInoutStock()
    {
        ToolInoutStock toolInoutStock = new ToolInoutStock();
        toolInoutStock.setToolId(toolId);
        toolInoutStock.setName(name);
        toolInoutStock.setBrand(brand);
        toolInoutStock.setModel(model);
        toolInoutStock.setUnit(unit);
        toolInoutStock.setQuantity(0-useQuantity);
        toolInoutStock.setCreateTime(new Date());
        toolInoutStock.setOrderType(ToolOrderType.USE.getType());
        return toolInoutStock;
    }

    public ToolInoutStock toToolReturnInoutStock()
    {
        ToolInoutStock toolInoutStock = new ToolInoutStock();
        toolInoutStock.setToolId(toolId);
        toolInoutStock.setName(name);
        toolInoutStock.setBrand(brand);
        toolInoutStock.setModel(model);
        toolInoutStock.setUnit(unit);
        toolInoutStock.setQuantity(returnQuantity);
        toolInoutStock.setCreateTime(new Date());
        toolInoutStock.setOrderType(ToolOrderType.RETURN.getType());
        return toolInoutStock;
    }
}