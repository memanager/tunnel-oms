package com.hulman.oms.bean;

import lombok.Data;

import java.util.List;


/**
 * 保养设备
 *
 * @Author: maxwellens
 */
@Data
public class MaintainDevice
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 保养项目ID
     */
    private Integer maintainItemId;

    /**
     * 设备ID
     */
    private Integer deviceId;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备检查条目
     */
    private List<MaintainEntry> maintainEntries;

}
