package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 设备更换
 *
 * @Author: maxwellens
 */
@Data
public class DeviceReplace
{
    /**
     * 更换中
     */
    public static final Integer STATE_REPLACING = 1;

    /**
     * 更换中
     */
    public static final Integer STATE_REPLACED = 2;

    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 3;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 工单号
     */
    private String no;

    /**
     * 设备ID
     */
    private Integer deviceId;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 更换前品牌
     */
    private String brand1;

    /**
     * 更换前型号
     */
    private String model1;

    /**
     * 更换前规格
     */
    private String spec1;

    /**
     * 更换前出厂编号
     */
    private String productNo1;

    /**
     * 更换前生产日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date productDate1;

    /**
     * 更换前备注
     */
    private String note1;

    /**
     * 创建人ID
     */
    private Integer createById;

    /**
     * 创建人
     */
    private String createByName;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 更换后品牌
     */
    private String brand2;

    /**
     * 更换后型号
     */
    private String model2;

    /**
     * 更换后规格
     */
    private String spec2;

    /**
     * 更换后出厂编号
     */
    private String productNo2;

    /**
     * 更换后生产日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date productDate2;

    /**
     * 更换后备注
     */
    private String note2;

    /**
     * 更换人ID
     */
    private Integer replaceById;

    /**
     * 更换人
     */
    private String replaceByName;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 更换时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date replaceTime;

    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_REPLACING)
        {
            return "更换中";
        }
        if (state == STATE_REPLACED)
        {
            return "已更换";
        }
        if (state == STATE_TERMINATED)
        {
            return "已撤销";
        }
        return "";
    }

    public String getInfo1()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("品牌:").append(brand1).append(";");
        sb.append("型号:").append(model1).append(";");
        sb.append("规格:").append(spec1).append(";");
        sb.append("出厂编号:").append(productNo1).append(";");
        sb.append("生产日期:").append(productDate1).append(";");
        return sb.toString();
    }

    public String getInfo2()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("品牌:").append(brand2).append(";");
        sb.append("型号:").append(model2).append(";");
        sb.append("规格:").append(spec2).append(";");
        sb.append("出厂编号:").append(productNo2).append(";");
        sb.append("生产日期:").append(productDate2).append(";");
        return sb.toString();
    }

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }

}
