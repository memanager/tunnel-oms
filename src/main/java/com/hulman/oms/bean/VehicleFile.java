package com.hulman.oms.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 车辆文件
 */
@Data
public class VehicleFile {
    /**
    * 主键ID
    */
    private Integer id;

    /**
     * 车辆ID
     */
    private Integer vehicleId;

    /**
    * 人员ID
    */
    private Integer userId;

    /**
    * UUID
    */
    private String uuid;

    /**
    * 文件名 -S
    */
    private String name;

    /**
    * 扩展名
    */
    private String ext;

    /**
    * 上传时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public String getStorageName()
    {
        if (ext == null || "".equals(ext))
        {
            return uuid;
        } else
        {
            return uuid + "." + ext;
        }
    }
}