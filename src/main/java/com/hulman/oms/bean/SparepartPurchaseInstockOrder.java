package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 备件采购入库单
 *
 * @Author: maxwellens
 */
@Data
public class SparepartPurchaseInstockOrder
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 单据号
     */
    private String no;

    /**
     * 采购订单号
     */
    private String purchaseNo;

    /**
     * 供应商
     */
    private String supplier;

    /**
     * 操作人
     */
    private String createBy;

    /**
     * 单据时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 单据明细
     */
    private List<SparepartPurchaseInstockItem> items;

}
