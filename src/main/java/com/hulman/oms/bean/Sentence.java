package com.hulman.oms.bean;

import lombok.Data;


/**
 * 签到与
 *
 * @Author: maxwellens
 */
@Data
public class Sentence
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 考勤语
     */
    private String content;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 类型显示值
     */
    public String getTypeText()
    {
        if (type == null)
        {
            return "";
        }
        if (type == 1)
        {
            return "签到";
        }
        if (type == 2)
        {
            return "签退";
        }
        return "";
    }
}
