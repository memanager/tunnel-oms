package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


/**
 * 访问策略
 *
 * @Author: maxwellens
 */
@Data
public class AccessPolicy
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 访问策略名称
     */
    private String name;

    /**
     * 描述
     */
    private String note;

    /**
     * 是否选中
     */
    @JsonProperty("LAY_CHECKED")
    private Boolean checked;

}
