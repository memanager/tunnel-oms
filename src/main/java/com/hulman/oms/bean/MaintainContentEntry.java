package com.hulman.oms.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MaintainContentEntry
{
    private Integer id;

    private String name;

    private boolean checked;
}
