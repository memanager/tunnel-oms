package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 用电报表项
 *
 * @Author: maxwellens
 */
@Data
public class ElectricReportItem
{
    /**
     * 序号
     */
    @JsonIgnore
    private Integer sn;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 报表ID
     */
    private Integer reportId;

    /**
     * 电表ID
     */
    private Integer meterId;

    /**
     * 名称
     */
    private String meterName;

    /**
     * 总户号
     */
    private String meterNo;

    /**
     * 倍率
     */
    private Integer ratio;

    /**
     * 上周有功(kwh)
     */
    private Integer lastAmount;

    /**
     * 本周有功(kwh)
     */
    private Integer thisAmount;

    /**
     * 周用电量(kwh)
     */
    private Integer weeklyAmount;

    /**
     * 上周电费余数(元)
     */
    private Integer lastBalance;

    /**
     * 本周电费余数(元)
     */
    private Integer thisBalance;

    /**
     * 周用电数(元)
     */
    private Integer weeklyCost;

    /**
     * 预计日耗电数(元)
     */
    private Integer dailyCost;

    /**
     * 预计剩余用电天数
     */
    private Integer leftDays;

    /**
     * 是否需要充值
     */
    private Integer charge;

    /**
     * 是否需要充值显示值
     */
    public String getChargeText()
    {
        if (charge == null)
        {
            return "";
        }
        if (charge == 1)
        {
            return "需要";
        }
        if (charge == 2)
        {
            return "不需要";
        }
        return "";
    }

    /**
     * 抄表人
     */
    private String reader;

    /**
     * 抄表时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date readTime;

    /**
     * 现场照片
     */
    private String imgs;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 0)
        {
            return "未抄";
        }
        if (state == 1)
        {
            return "已抄";
        }
        return "";
    }
}
