package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


/**
 * 工具
 *
 * @Author: maxwellens
 */
@Data
public class Tool
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 类型
     */
    @ExcelIgnore
    private Integer type;

    @ExcelProperty(index = 0, value = "类型")
    @JsonIgnore
    private String excelTypeText;

    /**
     * 名称
     */
    @ExcelProperty(index = 1, value = "工具名称")
    private String name;

    /**
     * 品牌
     */
    @ExcelProperty(index = 2, value = "品牌")
    private String brand;

    /**
     * 型号
     */
    @ExcelProperty(index = 3, value = "规格/型号")
    private String model;

    /**
     * 数量
     */
    @ExcelProperty(index = 4, value = "数量")
    private Integer quantity;

    /**
     * 单位
     */
    @ExcelProperty(index = 5, value = "单位")
    private String unit;

    /**
     * 备注
     */
    @ExcelProperty(index = 6, value = "备注")
    private String note;

    public String getTypeText()
    {
        if (type == 1)
        {
            return "电气";
        } else if (type == 2)
        {
            return "管道";
        } else if (type == 3)
        {
            return "电动";
        } else
        {
            return "其它";
        }
    }

    public static Integer parseType(String text)
    {
        if ("电气".equals(text))
        {
            return 1;
        } else if ("管道".equals(text))
        {
            return 2;
        } else if ("电动".equals(text))
        {
            return 3;
        } else
        {
            return 4;
        }
    }

    @ExcelProperty(index = 7, value = "仓库")
    private String warehouseName;

    private Integer warehouseId;

}
