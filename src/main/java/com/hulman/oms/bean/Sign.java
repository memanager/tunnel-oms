package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 考勤记录
 *
 * @Author: maxwellens
 */
@Data
public class Sign
{
    public static final Integer STATE_CHECK_IN = 1;

    public static final Integer STATE_CHECK_OUT = 2;

    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 人员ID
     */
    @ExcelIgnore
    private Integer userId;

    /**
     * 人员姓名
     */
    @ExcelProperty(value = "人员姓名")
    private String userName;

    /**
     * 签到位置
     */
    @ExcelProperty(value = "签到位置")
    private String locationIn;

    /**
     * 签到时间
     */
    @ExcelProperty(value = "签到时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timeIn;

    /**
     * 签到经度
     */
    @ExcelIgnore
    private Double lngIn;

    /**
     * 签到纬度
     */
    @ExcelIgnore
    private Double latIn;

    /**
     * 签退位置
     */
    @ExcelProperty(value = "签退位置")
    private String locationOut;

    /**
     * 签退时间
     */
    @ExcelProperty(value = "签退时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timeOut;

    /**
     * 签退经度
     */
    @ExcelIgnore
    private Double lngOut;

    /**
     * 签退纬度
     */
    @ExcelIgnore
    private Double latOut;

    /**
     * 工作时长（秒）
     */
    @ExcelProperty(value = "工作时长（秒）")
    private Integer workingTime;

    /**
     * 备注说明
     */
    @ExcelProperty(value = "备注说明")
    private String note;

    /**
     * 考勤状态
     */
    @ExcelIgnore
    private Integer state;

    /**
     * 考勤状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "已签到";
        }
        if (state == 2)
        {
            return "已签退";
        }
        if (state == 3)
        {
            return "未签退";
        }
        return "";
    }

    public String getWorkingTimeText()
    {
        return DateUtil.toBriefChineseSeconds(workingTime);
    }
}
