package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 司机
 *
 * @Author: maxwellens
 */
@Data
public class Driver
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 姓名
     */
    @ExcelProperty(index = 0, value = "姓名")
    private String name;

    /**
     * 备注
     */
    @ExcelProperty(index = 1, value = "备注")
    private String note;

}
