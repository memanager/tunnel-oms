package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 任务工单
 *
 * @Author: maxwellens
 */
@Data
public class WorkTask
{

    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 1;

    /**
     * 已归档
     */
    public static final Integer STATE_DOCUMENTED = 2;

    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 3;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 工单号
     */
    private String no;

    /**
     * 维修设备
     */
    private String devices;

    /**
     * 位置信息
     */
    private String locations;

    /**
     * 任务内容
     */
    private String content;

    /**
     * 创建人ID
     */
    private Integer createById;

    /**
     * 创建人
     */
    private String createByName;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 完成情况
     */
    private String completion;

    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeTime;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_EXECUTING)
        {
            return "执行中";
        }
        if (state == STATE_DOCUMENTED)
        {
            return "已归档";
        }
        if (state == STATE_TERMINATED)
        {
            return "已撤销";
        }
        return "";
    }

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }
}
