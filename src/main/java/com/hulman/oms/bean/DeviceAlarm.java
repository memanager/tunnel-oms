package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 设备告警
 *
 * @Author: maxwellens
 */
@Data
public class DeviceAlarm
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 站点ID
     */
    private Integer stationId;

    /**
     * 站点名称
     */
    private String stationName;

    /**
     * 设备ID
     */
    private Integer deviceId;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 事件标识符
     */
    private String identifier;

    /**
     * 告警名称
     */
    private String name;

    /**
     * 告警级别
     */
    private Integer level;

    /**
     * 告警内容
     */
    private String msg;

    /**
     * 设备系统
     */
    private String system;

    /**
     * 发生时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 状态
     */
    private Integer state;

    private String simpleCreateTime;

    /**
     * 状态显示值
     */
    private String stateText;

    /**
     * 级别显示值
     */
    private String levelText;

}
