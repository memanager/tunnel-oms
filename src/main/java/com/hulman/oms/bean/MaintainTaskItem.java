package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 专项任务子项
 *
 * @Author: maxwellens
 */
@Data
public class MaintainTaskItem
{
    /**
     * 待执行（已创建）
     */
    public static final Integer STATE_CREATED = 1;
    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 2;

    /**
     * 审核中
     */
    public static final Integer STATE_AUDITING = 3;

    /**
     * 已审核
     */
    public static final Integer STATE_AUDITED = 4;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 专项保养任务ID
     */
    private Integer maintainTaskId;

    /**
     * 保养单元
     */
    private Integer unit;

    /**
     * 建筑群
     */
    private String tunnel;

    /**
     * 区域
     */
    private String area;

    /**
     * 设备系统
     */
    private String deviceSystem;

    /**
     * 设备代号
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 保养内容
     */
    private String content;

    /**
     * 保养结果
     */
    private Integer result;

    /**
     * 保养结果显示值
     */
    public String getResultText()
    {
        if (result == null)
        {
            return "";
        }
        if (result == 1)
        {
            return "合格";
        }
        if (result == 2)
        {
            return "不合格";
        }
        return "";
    }

    /**
     * 现场照片
     */
    private String imgs;

    /**
     * 保养情况
     */
    private String completion;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "合格";
        }
        if (auditResult == 2)
        {
            return "不合格";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "待执行";
        }
        if (state == 2)
        {
            return "执行中";
        }
        if (state == 3)
        {
            return "待审核";
        }
        if (state == 4)
        {
            return "已审核";
        }
        return "";
    }

    /**
     * 保养对象
     *
     * @return
     */
    public String getObject()
    {
        if (unit == MaintainPlan.UNIT_TUNNEL)
        {
            return tunnel;
        } else if (unit == MaintainPlan.UNIT_AREA)
        {
            return area;
        } else if (unit == MaintainPlan.UNIT_DEVICE)
        {
            return deviceName;
        } else
        {
            return "";
        }
    }
}
