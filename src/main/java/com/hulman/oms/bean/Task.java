package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hulman.oms.util.DateUtil;
import com.hulman.oms.util.JsonUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 流程任务
 *
 * @Author: maxwellens
 */
@Data
public class Task
{
    /**
     * 待完成
     */
    public static final Integer ONGOING = 1;

    /**
     * 已完成
     */
    public static final Integer COMPLETED = 2;

    /**
     * 未完成
     */
    public static final Integer UNCOMPLETED = 3;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 所有者ID
     */
    private Integer ownerId;

    /**
     * 所有者姓名
     */
    private String ownerName;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 标签
     */
    private String tag;

    /**
     * 子标签
     */
    private String subTag;

    /**
     * 表单键值
     */
    private String formKey;

    /**
     * 小程序路径
     */
    private String path;

    /**
     * PC端页面
     */
    private String page;

    /**
     * 额外信息
     */
    @JsonIgnore
    private String extras;

    private List<Extra> extraList = new ArrayList<>();

    /**
     * 任务状态
     */
    private Integer state;

    /**
     * 任务状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == ONGOING)
        {
            return "待完成";
        }
        if (state == COMPLETED)
        {
            return "已完成";
        }
        if (state == UNCOMPLETED)
        {
            return "未完成";
        }
        return "";
    }

    /**
     * 到期时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dueTime;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeTime;

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }

    public void addExtra(String name, String value)
    {
        extraList.add(new Extra(name, value));
        this.extras = JsonUtil.toJSONString(extraList);
    }

    @JsonProperty("extras")
    public Object getExtraList()
    {
        return JsonUtil.parse(extras);
    }

    public List<Extra> extraList()
    {
        return extraList;
    }

    /**
     * 任务转消息
     *
     * @return
     */
    public Msg toMsg()
    {
        Msg msg = new Msg();
        String tag = this.getTag();
        if (Msg.TYPE_SYS.equals(tag))
        {
            msg.setType(Msg.TYPE_SYS);
        } else
        {
            msg.setType(Msg.TYPE_OM);
        }
        msg.setOwnerId(this.getOwnerId());
        msg.setOwnerName(this.getOwnerName());
        msg.setTitle(this.getName());
        this.extraList().stream().forEach(extra ->
        {
            msg.addItem(extra.getName(), extra.getValue());
        });
        msg.setCreateTime(this.getCreateTime());
        msg.setPath(this.getPath());
        return msg;
    }

    @Data
    public static class Extra
    {
        private String name;

        private String value;

        public Extra(String name, String value)
        {
            this.name = name;
            this.value = value;
        }
    }

}
