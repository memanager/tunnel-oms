package com.hulman.oms.bean;

import lombok.Data;

import java.util.Date;

/**
 * 工具采购入库明细
 */
@Data
public class ToolPurchaseInstockItem {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 工具采购入库单ID
    */
    private Integer toolPurchaseOrderId;

    /**
    * 工具ID
    */
    private Integer toolId;

    /**
    * 工具名称
    */
    private String name;

    /**
    * 品牌
    */
    private String brand;

    /**
    * 型号/规格
    */
    private String model;

    /**
    * 数量
    */
    private Integer quantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 出厂编号
    */
    private String productNo;

    /**
    * 生产日期
    */
    private String productDate;

    /**
    * 备注
    */
    private String note;

    public ToolInoutStock toToolInoutStock()
    {
        ToolInoutStock toolInoutStock = new ToolInoutStock();
        toolInoutStock.setToolId(toolId);
        toolInoutStock.setName(name);
        toolInoutStock.setBrand(brand);
        toolInoutStock.setModel(model);
        toolInoutStock.setUnit(unit);
        toolInoutStock.setQuantity(quantity);
        toolInoutStock.setCreateTime(new Date());
        toolInoutStock.setOrderType(ToolOrderType.PURCHASE.getType());
        return toolInoutStock;
    }
}