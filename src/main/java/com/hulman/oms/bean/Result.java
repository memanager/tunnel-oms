package com.hulman.oms.bean;

import lombok.Data;

@Data
public class Result
{
    /**
     * 默认
     */
    public static final Integer CODE_DEFAULT = 0;
    /**
     * 成功
     */
    public static final Integer CODE_SUCCESS = 200;
    /**
     * 未授权
     */
    public static final Integer CODE_UNAUTHORIZED = 401;
    /**
     * 服务器错误
     */
    public static final Integer CODE_SERVER_ERROR = 500;
    /**
     * 部分错误
     */
    public static final Integer CODE_PARTIAL_ERROR = 501;

    public static final Result SUCCESS = new Result(CODE_SUCCESS, "", null);

    private int code = 0;
    private int count;
    private String msg = "";
    private Object data;

    public Result(Object data, int count)
    {
        this.data = data;
        this.count = count;
    }

    public Result(int code, String msg, Object data)
    {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result(String msg, Object data)
    {
        this.msg = msg;
        this.data = data;
    }

    public Result(int code, Object data)
    {
        this.code = code;
        this.data = data;
    }

    public Result(Object data)
    {
        this.code = 0;
        this.msg = "";
        this.data = data;
    }

}
