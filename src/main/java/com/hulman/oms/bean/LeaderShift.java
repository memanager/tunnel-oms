package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 值班长交接
 *
 * @Author: maxwellens
 */
@Data
public class LeaderShift
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 交接人ID
     */
    private Integer leaderId1;

    /**
     * 交接人
     */
    private String leaderName1;

    /**
     * 承接人ID
     */
    private Integer leaderId2;

    /**
     * 承接人
     */
    private String leaderName2;

    /**
     * 交接时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date shiftTime;

}
