package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 备件出入库记录
 *
 * @Author: maxwellens
 */
@Data
public class SparepartInoutStock
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 备件ID
     */
    private Integer sparepartId;

    /**
     * 备件名称
     */
    private String name;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 型号/规格
     */
    private String model;

    /**
     * 出入库数量
     */
    private Integer quantity;

    /**
     * 单位
     */
    private String unit;

    /**
     * 单据号
     */
    private String orderNo;

    /**
     * 单据类型
     */
    private Integer orderType;

    /**
     * 出入库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public String getOrderTypeText()
    {
        return SparepartOrderType.getText(orderType);
    }

    public String getDisplayQuantity()
    {
        if (quantity > 0)
        {
            return "+" + quantity;
        } else
        {
            return quantity + "";
        }
    }

}
