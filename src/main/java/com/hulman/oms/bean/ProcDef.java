package com.hulman.oms.bean;

import lombok.Data;


/**
 * 流程定义
 *
 * @Author: maxwellens
 */
@Data
public class ProcDef
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 键值
     */
    private String key;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String note;


}
