package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 物联设备
 *
 * @Author: maxwellens
 */
@Data
public class IotDevice
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 设备ID
     */
    private Integer deviceId;

    @ExcelProperty(index = 0, value = "设备编码")
    private String deviceCode;

    /**
     * 物联设备名称
     */
    @ExcelProperty(index = 1, value = "物联设备名称")
    private String iotName;

    /**
     * 物联设备编码
     */
    @ExcelProperty(index = 2, value = "物联设备编码")
    private String iotCode;

}
