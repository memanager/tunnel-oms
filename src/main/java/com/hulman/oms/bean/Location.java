package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 位置
 *
 * @Author: maxwellens
 */
@Data
public class Location
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 隧道ID
     */
    @ExcelIgnore
    private Integer tunnelId;

    /**
     * 隧道名称
     */
    @ExcelProperty(index = 0, value = "隧道名称")
    private String tunnelName;

    /**
     * 位置名称
     */
    @ExcelProperty(index = 1, value = "位置名称")
    private String name;

    /**
     * 位置简称
     */
    @ExcelProperty(index = 2, value = "位置简称")
    private String abbr;

    /**
     * 位置编码
     */
    @ExcelProperty(index = 3, value = "位置编码")
    private String code;

}
