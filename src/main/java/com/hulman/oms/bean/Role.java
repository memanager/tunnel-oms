package com.hulman.oms.bean;

import lombok.Data;


/**
 * 角色
 *
 * @Author: maxwellens
 */
@Data
public class Role
{
    /**
     * 经理级
     */
    public static final Integer LEVEL_MANAGER = 1;

    /**
     * 负责人
     */
    public static final Integer LEVEL_DIRECTOR = 2;

    /**
     * 班长级
     */
    public static final Integer LEVEL_LEADER = 3;

    /**
     * 工人级
     */
    public static final Integer LEVEL_WORKER = 4;

    /**
     *
     */
    private Integer id;
    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色等级
     */
    private Integer level;
    /**
     * 备注
     */
    private String note;
    /**
     * 菜单项
     */
    private String menus;
    /**
     * 工作台
     */
    private String workbenches;
    /**
     * 访问控制
     */
    private String accessPolicies;

    /**
     * 级别名称
     *
     * @return
     */
    public String getLevelName()
    {
        if (level == null)
        {
            return "";
        } else if (level == LEVEL_MANAGER)
        {
            return "经理级";
        } else if (level == LEVEL_DIRECTOR)
        {
            return "负责人";
        } else if (level == LEVEL_LEADER)
        {
            return "班长级";
        } else if (level == LEVEL_WORKER)
        {
            return "工人级";
        } else
        {
            return "";
        }
    }

}
