package com.hulman.oms.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 车辆加油
 */
@Data
public class VehicleRefuel
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 车辆ID
     */
    private Integer vehicleId;

    /**
     * 车牌号
     */
    private String vehicleName;

    /**
     * 加油卡号
     */
    private String fuelCard;

    /**
     * 油号
     */
    private String fuelType;

    /**
     * 加油量（升）
     */
    private Double amount;

    /**
     * 加油里程
     */
    private Double mileage;

    /**
     * 上次余额
     */
    private Double lastBalance;

    /**
     * 加油金额
     */
    private Double expense;

    /**
     * 本次余额
     */
    private Double thisBalance;

    /**
     * 驾驶员ID
     */
    private Integer driverId;

    /**
     * 驾驶员
     */
    private String driverName;

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "通过";
        }
        if (auditResult == 2)
        {
            return "退回";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 状态 -E1:待审核,2:已审核 -S
     */
    private Integer state;

    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "待审核";
        }
        if (state == 2)
        {
            return "已审核";
        }
        return "";
    }

    /**
     * 加油时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}