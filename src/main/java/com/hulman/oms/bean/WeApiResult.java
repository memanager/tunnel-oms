package com.hulman.oms.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 微信API接口返回结果
 */
@Data
public class WeApiResult
{
    private Integer errCode;

    private String errMsg;

    private Map<String, Object> data = new LinkedHashMap<>();

    public WeApiResult(Integer errCode, String errMsg)
    {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
}
