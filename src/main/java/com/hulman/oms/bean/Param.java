package com.hulman.oms.bean;

import lombok.Data;


/**
 * 参数设置
 *
 * @Author: maxwellens
 */
@Data
public class Param
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 参数名称
     */
    private String name;

    /**
     * 参数健名
     */
    private String key;

    /**
     * 参数健值
     */
    private String value;

    /**
     * 备注
     */
    private String remark;

}
