package com.hulman.oms.bean;

import lombok.Data;

import java.util.List;


/**
 * 资源
 *
 * @Author: maxwellens
 */
@Data
public class Menu
{
    /**
     * ID
     */
    private Integer id;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源图标
     */
    private String icon;

    /**
     * 上级资源ID
     */
    private Integer parentId;

    /**
     * 资源链接地址
     */
    private String url;

    /**
     * 排序
     */
    private Integer sn;

    /**
     * 子菜单项
     */
    private List<Menu> children;

}
