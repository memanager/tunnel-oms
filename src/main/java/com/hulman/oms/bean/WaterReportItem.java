package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 用水报表项
 *
 * @Author: maxwellens
 */
@Data
public class WaterReportItem
{
    /**
     * 序号
     */
    @JsonIgnore
    private Integer sn;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 报表ID
     */
    private Integer reportId;

    /**
     * 水表ID
     */
    private Integer meterId;

    /**
     * 名称
     */
    private String meterName;

    /**
     * 水表直径
     */
    private String diameter;

    /**
     * 倍率
     */
    private Integer ratio;

    /**
     * 上周数量
     */
    private Integer lastAmount;

    /**
     * 本周数量
     */
    private Integer thisAmount;

    /**
     * 实用数量
     */
    private Integer useAmount;

    /**
     * 抄表人
     */
    private String reader;

    /**
     * 抄表时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date readTime;

    /**
     * 现场照片
     */
    private String imgs;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 0)
        {
            return "待抄";
        }
        if (state == 1)
        {
            return "已抄";
        }
        return "";
    }
}
