package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 大屏接口
 *
 * @Author: maxwellens
 */
@Data
public class ScadaReport
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 标识符
     */
    private String identifier;

    /**
     * 名称
     */
    private String name;

    /**
     * 备注
     */
    private String note;

    /**
     * 脚本
     */
    private String sql;

}
