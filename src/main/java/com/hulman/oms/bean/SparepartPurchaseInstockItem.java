package com.hulman.oms.bean;

import lombok.Data;

import java.util.Date;


/**
 * 备件采购入库明细
 *
 * @Author: maxwellens
 */
@Data
public class SparepartPurchaseInstockItem
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 备件采购入库单ID
     */
    private Integer sparepartPurchaseOrderId;

    /**
     * 备件ID
     */
    private Integer sparepartId;

    /**
     * 备件名称
     */
    private String name;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 型号/规格
     */
    private String model;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 单位
     */
    private String unit;

    /**
     * 出厂编号
     */
    private String productNo;

    /**
     * 生产日期
     */
    private String productDate;

    /**
     * 备注
     */
    private String note;

    /**
     * 生成出入库记录
     *
     * @return
     */
    public SparepartInoutStock toSparepartInoutStock()
    {
        SparepartInoutStock sparepartInoutStock = new SparepartInoutStock();
        sparepartInoutStock.setSparepartId(sparepartId);
        sparepartInoutStock.setName(name);
        sparepartInoutStock.setBrand(brand);
        sparepartInoutStock.setModel(model);
        sparepartInoutStock.setUnit(unit);
        sparepartInoutStock.setQuantity(quantity);
        sparepartInoutStock.setCreateTime(new Date());
        sparepartInoutStock.setOrderType(SparepartOrderType.PURCHASE.getType());
        return sparepartInoutStock;
    }

}
