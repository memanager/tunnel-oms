package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 设备附件
 *
 * @Author: maxwellens
 */
@Data
public class Accessory
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    @ExcelIgnore
    private Integer tunnelId;


    /**
     * 建筑群
     */
    @ExcelProperty(value = "建筑群")
    private String tunnelName;

    /**
     * 设备系统
     */
    @ExcelProperty(value = "设备系统")
    private String system;

    /**
     * 设备类型
     */
    @ExcelProperty(value = "设备类型")
    private String deviceType;

    /**
     * 设备ID
     */
    @ExcelIgnore
    private Integer deviceId;

    /**
     * 设备名称
     */
    @ExcelProperty(value = "设备名称")
    private String deviceName;

    /**
     * 设备编号
     */
    @ExcelProperty(index = 0, value = "设备编号")
    private String deviceCode;

    /**
     * 设备代号
     */
    @ExcelProperty(index = 1, value = "设备代号")
    private String no;

    /**
     * 附件名称
     */
    @ExcelProperty(index = 2, value = "附件名称")
    private String name;

    /**
     * 品牌
     */
    @ExcelProperty(index = 3, value = "品牌")
    private String brand;

    /**
     * 型号/规格
     */
    @ExcelProperty(index = 4, value = "型号/规格")
    private String model;

    /**
     * 数量
     */
    @ExcelProperty(index = 5, value = "数量")
    private Double quantity;

    /**
     * 单位
     */
    @ExcelProperty(index = 6, value = "单位")
    private String unit;

    /**
     * 出厂编号
     */
    @ExcelProperty(index = 7, value = "出厂编号")
    private String productNo;

    /**
     * 生产日期
     */
    @ExcelProperty(index = 8, value = "生产日期")
    private String productDate;

    /**
     * 使用年限
     */
    @ExcelProperty(index = 9, value = "使用年限")
    private Integer serviceYears;

    /**
     * 备注
     */
    @ExcelProperty(index = 10, value = "备注")
    private String note;

    /**
     * 区域
     */
    @ExcelProperty(index = 11, value = "区域")
    private String locationName;

    /**
     * 设备简称
     */
    @ExcelProperty(index = 12, value = "设备简称")
    private String abbr;

    /**
     * 设备编码
     */
    @ExcelIgnore
    private String code;


}
