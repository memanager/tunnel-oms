package com.hulman.oms.bean;

import lombok.Data;


/**
 * 字典
 *
 * @Author: maxwellens
 */
@Data
public class Dict
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 字典名称
     */
    private String name;

    /**
     * 字典键名
     */
    private String key;

    /**
     * 备注
     */
    private String remark;

}
