package com.hulman.oms.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 工具领用单
 */
@Data
public class ToolUseOrder {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 单据号 -S
    */
    private String no;

    /**
    * 领用人
    */
    private String useBy;

    /**
    * 领用原因
    */
    private String purpose;

    /**
    * 领用操作人
    */
    private String useCreateBy;

    /**
    * 领用时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date useTime;

    /**
    * 归还人
    */
    private String returnBy;

    /**
    * 归还操作人
    */
    private String returnCreateBy;

    /**
    * 归还时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date returnTime;

    /**
    * 状态
    */
    private Integer state;

    /**
     * 领用单据
     */
    private List<ToolUseItem> items;

    public String getStateText()
    {
        if(state == 1)
        {
            return "待归还";
        }else if(state == 2)
        {
            return "已归还";
        }
        return null;
    }
}