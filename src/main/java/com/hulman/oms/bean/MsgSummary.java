package com.hulman.oms.bean;

import lombok.Data;

/**
 * 消息摘要
 */
@Data
public class MsgSummary
{
    private Integer count;

    private String note;

    private String time;
}
