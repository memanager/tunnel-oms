
package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 缺陷工单
 *
 * @Author: maxwellens
 */
@Data
public class Fault {
    /**
     * 创建中
     */
    public static final Integer STATE_CREATING = 0;
    /**
     * 待处置
     */
    public static final Integer STATE_HANDLING = 1;
    /**
     * 待维修
     */
    public static final Integer STATE_TO_REPAIR = 2;
    /**
     * 维修中
     */
    public static final Integer STATE_REPAIRING = 3;
    /**
     * 待审核
     */
    public static final Integer STATE_AUDITING = 4;
    /**
     * 已归档
     */
    public static final Integer STATE_DOCUMENTED = 5;
    /**
     * 已撤销
     */
    public static final Integer STATE_TERMINATED = 6;

    /**
     * 当场处置
     */
    public static final Integer FAULT_HANDLE_IMMEDIATE = 1;

    /**
     * 上报处置
     */
    public static final Integer FAULT_HANDLE_REPORT = 2;

    /**
     * 人工上报
     */
    public static final Integer SOURCE_MANUAL = 1;

    /**
     * 物联上报
     */
    public static final Integer SOURCE_IOT = 2;

    /**
     * AI上报
     */
    public static final Integer SOURCE_AI = 3;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 工单号
     */
    private String no;

    /**
     * 流程实例ID
     */
    private Integer procInstId;

    /**
     * 缺陷描述
     */
    private String content;

    /**
     * 缺陷来源
     */
    private Integer source;

    /**
     * 设备告警ID
     */
    private Integer alarmId;

    /**
     * 缺陷设备ID
     */
    private Integer deviceId;

    /**
     * 缺陷设备
     */
    private String deviceName;

    /**
     * 设备地址
     */
    private String deviceLocation;

    /**
     * 上报方式
     */
    private Integer faultHandle;

    /**
     * 上报方式显示值
     */
    public String getFaultHandleText() {
        if (faultHandle == null) {
            return "";
        }
        if (faultHandle == 1) {
            return "当场处置";
        }
        if (faultHandle == 2) {
            return "上报处置";
        }
        return "";
    }

    /**
     * 缺陷照片
     */
    private String faultImgs;

    /**
     * 上报人ID
     */
    private Integer reportById;

    /**
     * 上报人
     */
    private String reportByName;

    /**
     * 上报时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date reportTime;

    /**
     * 巡检班组ID
     */
    private Integer teamId;

    /**
     * 巡检班组
     */
    private String teamName;

    /**
     * 班组长ID
     */
    private Integer teamLeaderId;

    /**
     * 班组长
     */
    private String teamLeaderName;

    /**
     * 缺陷等级
     */
    private Integer level;

    /**
     * 缺陷等级显示值
     */
    public String getLevelText() {
        if (level == null) {
            return "";
        } else if (level == 1) {
            return "一般";
        } else if (level == 2) {
            return "严重";
        }
        return "";
    }

    /**
     * 维修方式
     */
    private Integer repairHandle;

    /**
     * 相关检修工单ID
     */
    private Integer repairId;

    /**
     * 维修方式显示值
     */
    public String getRepairHandleText() {
        if (repairHandle == null) {
            return "";
        } else if (repairHandle == 1) {
            return "立即维修";
        } else if (repairHandle == 2) {
            return "稍后维修";
        }
        return "";
    }

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核位置
     */
    private String auditLocation;

    /**
     * 审核图片
     */
    private String auditImgs;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText() {
        if (auditResult == null) {
            return "";
        } else if (auditResult == 1) {
            return "合格";
        } else if (auditResult == 2) {
            return "不合格";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText() {
        if (state == null) {
            return "";
        } else if (state == STATE_HANDLING) {
            return "待处置";
        } else if (state == STATE_TO_REPAIR) {
            return "待维修";
        } else if (state == STATE_REPAIRING) {
            return "维修中";
        } else if (state == STATE_AUDITING) {
            return "待审核";
        } else if (state == STATE_DOCUMENTED) {
            return "已归档";
        } else if (state == STATE_TERMINATED) {
            return "已撤销";
        }
        return "";
    }

    public String getSourceText() {
        if (source == null) {
            return "";
        } else if (SOURCE_MANUAL.equals(source)) {
            return "人工上报";
        } else if (SOURCE_IOT.equals(source)) {
            return "物联上报";
        } else if (SOURCE_AI.equals(source)) {
            return "AI上报";
        } else {
            return "";
        }
    }

    public String getSimpleReportTime() {
        return DateUtil.toSimpleTime(this.reportTime);
    }

    /**
     * 建筑群
     */
    private String buildingGroup;

    /**
     * 设备系统
     */
    private String deviceSystem;

    /**
     * 区域
     */
    private String area;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备简称
     */
    private String deviceShortName;

    /**
     * 维修内容
     */
    private String repairContent;
}