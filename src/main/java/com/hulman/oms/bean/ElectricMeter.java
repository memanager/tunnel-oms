package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 电表
 *
 * @Author: maxwellens
 */
@Data
public class ElectricMeter
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 名称
     */
    @ExcelProperty(index = 0, value = "名称")
    private String name;

    /**
     * 总户号
     */
    @ExcelProperty(index = 1, value = "总户号")
    private String no;

    /**
     * 倍率
     */
    @ExcelProperty(index = 2, value = "倍率")
    private Integer ratio;

    /**
     * 最近读数
     */
    @ExcelProperty(index = 3, value = "最近读数")
    private Integer amount;

    /**
     * 最近余额
     */
    @ExcelProperty(index = 4, value = "最近余额")
    private Integer balance;

    /**
     * 最近抄表时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelIgnore
    private Date readTime;

}
