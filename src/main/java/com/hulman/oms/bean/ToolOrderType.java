package com.hulman.oms.bean;

import lombok.Getter;

@Getter
public enum ToolOrderType
{
    PURCHASE(1, "采购入库"), USE(2, "领用出库"), RETURN(3, "归还入库");

    private int type;

    private String text;

    ToolOrderType(int type, String text)
    {
        this.type = type;
        this.text = text;
    }

    public static String getText(int type)
    {
        for (ToolOrderType toolOrderType : ToolOrderType.values())
        {
            if (toolOrderType.getType() == type)
            {
                return toolOrderType.getText();
            }
        }
        return null;
    }

};
