package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.StringUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 专项任务
 *
 * @Author: maxwellens
 */
@Data
public class MaintainTask
{
    /**
     * 待执行
     */
    public static final Integer STATE_EXECUTING = 1;

    /**
     * 已执行
     */
    public static final Integer STATE_EXECUTED = 2;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 专项保养单ID
     */
    private Integer maintainId;

    /**
     * 专项名称
     */
    private String name;

    /**
     * 保养单元
     */
    private Integer unit;

    /**
     * 建筑群
     */
    private String tunnels;

    /**
     * 区域
     */
    private String areas;

    /**
     * 设备系统
     */
    private String deviceSystem;

    /**
     * 执行人ID
     */
    private Integer executeById;

    /**
     * 执行人
     */
    private String executeByName;

    /**
     * 通知人
     */
    private String notifyNames;

    /**
     * 随行车辆
     */
    private String vehicle;

    /**
     * 随行司机
     */
    private String driver;

    /**
     * 创建日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 完成日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeTime;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_EXECUTING)
        {
            return "未完成";
        }
        if (state == STATE_EXECUTED)
        {
            return "已完成";
        }
        return "";
    }

    public String getUnitName()
    {
        return MaintainPlan.getUnitName(unit);
    }

    /**
     * 保养对象
     *
     * @return
     */
    public String getObject()
    {
        if (unit == MaintainPlan.UNIT_TUNNEL)
        {
            return tunnels;
        } else if (unit == MaintainPlan.UNIT_AREA)
        {
            return areas;
        } else if (unit == MaintainPlan.UNIT_DEVICE)
        {
            return deviceSystem;
        } else
        {
            return "";
        }
    }

}
