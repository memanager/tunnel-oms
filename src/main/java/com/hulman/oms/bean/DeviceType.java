package com.hulman.oms.bean;

import lombok.Data;


/**
 * 设备类型
 *
 * @Author: maxwellens
 */
@Data
public class DeviceType
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 设备类型名称
     */
    private String name;

}
