package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.StringUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 设备
 *
 * @Author: maxwellens
 */
@Data
public class Device
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 设备名称
     */
    @ExcelProperty(index = 0, value = "设备名称")
    private String name;

    /**
     * 设备简称
     */
    @ExcelProperty(index = 1, value = "设备简称")
    private String abbr;

    /**
     * 设备编码
     */
    @ExcelProperty(index = 2, value = "设备编码")
    private String code;

    /**
     * 设备代号
     */
    @ExcelProperty(index = 3, value = "设备代号")
    private String no;

    /**
     * 设备系统
     */
    @ExcelProperty(index = 4, value = "设备系统")
    private String system;

    /**
     * 设备类型
     */
    @ExcelProperty(index = 5, value = "设备类型")
    private String type;


    /**
     * 隧道ID
     */
    @ExcelIgnore
    private Integer tunnelId;

    /**
     * 建筑群
     */
    @ExcelProperty(index = 6, value = "建筑群")
    private String tunnelName;

    /**
     * 区域ID
     */
    private Integer locationId;

    /**
     * 区域
     */
    @ExcelProperty(index = 7, value = "区域")
    private String locationName;


    /**
     * 品牌
     */
    @ExcelProperty(index = 8, value = "品牌")
    private String brand;

    /**
     * 型号
     */
    @ExcelProperty(index = 9, value = "型号")
    private String model;

    /**
     * 规格
     */
    @ExcelProperty(index = 10, value = "规格")
    private String spec;

    /**
     * 出厂编号
     */
    @ExcelProperty(index = 11, value = "出厂编号")
    private String productNo;

    /**
     * 生产日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(index = 12, value = "生产日期")
    private Date productDate;

    /**
     * 物联设备
     */
    @ExcelProperty(index = 13, value = "物联设备")
    private Integer isIot;

    /**
     * 上级设备
     */
    @ExcelProperty(index = 14, value = "上级设备")
    private String supCodes;

    /**
     * 下级设备
     */
    @ExcelProperty(index = 15, value = "下级设备")
    private String subCodes;

    /**
     * 运维次数
     */
    @ExcelProperty(index = 16, value = "运维次数")
    private Integer maintainTimes;

    /**
     * 最近运维日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(index = 17, value = "最近运维日期")
    private Date lastMaintainDate;

    /**
     * 计划运维日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(index = 18, value = "计划运维日期")
    private Date nextMaintainDate;

    public String getIotText()
    {
        if (isIot == null)
        {
            return "";
        } else if (isIot == 1)
        {
            return "是";
        } else if (isIot == 0)
        {
            return "否";
        } else
        {
            return "";
        }
    }

    /**
     * locationName别名，兼容低版本小程序端格式
     *
     * @return
     */
    public String getLocation()
    {
        return locationName;
    }

    /**
     * 信息简介
     *
     * @return
     */
    public String info()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("品牌:").append(brand).append(";");
        sb.append("型号:").append(model).append(";");
        sb.append("规格:").append(spec).append(";");
        sb.append("出厂编号:").append(productNo).append(";");
        sb.append("生产日期:").append(productDate).append(";");
        return sb.toString();
    }

    /**
     * 使用年限
     */
    private Integer serviceYears;

    /**
     * 备注
     */
    private String note;

    /**
     * 设备状态
     */
    private Integer state;

    public String getStateText()
    {
        if (state == 0)
        {
            return "禁用";
        } else if (state == 1)
        {
            return "运行";
        } else if (state == 2)
        {
            return "维修";
        } else
        {
            return "";
        }
    }


}
