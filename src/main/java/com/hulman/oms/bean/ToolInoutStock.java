package com.hulman.oms.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 工具出入库记录
 */
@Data
public class ToolInoutStock {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 工具ID -S
    */
    private Integer toolId;

    /**
    * 工具名称 -S
    */
    private String name;

    /**
    * 品牌 -S
    */
    private String brand;

    /**
    * 型号/规格 -S
    */
    private String model;

    /**
    * 出入库数量
    */
    private Integer quantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 单据号
    */
    private String orderNo;

    /**
    * 单据类型 -S
    */
    private Integer orderType;

    /**
    * 出入库时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public String getOrderTypeText()
    {
        return SparepartOrderType.getText(orderType);
    }

    public String getDisplayQuantity()
    {
        if (quantity > 0)
        {
            return "+" + quantity;
        } else
        {
            return quantity + "";
        }
    }
}