package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 水表
 *
 * @Author: maxwellens
 */
@Data
public class WaterMeter
{
    public static final Integer DEFAULT_RATIO = 1;
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 名称
     */
    @ExcelProperty(index = 0, value = "名称")
    private String name;

    /**
     * 水表直径
     */
    @ExcelProperty(index = 1, value = "水表直径")
    private String diameter;

    /**
     * 倍率
     */
    @ExcelProperty(index = 2, value = "倍率")
    private Integer ratio;

    /**
     * 最近数量
     */
    @ExcelProperty(index = 3, value = "最近数量")
    private Integer amount;

    /**
     * 最近抄表时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date readTime;

}
