package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 保养项目
 *
 * @Author: maxwellens
 */
@Data
public class MaintainItem
{

    /**
     * 待分配
     */
    public static final Integer STATE_ASSIGNING = 0;
    /**
     * 待发布
     */
    public static final Integer STATE_PUBLISHING = 1;

    /**
     * 待执行（准备开始）
     */
    public static final Integer STATE_STARTING = 2;

    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 3;

    /**
     * 待审核
     */
    public static final Integer STATE_AUDITING = 4;

    /**
     * 已归档
     */
    public static final Integer STATE_AUDITED = 5;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 工单ID
     */
    private Integer maintainId;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 工人ID
     */
    private Integer workerId;

    /**
     * 工人
     */
    private String workerName;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 随行车辆
     */
    private String vehicle;

    /**
     * 随行司机
     */
    private String driver;

    /**
     * 现场照片
     */
    private String imgs;

    /**
     * 完成情况
     */
    private String completion;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 保养设备
     */
    private List<MaintainDevice> maintainDevices;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "合格";
        }
        if (auditResult == 2)
        {
            return "不合格";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_ASSIGNING)
        {
            return "待分配";
        }
        if (state == STATE_PUBLISHING)
        {
            return "待发布";
        }
        if (state == STATE_STARTING)
        {
            return "待执行";
        }
        if (state == STATE_EXECUTING)
        {
            return "执行中";
        }
        if (state == STATE_AUDITING)
        {
            return "待审核";
        }
        if (state == STATE_AUDITED)
        {
            return "已审核";
        }
        return "";
    }
}
