package com.hulman.oms.bean;

import lombok.Getter;

@Getter
public enum SparepartOrderType
{
    PURCHASE(1, "采购入库"), USE(2, "领用出库"), RETURN(3, "归还入库");

    private int type;

    private String text;

    SparepartOrderType(int type, String text)
    {
        this.type = type;
        this.text = text;
    }

    public static String getText(int type)
    {
        for (SparepartOrderType sparepartOrderType : SparepartOrderType.values())
        {
            if (sparepartOrderType.getType() == type)
            {
                return sparepartOrderType.getText();
            }
        }
        return null;
    }

};
