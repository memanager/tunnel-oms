package com.hulman.oms.bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeNode
{
    private Integer id;

    private String title;

    private List<TreeNode> children = new ArrayList<>();

    public void add(TreeNode node)
    {
        this.children.add(node);
    }
}
