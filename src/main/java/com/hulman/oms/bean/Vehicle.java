package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 车辆
 *
 * @Author: maxwellens
 */
@Data
public class Vehicle
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 车牌号
     */
    @ExcelProperty(index = 0, value = "车牌号")
    private String name;

    /**
     * 车辆类型
     */
    @ExcelProperty(index = 1, value = "车辆类型")
    private String type;

    /**
     * 品牌
     */
    @ExcelProperty(index = 2, value = "品牌")
    private String brand;

    /**
     * 产权单位
     */
    @ExcelProperty(index = 3, value = "产权单位")
    private String owner;

    /**
     * 注册日期
     */
    @ExcelProperty(index = 4, value = "注册日期")
    private String regDate;

    /**
     * 备注
     */
    @ExcelProperty(index = 5, value = "备注")
    private String note;

    /**
     * 加油卡号
     */
    @ExcelIgnore
    private String fuelCard;

    /**
     * 卡内余额
     */
    @ExcelIgnore
    private Double balance;

    /**
     * 行驶总里程
     */
    @ExcelIgnore
    private Double mileage;

    /**
     * 状态
     */
    @ExcelIgnore
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "在库";
        }
        if (state == 2)
        {
            return "外出";
        }
        if (state == 3)
        {
            return "维修";
        }
        if (state == 4)
        {
            return "报废";
        }
        return "";
    }

    public String getMileageText()
    {
        if (mileage != null)
        {
            return mileage + "公里";
        } else
        {
            return "";
        }

    }
}
