package com.hulman.oms.bean;

import lombok.Data;

/**
 * 工具仓库
 */
@Data
public class ToolWarehouse {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 仓库名称
    */
    private String name;

    /**
    * 备注
    */
    private String note;
}