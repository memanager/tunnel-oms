package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulman.oms.util.DateUtil;
import com.hulman.oms.util.StringUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 出车记录
 *
 * @Author: maxwellens
 */
@Data
public class VehicleUse
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 车辆ID
     */
    private Integer vehicleId;

    /**
     * 车牌号
     */
    private String vehicleName;

    /**
     * 出车时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 出车里程
     */
    private Double startMile;

    /**
     * 驾驶员ID
     */
    private Integer driverId;

    /**
     * 驾驶员
     */
    private String driverName;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 结束里程
     */
    private Double endMile;

    /**
     * 使用时间
     */
    private Integer useTime;

    /**
     * 行驶里程
     */
    private Double driveMile;

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "通过";
        }
        if (auditResult == 2)
        {
            return "退回";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "出车中";
        }
        if (state == 2)
        {
            return "待审核";
        }
        if (state == 3)
        {
            return "已审核";
        }
        return "";
    }

    public String getUseTimeText()
    {
        return DateUtil.toChineseMinutes(useTime);
    }

    public String getSimpleStartTime()
    {
        return DateUtil.toSimpleTime(startTime);
    }

    public String getStartMileText()
    {
        return startMile + "公里";
    }

    public String getEndMileText()
    {
        return endMile + "公里";
    }

    public String getDriveMileText()
    {
        if (driveMile != null)
        {
            return driveMile + "公里";
        } else
        {
            return "";
        }
    }
}
