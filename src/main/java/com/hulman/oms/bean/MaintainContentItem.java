package com.hulman.oms.bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MaintainContentItem
{
    private String name;

    private List<MaintainContentEntry> entries = new ArrayList<>();

    public void addEntry(MaintainContentEntry entry)
    {
        entries.add(entry);
    }
}
