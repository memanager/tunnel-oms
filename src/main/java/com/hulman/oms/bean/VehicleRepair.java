package com.hulman.oms.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 车辆维修
 */
@Data
public class VehicleRepair
{
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 车辆ID -S
     */
    private Integer vehicleId;

    /**
     * 车牌号 -S
     */
    private String vehicleName;

    /**
     * 维修地点
     */
    private String location;

    /**
     * 维修内容
     */
    private String content;

    /**
     * 结束里程
     */
    private Double mileage;

    /**
     * 驾驶员ID -S
     */
    private Integer driverId;

    /**
     * 驾驶员 -S
     */
    private String driverName;

    /**
     * 送修时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    /**
     * 取回时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date retrieveTime;

    /**
     * 审核人ID
     */
    private Integer auditById;

    /**
     * 审核人
     */
    private String auditByName;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "通过";
        }
        if (auditResult == 2)
        {
            return "退回";
        }
        return "";
    }

    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 状态 -E1:待取回,2:待审核,3:已审核 -S
     */
    private Integer state;

    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == 1)
        {
            return "待取回";
        }
        if (state == 2)
        {
            return "待审核";
        }
        if (state == 3)
        {
            return "已审核";
        }
        return "";
    }

    public String getMileageText()
    {
        if (mileage != null)
        {
            return mileage + "公里";
        } else
        {
            return "";
        }
    }

}