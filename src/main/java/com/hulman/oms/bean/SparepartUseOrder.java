package com.hulman.oms.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 备件领用单
 */
@Data
public class SparepartUseOrder {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 单据号 -S
    */
    private String no;

    /**
    * 领用人
    */
    private String useBy;

    /**
    * 领用原因
    */
    private String purpose;

    /**
    * 操作人
    */
    private String createBy;

    /**
    * 单据时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 单据明细
     */
    private List<SparepartUseItem> items;
}