package com.hulman.oms.bean;

import lombok.Data;

/**
 * 备件采购明细
 */
@Data
public class SparepartPurchaseItem {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 备件采购订单ID
    */
    private Integer sparepartPurchaseOrderId;

    /**
    * 备件ID
    */
    private Integer sparepartId;

    /**
    * 备件名称
    */
    private String name;

    /**
    * 品牌
    */
    private String brand;

    /**
    * 型号/规格
    */
    private String model;

    /**
    * 数量
    */
    private Double quantity;

    /**
    * 单位
    */
    private String unit;

    /**
    * 备注
    */
    private String note;
}