package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 保养条目
 *
 * @Author: maxwellens
 */
@Data
public class MaintainEntry
{
    /**
     * 已勾选
     */
    public static final Integer STATE_CHECKED = 1;

    /**
     * 未勾选
     */
    public static final Integer STATE_NOT_CHECKED = 0;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 保养设备ID
     */
    private Integer maintainDeviceId;

    /**
     * 检查条目
     */
    private String name;

    /**
     * 是否选中
     */
    private Integer checked;

    public Boolean getChecked()
    {
        return checked > 0;
    }

}
