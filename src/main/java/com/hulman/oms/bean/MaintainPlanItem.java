package com.hulman.oms.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 保养计划项目
 *
 * @Author: maxwellens
 */
@Data
public class MaintainPlanItem
{
    /**
     * 主键ID
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 专项保养计划ID
     */
    @ExcelIgnore
    private Integer maintainPlanId;

    /**
     * 专项保养项目
     */
    @ExcelProperty(index = 0, value = "检查项目")
    private String item;

    /**
     * 专项保养条目
     */
    @ExcelProperty(index = 01, value = "检查标准")
    private String entry;

}
