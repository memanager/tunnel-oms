package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 维修项目
 *
 * @Author: maxwellens
 */
@Data
public class RepairItem
{
    /**
     * 待执行（已创建）
     */
    public static final Integer STATE_CREATED = 1;
    /**
     * 执行中
     */
    public static final Integer STATE_EXECUTING = 2;

    /**
     * 审核中
     */
    public static final Integer STATE_AUDITING = 3;

    /**
     * 已审核
     */
    public static final Integer STATE_AUDITED = 4;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 工单ID
     */
    private Integer repairId;

    /**
     * 设备ID
     */
    private Integer deviceId;

    /**
     * 维修设备
     */
    private String deviceName;

    /**
     * 设备位置
     */
    private String deviceLocation;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 备注
     */
    private String note;

    /**
     * 执行人ID
     */
    private Integer executeById;

    /**
     * 执行人
     */
    private String executeByName;

    /**
     * 通知人
     */
    private String notifyNames;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 随行车辆
     */
    private String vehicle;

    /**
     * 随行司机
     */
    private String driver;

    /**
     * 维修工具
     */
    private String tools;

    /**
     * 维修材料
     */
    private String materials;

    /**
     * 现场照片
     */
    private String imgs;

    /**
     * 完成结果
     */
    private String completion;

    /**
     * 审核结果
     */
    private Integer auditResult;

    /**
     * 审核次数
     */
    private Integer auditTimes;

    /**
     * 审核结果显示值
     */
    public String getAuditResultText()
    {
        if (auditResult == null)
        {
            return "";
        }
        if (auditResult == 1)
        {
            return "合格";
        }
        if (auditResult == 2)
        {
            return "不合格";
        }
        return "";
    }
    /**
     * 审核意见
     */
    private String auditComment;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 状态显示值
     */
    public String getStateText()
    {
        if (state == null)
        {
            return "";
        }
        if (state == STATE_CREATED)
        {
            return "待执行";
        }
        if (state == STATE_EXECUTING)
        {
            return "执行中";
        }
        if (state == STATE_AUDITING)
        {
            return "审核中";
        }
        if (state == STATE_AUDITED)
        {
            return "已审核";
        }
        return "";
    }
}
