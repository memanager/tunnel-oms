package com.hulman.oms.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 工具采购订单
 */
@Data
public class ToolPurchaseOrder {
    /**
    * 主键ID
    */
    private Integer id;

    /**
    * 单据号
    */
    private String no;

    /**
    * 供应商
    */
    private String supplier;

    /**
    * 操作人
    */
    private String createBy;

    /**
    * 单据时间
    */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private List<ToolPurchaseItem> items;
}