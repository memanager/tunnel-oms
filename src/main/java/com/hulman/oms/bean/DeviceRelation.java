package com.hulman.oms.bean;

import lombok.Data;


/**
 * 设备关联
 *
 * @Author: maxwellens
 */
@Data
public class DeviceRelation
{
    /**
     * 上级设备
     */
    public static final Integer SUP = 1;

    /**
     * 下级设备
     */
    public static final Integer SUB = 2;
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 设备ID
     */
    private Integer deviceId;

    /**
     * 关联设备ID
     */
    private Integer relatedDeviceId;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备系统
     */
    private String deviceSystem;

    /**
     * 关联类型
     */
    private Integer type;

    /**
     * 关联类型显示值
     */
    public String getTypeText()
    {
        if (type == null)
        {
            return "";
        } else if (type == SUP)
        {
            return "上级设备↑";
        } else if (type == SUB)
        {
            return "下级设备↓";
        }
        return "";
    }

    /**
     * 区域
     */
    private String locationName;

    /**
     * 区域ID
     */
    private String locationId;

    /**
     * 设备简称
     */
    private String abbr;
}
