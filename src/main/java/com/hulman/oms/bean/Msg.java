package com.hulman.oms.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hulman.oms.util.DateUtil;
import com.hulman.oms.util.JsonUtil;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 通知消息
 *
 * @Author: maxwellens
 */
@Data
public class Msg
{
    /**
     * 未读
     */
    public static final Integer STATE_UNREAD = 0;

    /**
     * 已读
     */
    public static final Integer STATE_READ = 1;

    /**
     * 运维消息
     */
    public static final String TYPE_OM = "om";

    /**
     * 系统消息
     */
    public static final String TYPE_SYS = "sys";

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 消息类型
     */
    private String type;

    /**
     * 所有人ID
     */
    private Integer ownerId;

    /**
     * 所有人姓名
     */
    private String ownerName;

    /**
     * 标题
     */
    private String title;

    /**
     * 明细项目
     */
    private String items;

    private List<Item> itemList = new ArrayList<>();

    public void addItem(String name, String value)
    {
        itemList.add(new Msg.Item(name, value));
        this.items = JsonUtil.toJSONString(itemList);
    }

    @JsonProperty("items")
    public Object getItemList()
    {
        return JsonUtil.parse(items);
    }

    public List<Msg.Item> itemList()
    {
        return itemList;
    }

    /**
     * 跳转路径
     */
    private String path;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 状态
     */
    private Integer state;

    public String getStateText()
    {
        if (state == null)
        {
            return "";
        } else if (state == STATE_UNREAD)
        {
            return "未读";
        } else if (state == STATE_READ)
        {
            return "已读";
        } else
        {
            return "";
        }
    }

    public String getSimpleCreateTime()
    {
        return DateUtil.toSimpleTime(this.createTime);
    }

    @Data
    public static class Item
    {
        private String name;

        private String value;

        public Item(String name, String value)
        {
            this.name = name;
            this.value = value;
        }
    }

}
