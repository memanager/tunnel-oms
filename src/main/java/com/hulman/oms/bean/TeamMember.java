package com.hulman.oms.bean;

import lombok.Data;


/**
 * 班组成员
 *
 * @Author: maxwellens
 */
@Data
public class TeamMember
{

    /**
     * 班组ID
     */
    private Integer teamId;

    /**
     * 人员ID
     */
    private Integer userId;

    /**
     * 人员姓名
     */
    private String userName;

}
