package com.hulman.oms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppWebConfig implements WebMvcConfigurer
{
    @Value("${img-home}")
    private String imgHome;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/img/**").addResourceLocations("file:" + imgHome);
    }

    @Bean
    public MultipartResolver multipartResolver()
    {
        return new CommonsMultipartResolver();
    }

    @Bean
    public RestTemplate restTemplate()
    {
        return new RestTemplate();
    }

}
