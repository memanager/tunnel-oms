package com.hulman.oms.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig
{

    @Bean
    public Cache<String, String> tokenCache()
    {
        Cache<String, String> cache = Caffeine.newBuilder().expireAfterWrite(7200, TimeUnit.SECONDS).build();
        return cache;
    }
}
