package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartReturnOrder;
import com.hulman.oms.bean.SparepartReturnOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartReturnOrderDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartReturnOrder record);

    int insertSelective(SparepartReturnOrder record);

    SparepartReturnOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartReturnOrder record);

    int updateByPrimaryKey(SparepartReturnOrder record);

    List<SparepartReturnOrder> findSparepartReturnOrders(Map<String,Object> map);

    Integer findSparepartReturnOrdersCount(Map<String, Object> map);

    SparepartReturnOrder selectByNo(String no);
}