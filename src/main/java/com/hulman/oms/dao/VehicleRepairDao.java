package com.hulman.oms.dao;

import com.hulman.oms.bean.VehicleRepair;
import com.hulman.oms.bean.VehicleRepair;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleRepairDao {
    int deleteByPrimaryKey(Integer id);

    int insert(VehicleRepair record);

    int insertSelective(VehicleRepair record);

    VehicleRepair selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VehicleRepair record);

    int updateByPrimaryKey(VehicleRepair record);

    List<VehicleRepair> findVehicleRepairs(Map<String, Object> map);

    Integer findVehicleRepairsCount(Map<String, Object> map);

    List<VehicleRepair> selectByVehicleId(Integer vehicleId);
}