package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartUseOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartUseOrderDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartUseOrder record);

    int insertSelective(SparepartUseOrder record);

    SparepartUseOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartUseOrder record);

    int updateByPrimaryKey(SparepartUseOrder record);

    List<SparepartUseOrder> findSparepartUserOrders(Map<String,Object> map);

    Integer findSparepartUserOrdersCount(Map<String, Object> map);

    SparepartUseOrder selectByNo(String no);


}