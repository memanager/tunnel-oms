package com.hulman.oms.dao;

import com.hulman.oms.bean.WeApiResult;
import com.hulman.oms.exception.IoTException;
import com.hulman.oms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * 企业微信联系人
 */
@Repository
@Slf4j
public class ContactDao
{
    @Value("${wx.corp-id:}")
    private String corpId;
    @Value("${wx.contact.secret:}")
    private String secret;
    @Autowired
    private RestTemplate restTemplate;

    public String getAccessToken()
    {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={1}&corpsecret={2}";
        Map<String, Object> result = restTemplate.getForObject(url, Map.class, corpId, secret);
        Integer errCode = (Integer) result.get("errcode");
        String accessToken = (String) result.get("access_token");
        if (errCode == 0 && !StringUtil.isEmpty(accessToken))
        {
            return accessToken;
        } else
        {
            log.error(result.toString());
            throw new IoTException("无法获取有效的accessToken");
        }
    }

    public WeApiResult create(String accessToken, Map<String, Object> contact)
    {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={1}";
        Map<String, Object> result = restTemplate.postForObject(url, contact, Map.class, accessToken);
        Integer errCode = (Integer) result.get("errcode");
        String errMsg = (String) result.get("errmsg");
        return new WeApiResult(errCode, errMsg);
    }

    public WeApiResult update(String accessToken, Map<String, Object> contact)
    {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={1}";
        Map<String, Object> result = restTemplate.postForObject(url, contact, Map.class, accessToken);
        Integer errCode = (Integer) result.get("errcode");
        String errMsg = (String) result.get("errmsg");
        return new WeApiResult(errCode, errMsg);
    }

    public WeApiResult delete(String accessToken, String userId)
    {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={1}&userid={2}";
        Map<String, Object> result = restTemplate.getForObject(url, Map.class, accessToken, userId);
        Integer errCode = (Integer) result.get("errcode");
        String errMsg = (String) result.get("errmsg");
        return new WeApiResult(errCode, errMsg);
    }

    public WeApiResult getUserId(String accessToken, Map<String, Object> contact)
    {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token={1}";
        Map<String, Object> result = restTemplate.postForObject(url, contact, Map.class, accessToken);
        Integer errCode = (Integer) result.get("errcode");
        String errMsg = (String) result.get("errmsg");
        WeApiResult weApiResult = new WeApiResult(errCode,errMsg);
        weApiResult.getData().put("userid",result.get("userid"));
        return weApiResult;
    }

}
