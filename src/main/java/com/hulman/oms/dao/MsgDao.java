package com.hulman.oms.dao;

import com.hulman.oms.bean.Msg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MsgDao
{
    List<Msg> findMsgs(Map<String, Object> map);

    Integer findMsgsCount(Map<String, Object> map);

    List<Msg> findLatestMsgs(@Param("ownerId") Integer ownerId,@Param("msgId") Integer msgId);

    Msg findLatestMsg(@Param("ownerId") Integer ownerId,@Param("type") String type);

    Msg findMsgById(Integer id);

    int insertMsg(Msg msg);

    void deleteMsgById(Integer id);

    void deleteMsgs(int[] ids);

    int updateMsg(Msg msg);

    void readMsg(Integer msgId);

    void readAllMsgs(Integer ownerId, String type);

    void clearAllMsgs(Integer ownerId, String type);
}
