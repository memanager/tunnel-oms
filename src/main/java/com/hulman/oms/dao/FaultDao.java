package com.hulman.oms.dao;

import com.hulman.oms.bean.Fault;
import com.hulman.oms.bean.NameValue;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface FaultDao
{
    List<Fault> findFaults(Map<String, Object> map);

    List<Fault> findTodayFaults();

    List<Fault> findRecentFaults(Integer count);

    Integer findFaultsCount(Map<String, Object> map);

    Fault findFaultById(Integer id);

    int insertFault(Fault fault);

    void deleteFaultById(Integer id);

    void deleteFaults(int[] ids);

    int updateFault(Fault fault);

    Integer findTodayFaultsCount(Integer tunnelId);

    Integer findTodayAutoFaultsCount(Integer tunnelId);

    Integer findTodayManualFaultsCount(Integer tunnelId);

    Integer findTodayFaultsTotalCount();

    List<NameValue> statePendingCountBySystem();

    List<NameValue> statDailyFaults(Integer days);
}
