package com.hulman.oms.dao;

import com.hulman.oms.bean.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface RoleDao
{
    List<Role> findRoles(Map<String, Object> map);

    List<Role> findRolesByIds(String ids);

    Role findRoleByName(String name);

    Integer findRolesCount(Map<String, Object> map);

    Role findRoleById(Integer id);

    int insertRole(Role role);

    void deleteRoleById(Integer id);

    void deleteRoles(String ids);

    int updateRole(Role role);

    int updateMenus(@Param("id") Integer id, @Param("menus") String menus);

    int updateWorkbenches(@Param("id") Integer id, @Param("workbenches") String workbenches);

    int updateAccessPolicies(@Param("id") Integer id, @Param("accessPolices") String accessPolices);

}
