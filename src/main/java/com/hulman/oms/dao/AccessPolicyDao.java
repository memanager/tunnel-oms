package com.hulman.oms.dao;

import com.hulman.oms.bean.AccessPolicy;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface AccessPolicyDao
{
    List<AccessPolicy> findAccessPolicies(Map<String, Object> map);

    Integer findAccessPoliciesCount(Map<String, Object> map);

    AccessPolicy findAccessPolicyById(Integer id);

    int insertAccessPolicy(AccessPolicy accessPolicy);

    void deleteAccessPolicyById(Integer id);

    void deleteAccessPolicies(String ids);

    int updateAccessPolicy(AccessPolicy accessPolicy);
}
