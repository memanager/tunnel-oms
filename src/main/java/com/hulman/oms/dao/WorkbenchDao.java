package com.hulman.oms.dao;

import com.hulman.oms.bean.Workbench;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WorkbenchDao
{
    List<Workbench> findWorkbenches();

    List<Workbench> findRootWorkbenches();

    Integer findWorkbenchesCount();

    List<Workbench> findWorkbenchesByParentId(Integer id);

    Workbench findWorkbenchById(Integer id);

    int insertWorkbench(Workbench workbench);

    void deleteWorkbenchById(Integer id);

    int updateWorkbench(Workbench workbench);
}
