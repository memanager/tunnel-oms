package com.hulman.oms.dao;

import com.hulman.oms.bean.WaterReportItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WaterReportItemDao
{
    List<WaterReportItem> findWaterReportItems(Map<String, Object> map);

    Integer findWaterReportItemsCount(Map<String, Object> map);

    WaterReportItem findWaterReportItemById(Integer id);

    int insertWaterReportItem(WaterReportItem waterReportItem);

    void deleteWaterReportItemById(Integer id);

    void deleteWaterReportItems(int[] ids);

    int updateWaterReportItem(WaterReportItem waterReportItem);
}
