package com.hulman.oms.dao;

import com.hulman.oms.bean.Sparepart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SparepartDao
{
    List<Sparepart> findSpareparts(Map<String, Object> map);

    Integer findSparepartsCount(Map<String, Object> map);

    Sparepart findSparepartById(Integer id);

    int insertSparepart(Sparepart Sparepart);

    void deleteSparepartById(Integer id);

    void deleteSpareparts(int[] ids);

    int updateSparepart(Sparepart Sparepart);

    int addQuantity(Integer id, Integer delta);
}
