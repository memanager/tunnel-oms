package com.hulman.oms.dao;

import com.hulman.oms.bean.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MenuDao
{
    List<Menu> findMenus();

    List<Menu> findRootMenus();

    Integer findMenusCount();

    List<Menu> findMenusByParentId(Integer id);

    Menu findMenuById(Integer id);

    int insertMenu(Menu menu);

    void deleteMenuById(Integer id);

    int updateMenu(Menu menu);
}
