package com.hulman.oms.dao;

import com.hulman.oms.bean.ProcState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ProcStateDao
{
    List<ProcState> findProcStates(Map<String, Object> map);

    Integer findProcStatesCount(Map<String, Object> map);

    ProcState findProcStateById(Integer id);

    String findProcStateNameByState(@Param("procDefId") Integer processDefId, @Param("state") Integer state);

    ProcState findProcStateByState(@Param("procDefId") Integer processDefId, @Param("state") Integer state);

    ProcState findNextProcState(@Param("procDefId") Integer processDefId, @Param("state") Integer state);

    ProcState findPreProcState(@Param("procDefId") Integer processDefId, @Param("state") Integer state);

    int insertProcState(ProcState procState);

    void deleteProcStateById(Integer id);

    void deleteProcStates(int[] ids);

    int updateProcState(ProcState procState);
}
