package com.hulman.oms.dao;

import com.hulman.oms.bean.RepairItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface RepairItemDao
{
    List<RepairItem> findRepairItems(Map<String, Object> map);

    Integer findRepairItemsCount(Map<String, Object> map);

    RepairItem findRepairItemById(Integer id);

    int insertRepairItem(RepairItem workTaskItem);

    void deleteRepairItemById(Integer id);

    void deleteRepairItems(int[] ids);

    int updateRepairItem(RepairItem workTaskItem);
}
