package com.hulman.oms.dao;

import com.hulman.oms.bean.DeviceRelation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DeviceRelationDao
{
    List<DeviceRelation> findDeviceRelations(Map<String, Object> map);

    Integer findDeviceRelationsCount(Map<String, Object> map);

    int insertDeviceRelation(DeviceRelation deviceRelation);

    void deleteDeviceRelationById(Integer id);

    void deleteDeviceRelations(int[] ids);

    int updateDeviceRelation(DeviceRelation deviceRelation);

    void clearDeviceRelationsByDeviceId(Integer deviceId);
}
