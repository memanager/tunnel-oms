package com.hulman.oms.dao;

import com.hulman.oms.bean.DeviceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DeviceTypeDao
{
    List<DeviceType> findDeviceTypes();

    DeviceType findDeviceTypeByName(String name);

    int insertDeviceType(DeviceType deviceType);

    void deleteDeviceTypeById(Integer id);

    int updateDeviceType(DeviceType deviceType);
}
