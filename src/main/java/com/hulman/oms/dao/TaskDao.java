package com.hulman.oms.dao;

import com.hulman.oms.bean.Task;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface TaskDao
{
    List<Task> findTasks(Map<String, Object> map);

    Integer findTasksCount(Map<String, Object> map);

    List<Task> findProcInstTasks(Integer procInstId);

    Task findTaskById(Integer id);

    List<Task> findTimeoutTasks();

    Task findTaskByTagAndFormKey(@Param("tag") String tag,@Param("formKey")  String formKey);

    int insertTask(Task task);

    void deleteTaskById(Integer id);

    void deleteTasks(int[] ids);

    int updateTask(Task task);
}
