package com.hulman.oms.dao;

import com.hulman.oms.bean.Dict;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DictDao
{
    List<Dict> findDicts(Map<String, Object> map);

    Integer findDictsCount(Map<String, Object> map);

    Dict findDictById(Integer id);

    Dict findDictByKey(String key);

    int insertDict(Dict dict);

    void deleteDictById(Integer id);

    void deleteDicts(String ids);

    int updateDict(Dict dict);
}
