package com.hulman.oms.dao;

import com.hulman.oms.bean.ToolPurchaseOrder;
import com.hulman.oms.bean.ToolPurchaseOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolPurchaseOrderDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolPurchaseOrder record);

    int insertSelective(ToolPurchaseOrder record);

    ToolPurchaseOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolPurchaseOrder record);

    int updateByPrimaryKey(ToolPurchaseOrder record);

    ToolPurchaseOrder selectByNo(Integer no);

    List<ToolPurchaseOrder> findToolPuchaseOrders(Map<String, Object> map);

    Integer findToolPuchaseOrdersCount(Map<String, Object> map);
}