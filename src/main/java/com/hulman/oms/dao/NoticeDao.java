package com.hulman.oms.dao;

import com.hulman.oms.bean.Notice;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: maxwellens
 */
@Mapper
public interface NoticeDao
{

    Notice findNotice();

    int insertNotice(Notice notice);

    int updateNotice(Notice notice);
}
