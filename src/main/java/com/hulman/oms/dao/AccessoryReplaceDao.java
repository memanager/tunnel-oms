package com.hulman.oms.dao;

import com.hulman.oms.bean.AccessoryReplace;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface AccessoryReplaceDao
{
    List<AccessoryReplace> findAccessoryReplaces(Map<String, Object> map);

    Integer findAccessoryReplacesCount(Map<String, Object> map);

    AccessoryReplace findAccessoryReplaceById(Integer id);

    int insertAccessoryReplace(AccessoryReplace accessoryReplace);

    void deleteAccessoryReplaceById(Integer id);

    void deleteAccessoryReplaces(int[] ids);

    int updateAccessoryReplace(AccessoryReplace accessoryReplace);
}
