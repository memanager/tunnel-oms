package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartPurchaseOrderDao {
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartPurchaseOrder record);

    int insertSelective(SparepartPurchaseOrder record);

    SparepartPurchaseOrder selectByPrimaryKey(Integer id);

    SparepartPurchaseOrder selectByNo(Integer no);

    int updateByPrimaryKeySelective(SparepartPurchaseOrder record);

    int updateByPrimaryKey(SparepartPurchaseOrder record);

    List<SparepartPurchaseOrder> findSparepartPuchaseOrders(Map<String, Object> map);

    Integer findSparepartPuchaseOrdersCount(Map<String, Object> map);
}