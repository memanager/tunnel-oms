package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartReturnItem;
import com.hulman.oms.bean.SparepartReturnItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartReturnItemDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartReturnItem record);

    int insertSelective(SparepartReturnItem record);

    SparepartReturnItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartReturnItem record);

    int updateByPrimaryKey(SparepartReturnItem record);

    List<SparepartReturnItem> findSparepartReturnByOrderId(Integer orderId);

    int deleteByOrderId(Integer orderId);

    Integer findSparepartReturnItemsCount(Map<String, Object> map);

    List<SparepartReturnItem> findSparepartReturnItems(Map<String, Object> map);
}