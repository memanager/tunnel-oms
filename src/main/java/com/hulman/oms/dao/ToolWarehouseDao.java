package com.hulman.oms.dao;

import com.hulman.oms.bean.ToolWarehouse;
import com.hulman.oms.bean.ToolWarehouse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolWarehouseDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolWarehouse record);

    int insertSelective(ToolWarehouse record);

    ToolWarehouse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolWarehouse record);

    int updateByPrimaryKey(ToolWarehouse record);

    List<ToolWarehouse> findToolWareHouses(Map<String,Object> map);

    Integer findToolWarehouseCount(Map<String,Object> map);

    ToolWarehouse selectByName(String name);
}