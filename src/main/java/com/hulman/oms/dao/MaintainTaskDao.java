package com.hulman.oms.dao;

import com.hulman.oms.bean.MaintainTask;
import com.hulman.oms.bean.NameValue;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainTaskDao
{
    List<MaintainTask> findMaintainTasks(Map<String, Object> map);

    Integer findMaintainTasksCount(Map<String, Object> map);

    MaintainTask findMaintainTaskById(Integer id);

    int insertMaintainTask(MaintainTask maintainTask);

    void deleteMaintainTaskById(Integer id);

    void deleteMaintainTasks(int[] ids);

    int updateMaintainTask(MaintainTask maintainTask);

    List<NameValue> statDailyMaintainTasks(Integer days);
}
