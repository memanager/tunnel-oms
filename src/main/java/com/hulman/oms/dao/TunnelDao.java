package com.hulman.oms.dao;

import com.hulman.oms.bean.Tunnel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface TunnelDao
{
    List<Tunnel> findTunnels(Map<String, Object> map);

    Integer findTunnelsCount(Map<String, Object> map);

    Tunnel findTunnelById(Integer id);

    Tunnel findTunnelByName(String name);

    List<Tunnel> findTunnelsByIds(int[] ids);

    int insertTunnel(Tunnel tunnel);

    void deleteTunnelById(Integer id);

    void deleteTunnels(int[] ids);

    int updateTunnel(Tunnel tunnel);
}
