package com.hulman.oms.dao;

import com.hulman.oms.bean.Device;
import com.hulman.oms.bean.NameValue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DeviceDao
{
    List<Device> findDevices(Map<String, Object> map);

    List<Device> findDevicesBySystem(String system);

    Integer findDevicesCount(Map<String, Object> map);

    Device findDeviceById(Integer id);

    Device findDeviceByCode(String code);

    Device findDeviceByIotCode(String iotCode);

    Device findDeviceByName(String name);

    Device findDeviceByTunnelIdAndName(@Param("tunnelId") Integer tunnelId,@Param("name")  String name);

    int insertDevice(Device device);

    void deleteDeviceById(Integer id);

    void deleteDevices(int[] ids);

    List<Device> findDevicesByIds(int[] ids);

    int updateDevice(Device device);

    List<NameValue> statSystemCount();

    void refreshMaintainInfo(Integer id);
}
