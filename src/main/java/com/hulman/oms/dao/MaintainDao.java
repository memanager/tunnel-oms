package com.hulman.oms.dao;

import com.hulman.oms.bean.Maintain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainDao
{
    List<Maintain> findMaintains(Map<String, Object> map);

    Integer findMaintainsCount(Map<String, Object> map);

    Maintain findMaintainById(Integer id);

    int insertMaintain(Maintain maintain);

    void deleteMaintainById(Integer id);

    void deleteMaintains(int[] ids);

    int updateMaintain(Maintain maintain);
}
