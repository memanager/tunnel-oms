package com.hulman.oms.dao;

import com.hulman.oms.bean.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ParamDao
{
    List<Param> findParams(Map<String, Object> map);

    Integer findParamsCount(Map<String, Object> map);

    Param findParamById(Integer id);

    int insertParam(Param param);

    void deleteParamById(Integer id);

    void deleteParams(String ids);

    int updateParam(Param param);
}
