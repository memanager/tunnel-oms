package com.hulman.oms.dao;

import com.hulman.oms.bean.Accessory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface AccessoryDao
{
    List<Accessory> findAccessories(Map<String, Object> map);

    Integer findAccessoriesCount(Map<String, Object> map);

    Accessory findAccessoryById(Integer id);

    Accessory findAccessoryByDeviceIdAndName(Integer deviceId, String name);

    int insertAccessory(Accessory accessory);

    void deleteAccessoryById(Integer id);

    void deleteAccessories(int[] ids);

    int updateAccessory(Accessory accessory);

    void clearAccessoriesByDeviceId(Integer deviceId);
}
