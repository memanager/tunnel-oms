package com.hulman.oms.dao;

import com.hulman.oms.bean.ToolUseOrder;
import com.hulman.oms.bean.ToolUseOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolUseOrderDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolUseOrder record);

    int insertSelective(ToolUseOrder record);

    ToolUseOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolUseOrder record);

    int updateByPrimaryKey(ToolUseOrder record);

    List<ToolUseOrder> findToolUserOrders(Map<String,Object> map);

    Integer findToolUserOrdersCount(Map<String, Object> map);

    ToolUseOrder selectByNo(String no);
}