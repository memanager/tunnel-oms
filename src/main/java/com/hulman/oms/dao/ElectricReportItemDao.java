package com.hulman.oms.dao;

import com.hulman.oms.bean.ElectricReportItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ElectricReportItemDao
{
    List<ElectricReportItem> findElectricReportItems(Map<String, Object> map);

    Integer findElectricReportItemsCount(Map<String, Object> map);

    ElectricReportItem findElectricReportItemById(Integer id);

    int insertElectricReportItem(ElectricReportItem electricReportItem);

    void deleteElectricReportItemById(Integer id);

    void deleteElectricReportItems(int[] ids);

    int updateElectricReportItem(ElectricReportItem electricReportItem);
}
