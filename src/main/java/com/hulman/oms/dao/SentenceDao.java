package com.hulman.oms.dao;

import com.hulman.oms.bean.Sentence;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SentenceDao
{
    List<Sentence> findSentences(Map<String, Object> map);

    Integer findSentencesCount(Map<String, Object> map);

    Sentence findSentenceById(Integer id);

    int insertSentence(Sentence sentence);

    void deleteSentenceById(Integer id);

    void deleteSentences(int[] ids);

    int updateSentence(Sentence sentence);

    Sentence randomSignInSentence();

    Sentence randomSignOutSentence();
}
