package com.hulman.oms.dao;

import com.hulman.oms.bean.MaintainPlanItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainPlanItemDao
{
    List<MaintainPlanItem> findMaintainPlanItems(Map<String, Object> map);

    Integer findMaintainPlanItemsCount(Map<String, Object> map);

    MaintainPlanItem findMaintainPlanItemById(Integer id);

    int insertMaintainPlanItem(MaintainPlanItem maintainPlanItem);

    void deleteMaintainPlanItemById(Integer id);

    void deleteMaintainPlanItems(int[] ids);

    int updateMaintainPlanItem(MaintainPlanItem maintainPlanItem);
}
