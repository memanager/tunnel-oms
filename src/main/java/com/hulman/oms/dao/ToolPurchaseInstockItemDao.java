package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import com.hulman.oms.bean.ToolPurchaseInstockItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolPurchaseInstockItemDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolPurchaseInstockItem record);

    int insertSelective(ToolPurchaseInstockItem record);

    ToolPurchaseInstockItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolPurchaseInstockItem record);

    int updateByPrimaryKey(ToolPurchaseInstockItem record);

    List<ToolPurchaseInstockItem> findToolPurchaseInstockItemsByOrderId(Integer orderId);

    int deleteByOrderId(Integer orderId);

    List<ToolPurchaseInstockItem> findToolPurchaseInstockItems(Map<String, Object> map);

    Integer findToolPurchaseInstockItemsCount(Map<String, Object> map);
}