package com.hulman.oms.dao;

import com.hulman.oms.bean.TeamMember;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: maxwellens
 */
@Mapper
public interface TeamMemberDao
{

    List<TeamMember> findTeamMembers();

    Integer findTeamId(Integer userId);

    int insertTeamMember(@Param("teamId") Integer teamId, @Param("userId") Integer userId);

    void deleteTeamMember(@Param("teamId") Integer teamId, @Param("userId") Integer userId);

}
