package com.hulman.oms.dao;

import com.hulman.oms.bean.Vehicle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface VehicleDao
{
    List<Vehicle> findVehicles(Map<String, Object> map);

    Integer findVehiclesCount(Map<String, Object> map);

    Vehicle findVehicleById(Integer id);

    int insertVehicle(Vehicle vehicle);

    void deleteVehicleById(Integer id);

    void deleteVehicles(int[] ids);

    int updateVehicle(Vehicle vehicle);
}
