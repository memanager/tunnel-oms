package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartWarehouse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartWarehouseDao {
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartWarehouse record);

    int insertSelective(SparepartWarehouse record);

    SparepartWarehouse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartWarehouse record);

    int updateByPrimaryKey(SparepartWarehouse record);

    List<SparepartWarehouse> findSparepartWareHouses(Map<String,Object> map);

    Integer findSparepartWarehouseCount(Map<String,Object> map);

    SparepartWarehouse selectByName(String name);
}