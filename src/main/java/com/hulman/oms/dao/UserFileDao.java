package com.hulman.oms.dao;

import com.hulman.oms.bean.UserFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface UserFileDao
{
    List<UserFile> findUserFiles(Map<String, Object> map);

    Integer findUserFilesCount(Map<String, Object> map);

    UserFile findUserFileById(Integer id);

    UserFile findUserFileByUuid(String uuid);

    int insertUserFile(UserFile UserFile);

    void deleteUserFileById(Integer id);

    void deleteUserFiles(int[] ids);

    int updateUserFile(UserFile UserFile);
}
