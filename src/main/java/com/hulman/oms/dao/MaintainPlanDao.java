package com.hulman.oms.dao;

import com.hulman.oms.bean.MaintainPlan;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainPlanDao
{
    List<MaintainPlan> findMaintainPlans(Map<String, Object> map);

    List<MaintainPlan> findLatestMaintainPlans();

    List<MaintainPlan> findTodayMaintainPlans();

    Integer findMaintainPlansCount(Map<String, Object> map);

    MaintainPlan findMaintainPlanById(Integer id);

    int insertMaintainPlan(MaintainPlan maintainPlan);

    void deleteMaintainPlanById(Integer id);

    void deleteMaintainPlans(int[] ids);

    int updateMaintainPlan(MaintainPlan maintainPlan);

    void changeMaintainPlanState(Integer id, Integer state);
}
