package com.hulman.oms.dao;

import com.hulman.oms.bean.Team;
import com.hulman.oms.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface TeamDao
{
    List<Team> findTeams(Map<String, Object> map);

    Integer findTeamsCount(Map<String, Object> map);

    Team findTeamById(Integer id);

    Team findTeamByUserId(Integer id);

    Team findTeamByLeaderId(Integer leaderId);

    int insertTeam(Team team);

    void deleteTeamById(Integer id);

    void deleteTeams(int[] ids);

    int updateTeam(Team team);

    List<User> findMembersByTeamId(Integer teamId);
}
