package com.hulman.oms.dao;

import com.hulman.oms.bean.IotDevice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface IotDeviceDao
{
    List<IotDevice> findIotDevices(Map<String, Object> map);

    Integer findIotDevicesCount(Map<String, Object> map);

    IotDevice findIotDeviceById(Integer id);

    int insertIotDevice(IotDevice iotDevice);

    void deleteIotDeviceById(Integer id);

    void deleteIotDevices(int[] ids);

    int updateIotDevice(IotDevice iotDevice);

    void clearIotDevicesByDeviceId(Integer deviceId);

    IotDevice findIotDeviceByDeviceIdAndIotName(@Param("deviceId") Integer deviceId,@Param("iotName") String iotName);
}
