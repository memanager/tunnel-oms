package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseInstockOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SparepartPurchaseInstockOrderDao
{
    List<SparepartPurchaseInstockOrder> findSparepartPurchaseInstockOrders(Map<String, Object> map);

    Integer findSparepartPurchaseInstockOrdersCount(Map<String, Object> map);

    SparepartPurchaseInstockOrder findSparepartPurchaseInstockOrderById(Integer id);

    SparepartPurchaseInstockOrder findSparepartPurchaseInstockOrderByNo(String no);

    int insertSparepartPurchaseInstockOrder(SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder);

    void deleteSparepartPurchaseInstockOrderById(Integer id);

    void deleteSparepartPurchaseInstockOrders(int[] ids);

    int updateSparepartPurchaseInstockOrder(SparepartPurchaseInstockOrder sparepartPurchaseInstockOrder);
}
