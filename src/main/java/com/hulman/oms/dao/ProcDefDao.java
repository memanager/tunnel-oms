package com.hulman.oms.dao;

import com.hulman.oms.bean.ProcDef;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ProcDefDao
{
    List<ProcDef> findProcDefs(Map<String, Object> map);

    Integer findProcDefsCount(Map<String, Object> map);

    ProcDef findProcDefById(Integer id);

    ProcDef findProcDefByKey(String key);

    int insertProcDef(ProcDef procDef);

    void deleteProcDefById(Integer id);

    void deleteProcDefs(int[] ids);

    int updateProcDef(ProcDef procDef);
}
