package com.hulman.oms.dao;

import com.hulman.oms.bean.LeaderShift;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface LeaderShiftDao
{
    List<LeaderShift> findLeaderShifts(Map<String, Object> map);

    Integer findLeaderShiftsCount(Map<String, Object> map);

    LeaderShift findLeaderShiftById(Integer id);

    int insertLeaderShift(LeaderShift leaderShift);

    void deleteLeaderShiftById(Integer id);

    void deleteLeaderShifts(int[] ids);

    int updateLeaderShift(LeaderShift leaderShift);
}
