package com.hulman.oms.dao;

import com.hulman.oms.bean.DeviceSystem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DeviceSystemDao
{
    List<DeviceSystem> findDeviceSystems();

    DeviceSystem findDeviceSystemByName(String name);

    DeviceSystem findDeviceSystemById(Integer id);

    int insertDeviceSystem(DeviceSystem deviceSystem);

    void deleteDeviceSystemById(Integer id);

    int updateDeviceSystem(DeviceSystem deviceSystem);
}
