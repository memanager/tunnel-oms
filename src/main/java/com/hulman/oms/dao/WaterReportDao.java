package com.hulman.oms.dao;

import com.hulman.oms.bean.WaterMeter;
import com.hulman.oms.bean.WaterReport;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WaterReportDao
{
    List<WaterReport> findWaterReports(Map<String, Object> map);

    Integer findWaterReportsCount(Map<String, Object> map);

    WaterReport findWaterReportById(Integer id);

    int insertWaterReport(WaterReport waterReport);

    void deleteWaterReportById(Integer id);

    void deleteWaterReports(int[] ids);

    int updateWaterReport(WaterReport waterReport);

}
