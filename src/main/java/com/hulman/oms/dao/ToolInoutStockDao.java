package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartInoutStock;
import com.hulman.oms.bean.ToolInoutStock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolInoutStockDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolInoutStock record);

    int insertSelective(ToolInoutStock record);

    ToolInoutStock selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolInoutStock record);

    int updateByPrimaryKey(ToolInoutStock record);

    int deleteToolInoutStockByOrderNo(String orderNo);

    List<ToolInoutStock> findToolInoutStocks(Map<String, Object> map);

    Integer findToolInoutStocksCount(Map<String, Object> map);
}