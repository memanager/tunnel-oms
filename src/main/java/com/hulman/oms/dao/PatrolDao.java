package com.hulman.oms.dao;

import com.hulman.oms.bean.NameValue;
import com.hulman.oms.bean.Patrol;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface PatrolDao
{
    List<Patrol> findPatrols(Map<String, Object> map);

    List<Patrol> findTodayPatrols();

    List<Patrol> findLatestPatrols();

    Integer findPatrolsCount(Map<String, Object> map);

    Patrol findPatrolById(Integer id);

    int insertPatrol(Patrol patrol);

    void deletePatrolById(Integer id);

    void deletePatrols(int[] ids);

    int updatePatrol(Patrol patrol);

    List<NameValue> statDailyPatrols(Integer days);
}
