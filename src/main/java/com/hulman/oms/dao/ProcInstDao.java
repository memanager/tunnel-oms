package com.hulman.oms.dao;

import com.hulman.oms.bean.ProcInst;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ProcInstDao
{
    List<ProcInst> findProcInsts(Map<String, Object> map);

    Integer findProcInstsCount(Map<String, Object> map);

    ProcInst findProcInstById(Integer id);

    int insertProcInst(ProcInst procInst);

    void deleteProcInstById(Integer id);

    void deleteProcInsts(int[] ids);

    int updateProcInst(ProcInst procInst);
}
