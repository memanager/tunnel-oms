package com.hulman.oms.dao;

import com.hulman.oms.bean.MaintainTaskItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainTaskItemDao
{
    List<MaintainTaskItem> findMaintainTaskItems(Map<String, Object> map);

    Integer findMaintainTaskItemsCount(Map<String, Object> map);

    MaintainTaskItem findMaintainTaskItemById(Integer id);

    MaintainTaskItem findMaintainTaskItemByDeviceCode(@Param("maintainTaskId") Integer maintainTaskId, @Param("deviceCode") String deviceCode);

    MaintainTaskItem findMaintainTaskItemByArea(@Param("maintainTaskId") Integer maintainTaskId, @Param("area") String area);

    int insertMaintainTaskItem(MaintainTaskItem maintainTaskItem);

    void deleteMaintainTaskItemById(Integer id);

    void deleteMaintainTaskItems(int[] ids);

    int updateMaintainTaskItem(MaintainTaskItem maintainTaskItem);
}
