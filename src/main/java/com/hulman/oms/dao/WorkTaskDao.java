package com.hulman.oms.dao;

import com.hulman.oms.bean.NameValue;
import com.hulman.oms.bean.WorkTask;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WorkTaskDao
{
    List<WorkTask> findWorkTasks(Map<String, Object> map);

    Integer findWorkTasksCount(Map<String, Object> map);

    WorkTask findWorkTaskById(Integer id);

    int insertWorkTask(WorkTask workTask);

    void deleteWorkTaskById(Integer id);

    void deleteWorkTasks(int[] ids);

    int updateWorkTask(WorkTask workTask);

    List<WorkTask> findRecentWorkTasks(Integer count);

    Integer findTodayWorkTasksTotalCount();

    List<NameValue> statDailyWorkTasks(Integer days);
}
