package com.hulman.oms.dao;

import com.hulman.oms.bean.ElectricMeter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ElectricMeterDao
{
    List<ElectricMeter> findElectricMeters(Map<String, Object> map);

    Integer findElectricMetersCount(Map<String, Object> map);

    ElectricMeter findElectricMeterById(Integer id);

    int insertElectricMeter(ElectricMeter electricMeter);

    void deleteElectricMeterById(Integer id);

    void deleteElectricMeters(int[] ids);

    int updateElectricMeter(ElectricMeter electricMeter);
}
