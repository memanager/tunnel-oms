package com.hulman.oms.dao;

import com.hulman.oms.bean.WaterMeter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WaterMeterDao
{
    List<WaterMeter> findWaterMeters(Map<String, Object> map);

    Integer findWaterMetersCount(Map<String, Object> map);

    WaterMeter findWaterMeterById(Integer id);

    int insertWaterMeter(WaterMeter waterMeter);

    void deleteWaterMeterById(Integer id);

    void deleteWaterMeters(int[] ids);

    int updateWaterMeter(WaterMeter waterMeter);
}
