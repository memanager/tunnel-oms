package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseItem;
import com.hulman.oms.bean.SparepartReturnItem;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartPurchaseItemDao {
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartPurchaseItem record);

    int insertSelective(SparepartPurchaseItem record);

    SparepartPurchaseItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartPurchaseItem record);

    int updateByPrimaryKey(SparepartPurchaseItem record);

    List<SparepartPurchaseItem> findSparepartPurchaseItems(Map<String, Object> map);

    List<SparepartPurchaseItem> findSparepartPurchaseByOrderId(Integer orderId);

    Integer findSparepartPurchaseItemsCount(Map<String, Object> map);

    Integer deleteByOrderId(Integer orderId);
}