package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SparepartPurchaseInstockItemDao
{
    List<SparepartPurchaseInstockItem> findSparepartPurchaseInstockItems(Map<String, Object> map);

    List<SparepartPurchaseInstockItem> findSparepartPurchaseInstockItemsByOrderId(Integer orderId);

    Integer findSparepartPurchaseInstockItemsCount(Map<String, Object> map);

    SparepartPurchaseInstockItem findSparepartPurchaseInstockItemById(Integer id);

    int insertSparepartPurchaseInstockItem(SparepartPurchaseInstockItem sparepartPurchaseInstockItem);

    void deleteSparepartPurchaseInstockItemById(Integer id);

    void deleteSparepartPurchaseInstockItemByOrderId(Integer orderId);

    void deleteSparepartPurchaseInstockItems(int[] ids);

    int updateSparepartPurchaseInstockItem(SparepartPurchaseInstockItem sparepartPurchaseInstockItem);
}
