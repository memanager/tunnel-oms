package com.hulman.oms.dao;

import com.hulman.oms.bean.DeviceReplace;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DeviceReplaceDao
{
    List<DeviceReplace> findDeviceReplaces(Map<String, Object> map);

    Integer findDeviceReplacesCount(Map<String, Object> map);

    DeviceReplace findDeviceReplaceById(Integer id);

    int insertDeviceReplace(DeviceReplace deviceReplace);

    void deleteDeviceReplaceById(Integer id);

    void deleteDeviceReplaces(int[] ids);

    int updateDeviceReplace(DeviceReplace deviceReplace);
}
