package com.hulman.oms.dao;

import com.hulman.oms.bean.NameValue;
import com.hulman.oms.bean.Repair;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface RepairDao
{
    List<Repair> findRepairs(Map<String, Object> map);

    List<Repair> findTodayRepairs();

    Integer findRepairsCount(Map<String, Object> map);

    Repair findRepairById(Integer id);

    Repair findRepairByFaultId(Integer fault);

    int insertRepair(Repair repair);

    void deleteRepairById(Integer id);

    void deleteRepairs(int[] ids);

    int updateRepair(Repair repair);

    Integer findTodayCompletedRepairsCount(Integer tunnelId);

    List<Repair> findRecentRepairs(Integer count);

    Integer findTodayRepairsTotalCount();

    List<NameValue> statDailyRepairs(Integer days);
}
