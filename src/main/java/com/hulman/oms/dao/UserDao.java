package com.hulman.oms.dao;

import com.hulman.oms.bean.NameValue;
import com.hulman.oms.bean.User;
import com.hulman.oms.bean.UserStat;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface UserDao
{
    List<User> findUsers(Map<String, Object> map);

    Integer findUsersCount(Map<String, Object> map);

    User findUserById(Integer id);

    User findUserByName(String name);

    User findUserByAccount(String account);

    User findUserByMobile(String mobile);

    User findUserByLoginName(String loginName);

    int insertUser(User user);

    void deleteUserById(Integer id);

    void deleteUsers(String ids);

    int updateUser(User user);

    void toggleState(Integer id);

    void takeShiftLeader(Integer id);

    void untakeShiftLeaderForAll();

    User findShiftLeader();

    void changePassword(@Param("id") Integer id, @Param("password") String password);

    void changeSignState(@Param("id") Integer id,@Param("signState") Integer signState);

    void changeSync(@Param("id") Integer id,@Param("sync") Integer sync);

    List<UserStat> statUsers();

    Integer findOnDutyUsersCount();

    List<Map<String,Object>> statUserWorkload(Integer limit);

    List<NameValue> statDailyWorkOrders(Integer days);
}
