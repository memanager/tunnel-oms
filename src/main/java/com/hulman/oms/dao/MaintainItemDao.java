package com.hulman.oms.dao;

import com.hulman.oms.bean.MaintainItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaintainItemDao
{
    List<MaintainItem> findMaintainItems(Map<String, Object> map);

    List<MaintainItem> findMaintainItemsByMaintainId(Integer maintainId);

    Integer findMaintainItemsCount(Map<String, Object> map);

    MaintainItem findMaintainItemById(Integer id);

    int insertMaintainItem(MaintainItem maintainItem);

    void deleteMaintainItemById(Integer id);

    void deleteMaintainItems(int[] ids);

    int updateMaintainItem(MaintainItem maintainItem);
}
