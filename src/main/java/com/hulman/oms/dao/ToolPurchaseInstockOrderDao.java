package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseInstockOrder;
import com.hulman.oms.bean.ToolPurchaseInstockOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolPurchaseInstockOrderDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolPurchaseInstockOrder record);

    int insertSelective(ToolPurchaseInstockOrder record);

    ToolPurchaseInstockOrder selectByPrimaryKey(Integer id);

    ToolPurchaseInstockOrder selectByNo(String no);

    int updateByPrimaryKeySelective(ToolPurchaseInstockOrder record);

    int updateByPrimaryKey(ToolPurchaseInstockOrder record);

    List<ToolPurchaseInstockOrder> findToolPurchaseInstockOrders(Map<String, Object> map);

    Integer findToolPurchaseInstockOrdersCount(Map<String, Object> map);
}