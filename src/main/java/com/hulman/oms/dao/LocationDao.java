package com.hulman.oms.dao;

import com.hulman.oms.bean.Location;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface LocationDao
{
    List<Location> findLocations(Map<String, Object> map);

    List<Location> findLocationsByTunnelId(Integer tunnelId);

    Integer findLocationsCount(Map<String, Object> map);

    Location findLocationById(Integer id);

    Location findLocationByCode(String code);

    Location findLocationByName(String name);

    Location findLocationByTunnelIdAndAbbr(@Param("tunnelId") Integer tunnelId, @Param("abbr") String abbr);

    int insertLocation(Location location);

    void deleteLocationById(Integer id);

    void deleteLocations(int[] ids);

    int updateLocation(Location location);
}
