package com.hulman.oms.dao;

import com.hulman.oms.bean.Tool;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ToolDao
{
    List<Tool> findTools(Map<String, Object> map);

    Integer findToolsCount(Map<String, Object> map);

    Tool findToolById(Integer id);

    int insertTool(Tool tool);

    void deleteToolById(Integer id);

    void deleteTools(int[] ids);

    int updateTool(Tool tool);

    int addQuantity(Integer id, Integer delta);
}
