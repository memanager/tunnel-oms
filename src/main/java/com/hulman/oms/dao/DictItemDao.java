package com.hulman.oms.dao;

import com.hulman.oms.bean.DictItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DictItemDao
{
    List<DictItem> findDictItemsByDictId(Integer dictId);

    List<DictItem> findDictItemsByDictKey(String key);

    Integer findDictItemsCount(Map<String, Object> map);

    DictItem findDictItemById(Integer id);

    int insertDictItem(DictItem dictItem);

    void deleteDictItemById(Integer id);

    void deleteDictItems(String ids);

    int updateDictItem(DictItem dictItem);
}
