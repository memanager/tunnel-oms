package com.hulman.oms.dao;

import com.hulman.oms.bean.VehicleUse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface VehicleUseDao
{
    List<VehicleUse> findVehicleUses(Map<String, Object> map);

    Integer findVehicleUsesCount(Map<String, Object> map);

    VehicleUse findVehicleUseById(Integer id);

    int insertVehicleUse(VehicleUse vehicleUse);

    void deleteVehicleUseById(Integer id);

    void deleteVehicleUses(int[] ids);

    int updateVehicleUse(VehicleUse vehicleUse);
}
