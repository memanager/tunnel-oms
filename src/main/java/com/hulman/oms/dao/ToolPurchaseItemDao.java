package com.hulman.oms.dao;

import com.hulman.oms.bean.ToolPurchaseItem;
import com.hulman.oms.bean.ToolPurchaseItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolPurchaseItemDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolPurchaseItem record);

    int insertSelective(ToolPurchaseItem record);

    ToolPurchaseItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolPurchaseItem record);

    int updateByPrimaryKey(ToolPurchaseItem record);

    List<ToolPurchaseItem> findToolPurchaseItems(Map<String, Object> map);

    List<ToolPurchaseItem> findToolPurchaseByOrderId(Integer orderId);

    Integer findToolPurchaseItemsCount(Map<String, Object> map);

    Integer deleteByOrderId(Integer orderId);
}