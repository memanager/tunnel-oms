package com.hulman.oms.dao;

import com.hulman.oms.bean.ScadaReport;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ScadaReportDao
{
    List<ScadaReport> findScadaReports(Map<String, Object> map);

    Integer findScadaReportsCount(Map<String, Object> map);

    ScadaReport findScadaReportById(Integer id);

    ScadaReport findScadaReportByIdentifier(String identifier);

    int insertScadaReport(ScadaReport scadaReport);

    void deleteScadaReportById(Integer id);

    void deleteScadaReports(int[] ids);

    int updateScadaReport(ScadaReport scadaReport);
}
