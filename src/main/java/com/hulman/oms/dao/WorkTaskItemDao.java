package com.hulman.oms.dao;

import com.hulman.oms.bean.WorkTaskItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface WorkTaskItemDao
{
    List<WorkTaskItem> findWorkTaskItems(Map<String, Object> map);

    Integer findWorkTaskItemsCount(Map<String, Object> map);

    WorkTaskItem findWorkTaskItemById(Integer id);

    int insertWorkTaskItem(WorkTaskItem workTaskItem);

    void deleteWorkTaskItemById(Integer id);

    void deleteWorkTaskItems(int[] ids);

    int updateWorkTaskItem(WorkTaskItem workTaskItem);
}
