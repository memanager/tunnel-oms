package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartPurchaseInstockItem;
import com.hulman.oms.bean.SparepartUseItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SparepartUseItemDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(SparepartUseItem record);

    int insertSelective(SparepartUseItem record);

    SparepartUseItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SparepartUseItem record);

    int updateByPrimaryKey(SparepartUseItem record);

    List<SparepartUseItem> findSparepartUseByOrderId(Integer orderId);

    int deleteByOrderId(Integer orderId);

    Integer findSparepartUseItemsCount(Map<String, Object> map);

    List<SparepartUseItem> findSparepartUseItems(Map<String, Object> map);
}