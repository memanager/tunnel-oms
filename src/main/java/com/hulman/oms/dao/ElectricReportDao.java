package com.hulman.oms.dao;

import com.hulman.oms.bean.ElectricReport;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface ElectricReportDao
{
    List<ElectricReport> findElectricReports(Map<String, Object> map);

    Integer findElectricReportsCount(Map<String, Object> map);

    ElectricReport findElectricReportById(Integer id);

    int insertElectricReport(ElectricReport electricReport);

    void deleteElectricReportById(Integer id);

    void deleteElectricReports(int[] ids);

    int updateElectricReport(ElectricReport electricReport);
}
