package com.hulman.oms.dao;

import com.hulman.oms.bean.UserLicense;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserLicenseDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(UserLicense record);

    int insertSelective(UserLicense record);

    UserLicense selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserLicense record);

    int updateByPrimaryKey(UserLicense record);

    List<UserLicense> findUserLicenses(Map<String, Object> map);

    Integer findUserLicenseCount(Map<String, Object> map);

}