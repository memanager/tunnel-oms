package com.hulman.oms.dao;

import com.hulman.oms.bean.Driver;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface DriverDao
{
    List<Driver> findDrivers(Map<String, Object> map);

    Integer findDriversCount(Map<String, Object> map);

    Driver findDriverById(Integer id);

    int insertDriver(Driver driver);

    void deleteDriverById(Integer id);

    void deleteDrivers(int[] ids);

    int updateDriver(Driver driver);
}
