package com.hulman.oms.dao;

import com.hulman.oms.bean.Sign;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SignDao
{
    List<Sign> findSigns(Map<String, Object> map);

    Integer findSignsCount(Map<String, Object> map);

    Sign findSignById(Integer id);

    int insertSign(Sign Sign);

    void deleteSignById(Integer id);

    void deleteSigns(int[] ids);

    int updateSign(Sign Sign);

    Sign findLatestSignInSignByUserId(Integer userId);

}
