package com.hulman.oms.dao;

import com.hulman.oms.bean.Material;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface MaterialDao
{
    List<Material> findMaterials(Map<String, Object> map);

    Integer findMaterialsCount(Map<String, Object> map);

    Material findMaterialById(Integer id);

    int insertMaterial(Material material);

    void deleteMaterialById(Integer id);

    void deleteMaterials(int[] ids);

    int updateMaterial(Material material);
}
