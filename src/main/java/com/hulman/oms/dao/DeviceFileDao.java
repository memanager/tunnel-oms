package com.hulman.oms.dao;

import com.hulman.oms.bean.DeviceFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface DeviceFileDao
{
    int deleteByPrimaryKey(Integer id);

    int insert(DeviceFile record);

    int insertSelective(DeviceFile record);

    DeviceFile selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DeviceFile record);

    int updateByPrimaryKey(DeviceFile record);

    Integer findDeviceFilesCount(Map<String, Object> map);

    List<DeviceFile> findDeviceFiles(Map<String, Object> map);

    void deleteDeviceFileById(Integer id);

    void deleteDeviceFiles(int[] ids);

    DeviceFile findDeviceFilesByUuid(String uuid);
}