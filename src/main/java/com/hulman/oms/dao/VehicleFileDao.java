package com.hulman.oms.dao;

import com.hulman.oms.bean.VehicleFile;
import com.hulman.oms.bean.VehicleFile;
import com.hulman.oms.bean.VehicleFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleFileDao {
    int deleteByPrimaryKey(Integer id);

    int insert(VehicleFile record);

    int insertSelective(VehicleFile record);

    VehicleFile selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VehicleFile record);

    int updateByPrimaryKey(VehicleFile record);

    Integer findVehicleFilesCount(Map<String, Object> map);

    List<VehicleFile> findVehicleFiles(Map<String, Object> map);

    void deleteVehicleFileById(Integer id);

    void deleteVehicleFiles(int[] ids);

    VehicleFile findVehicleFilesByUuid(String uuid);

}