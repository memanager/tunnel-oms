package com.hulman.oms.dao;

import com.hulman.oms.bean.VehicleRefuel;
import com.hulman.oms.bean.VehicleRefuel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleRefuelDao {
    int deleteByPrimaryKey(Integer id);

    int insert(VehicleRefuel record);

    int insertSelective(VehicleRefuel record);

    VehicleRefuel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VehicleRefuel record);

    int updateByPrimaryKey(VehicleRefuel record);

    List<VehicleRefuel> findVehicleRefuels(Map<String, Object> map);

    Integer findVehicleRefuelsCount(Map<String, Object> map);
}