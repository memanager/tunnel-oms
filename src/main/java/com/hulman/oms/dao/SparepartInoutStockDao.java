package com.hulman.oms.dao;

import com.hulman.oms.bean.SparepartInoutStock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: maxwellens
 */
@Mapper
public interface SparepartInoutStockDao
{
    List<SparepartInoutStock> findSparepartInoutStocks(Map<String, Object> map);

    Integer findSparepartInoutStocksCount(Map<String, Object> map);

    SparepartInoutStock findSparepartInoutStockById(Integer id);

    int insertSparepartInoutStock(SparepartInoutStock sparepartInoutStock);

    void deleteSparepartInoutStockById(Integer id);

    void deleteSparepartInoutStockByOrderNo(String orderNo);

    void deleteSparepartInoutStocks(int[] ids);

    int updateSparepartInoutStock(SparepartInoutStock sparepartInoutStock);
}
