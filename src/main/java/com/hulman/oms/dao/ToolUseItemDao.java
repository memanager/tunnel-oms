package com.hulman.oms.dao;

import com.hulman.oms.bean.ToolUseItem;
import com.hulman.oms.bean.ToolUseItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ToolUseItemDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ToolUseItem record);

    int insertSelective(ToolUseItem record);

    ToolUseItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ToolUseItem record);

    int updateByPrimaryKey(ToolUseItem record);

    List<ToolUseItem> findToolUseByOrderId(Integer orderId);

    int deleteByOrderId(Integer orderId);

    Integer findToolUseItemsCount(Map<String, Object> map);

    List<ToolUseItem> findToolUseItems(Map<String, Object> map);
}