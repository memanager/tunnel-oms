function getParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]);
    return null; //返回参数值
}

function getProjectId() {
    return sessionStorage.getItem('projectId');
}

function checkGuest() {
    var isGuest = sessionStorage.getItem('isGuest');
    if (isGuest == '1') {
        layui.layer.alert('访客无操作权限', {icon: 2});
        return true;
    } else {
        return false;
    }
}

/**
 * 是否是管理员或负责人
 * @returns {string}
 */
function isManagerOrAdmin() {
    var manager = sessionStorage.getItem('manager');
    var admin = sessionStorage.getItem('admin');
    return manager == 'true' || admin == 'true';
}

var chnNumChar = ["零", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
var chnUnitSection = ["", "万", "亿", "万亿", "亿亿"];
var chnUnitChar = ["", "十", "百", "千"];

function sectionToChinese(section) {
    var strIns = '',
        chnStr = '';
    var unitPos = 0;
    var zero = true;
    while (section > 0) {
        var v = section % 10;
        if (v === 0) {
            if (!zero) {
                zero = true;
                chnStr = chnNumChar[v] + chnStr;
            }
        } else {
            zero = false;
            strIns = chnNumChar[v];
            strIns += chnUnitChar[unitPos];
            chnStr = strIns + chnStr;
        }
        unitPos++;
        section = Math.floor(section / 10);
    }
    return chnStr;
}

function toChineseNumber(num) {
    var unitPos = 0;
    var strIns = '',
        chnStr = '';
    var needZero = false;
    if (num === 0) {
        return chnNumChar[0];
    }
    while (num > 0) {
        var section = num % 10000;
        if (needZero) {
            chnStr = chnNumChar[0] + chnStr;
        }
        strIns = sectionToChinese(section);
        strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
        chnStr = strIns + chnStr;
        needZero = (section < 1000) && (section > 0);
        num = Math.floor(num / 10000);
        unitPos++;
    }
    return chnStr;
}
