
(function ($, window, document, undefined) {
    //==================================txt LIST===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var SystemCountWidget = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-system-count">
                <div class="title">N/A</div>
                <div class="info">
                    <div class="info1">
                        <div class="label">0</div>
                        <div class="value">0</div>
                    </div>
                    <div class="info2">
                        <div class="label">0</div>
                        <div class="value">0</div>
                    </div>
                    <div class="info3">
                        <div class="label">0</div>
                        <div class="value">0</div>
                    </div>
                </div>
               
               
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.title').text(this.options.title);
        this.$element.find('.info1').find('.label').text(this.options.label1);
        this.$element.find('.info2').find('.label').text(this.options.label2);
        this.$element.find('.info3').find('.label').text(this.options.label3);
        this.$element.find('.info1').find('.value').text(this.options.value1);
        this.$element.find('.info2').find('.value').text(this.options.value2);
        this.$element.find('.info3').find('.value').text(this.options.value3);

    }
    //2.定义组件的成员变量和方法
    SystemCountWidget.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {
            // var $label = this.$element.find(':nth-child(1)');
            // $label.text(p);

            //数据传输方式待定===============================
            // data.forEach(item => {
            //     if (item.name == '') {
            //         var $elem = this.$element.find('.device-name-value');
            //         $elem.text(item.displayValue);
            //     } else if (item.name == '') {
            //         var $elem = this.$element.find('.device-count-value');
            //         $elem.text(item.displayValue);

            //     }
            // });
        },

    }
    //3.注册组件为jquery插件
    $.fn.systemCountWidget = function (options) {
        var plugin = new SystemCountWidget(this, options);
        return plugin; //返回组件对象
    }
    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var BubbleWidget = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-bubble">
                <div class="info">
                    <div class="title">N/A</div>
                    <div class="value"></div>            
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.title').text(this.options.title);
        this.$element.find('.value').text(this.options.value);
        this.$element.css({ 'width': this.options.diameter, 'height': this.options.diameter, 'background-color': this.options.color });
        if (this.options.highlight) {
            this.$element.css({ 'border': this.options.color + 'double 0.3rem', 'box-shadow': this.options.color + '0 0 0.5rem' });
        }
    }
    //2.定义组件的成员变量和方法
    BubbleWidget.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.bubbleWidget = function (options) {
        var plugin = new BubbleWidget(this, options);
        return plugin; //返回组件对象
    }
    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var TimeLine = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-timeline">
                <div class="time"></div>
                <div class="point">❂</div>
                <div class="line"></div>
                <img class="icon"  src='../img/main/tunnel-icon.png'/>
                <div class="info">
                    <div class="title"></div>
                    <div class="infos">
                        <div class="length"></div>
                        <div class="area"></div>
                    </div>
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.time').text(this.options.time);
        this.$element.find('.title').text(this.options.title);
        this.$element.find('.length').text(this.options.length);
        this.$element.find('.icon').attr('src', this.options.icoUrl);
        this.$element.find('.area').text(this.options.area + '分');
        if (this.options.reverse) {
            this.$element.prepend(this.$element.children().get().reverse())
            this.$element.css({ 'margin-top': '-5rem' })
        }
        if (this.options.area < 80) {
            this.$element.find('.area').css({ 'color': 'orange' })
        }
    }
    //2.定义组件的成员变量和方法
    TimeLine.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.timeLine = function (options) {
        var plugin = new TimeLine(this, options);
        return plugin; //返回组件对象
    }
    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var LabelIconValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-label-icon-value">
                <div class="label"></div>
                <div class="mid-icon">
                    <img class="icon"  src='../img/main/mile-icon.png'/>
                </div>
                <div class="value"></div>

            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        this.$element.find('.value').text(this.options.value);
        this.$element.find('.icon').attr('src', this.options.icoUrl);

    }
    //2.定义组件的成员变量和方法
    LabelIconValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.labelIconValue = function (options) {
        var plugin = new LabelIconValue(this, options);
        return plugin; //返回组件对象
    }


    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var CarCountWidget = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-car-count">
                <div class="info-top">
                    <div class="title">加载中</div>
                    <div class="count">加载中</div>
                </div>
                <div class="info-bottom">
                    <div class="info">
                        <div class="label">出车次数</div>
                        <div class="value value1">加载中</div>
                    </div>
                    <div class="info">
                        <div class="label">行驶里程</div>
                        <div class="value value2">加载中</div>
                    </div>
                </div>

            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.title').text(this.options.title);
        this.$element.find('.count').text(this.options.count);
        this.$element.find('.value1').text(this.options.value1);
        this.$element.find('.value2').text(this.options.value2);

    }
    //2.定义组件的成员变量和方法
    CarCountWidget.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.carCountWidget = function (options) {
        var plugin = new CarCountWidget(this, options);
        return plugin; //返回组件对象
    }

    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var LabelValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-label-value">
                <div class="label">N/A</div>
                <div class="value"></div>          
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        this.$element.find('.value').text(this.options.value);
        if (this.options.color) {
            this.$element.find('.value').css({ 'color': this.options.color })
        }
    }
    //2.定义组件的成员变量和方法
    LabelValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.labelValue = function (options) {
        var plugin = new LabelValue(this, options);
        return plugin; //返回组件对象
    }
    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var LabelValueUnit = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-label-value-unit">
                <div class="label">N/A</div>
                <div class="info">
                    <div class="value"></div>    
                    <div class="unit"></div>    
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text('◈ ' + this.options.label);
        this.$element.find('.value').text(this.options.value);
        this.$element.find('.unit').text(this.options.unit);

    }
    //2.定义组件的成员变量和方法
    LabelValueUnit.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.labelValueUnit = function (options) {
        var plugin = new LabelValueUnit(this, options);
        return plugin; //返回组件对象
    }

    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var IconLabelValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-icon-label-value">
                <img class="icon"  src='../img/main/person-icon.png'/>
                <div class="label"></div>
                <div class="value"></div>

            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        if (this.options.color) {
            this.$element.find('.value').css({ 'color': this.options.color })
        }
        if (this.options.break) {
            let text = this.options.value
            let slashIndex = text.indexOf('/');
            let beforeSlash = text.slice(0, slashIndex);
            let afterSlash = text.slice(slashIndex);
            let $span = $(`<span></span>`)
            $span.text(afterSlash)
            $span.css({ 'color': 'gray', 'font-size': '0.8rem' })
            this.$element.find('.value').text(beforeSlash);
            this.$element.find('.value').append($span);

        } else {
            this.$element.find('.value').text(this.options.value);

        }

    }
    //2.定义组件的成员变量和方法
    IconLabelValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.iconLabelValue = function (options) {
        var plugin = new IconLabelValue(this, options);
        return plugin; //返回组件对象
    }


    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var LightLabelValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-light-label-value">
                <div class="light"></div>
                <div class="label"></div>
                <div class="value"></div>
                <div class="unit"></div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        this.$element.find('.value').text(this.options.value);
        this.$element.find('.unit').text(this.options.unit);
        if (this.options.color) {
            this.$element.find('.light').css({ 'background-color': this.options.color })
        }

    }
    //2.定义组件的成员变量和方法
    LightLabelValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.lightLabelValue = function (options) {
        var plugin = new LightLabelValue(this, options);
        return plugin; //返回组件对象
    }


    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var TripleLightLabelValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-triple-label-value">
                <div class="info info1">
                    <div class="label label1"></div>
                    <div class="value value1"></div>
                </div>
                <div class="info info2">
                    <div class="info">
                        <div class="label label2"></div>
                        <div class="value value2"></div>
                    </div>
                    <div class="info">
                        <div class="label label3"></div>
                        <div class="value value3"></div>
                    </div>
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label1').text('➣ ' + this.options.label1);
        this.$element.find('.value1').text(this.options.value1);
        this.$element.find('.label2').text(this.options.label2);
        this.$element.find('.value2').text(this.options.value2);
        this.$element.find('.label3').text(this.options.label3);
        this.$element.find('.value3').text(this.options.value3);

    }
    //2.定义组件的成员变量和方法
    TripleLightLabelValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.tripleLightLabelValue = function (options) {
        var plugin = new TripleLightLabelValue(this, options);
        return plugin; //返回组件对象
    }

    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var HexagonLabelValue = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-hexagon-label-value">
                <div class="label"></div>
                <div class="info ">
                    <div class="value value1"></div>
                    <div class="value value2"></div>
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        this.$element.find('.value1').text(this.options.value1);
        this.$element.find('.value2').text('/' + this.options.value2);
        if (this.options.smallSize) {
            this.$element.css({ 'transform': 'scale(0.8)' })
        }

    }
    //2.定义组件的成员变量和方法
    HexagonLabelValue.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.hexagonLabelValue = function (options) {
        var plugin = new HexagonLabelValue(this, options);
        return plugin; //返回组件对象
    }

    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var FileLabel = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-file-label">
                <div class="label"></div>
                <div class="lines">
                    <div class="point point1"></div>
                    <div class="point point2"></div>
                    <div class="point point3"></div>
                    <div class="line line1"></div>
                    <div class="line line2"></div>
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        let $lines = this.$element.find('.lines')
        let $line1 = this.$element.find('.line1')
        let $line2 = this.$element.find('.line2')
        let $point1 = this.$element.find('.point1')
        let $point2 = this.$element.find('.point2')
        let $point3 = this.$element.find('.point3')
        switch (this.options.pos) {
            case 'top-left':
                $line2.css({ transform: 'rotate(-45deg)' });
                $point3.css({ display: 'none' });
                break;
            case 'top-right':
                $line2.css({ transform: 'rotate(45deg)' });
                $point3.css({ display: 'none' });
                break;
            case 'top':
                $point2.css({ display: 'none' });
                $line1.css({ height: '6rem' });
                $line2.css({ display: 'none' });
                break;
            case 'bottom-left':
                $lines.css({ 'margin-top': '-7.5rem' });
                $line1.css({ transform: 'rotate(45deg)' });
                $line2.css({ height: '2.8rem' });
                $point1.css({ left: '2rem', top: '0.8rem' });
                break;
            case 'bottom-right':
                $lines.css({ 'margin-top': '-7.5rem' });
                $line1.css({ transform: 'rotate(-45deg)' });
                $line2.css({ height: '2.8rem' });
                $point1.css({ left: '-2rem', top: '0.8rem' });
                break;
            default: break;

        }
    }
    //2.定义组件的成员变量和方法
    FileLabel.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.fileLabel = function (options) {
        var plugin = new FileLabel(this, options);
        return plugin; //返回组件对象
    }


    //============================================================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var CircleLabel = function (parent, opt) {
        //this指代组件本身（DeviceCountList ）
        //设备名称
        const tpl =
            `<div class="widget-circle-label">
                <div class="circle">
                    <div class="value"></div>
                    <div class="unit"></div>
                </div>
                <div class="label"></div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0',
            'left': '0'
        };
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        // this.$element.find('.system-ico').attr('src', this.options.icoUrl);
        this.$element.find('.label').text(this.options.label);
        this.$element.find('.unit').text(this.options.unit);
        this.$element.find('.value').text(this.options.value);

        if (this.options.alarm) {
            this.$element.find('.value').css({ color: 'orange' });

        }
    }
    //2.定义组件的成员变量和方法
    CircleLabel.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data 
         */

        //数据传输方式待定===============================
        update(p, data) {

        },

    }
    //3.注册组件为jquery插件
    $.fn.circleLabel = function (options) {
        var plugin = new CircleLabel(this, options);
        return plugin; //返回组件对象
    }


    //================================= 自动填充表格 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var Formcell = function (parent, opt) {
        const tpl =
            ` <div class="formcell-row">
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        var that = this
        var c = 0
        for (var i in this.options) {
            let tpl_child = `<div class="formcell-col"></div>`
            this.$cell = $(tpl_child)
            if (typeof this.options[i] === 'object' && this.options[i] !== null) {
                this.$cell.text(this.options[i].value)
                this.$cell.css({ 'color': this.options[i].color, 'font-weight': '600' })
                this.$cell.attr('title', this.options[i].value)

            } else {
                this.$cell.text(this.options[i])
                this.$cell.attr('title', this.options[i])

            }
            c++;
            this.$cell.attr('id', "formcell-col-" + c)
            this.$element.append(this.$cell)
        }

    }
    //2.定义组件的成员变量和方法
    Formcell.prototype = {
        update(p, data) {
            // this.$element.children().eq(2).text(Object.values(data)[0].displayValue)
            // this.$element.children().eq(2).attr('title',Object.values(data)[0].displayValue)
        },
        detail(data, imgs) {
            //点击打开弹窗
            this.$element.on('click', function () {
                let $window = $('#container-window-test').windowWidget({
                    title: '详细信息',
                    width: '510px',
                    left: '700px',
                    top: '120px',
                    closeBro: 1,

                },)
                console.log($window);
                //在窗口组件中加入多个组件
                $window.createList({ hideSubTitle: 1, data: data });
                if (imgs) {
                    $window.createPic({ hideSubTitle: 1, data: imgs, size: 'small' })
                }
            })
        },
        detailIframe(data, ifr) {
            //点击打开弹窗
            this.$element.on('click', function () {
                let $window = $('#container-window-test').windowWidget({
                    title: '详细信息',
                    width: '510px',
                    left: '700px',
                    top: '120px',
                    closeBro: 1,

                },)
                console.log($window);
                //在窗口组件中加入多个组件
                $window.createList({ hideSubTitle: 1, data: data });
                if (ifr) {
                    $window.createIframe({ url: ifr.url })
                }
            })
        },
        pageToggle() {
            this.$element.on('click', function () {
                $('#panel-intelligence-bottom').css({ 'transform': 'rotateX(90deg)', 'opacity': '0' })
                $('#panel-intelligence-singal-ai').css({ 'transform': 'rotateX(0)', 'opacity': '1' })

            })
        }

    }
    //3.注册组件为jquery插件
    $.fn.formcell = function (options) {
        var plugin = new Formcell(this, options);
        return plugin;//返回组件对象
    }
})(jQuery, window, document);