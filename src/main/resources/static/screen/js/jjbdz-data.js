//青祁北变数据
var switchData1 = [
    // {name: "1213", code: "QQB_1213", top: "8.8rem", left: "17.9rem"},
    { name: "101", code: "JJ_101", top: "6.5rem", left: "39.55rem" },
    { name: "102", code: "JJ_102", top: "6.5rem", left: "73rem" },
    { name: "402", code: "JJ_402", top: "30.6rem", left: "7.26rem" },
    { name: "4215", code: "JJ_4215", top: "33.5rem", left: "45.05rem" },
    { name: "410", code: "JJ_410", top: "30.5rem", left: "56.3rem" },
    { name: "4115", code: "JJ_4115", top: "33.5rem", left: "70.4rem" },
    { name: "421", code: "JJ_421", top: "30.6rem", left: "109.2rem" },

    { name: "421A", code: "JJ_421A", top: "30.5rem", left: "12rem", type: 'h' },
    { name: "421B", code: "JJ_421B", top: "33.6rem", left: "12rem", type: 'h' },
    { name: "421C", code: "JJ_421C", top: "36.6rem", left: "12rem", type: 'h' },
    { name: "421D", code: "JJ_421D", top: "39.6rem", left: "12rem", type: 'h' },
    { name: "421E", code: "JJ_421E", top: "42.6rem", left: "12rem", type: 'h' },
    { name: "421F", code: "JJ_421F", top: "45.6rem", left: "12rem", type: 'h' },

    { name: "422A", code: "JJ_422A", top: "30.3rem", left: "20.8rem", type: 'h' },
    { name: "422B", code: "JJ_422B", top: "32.5rem", left: "20.8rem", type: 'h' },
    { name: "422C", code: "JJ_422C", top: "34.7rem", left: "20.8rem", type: 'h' },
    { name: "422D", code: "JJ_422D", top: "36.9rem", left: "20.8rem", type: 'h' },
    { name: "422E", code: "JJ_422E", top: "39.6rem", left: "20.8rem", type: 'h' },
    { name: "422F", code: "JJ_422F", top: "41.6rem", left: "20.8rem", type: 'h' },
    { name: "422G", code: "JJ_422G", top: "43.6rem", left: "20.8rem", type: 'h' },
    { name: "422H", code: "JJ_422H", top: "45.6rem", left: "20.8rem", type: 'h' },
    { name: "422I", code: "JJ_422I", top: "47.6rem", left: "20.8rem", type: 'h' },

    { name: "422J", code: "JJ_422J", top: "39.6rem", left: "29.7rem", type: 'h' },
    { name: "422K", code: "JJ_422K", top: "41.6rem", left: "29.7rem", type: 'h' },
    { name: "422L", code: "JJ_422L", top: "43.6rem", left: "29.7rem", type: 'h' },
    { name: "422M", code: "JJ_422M", top: "45.6rem", left: "29.7rem", type: 'h' },
    { name: "422N", code: "JJ_422N", top: "47.6rem", left: "29.7rem", type: 'h' },

    { name: "412A", code: "JJ_412A", top: "30.3rem", left: "82.6rem", type: 'h' },
    { name: "412B", code: "JJ_412B", top: "32.5rem", left: "82.6rem", type: 'h' },
    { name: "412C", code: "JJ_412C", top: "34.7rem", left: "82.6rem", type: 'h' },
    { name: "412D", code: "JJ_412D", top: "36.9rem", left: "82.6rem", type: 'h' },
    { name: "412E", code: "JJ_412E", top: "39.6rem", left: "82.6rem", type: 'h' },
    { name: "412F", code: "JJ_412F", top: "41.6rem", left: "82.6rem", type: 'h' },
    { name: "412G", code: "JJ_412G", top: "43.6rem", left: "82.6rem", type: 'h' },
    { name: "412H", code: "JJ_412H", top: "45.6rem", left: "82.6rem", type: 'h' },
    { name: "412I", code: "JJ_412I", top: "47.6rem", left: "82.6rem", type: 'h' },

    { name: "412J", code: "JJ_412J", top: "39.6rem", left: "91.7rem", type: 'h' },
    { name: "412K", code: "JJ_412K", top: "41.6rem", left: "91.7rem", type: 'h' },
    { name: "412L", code: "JJ_412L", top: "43.6rem", left: "91.7rem", type: 'h' },
    { name: "412M", code: "JJ_412M", top: "45.6rem", left: "91.7rem", type: 'h' },
    { name: "412N", code: "JJ_412N", top: "47.6rem", left: "91.7rem", type: 'h' },
    
    { name: "411A", code: "JJ_411A", top: "30.5rem", left: "99.9rem", type: 'h' },
    { name: "411B", code: "JJ_411B", top: "33.6rem", left: "99.9rem", type: 'h' },
    { name: "411C", code: "JJ_411C", top: "36.6rem", left: "99.9rem", type: 'h' },
    { name: "411D", code: "JJ_411D", top: "39.6rem", left: "99.9rem", type: 'h' },
    { name: "411E", code: "JJ_411E", top: "42.6rem", left: "99.9rem", type: 'h' },
    { name: "411F", code: "JJ_411F", top: "45.6rem", left: "99.9rem", type: 'h' },
];

var transformerData = [
    { name: "T2", code: "QQB_T2", label_a: "T2A", label_b: "T2B", label_c: "T2C", top: "20.5rem", left: "18rem" },
    { name: "T1", code: "QQB_T1", label_a: "T1A", label_b: "T1B", label_c: "T1C", top: "20.5rem", left: "80.5rem" },
];
// 温湿度
var thData = [
    { name: "HT", code: "JJ_HT", top: "4.5rem", left: "81rem" },
];
var reportData = [
    // {name: "401_1",code: "QQB_401_1"}, {name: "4115",code: "QQB_4115"},
    // {name: "410",code: "QQB_410"}, {name: "4215",code: "QQB_4215"},

];
