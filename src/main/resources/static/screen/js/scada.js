    //===============计时器=====================
    if( $('.clock').text()==''){
        $('.clock').text('0000-00-00' + "\xa0" +'00:00:00')
    }
    setInterval(function startTime() {
        var today = new Date()
        var y = today.getFullYear()
        var mon = today.getMonth() + 1
        var d = today.getDate()
        var h = today.getHours()
        var m = today.getMinutes()
        var s = today.getSeconds()
        // mon < 10 ? mon ='0'+ mon:
        mon = checkTime(mon)
        d = checkTime(d)
        h = checkTime(h)
        m = checkTime(m)
        s = checkTime(s)
        $('.clock').text(y + "-" + mon + "-" + d + "\xa0" + h + ":" + m + ":" + s);
    }, 1000);

    function checkTime(i) {
        if (i < 10) {
            i = '0' + i
        }
        return i
    }

//========================= 初始化rem单位 ===========================
; (function (win) {
    var tid;

    function refreshRem() {
        let designSize = 1920; // 设计图尺寸
        let html = document.documentElement;
        let wW = html.clientWidth;// 窗口宽度
        let rem = wW * 16 / designSize;
        document.documentElement.style.fontSize = rem + 'px';
    }

    win.addEventListener('resize', function () {
        clearTimeout(tid);
        tid = setTimeout(refreshRem, 30);
    }, false);
    win.addEventListener('pageshow', function (e) {
        if (e.persisted) {
            clearTimeout(tid);
            tid = setTimeout(refreshRem, 300);
        }
    }, false);

    refreshRem();

})(window);

/**
 * 查询多个设备的实时属性值
 * @param devices 设备编码(多个用半角逗号分隔)
 */
function findPropertiesByCodes(devices, callback) {
    $.ajax({
        url: 'https://tunnel.memanager.cn/dev/device-runtime-properties',
        type: 'POST',
        data: {
            devices
        },
        success: function (result) {
            // console.log(result.data);
            if (callback) {
                callback(result.data);
            }
        }
    });
}

/**
 * 查询多个设备的标签值
 * @param devices
 * @param callback
 */
function findDeviceLabelsByCodes(devices, callback) {
    $.ajax({
        url: '/dev/device-labels',
        type: 'POST',
        data: {
            devices
        },
        success: function (result) {
            if (callback) {
                callback(result.data);
            }
        }
    });
}

/**
 * 修改设备属性
 * eg: setDeviceProperty('xxx','BAC_xxx',1)
 * @param device 设备编码
 * @param property 属性名
 * @param value 属性值
 * @param callback 回调函数
 */
 function setDeviceProperty(device, property, value, callback) {
    var data = {};
    data[property] = value + '';
    $.ajax({
        url: '/dev/devices/' + device + '/properties',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        success: function (result) {
            console.log(result);
            if (callback) {
                callback(result);
            }
        }
    });
}

function sendMessage(data) {
    document.getElementById('gis-iframe').contentWindow.postMessage(data, "*");
}