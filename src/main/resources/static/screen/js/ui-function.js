//界面交互=========================================================================
let gisTemp = '2d'
$('#btn-toggle-gis').on('click',function(){
    if(gisTemp == '3d'){
        $('#gis-iframe').css( 'transform','scale(0)')
        $('#btn-toggle-gis').children().eq(1).text('城市模型')
        gisTemp = '2d'
    }else{
        $('#gis-iframe').css( 'transform','scale(1)')
        $('#btn-toggle-gis').children().eq(1).text('GIS地图')

        gisTemp = '3d'
    }
})
$('#panel-intelligence-singal-ai').find('.page-toggle').on('click', function () {
    $('#panel-intelligence-bottom').css({ 'transform': 'rotateX(0)', 'opacity': '1' })
    $('#panel-intelligence-singal-ai').css({ 'transform': 'rotateX(90deg)', 'opacity': '0' })

})
let $panelDigitalTwin = false
// let $panelDigitalTwin = $(`<iframe class="panel-digitalTwin" id="panel-digitalTwin" src="../webpack/index.html"></iframe>`)

$('#brain').on('click', function () {
    openChildPage({ url: 'https://chatglm.cn/main/alltoolsdetail?lang=zh', width: 600, height: 550, left: 0, top: 180 });
})
function openChildPage(opt) {
    // 打开新窗口
    var newWindow = window.open(opt.url, '_blank', 'width=' + opt.width + ',height=' + opt.height + ',left=' + opt.left + ',top=' + opt.top);

    // 确保新窗口获得焦点
    newWindow.focus();

    // 注意：以下两行代码通常是不必要的，因为它们已经在window.open()中设置过了
    // 如果需要，你可以取消注释，但请记住浏览器限制
    // newWindow.moveTo(opt.left, opt.top);
    // newWindow.resizeTo(opt.width, opt.height);
}

$('#classification-device-btns-type').hide()
$('.classification-btn').on('click', function () {
    $(this).addClass('btn-choosen')
    $(this).siblings().removeClass('btn-choosen')
})

$('#classification-btn-system').on('click', function () {
    $('#classification-device-btns-system').show()
    $('#classification-device-btns-type').hide()
    $('.classification-device-btns2').find('.classification-btn').removeClass('btn-choosen')
})
$('#classification-btn-type').on('click', function () {
    $('#classification-device-btns-system').hide()
    $('#classification-device-btns-type').show()
    $('.classification-device-btns2').find('.classification-btn').removeClass('btn-choosen')
})

$('#panel-tunnel-alarm').hide()
$('#btn-flip-tunnel-alarm').on('click', function () {
    $('#panel-tunnel-alarm').hide()
    $('#panel-bubble-sensor').show()
})
$('#btn-flip-bubble-sensor').on('click', function () {
    $('#panel-tunnel-alarm').show()
    $('#panel-bubble-sensor').hide()
})
$('#box-intelligence-import-device2').hide()
$('#btn-flip-intelligence-import-device1').on('click', function () {
    $('#box-intelligence-import-device1').hide()
    $('#box-intelligence-import-device2').show()
})
$('#btn-flip-intelligence-import-device2').on('click', function () {
    $('#box-intelligence-import-device1').show()
    $('#box-intelligence-import-device2').hide()
})

let $scrollDiv = $("body *").filter(function () {
    return $(this).css("overflow") === "auto" || $(this).css("overflow-y") === "scroll";
})
initNiceScroll('#container-table-person')
initNiceScroll('#container-table-car')
initNiceScroll('#container-table-tool')
initNiceScroll('#container-table-spare')
initNiceScroll('#container-table-intelligence-spare')
initNiceScroll('#container-table-intelligence-person')
initNiceScroll('#container-ai-real-warning')
initNiceScroll('#container-table-car-main')
function initNiceScroll(dom) {
    if (dom) {
        $(dom).niceScroll({
            cursorcolor: "aqua",
            cursorwidth: "0.5rem", // 自定义滚动条宽度
            cursorborder: "none", // 自定义滚动条边框样式
            horizrailenabled: false, // 启用水平滚动条
            autohidemode: true // 始终隐藏滚动条，除非用户滚动或鼠标悬停在滚动区域上
        });
    } else {
        // 选择所有具有 overflow: auto 或 overflow: scroll 的元素
        $scrollDiv.niceScroll({
            cursorcolor: "aqua",
            cursorwidth: "0.5rem", // 自定义滚动条宽度
            cursorborder: "none", // 自定义滚动条边框样式
            horizrailenabled: false,// 启用水平滚动条
            autohidemode: true // 始终隐藏滚动条，除非用户滚动或鼠标悬停在滚动区域上
        });
    }

    // let list = $(dom);
    // let listHeight = list.outerHeight();
    // // 开始自动滚动
    // setInterval(function () {
    //     list.animate({
    //         scrollTop: list.scrollTop() + 1 // 每次滚动1像素
    //     }, 0);

    //     // 检测是否到达列表底部，如果是，重新滚动到顶部
    //     if (list.scrollTop() + listHeight >= list[0].scrollHeight) {
    //         list.scrollTop(0);
    //     }
    // }, 80);

}


// 侧拉菜单
$('#btn-show-tunnel-menu').on('click', function () {
    $('#side-tunnel-tunnel').toggle()
})

tunnelMenuArry.forEach(item => {
    this.$tunnelBtn = $(`
         <div class="tunnel-btn">
             <div class="tunnel-main-btn">隧道名称</div>
             <div class="tunnel-sub-btns"></div>
         </div>
     `)
    this.$tunnelBtn.find('.tunnel-main-btn').text('➣ ' + item.title)
    this.$tunnelBtn.find('.tunnel-sub-btns').hide()
    let that = this

    if (item.btns) {
        item.btns.forEach(index => {
            this.$btn = $(`
                 <div class="tunnel-sub-btn">
                 </div>
             `)
            this.$btn.text(index.name)
            if (index.link) {
                this.$btn.on('click', function () {
                    let pathArray = window.location.href.split('/');
                    let protocol = pathArray[0];
                    let host = pathArray[2];
                    let baseUrl = protocol + '//' + host;
                    window.open(baseUrl + index.link)
                })
            }
            that.$tunnelBtn.find('.tunnel-sub-btns').append(this.$btn)
        })
        this.$tunnelBtn.find('.tunnel-main-btn').on('click', function () {
            $(this).siblings().toggle()
        })

    }
    $('#side-tunnel-tunnel').append(this.$tunnelBtn)
})
//报警弹窗
var alarmPanelTemp = 'hide'
alarmPanelHide()
$('#alarm-bell').on('click', function () {
    if (alarmPanelTemp == 'hide') {
        alarmPanelTemp = 'show'
        alarmPanelShow()
    } else {
        alarmPanelTemp = 'hide'
        alarmPanelHide()
    }
})
function alarmPanelHide() {
    $('#box-alarm-list').css({ 'transform': 'translateX(25rem)' })
}
function alarmPanelShow() {
    $('#box-alarm-list').css({ 'transform': '' })
}

let tasksPanelState = 'hide'
$('#btn-show-all-tasks').on('click', function () {
    if (tasksPanelState == 'hide') {
        $('#box-recent-cruise').show()
        $('#box-recent-work-tasks').show()
        $('#box-recent-faults').show()
        $('#box-recent-repairs').show()
        $('#box-recent-cruise').css({ left: '30rem', top: '1.5rem', transform: ' translateX(-50%) scale(0.9)' })
        $('#box-recent-work-tasks').css({ left: '90rem', top: '1.5rem', transform: ' translateX(-50%) scale(0.9)' })
        $('#box-recent-faults').css({ left: '30rem', top: '32rem', transform: ' translateX(-50%) scale(0.9)' })
        $('#box-recent-repairs').css({ left: '90rem', top: '32rem', transform: ' translateX(-50%) scale(0.9)' })
        $('.box-operation-side').css({ opacity: '0' })
        $('.btn-show-all-tasks').css({ width: '5rem', height: '5rem', top: '30.5rem', 'line-height': '5rem' })
        $('.btn-show-all-tasks').text('※')
        tasksPanelState = 'show'

    } else {
        $('#box-recent-cruise').css({ left: '', top: '', transform: '' })
        $('#box-recent-work-tasks').css({ left: '', top: '', transform: '' })
        $('#box-recent-faults').css({ left: '', top: '', transform: '' })
        $('#box-recent-repairs').css({ left: '', top: '', transform: '' })
        $('.box-operation-side').css({ opacity: '1' })
        $('.btn-show-all-tasks').css({ width: '', height: '', top: '', 'line-height': '' })
        $('.btn-show-all-tasks').text('▼')
        setTimeout(() => {
            $('#box-recent-work-tasks').hide()
            $('#box-recent-faults').hide()
            $('#box-recent-repairs').hide()

        }, 500)
        tasksPanelState = 'hide'
    }
})
$('#btn-change-tasks').find('.btn').on('click', function () {
    $(this).addClass('btn-choosen')
    $(this).siblings().removeClass('btn-choosen')
})
$('#box-recent-work-tasks').hide()
$('#box-recent-faults').hide()
$('#box-recent-repairs').hide()
$('#btn-cruise').on('click', function () {
    $('#box-recent-cruise').show()
    $('#box-recent-work-tasks').hide()
    $('#box-recent-faults').hide()
    $('#box-recent-repairs').hide()
    initNiceScroll('#container-recent-cruise')
})
$('#btn-tasks').on('click', function () {
    $('#box-recent-cruise').hide()
    $('#box-recent-work-tasks').show()
    $('#box-recent-faults').hide()
    $('#box-recent-repairs').hide()
    initNiceScroll('#container-recent-work-tasks')
})
$('#btn-faults').on('click', function () {
    $('#box-recent-cruise').hide()
    $('#box-recent-work-tasks').hide()
    $('#box-recent-faults').show()
    $('#box-recent-repairs').hide()
    initNiceScroll('#container-recent-faults')
})
$('#btn-repairs').on('click', function () {
    $('#box-recent-cruise').hide()
    $('#box-recent-work-tasks').hide()
    $('#box-recent-faults').hide()
    $('#box-recent-repairs').show()
    initNiceScroll('#container-recent-repairs')
})
// 底部按钮点击动画
let bottomBtnsTemp = 'hide'
hideBottomBtns()
$('#btn-zhgl').on('click', function () {
    hideBottomBtns()
    $('#panel-operation').css({ transform: 'translateY(0)' })
    $('#panel-intelligence').css({ transform: 'translateY(0)' })
    $('.panel-digitalTwin').css({ transform: 'translateY(0)' })
    // $('body').children('.panel-digitalTwin').remove();
})
$('#btn-ywgl').on('click', function () {
    hideBottomBtns()
    $('#panel-operation').css({ transform: 'translateY(128rem)' })
    $('#panel-intelligence').css({ transform: 'translateY(0)' })
    $('.panel-digitalTwin').css({ transform: 'translateY(0)' })

})
$('#btn-szls').on('click', function () {
    hideBottomBtns()
    $('#panel-operation').css({ transform: 'translateY(0)' })
    $('#panel-intelligence').css({ transform: 'translateY(0)' })
    if (!$panelDigitalTwin) {
        $panelDigitalTwin = $(`<iframe class="panel-digitalTwin" id="panel-digitalTwin" src="../webpack/index.html"></iframe>`)

        $('body').append($panelDigitalTwin)
        setTimeout(() => {
            $('.panel-digitalTwin').css({ transform: 'translateY(125rem)' })
        }, 200)
    } else {
        $('.panel-digitalTwin').css({ transform: 'translateY(125rem)' })
    }
})
$('#btn-zhjc').on('click', function () {
    hideBottomBtns()
    $('#panel-operation').css({ transform: 'translateY(0)' })
    $('#panel-intelligence').css({ transform: 'translateY(125rem)' })
    $('.panel-digitalTwin').css({ transform: 'translateY(0)' })

})
$('#btns-bottom').find('.btn').on('click', function () {
    $(this).addClass('btn-choosen')
    $(this).siblings().removeClass('btn-choosen')
})
$('#toggle-btns-bottom').on('click', function () {
    if (bottomBtnsTemp == 'hide') {
        showBottomBtns()
    } else {
        hideBottomBtns()
    }

})
function showBottomBtns() {
    bottomBtnsTemp = 'show'
    $('#btns-bottom').css({ 'transform': 'translateX(-50%) translateY(0)' })
    $('#btns-bottom').find('.btns').css({ 'transform': 'rotate(0) translateY(-0)' })
    $('#panel-bottom').css({ 'opacity': '0.2', 'filter': 'blur(0.2rem)' })
}
function hideBottomBtns() {
    bottomBtnsTemp = 'hide'
    $('#btns-bottom').css({ 'transform': 'translateX(-50%) translateY(3rem)' })
    $('#btns-bottom').find('.btns').css({ 'transform': 'rotate(180deg) translateY(-4rem)' })
    $('#panel-bottom').css({ 'opacity': '1', 'filter': 'blur(0)' })

}
