// const { color } = require("echarts");

//创建柱状图
function createBarOption(option) {
    var option = {
        title: {
            // text: '特性示例：渐变色 阴影 点击缩放',
            // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        // legend: {
        //     data: [option.label1, option.label2, option.label3],
        //     x: '45%',
        //     // y:'center'
        //     textStyle: {
        //         fontFamily: '微软雅黑',
        //         fontSize: 15,
        //         fontStyle: 'normal',
        //         fontWeight: '100',
        //         color: 'white',
        //         letterSpacing: 2,
        //     }
        // },
        grid: {
            left: '10%',
            top: '25%'
        },
        xAxis: [{
            data: option.dataX,
            position: 'bottom',
            type: 'category',
            axisLabel: {
                // show: false,
                inside: false,
                color: 'rgba(0, 255, 255, 0.87)',
                // interval: 0
            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: false
            },
            splitLine: {
                show: false,
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 0.5)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [25, 20, 0, -8],
                left: '120%',
            },
            z: 10
        },
        ],

        yAxis: {
            name: option.labelY,

            type: 'value',
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 1)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 15, // 大小
                padding: [0, 0, 2, 0]
            },

        },
        dataZoom: [
            {
                type: 'inside'
            }
        ],
        series: [
            {
                // name: option.label1,
                type: 'bar',
                barWidth: '50%',
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                        { offset: 0, color: 'rgba(0, 255, 255,0.9)' },
                        { offset: 0.5, color: 'rgba(0, 255, 255,0.5)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),

                    // color: 'rgba(255, 223, 154,0.6)'
                },
                data: option.data,
                markPoint: {
                    data: [
                        {
                            type: 'max',
                            name: '最大值',
                            symbolSize: 45, // 调整标注的大小
                            itemStyle: {
                                color: 'red' // 调整标注的颜色
                            }
                        }, {
                            type: 'min',
                            name: '最小',
                            symbolSize: 45, // 调整标注的大小
                            itemStyle: {
                                color: 'rgb(13, 219, 82)' // 调整标注的颜色
                            }
                        }
                    ]
                }
                // xAxisIndex: 0,
            },

            // 折线
            {
                // name: option.label3,
                data: option.data,
                type: 'line',
                // inverse:'true',
                itemStyle: {
                    color: 'rgb(255, 221, 148)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        { offset: 1, color: 'rgba(255, 221, 148,0)' },
                        { offset: 0.9, color: 'rgba(255, 221, 148,0.2)' },
                        { offset: 0.6, color: 'rgba(255, 221, 148,0.5)' },
                        { offset: 0, color: 'rgba(255, 221, 148,0.8)' }
                    ])
                },
                // xAxisIndex: 2,
                smooth: true
            }
        ]
    };
    return option
}

//创建双折线图
function createDoubleLineOption(option) {
    var option = {
        title: {
            // text: '特性示例：渐变色 阴影 点击缩放',
            // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: [option.label1, option.label2],
            x: '35%',
            y:'0%',
            icon: 'triangle',
            itemHeight: 15,
            itemWidth: 18,
            itemGap: 30,
            // y:'center'
            textStyle: {
                fontFamily: '微软雅黑',
                fontSize: 15,
                fontStyle: 'normal',
                fontWeight: '100',
                color: 'white',
                letterSpacing: 2,
            }
        },
        grid: {
            left: '8%',
            top: '25%',
            bottom: '25%',
            right:'15%'
        },
        xAxis: [{
            name: '',
            data: option.dataX,
            // position: 'bottom',
            type: 'category',
            axisLabel: {
                show: true,
                inside: false,
                color: 'rgba(0, 255, 255, 0.87)',
            },
            // axisTick: {
            //     show: false
            // },
            // axisLine: {
            //     show: false
            // },
            // splitLine: {
            //     show: false,
            // },
            // nameTextStyle: {
            //     show: false,
            //     // color: 'rgba(255, 255, 255, 0.5)',
            //     // fontSize: 12, // 大小
            //     // padding: [25, 20, 0, -8],
            //     // left: '120%',
            // },
            // z: 10
        },
        ],

        yAxis:[ {
            type: 'value',
            name: option.labelY1,
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgb(255, 176, 6)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [0, 0, 2, 0]
            },

        }, {
            type: 'value',
            name: option.labelY2,
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgb(28, 255, 149)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [0, 0, 2, 0]
            },

        },],
        // dataZoom: [
        //     {
        //         type: 'inside'
        //     }
        // ],
        series: [
            {
                name: option.label1,
                type: 'line',
                barWidth: '30%',
                showBackground: true,
                itemStyle: {
                    color: 'rgb(255, 176, 6)',
                    
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        { offset: 0, color: 'rgb(255, 176, 6)' },
                        { offset: 0.5, color: 'rgba(255, 206, 99,0.8)' },
                        { offset: 1, color: 'rgba(255, 206, 99,0)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data1,
                label: {
                    show: false,
                    position: 'inside'
                },
                smooth:true,
                markPoint: {
                    data: [
                        {
                            type: 'max',
                            name: '最大值',
                            symbolSize: 30, // 调整标注的大小
                            itemStyle: {
                                color: 'red' // 调整标注的颜色
                            }
                        }, 
                    ]
                }
                // xAxisIndex: 0,
            },

            {
                name: option.label2,
                type: 'line',
                barWidth: '30%',
                yAxisIndex: 1,
                showBackground: true,
                

                itemStyle: {
                    color: 'rgb(28, 255, 149)',
                    
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0,1, [
                        { offset: 0, color: 'rgba(28, 255, 149,0.9)' },
                        { offset: 0.5, color: 'rgba(28, 255, 149,0.5)' },
                        { offset: 1, color: 'rgba(255, 206, 99,0)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data2,
                label: {
                    show: false,
                    position: 'inside'
                },
                smooth:true,
                markPoint: {
                    data: [
                        {
                            type: 'max',
                            name: '最大值',
                            symbolSize: 30, // 调整标注的大小
                            itemStyle: {
                                color: 'red' // 调整标注的颜色
                            }
                        }, 
                    ]
                }

                // xAxisIndex: 0,
            },
        ]
    };
    return option
}
//创建双柱状图
function createDoubleBarOption(option) {
    var option = {
        title: {
            // text: '特性示例：渐变色 阴影 点击缩放',
            // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: [option.label1, option.label2],
            x: '35%',
            y:'0%',
            // y:'center'
            textStyle: {
                fontFamily: '微软雅黑',
                fontSize: 15,
                fontStyle: 'normal',
                fontWeight: '100',
                color: 'white',
                letterSpacing: 2,
            }
        },
        grid: {
            left: '5%',
            top: '25%',
            bottom: '15%',
            right:'15%'
        },
        xAxis: [{
            name: '',
            data: option.dataX,
            // position: 'bottom',
            type: 'category',
            axisLabel: {
                show: true,
                inside: false,
                color: 'rgba(0, 255, 255, 0.87)',
                interval: 0

            },
            // axisTick: {
            //     show: false
            // },
            // axisLine: {
            //     show: false
            // },
            // splitLine: {
            //     show: false,
            // },
            // nameTextStyle: {
            //     show: false,
            //     // color: 'rgba(255, 255, 255, 0.5)',
            //     // fontSize: 12, // 大小
            //     // padding: [25, 20, 0, -8],
            //     // left: '120%',
            // },
            // z: 10
        },
        ],

        yAxis:[ {
            type: 'value',
            name: option.labelY1,
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 1)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [0, 0, 2, 0]
            },
            max:function (value) {
                return value.max + 5; // 假设我们希望Y轴的最大值比数据中的最大值大20
            }


        }, {
            type: 'value',
            name: option.labelY2,
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 1)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [0, 0, 2, 0]
            },

        },],
        // dataZoom: [
        //     {
        //         type: 'inside'
        //     }
        // ],
        series: [
            {
                name: option.label1,
                type: 'bar',
                barWidth: '20%',
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                        { offset: 0, color: 'rgba(0, 255, 255,0.9)' },
                        { offset: 0.5, color: 'rgba(0, 255, 255,0.5)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data1,
                label: {
                    show: false,
                    position: 'inside'
                },
                // xAxisIndex: 0,
            },

            {
                name: option.label2,
                type: 'bar',
                barWidth: '20%',
                yAxisIndex: 1,
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                        { offset: 0, color: 'rgba(255, 66, 0,0.5)' },
                        { offset: 0.5, color: 'rgb(255, 166, 0,0.9)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data2,
                label: {
                    show: false,
                    position: 'inside'
                },
                // xAxisIndex: 0,
            },
        ]
    };
    return option
}
// 双柱状图类型2
function createDoubleBarOptionType2(option) {
    var option = {
        title: {
            // text: '特性示例：渐变色 阴影 点击缩放',
            // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: [option.label1, option.label2],
            x: '35%',
            y:'0%',
            // y:'center'
            textStyle: {
                fontFamily: '微软雅黑',
                fontSize: 15,
                fontStyle: 'normal',
                fontWeight: '100',
                color: 'white',
                letterSpacing: 2,
            }
        },
        grid: {
            left: '5%',
            top: '25%',
            bottom: '15%',
            right:'15%'
        },
        xAxis: [{
            name: '',
            data: option.dataX,
            // position: 'bottom',
            type: 'category',
            axisLabel: {
                show: true,
                inside: false,
                color: 'rgba(0, 255, 255, 0.87)',
                interval: 0

            },

        },
        ],

        yAxis:{
            type: 'value',
            // name: option.labelY1,
            name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'white'
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 1)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 12, // 大小
                padding: [0, 0, 2, 0]
            },
            max:function (value) {
                return value.max + 5; // 假设我们希望Y轴的最大值比数据中的最大值大20
            }


        },
        series: [
            {
                name: option.label1,
                type: 'bar',
                barWidth: '20%',
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                        { offset: 0, color: 'rgba(0, 255, 255,0.9)' },
                        { offset: 0.5, color: 'rgba(0, 255, 255,0.5)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data1,
                label: {
                    show: false,
                    position: 'inside'
                },
                // xAxisIndex: 0,
            },
          
            {
                name: option.label2,
                type: 'bar',
                barWidth: '20%',
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                        { offset: 0, color: 'rgba(255, 66, 0,0.5)' },
                        { offset: 0.5, color: 'rgb(255, 166, 0,0.9)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [15, 15, 0, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data2,
                label: {
                    show: false,
                    position: 'inside'
                },
                // xAxisIndex: 0,
            },
        ]
    };
    return option
}


//创建排行柱状图
function createRankBarOption(option) {
    var option = {
        title: {
            // text: '特性示例：渐变色 阴影 点击缩放',
            // subtext: 'Feature Sample: Gradient Color, Shadow, Click Zoom'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        // legend: {
        //     data: [option.label1, option.label2, option.label3],
        //     x: '45%',
        //     // y:'center'
        //     textStyle: {
        //         fontFamily: '微软雅黑',
        //         fontSize: 15,
        //         fontStyle: 'normal',
        //         fontWeight: '100',
        //         color: 'white',
        //         letterSpacing: 2,
        //     }
        // },
        grid: {
            left: '40%',
            top: '5%',
            bottom: '20%'
        },
        xAxis: [{
            name: option.labelX,
            position: 'bottom',
            type: 'value',
            axisLabel: {
                show: false,
                // inside: false,
                // color: 'rgba(0, 255, 255, 0.87)'

            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: false
            },
            splitLine: {
                show: false,
            },
            nameTextStyle: {
                show: false,
                // color: 'rgba(255, 255, 255, 0.5)',
                // fontSize: 12, // 大小
                // padding: [25, 20, 0, -8],
                // left: '120%',
            },
            z: 10
        },
        ],

        yAxis: {
            type: 'category',
            data: option.dataY,
            // name: option.labelY,
            splitLine: {
                show: false // 不显示网格线
            },
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: 'rgb(255, 206, 99)'
            },
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 1)',
                // fontFamily: 'BankGothic Md BT', // 字体
                fontSize: 15, // 大小
                padding: [0, 0, 2, 0]
            },

        },
        dataZoom: [
            {
                type: 'inside'
            }
        ],
        series: [
            {
                // name: option.label1,
                type: 'bar',
                barWidth: '50%',
                showBackground: true,
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
                        { offset: 0, color: 'rgba(0, 255, 255,0.9)' },
                        { offset: 0.5, color: 'rgba(0, 255, 255,0.5)' },
                        // { offset: 0.9, color: 'rgba(0, 255, 255,0.2)' },
                        { offset: 1, color: 'rgb(255, 206, 99)' }
                    ]),
                    // color: 'rgba(255, 223, 154,0.6)'
                    barBorderRadius: [0, 15, 15, 0] // 分别设置上左、上右、下右、下左的圆角半径
                },
                data: option.data,
                label: {
                    show: true,
                    position: 'inside'
                },
                // xAxisIndex: 0,
            },

            // // 折线
            // {
            //     // name: option.label3,
            //     data: option.data,
            //     type: 'line',
            //     // inverse:'true',
            //     itemStyle: {
            //         color: 'rgb(255, 221, 148)'
            //     },
            //     areaStyle: {
            //         color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
            //             { offset: 1, color: 'rgba(255, 221, 148,0)' },
            //             { offset: 0.9, color: 'rgba(255, 221, 148,0.2)' },
            //             { offset: 0.6, color: 'rgba(255, 221, 148,0.5)' },
            //             { offset: 0, color: 'rgba(255, 221, 148,0.8)' }
            //         ])
            //     },
            //     xAxisIndex: 2,
            //     smooth: true
            // }
        ]
    };
    return option
}


//创建饼图
function createPieOption(option) {
    let ledOpt
    option.line ? ledOpt = {orient:'horizontal',left:'center',center:['50%', '58%']}:ledOpt = { orient: 'vertical',left: 'left',center:['65%', '38%']}
    var option = {
        color: ['aqua','orange', 'rgb(28, 255, 149)','yellow'],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            orient: ledOpt.orient,  // 设置布局方式为垂直
            // left: option.legendPostion,
            left: ledOpt.left,
            top: '15%',     // 设置 legend 的位置为左侧
            textStyle: {         // 设置文字样式
                color: '#fff'    // 设置文字颜色为白色
            }

        },
        toolbox: {
            show: false,
            feature: {
                mark: { show: true },
                dataView: { show: true, readOnly: false },
                restore: { show: true },
                saveAsImage: { show: true }
            }
        },
        series: [
            {
                name: 'Radius Mode',
                type: 'pie',
                radius: [35, 50],
                center:ledOpt.center,
                // roseType: 'radius',
                itemStyle: {
                    borderRadius: 10,
                    // borderColor: 'aqua',
                    // borderWidth: 1,
                    normal: {
                        // // 使用函数为每个扇区设置不同的渐变色
                        // color: function (params) {
                        //     // params 是一个包含当前扇区信息的对象
                        //     // params.dataIndex 是当前扇区的索引
                        //     // params.seriesIndex 是当前扇区所在的系列索引
                        //     var colorList = [
                        //         new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        //             offset: 0, color: 'rgba(255, 27, 27, 1)' // 0% 处的颜色
                        //         }, {
                        //             offset: 1, color: 'rgba(255, 157, 27, 0.5)' // 100% 处的颜色
                        //         }]),
                        //         new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        //             offset: 1, color: 'rgba(0,255,255,1)' // 0% 处的颜色
                        //         }, {
                        //             offset: 0, color: 'rgba(100,255,155,0.5)' // 100% 处的颜色
                        //         }]),                                
                        //         //... 可以添加更多的渐变色
                        //     ];
                        //     params.dataIndex>0?params.dataIndex=1:undefined;
                        //     // return colorList[params.dataIndex % colorList.length];
                        //     return colorList[params.dataIndex];
                        // }
                    }
                },
                label: {
                    show: false,
                    // normal: {
                    //     textStyle: {
                    //         color: 'aqua', // 设置字体颜色为红色
                    //         fontWeight: 600
                    //     }
                    // }
                },
                // 设置 labelLine 的样式和长度
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                emphasis: {
                    label: {
                        show: true
                    }
                },
                data: option.data
            },

        ]
    };
    return option
}

//创建百分比评分饼图
function createScorePieOption(option) {
    // option.data.sort((a, b) => b.value - a.value);
    var option = {
        color: ['aqua', 'rgb(13, 219, 82)'],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        title: {
            text: option.title, // 这里设置标题文本
            left: 'center', // 标题位置居中
            bottom: '30%', // 标题距离饼图下侧的距离
            textStyle: {
                color: 'rgba(100,255,155,0.8)', // 设置字体颜色为绿色
                fontSize: 12, // 设置字体大小为15
                fontWeight: 'bold' // 设置字体加粗
            }
        },
        // legend: {
        //     orient: 'vertical',  // 设置布局方式为垂直
        //     left: 'left',   
        //     top:'15%',     // 设置 legend 的位置为左侧
        //     textStyle: {         // 设置文字样式
        //         color: '#fff'    // 设置文字颜色为白色
        //     }

        // },
        toolbox: {
            show: false,
            feature: {
                mark: { show: true },
                dataView: { show: true, readOnly: false },
                restore: { show: true },
                saveAsImage: { show: true }
            }
        },
        series: [
            {
                name: 'Radius Mode',
                type: 'pie',
                radius: [30, 50],
                center: ['50%', '30%'],
                // roseType: 'radius',
                itemStyle: {
                    borderRadius: 10,
                    // borderColor: 'aqua',
                    // borderWidth: 1,
                    normal: {
                        // // 使用函数为每个扇区设置不同的渐变色
                        color: function (params) {
                            // params 是一个包含当前扇区信息的对象
                            // params.dataIndex 是当前扇区的索引
                            // params.seriesIndex 是当前扇区所在的系列索引
                            var colorList = [
                                new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                    offset: 0, color: 'rgba(255, 166, 0, 1)' // 0% 处的颜色
                                }, {
                                    offset: 1, color: 'rgba(0, 250, 250, 0.8)' // 100% 处的颜色
                                }]),
                                new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                    offset: 1, color: 'rgba(0,255,255,0.3)' // 0% 处的颜色
                                }, {
                                    offset: 0, color: 'rgba(100,255,155,0.2)' // 100% 处的颜色
                                }]),
                                //... 可以添加更多的渐变色
                            ];
                            params.dataIndex > 0 ? params.dataIndex = 1 : undefined;
                            // return colorList[params.dataIndex % colorList.length];
                            return colorList[params.dataIndex];
                        }
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'center',
                        formatter: function (params) {
                            // 只显示第一个值（最大的值）的百分比
                            if (params.dataIndex === 0) {
                                return (params.percent).toFixed(1) + '%';
                            } else {
                                return '';
                            }
                        },
                        textStyle: {
                            color: 'aqua', // 设置字体颜色为红色
                            fontWeight: 600,
                            fontSize: 18
                        }
                    }
                },
                // 设置 labelLine 的样式和长度
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                emphasis: {
                    label: {
                        show: true
                    }
                },
                data: option.data
            },

        ]
    };
    return option
}
//饼图自动触摸高亮动画
function pieChartAnimation(myChart) {
    var isSet = true // 为了做判断：当鼠标移动上去的时候，自动高亮就被取消
    var charPie3currentIndex = 0
    var chart = eval(myChart)
    // 2、鼠标移动上去的时候的高亮动画
    chart.on('mouseover', function (param) {
        isSet = false
        clearInterval(startCharts)
        // 取消之前高亮的图形
        chart.dispatchAction({
            type: 'downplay',
            seriesIndex: 0,
            dataIndex: charPie3currentIndex
        })
        // 高亮当前图形
        chart.dispatchAction({
            type: 'highlight',
            seriesIndex: 0,
            dataIndex: param.dataIndex
        })
        // 显示 tooltip
        chart.dispatchAction({
            type: 'showTip',
            seriesIndex: 0,
            dataIndex: param.dataIndex
        })
    })
    // 3、自动高亮展示
    var chartHover = function () {
        var dataLen = createPieOption(data2).series[0].data.length
        // 取消之前高亮的图形
        chart.dispatchAction({
            type: 'downplay',
            seriesIndex: 0,
            dataIndex: charPie3currentIndex
        })
        charPie3currentIndex = (charPie3currentIndex + 1) % dataLen
        // 高亮当前图形
        chart.dispatchAction({
            type: 'highlight',
            seriesIndex: 0,
            dataIndex: charPie3currentIndex
        })
        // 显示 tooltip
        chart.dispatchAction({
            type: 'showTip',
            seriesIndex: 0,
            dataIndex: charPie3currentIndex
        })
    }
    startCharts = setInterval(chartHover, 3000)
    // 4、鼠标移出之后，恢复自动高亮
    chart.on('mouseout', function (param) {
        if (!isSet) {
            startCharts = setInterval(chartHover, 3000)
            isSet = true
        }
    })
}

//创建折线图
function createLineOption(data_x, data1, data2, data3, name1, name2, name3) {
    var option = {
        grid: {
            // show:true,
            left: "10%",
            top: "18%",
            right: "2%",
            bottom: "15%"

        },
        legend: {
            textStyle: {
                fontFamily: 'BankGothic Md BT',
                fontSize: 15,
                fontStyle: 'normal',
                fontWeight: 'normal',
                color: 'white',
            },
        },
        xAxis: {
            type: 'category',
            data: data_x,
            axisLabel: {
                color: 'aqua'
            },
            // name: '时间',
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 0.5)',
                fontFamily: 'Source Han Sans CN', // 字体
                fontSize: 12, // 大小
                padding: [25, 20, 0, -8]
            },
            axisLine: {
                show: true,  //这里的show用于设置是否显示y轴那一条线 默认为true
                lineStyle: { //lineStyle里面写y轴那一条线的样式
                    color: '#6FC6F3',   //轴线的粗细 我写的是2 最小为0，值为0的时候线隐藏
                }
            }
        },
        yAxis: {
            type: 'value',
            // max: 100,
            axisLabel: {
                color: 'aqua'
            },
            name: '次数',
            nameTextStyle: {
                color: 'rgba(255, 255, 255, 0.5)',
                fontFamily: 'Source Han Sans CN', // 字体
                fontSize: 12, // 大小
                padding: [25, 20, 0, -8]
            },
            axisLine: {
                show: true,  //这里的show用于设置是否显示y轴那一条线 默认为true
                lineStyle: { //lineStyle里面写y轴那一条线的样式
                    color: '#6FC6F3',   //轴线的粗细 我写的是2 最小为0，值为0的时候线隐藏
                }
            },
            splitLine: {
                lineStyle: { //lineStyle里面写y轴那一条线的样式
                    color: 'rgba(255, 153, 57,0.2)',   //轴线的粗细 我写的是2 最小为0，值为0的时候线隐藏
                }
            }
        },
        series: [
            {
                name: name1,
                data: data1,
                type: 'line',
                itemStyle: {
                    color: 'rgb(255, 153, 57)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        { offset: 0, color: 'rgb(255, 153, 57)' },
                        { offset: 0.3, color: 'rgba(255, 153, 57,0.5)' },
                        { offset: 1, color: 'rgba(255, 153, 57,0)' }
                    ])
                },
                smooth: true
            },
            {
                name: name2,
                data: data2,
                type: 'line',
                itemStyle: {
                    color: 'rgba(255, 187, 0, 1)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        { offset: 0, color: 'rgba(255, 187, 0, 0.8)' },
                        { offset: 0.3, color: 'rgba(255, 187, 0, 0.5)' },
                        { offset: 1, color: 'rgba(255, 236, 185,0)' }
                    ])
                },
                smooth: true
            },
            {
                name: name3,
                data: data3,
                type: 'line',
                itemStyle: {
                    color: 'red'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                        { offset: 0, color: 'red' },
                        { offset: 0.3, color: 'rgba(255, 0, 0,0.5)' },
                        { offset: 1, color: 'rgba(255, 236, 185,0)' }
                    ])
                },
                smooth: true
            }
        ]
    };
    return option
}