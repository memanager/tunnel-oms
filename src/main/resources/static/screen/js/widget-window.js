//========================================================================
; (function ($, window, document, undefined) {
    //==================================窗口容器===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var WindowWidget = function (parent, opt) {
        //添加按钮div
        var tpl = `<div class="widget-window">
                        <div class="widget-window-line1">
                            <div class="title">我的窗口</div>
                            <div class="time">00:00</div>
                        </div>
                        <div class="close">⊗</div>
                        <div class="show-btn left-show-btn">《</div>
                        <div class="show-btn right-show-btn">》</div>
                        <div class="container"></div>
                    </div>`;
        this.$element = $(tpl);
        this.$parent = parent;

        this.defaults = {
            'top': 0,
            'left': 0,
            'title': "信息窗口",
        };
        var that = this
        this.options = $.extend({}, this.defaults, opt);
        // 判断是否关闭其他窗口
        if (this.options.closeBro == 1) {
            $('.widget-window').remove()
        }
        this.$parent.append(this.$element);

        this.$element.css('top', this.options.top);
        if (this.options.right) {
            this.$element.css('right', this.options.right);
        } else {
            this.$element.css('left', this.options.left);
        }
        this.$element.css('width', this.options.width);
        this.$element.find('.widget-window-line1').find('.title').text('◈ ' + this.options.title);
        this.$element.find('.close').on('click', function () {
            that.$element.hide()
        })
        this.$element.find('.show-btn').hide()
        this.$element.find('.right-show-btn').on('click', function () {
            that.$element.css({ 'transform': 'translateX(1%)' })
            that.$element.find('.show-btn').hide()
        })
        this.$element.find('.left-show-btn').on('click', function () {
            that.$element.css({ 'transform': 'translateX(-1%)' })
            that.$element.find('.show-btn').hide()
        })
        //-----------------------------窗口时间-------------------------------
        setInterval(function startTime() {
            var today = new Date()
            var h = today.getHours()
            var m = today.getMinutes()
            h = checkTime(h)
            m = checkTime(m)
            that.$element.find('.widget-window-line1').find('.time').text(h + ":" + m);
        }, 1000);

        function checkTime(i) {
            if (i < 10) {
                i = '0' + i
            }
            return i
        }
        //-----------------------------窗口拖拽-------------------------------
        // 获取DOM元素
        let dragDiv = this.$element[0]
        let dragArea = this.$element.find('.widget-window-line1')[0]
        // 鼠标按下事件 处理程序
        let putDown = function (event) {
            let offsetX = parseInt(dragDiv.style.left) // 获取当前的x轴距离
            let offsetY = parseInt(dragDiv.style.top) // 获取当前的y轴距离
            let innerX = event.clientX - offsetX // 获取鼠标在方块内的x轴距
            let innerY = event.clientY - offsetY // 获取鼠标在方块内的y轴距
            // 按住鼠标时为div添加一个效果
            that.$element.css('box-shadow', 'rgba(0, 255, 255, 0.5) 0 0 1rem');
            // 鼠标移动的时候不停的修改div的left和top值
            document.onmousemove = function (event) {
                dragDiv.style.left = event.clientX - innerX + 'px'
                dragDiv.style.top = event.clientY - innerY + 'px'

                let ww = document.documentElement.clientWidth + 'px'
                let leftMax = document.documentElement.clientWidth - dragDiv.offsetWidth + 'px'
                let topMax = document.documentElement.clientHeight - dragDiv.offsetHeight + 'px'

                // 边界判断
                if (parseInt(dragDiv.style.left) <= 0) {
                    dragDiv.style.left = '0px'
                }
                if (parseInt(dragDiv.style.top) <= 0) {
                    dragDiv.style.top = '0px'
                }
                if (parseInt(dragDiv.style.left) >= parseInt(leftMax)) {
                    dragDiv.style.left = leftMax
                }
                if (parseInt(dragDiv.style.top) >= parseInt(topMax)) {
                    dragDiv.style.top = topMax
                }

            }
            // 鼠标抬起时，清除绑定在文档上的mousemove和mouseup事件
            // 否则鼠标抬起后还可以继续拖拽方块
            document.onmouseup = function () {
                document.onmousemove = null
                document.onmouseup = null
                // 清除box-shadow
                that.$element.css('box-shadow', '');
                let leftMax = document.documentElement.clientWidth - dragDiv.offsetWidth + 'px'
                // 新增功能-靠近边界后隐藏面板
                if (parseInt(dragDiv.style.left) == 0) {
                    // that.$element.css({'animation':'windowHideLeft 0.5s 0.5s forwards'})
                    setTimeout(function () {
                        that.$element.css({ 'transform': 'translateX(-100%)' })
                    }, 500)
                    setTimeout(function () {
                        that.$element.find('.show-btn').show()
                    }, 600)
                }
                if (parseInt(dragDiv.style.left) == parseInt(leftMax)) {
                    setTimeout(function () {
                        that.$element.css({ 'transform': 'translateX(100%)' })
                    }, 500)
                    setTimeout(function () {
                        that.$element.find('.show-btn').show()
                    }, 600)
                }

            }
        }
        // 绑定鼠标按下事件
        dragArea.addEventListener('mousedown', putDown, false)

        //将不同组件添加到窗口中
        //type1：列表组件
        // if (this.options.type == 1) {
        //     this.$element.find('.container').empty();
        //     this.$element.find('.container').windowListWidget(this.options);
        // }
        // //type2：按钮组件
        // else if (this.options.type == 2) {
        //     this.$element.find('.container').empty();
        //     this.$element.find('.container').windowBtnsWidget(this.options);
        // }
        // //type3：图片组件
        // else if (this.options.type == 3) {
        //     this.$element.find('.container').empty();
        //     this.$element.find('.container').windowPicWidget(this.options);
        // }
        // //type4：iframe/视频组件
        // else if (this.options.type == 4) {
        //     this.$element.find('.container').empty();
        //     this.$element.find('.container').windowIframeWidget(this.options);
        // }
    }
    //2.定义组件的成员变量和方法
    WindowWidget.prototype = {
        title(text) {
            this.$element.find('.widget-window-line1').find('.title').text('◈ ' + text);
        },
        update(data) {

        },
        container() {
            return this.$element.find('.container');
        },
        empty() {
            this.$element.find('.container').empty();
        },
        createList(opt) {
            return this.$element.find('.container').windowListWidget(opt);
        },
        createBtn(opt) {
            return this.$element.find('.container').windowBtnsWidget(opt);
        },
        createPic(opt) {
            return this.$element.find('.container').windowPicWidget(opt);
        },
        createIframe(opt) {
            return this.$element.find('.container').windowIframeWidget(opt);
        },
    }
    //3.注册组件为jquery插件
    $.fn.windowWidget = function (options) {
        var plugin = new WindowWidget(this, options);
        return plugin;//返回组件对象
    }


    //================================== 窗口表格组件 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var WindowListWidget = function (parent, opt) {
        //this指代组件本身（txtList）
        //设备名称
        const tpl = `<div class="widget-window-list">
                        <div class="main-label window-widget-subtitle">我的标题</div>
                        <div class="container-list"></div>
                    </div>`;

        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0', 'left': '0'
        };
        var that = this
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.find('.main-label').text('▪ ' + this.options.label)
        var listData = this.options.data

        if (this.options.hideSubTitle == 1) {
            this.$element.find('.main-label').hide()
        }

        //创建一行内容
        listData.forEach(item => {
            var option = $.extend({}, item);
            var row = `<div class="list-row">
                            <div class="label">加载中</div>
                            <div class="value">加载中</div>
                        </div>`;
            this.$row = $(row);
            that.$element.find('.container-list').append(this.$row)
            this.$row.find('.label').text(option.label)
            this.$row.find('.value').text(option.value)
            if (option.alarm == 1) {
                this.$row.find('.label').css({ 'background': 'white', 'color': 'red', 'border': 'red solid 0.1rem', 'font-weight': '600' })
                this.$row.find('.value').css({ 'background': 'red', 'color': 'white', 'border': 'red solid 0.1rem', 'font-weight': '600' })
            }
        });




    }
    //2.定义组件的成员变量和方法
    WindowListWidget.prototype = {
        update(data) {

        },
    }
    //3.注册组件为jquery插件
    $.fn.windowListWidget = function (options) {
        var plugin = new WindowListWidget(this, options);
        return plugin;//返回组件对象
    }


    //================================== 窗口按钮组件 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var WindowBtnsWidget = function (parent, opt) {
        //this指代组件本身（txtList）
        //设备名称
        const tpl = `<div class="widget-window-btns">
                        <div class="main-label window-widget-subtitle">我的标题</div>
                        <div class="container-list"></div>
                    </div>`;

        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0', 'left': '0'
        };
        var that = this
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.find('.main-label').text('▪ ' + this.options.label)
        var listData = this.options.data

        if (this.options.hideSubTitle == 1) {
            this.$element.find('.main-label').hide()
        }

        //创建一行内容
        listData.forEach(item => {
            var option = $.extend({}, item);
            var row = `<div class="list-row">
                            <div class="window-btn"></div>
                        </div>`;
            this.$row = $(row);
            that.$element.find('.container-list').append(this.$row)
            this.$row.find('.window-btn').text(option.label + ' ☚')

            this.$row.find('.window-btn').on('click', function () {
                if (!option.callback) {
                    alert('no callback')
                } else {
                    option.callback(option.param)
                }
            })

        });

    }
    //2.定义组件的成员变量和方法
    WindowBtnsWidget.prototype = {
        update(data) {

        },
    }
    //3.注册组件为jquery插件
    $.fn.windowBtnsWidget = function (options) {
        var plugin = new WindowBtnsWidget(this, options);
        return plugin;//返回组件对象
    }


    //================================== 窗口图片组件 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var WindowPicWidget = function (parent, opt) {
        //this指代组件本身（txtList）
        //设备名称
        const tpl = `<div class="widget-window-pic">
                        <div class="main-label window-widget-subtitle">我的标题</div>
                        <div class="container-list"></div>
                    </div>`;

        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0', 'left': '0'
        };
        var that = this
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.update(opt);

    }
    //2.定义组件的成员变量和方法
    WindowPicWidget.prototype = {
        update(opt) {
            var that = this
            this.options = $.extend({}, this.defaults, opt);
            this.$element.find('.main-label').text('▪ ' + this.options.label)
            if (this.options.hideSubTitle == 1) {
                this.$element.find('.main-label').hide()
            }
            this.$element.find('.container-list').empty();
            //创建一行内容
            var picTitle = this.options.label
            const row = `<div class="list-row">
                            <img class="pic">
                            <div class="line"></div>
                        </div>`;
            const rowSmall = `<div class="list-row-small">
                                <img class="pic">
                                <div class="line" style="display:none"></div>
                            </div>`;
            this.options.data.forEach(item => {
                var option = $.extend({}, item);
                //判断图片尺寸
                if( this.options.size == 'small'){
                    this.$row = $(rowSmall)
                    this.$element.find('.container-list').addClass('container-list-flex')
                }else{
                    this.$row = $(row)
                    this.$element.find('.container-list').removeClass('container-list-flex')
                }
                this.$element.find('.container-list').append(this.$row)
                this.$row.find('.pic').attr('src', option.url)
                if (option.scan == 0) {
                    this.$row.find('.line').hide()
                    this.$row.find('.pic').css('animation', 'none')
                }

                this.$row.on('click', function () {
                    layer.open({
                        title: picTitle,
                        area: ['650px', '600px'],
                        content: '<img  class="layui-upload-img" style="width: auto;height: 98%;margin:0 auto" id="ProdPicture"/>'
                    });
                    //根据id查询图片
                    $('#ProdPicture').attr('src', option.url)
                })
            });


        },
    }
    //3.注册组件为jquery插件
    $.fn.windowPicWidget = function (options) {
        var plugin = new WindowPicWidget(this, options);
        return plugin;//返回组件对象
    }


    //================================== 窗口网页/视频播放组件 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var WindowIframeWidget = function (parent, opt) {
        //this指代组件本身（txtList）
        //设备名称
        const tpl = `<div class="widget-window-iframe">
                        <iframe class="my-iframe" name="my-iframe"></iframe>
                    </div>`;

        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': '0', 'left': '0'
        };
        var that = this
        // 叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.find('.my-iframe').attr('src', this.options.url)

    }
    //2.定义组件的成员变量和方法
    WindowIframeWidget.prototype = {
        update(data) {

        },
    }
    //3.注册组件为jquery插件
    $.fn.windowIframeWidget = function (options) {
        var plugin = new WindowIframeWidget(this, options);
        return plugin;//返回组件对象
    }

})(jQuery, window, document);