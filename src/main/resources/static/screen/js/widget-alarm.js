
//================================= 告警列表项 ===========================================
const projectUrl = 'https://tunnel.memanager.cn/'
//报警红点
$('.alarm-dot').hide()
// refreshDot()
function refreshDot() {
    var sysArry = [{ name: '', sysString: 'all' }, { name: '供配电', sysString: 'power' }, { name: '给排水', sysString: 'water' }, { name: '通排风', sysString: 'air' }, { name: '网关', sysString: 'gateway' }]
    sysArry.forEach(item => {
        $.get(projectUrl + '/device-alarms?page=1&limit=10&system=' + item.name.substring(0, item.name.length) + '&state=0', function (result) {
            if (result.data.length > 0) {
                $('.alarm-dot-' + item.sysString).show()
                //设备页面右上角 铃铛样式
                $('#btn-show-alarm').css({ 'filter': 'hue-rotate(-45deg) drop-shadow(red 0 0 0.5rem)' })
            } else {
                $('.alarm-dot-' + item.sysString).hide()
                if (item.name == '') {
                    $('#btn-show-alarm').css({ 'filter': '' })
                }
            }
        });
    })
}

//定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
//1.定义组件的构造函数
var AlarmItem = function (parent, opt) {
    const tpl =
        ` <div class="panel-alarm-list-main">
                <div class="list-line-1"> 
                    <div class="panel-alarm-device">暂无设备信息</div>
                    <div class="panel-alarm-time">暂无时间</div>
                </div>
                <div class="panel-alarm-info">暂无告警信息1</div>

                <div class="panel-operation">
                    <div class="btn confirm">确认</div>
                    <div class="btn handle">处理</div>
                    <div class="btn model">定位</div>
                    <div class="btn cancel">×</div>
                </div>
            </div>`;
    this.$parent = parent;
    this.$element = $(tpl);
    this.$parent.append(this.$element);
    this.defaults = {
        'top': 0,
        'left': 0,
        'label_width': '8rem',
    };
    //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
    // console.log(this.options);
    var that = this
    this.options = $.extend({}, this.defaults, opt);
    this.$element.css('top', this.options.top);
    this.$element.css('left', this.options.left);

    this.$element.find('.panel-alarm-device').text(this.options.deviceName);
    this.$element.find('.panel-alarm-time').text(this.options.simpleCreateTime);
    this.$element.find('.panel-alarm-info').text(this.options.msg);

    this.$element.find('.panel-alarm-device').attr('title', this.options.deviceName);
    this.$element.find('.panel-alarm-info').attr('title', this.options.info);

    // this.hideBtns()
    //未确认报警会打开操作栏
    // if (this.options.state == 1) {
    //     this.$element.find('.panel-alarm-info').on('click', function () {
    //         that.showBtns()
    //     })
    // }
    this.$element.find('.panel-alarm-info').on('click', function () {
        that.showBtns()

    })
    //确认告警
    this.$element.find('.confirm').on('click', function () {
        that.hideBtns();
        $.get(projectUrl + '/device-alarms/' + that.options.id + '/confirm', function (result) {
            if (result.code == 200) {
                layer.msg('告警已确认');
                refreshDot()
            } else {
                layer.msg(result.msg);
            }
        });
    })
    //处理告警
    this.$element.find('.handle').on('click', function () {
        that.hideBtns();
        $.get(projectUrl + '/device-alarms/' + that.options.id + '/handle', function (result) {
            if (result.code == 200) {
                layer.msg('告警已处理');
                refreshDot()
            } else {
                layer.msg(result.msg);
            }
        });
    })
    //报警定位
    this.$element.find('.model').on('click', function () {
        that.hideBtns();
        sendMessage({ func: 'flyToAlarmDevice', param: that.options.deviceCode });
        $('#robot-ctr-btn').show()
        $('#panel-top').css({ 'top': '-20rem', 'opacity': 0 })
        $('#fixed-inspection-btn').show()
        $('#container-window-test').show()
        $('.com-container').css({
            'transform': 'scale(5)',
            'opacity': '0',
            'visibility': 'hidden',
            'filter': 'blur(1.5rem)'
        })
    })

    this.$element.find('.cancel').on('click', function () {
        that.hideBtns()
    })

}
//2.定义组件的成员变量和方法
AlarmItem.prototype = {
    /**
     * 根据数据更新组件显示
     * @param {组件数据} data
     */
    update(p, data) {
        // this.$element.find('').text(data[''].value);
    },
    showBtns() {
        this.$element.find('.panel-operation').css({ 'left': '0', 'opacity': '1' })
    },
    hideBtns() {
        this.$element.find('.panel-operation').css({ 'left': '-20rem', 'opacity': '0' })
    },
}
//3.注册组件为jquery插件
$.fn.alarmItem = function (options) {
    var plugin = new AlarmItem(this, options);
    return plugin;//返回组件对象
}

//================================= 告警列表 ===========================================
//定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
//1.定义组件的构造函数
var AlarmList = function (parent, opt) {
    const tpl =
        `<div>
                <div class="title-alarm-window">{{标题}}</div>
                <div class="panel-choose-alarm-type">
                    <div class="alarm-type-btn alarm-type-btn-choosen" data-type="all">全部<div class="alarm-dot alarm-dot-all"></div></div>
                    <div class="alarm-type-btn" data-type="power">供配电<div class="alarm-dot alarm-dot-power"></div> </div>
                    <div class="alarm-type-btn" data-type="water">排水<div class="alarm-dot alarm-dot-water"></div></div>
                    <div class="alarm-type-btn" data-type="air">通排风<div class="alarm-dot alarm-dot-air"></div></div>
                    <div class="alarm-type-btn" data-type="gateway">网关<div class="alarm-dot alarm-dot-gateway"></div></div>
                </div>
                <div class="container-alarm-list"></div>
            </div>`;
    const noDataTpl = `<div class="no-data-txt"> 暂无数据... </div>`
    this.$parent = parent;
    this.$element = $(tpl);
    this.$noDataTpl = $(noDataTpl);
    this.$parent.append(this.$element);
    this.defaults = {
        'top': 0,
        'left': 0,
        'label_width': '8rem',
    };
    //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
    var that = this
    this.options = $.extend({}, this.defaults, opt);
    this.state = this.options.state;
    this.typeTxt = '';
    this.type = '';
    this.$element.css('top', this.options.top);
    this.$element.css('left', this.options.left);
    if (this.options.title) {
        this.$element.find('.title-alarm-window').text(this.options.title);
    } else {
        this.$element.find('.title-alarm-window').hide()
    }
    this.$element.find('.alarm-type-btn').on('click', function () {
        that.typeTxt = $(this).text();
        that.type = $(this).data('type');
        if (that.typeTxt == '全部') {
            that.typeTxt = ''
            that.type = 'all'
        } else if (that.typeTxt == '排水') {
            that.typeTxt = '给排水'
        }
        $(this).addClass('alarm-type-btn-choosen');
        $(this).siblings().removeClass('alarm-type-btn-choosen');
        that.refresh();
    })
    //鼠标移入组件：不刷新数据防止误操作，解决组件闪烁问题。
    this.$element.on('mouseover', function () {
        clearInterval(that.refreshTimer)
    })
    this.$element.on('mouseout', function () {
        that.refreshDataByTime()
    })
    this.refreshDataByTime()
}
//2.定义组件的成员变量和方法
AlarmList.prototype = {
    /**
     * 根据数据更新组件显示
     * @param {组件数据} data
     */
    update(data) {
        this.data = data;
        this.refresh();
        // console.log('报警：'+JSON.stringify(data))
    },
    refreshDataByTime() {
        let that = this
        this.refreshTimer = setInterval(function () {
            that.refresh();
        }, 2000)
    },
    refresh() {
        let that = this
        let type = that.type
        // refreshDot()
        $.get(projectUrl + '/device-alarms?page=1&limit=10&system=' + that.typeTxt.substring(0, that.typeTxt.length) + '&state=0', function (result) {
        // $.get( '../mock/alarm.json', function (result) {
            that.$element.find('.container-alarm-list').empty();
            result.data.forEach(item => {
                var option = $.extend({}, item);
                that.$element.find('.container-alarm-list').alarmItem(option)
            });

            if (result.data.length < 1) {
                that.$element.find('.container-alarm-list').append(that.$noDataTpl)
            } else {
                that.$element.find('.container-alarm-list').children('.no-data-txt').remove()
            }

        });
    }
}
//3.注册组件为jquery插件
$.fn.alarmList = function (options) {
    var plugin = new AlarmList(this, options);
    return plugin;//返回组件对象
}