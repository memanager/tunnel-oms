; (function (win) {
    var tid;

    function refreshRem() {
        let designSize = 1920; // 设计图尺寸
        let html = document.documentElement;
        let wW = html.clientWidth;// 窗口宽度
        let rem = wW * 16 / designSize;
        document.documentElement.style.fontSize = rem + 'px';
    }

    win.addEventListener('resize', function () {
        clearTimeout(tid);
        tid = setTimeout(refreshRem, 30);
    }, false);
    win.addEventListener('pageshow', function (e) {
        if (e.persisted) {
            clearTimeout(tid);
            tid = setTimeout(refreshRem, 300);
        }
    }, false);

    refreshRem();

})(window);


//========================================================================
; (function ($, window, document, undefined) {

    //================================== 开关1 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var Switch1 = function (parent, opt) {

        const tpl =
            `<div class="bdz">
                <div class="label">N/A</div>
                <img src="../img/bdz/switch_on.svg" class="switch-img switch-on"></img>
                <img src="../img/bdz/switch_off.svg" class="switch-img switch-off"></img>
                <img src="../img/bdz/switch_na.svg" class="switch-img switch-na"></img>
                <div class="circuit-info circuit-info-type1">负荷名称(1)</div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0
        };
        var that = this;
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        // this.$element.find('.label').text(this.options.name.slice(3));
        this.$element.find('.label').text(this.options.name);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);

        var $switch_on = this.$element.find('.switch-on');
        var $switch_off = this.$element.find('.switch-off');
        var $switch_na = this.$element.find('.switch-na');
        // $switch_on.hide();
        // $switch_off.hide();
        // $switch_na.show();

        $switch_on.hide();
        $switch_off.show();
        $switch_na.hide();

        this.$switchImg = this.$element.find('.switch-img');
        this.$switchLabel = this.$element.find('.label')
        //打开电表详情窗口


        if (this.options.type == 'h') {
            this.$element.find('.switch-img').css({ 'transform': 'rotate(90deg)', 'margin-top': '-4rem', 'margin-left': '1.8rem' })
        }

    }
    //2.定义组件的成员变量和方法
    Switch1.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
            this.$element.find('.circuit-info').text(data['LoadName'].value);
        },
        update(p, data) {
            let that = this
            this.data = data;
            this.data = simulateValue(p, this.data);
            var $switch_on = this.$element.find('.switch-on');
            var $switch_off = this.$element.find('.switch-off');
            var $switch_na = this.$element.find('.switch-na');

            if (data['Uab'] || data['Ubc'] || data['Uca']) {
                if (data['Uab'].value || data['Ubc'].value || data['Uca'].value) {
                    $switch_on.show();
                    $switch_off.hide();
                    $switch_na.hide();
                } else {
                    $switch_on.hide();
                    $switch_off.show();
                    $switch_na.hide();
                }
            } else {
                $switch_on.hide();
                $switch_off.hide();
                $switch_na.show();
            }
            this.alarmOff();

            this.$switchImg.on('click', function () {
                switchPanel = new SwitchPanel(that.options);
                switchPanel.show({
                    title: that.options.name,
                    data: that.data
                });
                switchPanel.createCurrrentCharts(
                    that.options.name,
                    that.data
                );
                //$('.switch-panel').css({'transform': 'translateX(0rem)', 'opacity': '1'})
            })
        },

        alarmOn() {
            this.$switchLabel.css({ 'animation': 'switchAlarmFlash 0.5s infinite linear' })
            // this.$element.css('cursor', 'pointer');
            // var that = this
            // this.$element.on('mouseover', function () {
            //     that.$switchAlarmInfo.show()
            // });
            // this.$element.on('mouseout', function () {
            //     that.$switchAlarmInfo.hide()
            // });
        },
        alarmOff() {
            this.$switchLabel.css({ 'animation': '' })
            // this.$element.css('cursor', '');
        },
    }
    //3.注册组件为jquery插件
    $.fn.switch1 = function (options) {
        var plugin = new Switch1(this, options);
        return plugin;//返回组件对象
    }


    //================================= 变压器统计界面 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var Transformer = function (parent, opt) {
        //this指代组件本身（transformer）

        const tpl =
            `<div class="transformer">
                <div class="transformer-cell">
                    <div class="label label-a"></div> 
                    <div class="value value-a">N/A</div>
                </div> 
                <div class="transformer-cell">
                    <div class="label label-b"></div> 
                    <div class="value value-b">N/A</div>
                </div> 
                <div class="transformer-cell">
                    <div class="label label-c"></div> 
                    <div class="value value-c">N/A</div>
                </div> 
                   
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0,
            'title': "N/A",
        };
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);

        this.$element.find('.label-a').text(this.options.label_a + ":");
        this.$element.find('.label-b').text(this.options.label_b + ":");
        this.$element.find('.label-c').text(this.options.label_c + ":");

        if (this.options.value) {
            this.$element.find('.value').text(this.options.value);
        }
    }
    //2.定义组件的成员变量和方法
    Transformer.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
        },
        update(p, data) {
            this.$element.find('.value-a').text(data['IA'].displayValue);
            this.$element.find('.value-b').text(data['IB'].displayValue);
            this.$element.find('.value-c').text(data['IC'].displayValue);
            /*if (data['Ta'].value > 85) {
                this.$element.find('.value-a').css({'color': 'red'})
            }
            if (data['Tb'].value > 85) {
                this.$element.find('.value-b').css({'color': 'red'})
            }
            if (data['Tc'].value > 85) {
                this.$element.find('.value-c').css({'color': 'red'})
            }*/
        },

    }
    //3.注册组件为jquery插件
    $.fn.transformer = function (options) {
        var plugin = new Transformer(this, options);
        return plugin;//返回组件对象
    }


    //================================= 温湿度统计  ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var RTRH = function (parent, opt) {
        //this指代组件本身（transformer）

        const tpl =
            `<div class="rtrh">
                   <div class="label label-rt">温度:</div> 
                   <div class="value value-rt">正在读取...</div>
                   <div class="label label-rh">湿度:</div> 
                   <div class="value value-rh">正在读取...</div>
                   
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0,
            'title': "N/A",
        };
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);

    }
    //2.定义组件的成员变量和方法
    RTRH.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
        },
        update(p, data) {
            console.log(data);
            this.$element.find('.value-rt').text(data['RT'].displayValue);
            this.$element.find('.value-rh').text(data['RH'].displayValue);
            if (data['RT'].value > 36) {
                this.rtAlarm()
            }
            if (data['RH'].value > 85) {
                this.rhAlarm()
            }
        },
        rtAlarm() {
            this.$element.find('.value-rt').css({ 'background': 'red' });
        },
        rhAlarm() {
            this.$element.find('.value-rh').css({ 'background': 'red' });
        }

    }
    //3.注册组件为jquery插件
    $.fn.rtrh = function (options) {
        var plugin = new RTRH(this, options);
        return plugin;//返回组件对象
    }


    //负荷开关
    var temp_loadname = 'hide'
    $('.show-loadname-bt').css({
        'background-color': '',
        'font-size': '',
        'line-height': '',
        'color': '',
        'box-shadow': ''
    })

    $('.show-loadname-bt').on('click', function () {

        if (temp_loadname === 'show') {
            $('.circuit-info').css({ 'opacity': '0' })
            temp_loadname = 'hide'
            $(this).css({ 'background-color': '', 'font-size': '', 'line-height': '', 'color': '', 'box-shadow': '' })
        } else {
            $('.circuit-info').css({ 'opacity': '1' })
            temp_loadname = 'show'
            $(this).css({
                'background-color': ' rgba(66, 211, 255, 0.445)',
                'font-size': '1.2rem',
                'line-height': '2.3rem',
                'color': ' #00ffc8',
                'box-shadow': 'black 0 0 0.5rem inset',
            })

        }
    })


    //================================== 表格 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var SwitchTable = function (parent, opt) {

        const tpl =
            `<div class="bdz-table-raw">
                <div class="bdz-table-col label">N/A</div>
                <div class="bdz-table-col info">N/A</div>
                <div class="bdz-table-col uab">N/A</div>
                <div class="bdz-table-col ubc">N/A</div>
                <div class="bdz-table-col uca">N/A</div>
                <div class="bdz-table-col ia">N/A</div>
                <div class="bdz-table-col ib">N/A</div>
                <div class="bdz-table-col ic">N/A</div>
                <div class="bdz-table-col p">N/A</div>
                <div class="bdz-table-col q">N/A</div>
                <div class="bdz-table-col cos">N/A</div>
                <div class="bdz-table-col ep1">N/A</div>
                <div class="bdz-table-col ep2">N/A</div>
                <div class="bdz-table-col eq1">N/A</div>
                <div class="bdz-table-col eq2">N/A</div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0
        };
        var that = this;
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.find('.label').text(this.options.name);
        this.$element.attr('id', 'bdz-table-raw' + this.options.name);//赋予独特的id
        // this.$element.css('top', this.options.top);
        // this.$element.css('left', this.options.left);

    }
    //2.定义组件的成员变量和方法
    SwitchTable.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
            this.$element.find('.circuit-info').text(data['LoadName'].value);
        },
        update(p, data) {
            this.$element.find('.ia').text(data['IA'].value);
            this.$element.find('.ib').text(data['IB'].value);
            this.$element.find('.ic').text(data['IC'].value);
            this.$element.find('.uab').text(data['UAB'].value);
            this.$element.find('.ubc').text(data['UBC'].value);
            this.$element.find('.uca').text(data['UCA'].value);
            this.$element.find('.p').text(data['P'].value);
            this.$element.find('.q').text(data['Q'].value);
            this.$element.find('.cos').text(data['COS'].value);
            this.$element.find('.ep1').text(data['EP1'].value);
            this.$element.find('.ep2').text(data['EP2'].value);
            this.$element.find('.eq1').text(data['EQ1'].value);
            this.$element.find('.eq2').text(data['EQ2'].value);


            /*var uab = data['Uab'].value;
            var ubc = data['Ubc'].value;
            var uca = data['Uca'].value;
            if (uab != null || ubc != null || uca != null) {

            } else {

            }*/
        },

    }
    //3.注册组件为jquery插件
    $.fn.switchTable = function (options) {
        var plugin = new SwitchTable(this, options);
        return plugin;//返回组件对象
    }


    //================================== 电表详情 三合一 ===========================================
    //定义插件的标准姿势：1.构造函数 2.成员定义 3.注册插件
    //1.定义组件的构造函数
    var SwitchPanel = function (opt) {

        const tpl =
            `<div class="switch-panel" tabindex="0">

                <div class="switch-panel-title">开关名称</div>
                <div class="switch-panel-subtitle">开关负荷名称</div>
                <div class="switch-panel-close">《</div>
                <div class="switch-panel-bg"></div>

                <div class="switch-panel-chart">
                    <div class="switch-panel-chart-container" id="switch-panel-chart-container"></div>
                </div>

                <div class="switch-panel-alarm">
                    <div class="alarm-details-list">
                        <div class="alarm-details-list-row state"><div class="alarm-details-label">设备状态:</div><div class="alarm-details-value device-name">正常</div></div>
                        <div class="alarm-details-list-row"><div class="alarm-details-label">报警时间:</div><div class="alarm-details-value alarm-time">十天前</div></div>
                        <div class="alarm-details-list-row"><div class="alarm-details-label">报警处理状态:</div><div class="alarm-details-value alarm-state">无</div></div>
                        <div class="alarm-details-list-row info"><div class="alarm-details-label">报警信息:</div><div class="alarm-details-value alarm-info">无</div></div>
                    </div>

                    <div class="alarm-details-bt-operation">
                        <div class="confirm-btn">确认</div>
                        <div class="handle-btn">处理</div>
                    </div>
                </div>

                <div class="switch-panel-info">
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Uab</div> 
                        <div class="switch-panel-info-child-value uab">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Ubc</div>
                        <div class="switch-panel-info-child-value ubc">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Uca</div>
                        <div class="switch-panel-info-child-value uca">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Ia</div>
                        <div class="switch-panel-info-child-value ia">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Ib</div>
                        <div class="switch-panel-info-child-value ib">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Ic</div>
                        <div class="switch-panel-info-child-value ic">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">P</div>
                        <div class="switch-panel-info-child-value p">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">Q</div>
                        <div class="switch-panel-info-child-value q">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">COS</div>
                        <div class="switch-panel-info-child-value cos">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">EP+</div>
                        <div class="switch-panel-info-child-value ep1">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">EP-</div>
                        <div class="switch-panel-info-child-value ep2">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">EQ+</div>
                        <div class="switch-panel-info-child-value eq1">N/A</div>
                    </div>
                    <div class="switch-panel-info-child">
                        <div class="switch-panel-info-child-label">EQ-</div>
                        <div class="switch-panel-info-child-value eq2">N/A</div>
                    </div>
                </div>
            </div>`;
        this.$element = $(tpl);
        // this.$parent = parent;
        // this.$parent.append(this.$element);
        $('.switch-panel-container').empty();
        $('.switch-panel-container').append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0
        };
        var that = this;
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        this.options = $.extend({}, this.defaults, opt);
        this.$element.find('.switch-panel-title').text(this.options.name);
        // this.createCurrrentCharts(this.options.code);
        // this.$element.attr('id','bdz-table-raw' + this.options.name);//赋予独特的id
        // this.$element.css('top', this.options.top);
        // this.$element.css('left', this.options.left);
        this.$element.find('.switch-panel-close').on('click', function () {
            $infowindow = $('.switch-panel').css({ 'transform': 'translateX(-80rem)', 'opacity': '0' })
        })

    }
    //2.定义组件的成员变量和方法
    SwitchPanel.prototype = {
        show(option) {
            $('.switch-panel').css({ 'transform': 'translateX(0rem)', 'opacity': '1' });
            this.$element.find('.switch-panel-title').text(option.title);
            var data = option.data;
            // data = simulateValue('', option);
            this.$element.find('.ia').text(data['Ia'].displayValue);
            this.$element.find('.ib').text(data['Ib'].displayValue);
            this.$element.find('.ic').text(data['Ic'].displayValue);
            this.$element.find('.uab').text(data['Uab'].displayValue);
            this.$element.find('.ubc').text(data['Ubc'].displayValue);
            this.$element.find('.uca').text(data['Uca'].displayValue);
            this.$element.find('.p').text(data['P'].displayValue);
            this.$element.find('.q').text(data['Q'].displayValue);
            this.$element.find('.cos').text(data['COS'].displayValue);
            this.$element.find('.ep1').text(data['EP1'].displayValue);
            this.$element.find('.ep2').text(data['EP2'].displayValue);
            this.$element.find('.eq1').text(data['EQ1'].displayValue);
            this.$element.find('.eq2').text(data['EQ2'].displayValue);
        },
        
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
            this.$element.find('.switch-panel-subtitle').text(data['LoadName'].value);
        },
        update(p, data) {

        },
        createCurrrentCharts(deviceCode, data) {
            data = simulateValue(deviceCode, data);
            if ($('#switch-panel-chart-container')[0]) {
                var switchRealTimeCharts = echarts.init(document.getElementById('switch-panel-chart-container'));
            }

            // 模拟数据==============
            let nowTime = new Date()
            let ia = data['IA'].value
            let ib = data['IB'].value
            let ic = data['IC'].value
            let data1 = [{ value: [nowTime.toLocaleString(), data['IA'].value] }];
            let data2 = [{ value: [nowTime.toLocaleString(), data['IB'].value] }];
            let data3 = [{ value: [nowTime.toLocaleString(), data['IC'].value] }];
            let now = Date.now();
            if (ia) {
                for (let i = 0; i < 100; i++) {
                    let item = [];
                    item[0] = now - i * 1000;
                    item[1] = simulate(ia)
                    data1.push(item);
                }
            }
            if (ib) {
                for (let i = 0; i < 100; i++) {
                    let item = [];
                    item[0] = now - i * 1000;
                    item[1] = simulate(ib)
                    data2.push(item);
                }
            }
            if (ic) {
                for (let i = 0; i < 100; i++) {
                    let item = [];
                    item[0] = now - i * 1000;
                    item[1] = simulate(ic)
                    data3.push(item);
                }
            }

            function simulate(value) {
                value = value + Math.random() * value * 0.015
                return value.toFixed(2)
            }

            /*$.get('scada/current/his/' + deviceCode, function (res) {
                if (res.data.data1) {
                    res.data.data1.forEach(tsdata => {
                        let item = [];
                        item[0] = tsdata.time;
                        item[1] = tsdata.value;
                        data1.push(item);
                    });
                }
                if (res.data.data2) {
                    res.data.data2.forEach(tsdata => {
                        let item = [];
                        item[0] = tsdata.time;
                        item[1] = tsdata.value;
                        data2.push(item);
                    });
                }
                if (res.data.data3) {
                    res.data.data3.forEach(tsdata => {
                        let item = [];
                        item[0] = tsdata.time;
                        item[1] = tsdata.value;
                        data3.push(item);
                    });
                }
            });*/
            var switchRealTimeOption = {
                title: {
                    left: 'center',
                    text: '实时电流曲线/kW',
                    textStyle: {
                        fontFamily: '微软雅黑',
                        fontSize: 20,
                        fontStyle: 'normal',
                        fontWeight: '600',
                        color: 'white',
                        letterSpacing: 2,
                    }
                },
                legend: {
                    data: ['Ia', 'Ib', 'Ic'],
                    x: 'right',
                    // y:'center'
                    textStyle: {
                        fontFamily: '微软雅黑',
                        fontSize: 15,
                        fontStyle: 'normal',
                        fontWeight: '100',
                        color: 'white',
                        letterSpacing: 2,
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        animation: false
                    },

                },
                xAxis: {
                    type: 'time',
                    splitLine: {
                        show: false
                    },
                    axisLabel: {
                        show: true,
                        textStyle: {
                            fontFamily: 'BankGothic Md BT',
                            fontSize: 15,
                            fontStyle: 'normal',
                            fontWeight: 'normal',
                            color: 'white',
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: 'white',
                        }
                    },
                },
                yAxis: {
                    type: 'value',
                    // boundaryGap: [0, '100%'],
                    axisLabel: {
                        show: true,
                        textStyle: {
                            fontFamily: 'BankGothic Md BT',
                            fontSize: 12,
                            fontStyle: 'normal',
                            fontWeight: 'normal',
                            color: 'white',
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: ['rgb(255, 245, 255,0.3)'],
                            width: 1,
                            type: 'solid',
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: 'white',
                        }
                    },
                },
                series: [
                    {
                        name: 'Ia',
                        type: 'line',
                        showSymbol: false,
                        smooth: 0.5,
                        data: data1,
                        itemStyle: {
                            color: 'aqua'
                        },
                        // itemStyle: {
                        //     normal: {
                        //         lineStyle: {
                        //             color: 'aqua' //改变折线颜色
                        //         }
                        //     }
                        // },

                    },
                    {
                        name: 'Ib',
                        type: 'line',
                        showSymbol: false,
                        smooth: 0.5,
                        data: data2,
                        itemStyle: {
                            color: 'yellow'
                        },

                    },
                    {
                        name: 'Ic',
                        type: 'line',
                        showSymbol: false,
                        smooth: 0.5,
                        data: data3,
                        itemStyle: {
                            color: 'pink'
                        },

                    }
                ]
            };
            if (switchRealTimeCharts) {
                switchRealTimeOption && switchRealTimeCharts.setOption(switchRealTimeOption);
                switchRealTimeOption && switchRealTimeCharts.setOption({
                    series: [
                        {
                            data: data1
                        },
                        {
                            data: data2
                        },
                        {
                            data: data3
                        }
                    ]
                });
            }
        }

    }
    //3.注册组件为jquery插件
    $.fn.switchPanel = function (options) {
        var plugin = new SwitchPanel(this, options);
        return plugin;//返回组件对象
    }


    //================================= 主页配电系统入口  ===========================================
    //--此组件不绑定数据，只作前端按钮显示--
    var BdzBtn = function (parent, opt) {
        //this指代组件本身（transformer）

        const tpl =
            `<div class="bdz-btn-panel" tabindex="0">
                <div class="bdz-icon-box">
                    <img src="img/bdz/bdzicon.svg" class="bdz-icon">
                    <div class="line"></div>
                </div>
                <div>
                    <div class="bdz-label">变电所</div>
                    <div class="bdz-link-bt bdz-link-bt-ai">智慧巡检</div>
                    <div class="bdz-link-bt bdz-link-bt-2d">电力系统图</div>
                    <div class="bdz-link-bt bdz-link-bt-3d">空间管理</div>
                </div>
            </div>`;
        this.$parent = parent;
        this.$element = $(tpl);
        this.$parent.append(this.$element);
        this.defaults = {
            'top': 0,
            'left': 0,
            'title': "N/A",
        };
        //叠加默认选项和传入选项到一个新的对象，防止修改默认选项
        var that = this
        this.options = $.extend({}, this.defaults, opt);
        this.$element.css('top', this.options.top);
        this.$element.css('left', this.options.left);
        this.$element.find('.bdz-label').text(this.options.title);
        if (!this.options.aiTemp) {
            this.$element.find('.bdz-link-bt-ai').css({
                'background-color': 'gray',
                'opacity': '0.8',
                'pointer-events': 'none',

            })
        }
        //网页间交互
        this.$element.find('.bdz-link-bt-ai').on('click', function () {
            if (that.options.click) {
                that.options.click();
            }
            sendMessage({ func: 'changeLevelToFloor' });
            // $(this).parent().siblings().css({ 'opacity': '0.2' })
            $('#robot-ctr-btn').show()
            $('#fixed-inspection-btn').show()
            $('#container-window-test').show()
            $('.com-container').css({
                'transform': 'scale(5)',
                'opacity': '0',
                'visibility': 'hidden',
                'filter': 'blur(1.5rem)'
            })
            setTimeout(function () {
                sendMessage({ type: that.options.name + 'Camera' });
            }, 2000)

        })

        //系统图入口
        this.$element.find('.bdz-link-bt-2d').on('click', function () {
            switch (that.options.name) {
                case 'bdz1':
                    window.open('/scada/qqbb.html', '_self');
                    break;
                case 'bdz2':
                    window.open('/scada/qqnb.html', '_self');
                    break;
                default:
                    break;
            }
        })

        //BIM入口
        this.$element.find('.bdz-link-bt-3d').on('click', function () {
            window.open('https://substation.memanager.cn/forge/model-viewer.html?id=79')
        })

    }
    //2.定义组件的成员变量和方法
    BdzBtn.prototype = {
        /**
         * 根据数据更新组件显示
         * @param {组件数据} data
         */
        showCircuitInfo(data) {
        },
        update(p, data) {

        },
    }
    //3.注册组件为jquery插件
    $.fn.bdzBtn = function (options) {
        var plugin = new BdzBtn(this, options);
        return plugin;//返回组件对象
    }

    function simulateValue(p, data) {
        var noSwitch;
        if (!data) {
            data = {};
            noSwitch = true;
        }
        if (!data['IA'] || !data['IA'].value) {
            if (!data['IA']) {
                data['IA'] = {};
            }
            data['IA'].value = 0.67;
            if (noSwitch) {
                data['IA'].value = 0;
            }
            if (p.indexOf('401') > -1) {
                data['IA'].value += 100;
            }
            data['IA'].displayValue = data['IA'].value + 'A';
        }
        if (!data['IB'] || !data['IB'].value) {
            if (!data['IB']) {
                data['IB'] = {};
            }
            data['IB'].value = 0.65;
            if (noSwitch) {
                data['IB'].value = 0;
            }
            if (p.indexOf('401') > -1) {
                data['IB'].value += 100;
            }
            data['IB'].displayValue = data['IB'].value + 'A';
        }
        if (!data['IC'] || !data['IC'].value) {
            if (!data['IC']) {
                data['IC'] = {};
            }
            data['IC'].value = 0.71;
            if (noSwitch) {
                data['IC'].value = 0;
            }
            if (p.indexOf('401') > -1) {
                data['IC'].value += 100;
            }
            data['IC'].displayValue = data['IC'].value + 'A';
        }
        if (!data['UAB'] || !data['UAB'].value) {
            if (!data['UAB']) {
                data['UAB'] = {};
            }
            data['UAB'].value = 394.55;
            data['UAB'].displayValue = '394.55V';
        }
        if (!data['UBC'] || !data['UBC'].value) {
            if (!data['UBC']) {
                data['UBC'] = {};
            }
            data['UBC'].value = 394.93;
            data['UBC'].displayValue = '394.93V';
        }
        if (!data['UCA'] || !data['UCA'].value) {
            if (!data['UCA']) {
                data['UCA'] = {};
            }
            data['UCA'].value = 393.11;
            data['UCA'].displayValue = '393.11V';
        }
        if (!data['P'] || !data['P'].value) {
            if (!data['P']) {
                data['P'] = {};
            }
            data['P'].value = 0.04;
            data['P'].displayValue = '0.04KV';
        }
        if (!data['Q'] || !data['Q'].value) {
            if (!data['Q']) {
                data['Q'] = {};
            }
            data['Q'].value = 0.07;
            data['Q'].displayValue = '0.07KVar';
        }
        if (!data['COS'] || !data['COS'].value) {
            if (!data['COS']) {
                data['COS'] = {};
            }
            data['COS'].value = -0.5;
            data['COS'].displayValue = '-0.5';
        }
        if (!data['EP1'] || !data['EP1'].value) {
            if (!data['EP1']) {
                data['EP1'] = {};
            }
            data['EP1'].value = 337.05;
            data['EP1'].displayValue = '37.05kwH';
        }
        if (!data['EP2'] || !data['EP2'].value) {
            if (!data['EP2']) {
                data['EP2'] = {};
            }
            data['EP2'].value = 83.4;
            data['EP2'].displayValue = '83.4kwH';
        }
        if (!data['EQ1'] || !data['EQ1'].value) {
            if (!data['EQ1']) {
                data['EQ1'] = {};
            }
            data['EQ1'].value = 554.4;
            data['EQ1'].displayValue = '554.4kVarH';
        }
        if (!data['EQ2'] || !data['EQ2'].value) {
            if (!data['EQ2']) {
                data['EQ2'] = {};
            }
            data['EQ2'].value = 0.3;
            data['EQ2'].displayValue = '0.3kwH';
        }
        return data;
    }
})(jQuery, window, document);
