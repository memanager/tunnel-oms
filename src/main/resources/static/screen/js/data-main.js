
let tunnelMenuArry = [
    { title: '隧道管理中心' },
    { title: '青祁隧道', btns: [{ name: '配电系统图(北变电站)' , link: '/screen/scada/qqbb.html'}, { name: '配电系统图(南变电站)' , link: '/screen/scada/qqnb.html'},{ name: '三维可视化' }] },
    { title: '界泾隧道', btns: [{ name: '配电系统图', link: '/screen/scada/jjbdz.html' }, { name: '三维可视化' }] },
    { title: '蠡湖隧道', btns: [{ name: '系统组态图' }, { name: '三维可视化' }] },
    { title: '惠山隧道', btns: [{ name: '系统组态图' }, { name: '三维可视化' }] },
    { title: '金城隧道', btns: [{ name: '系统组态图' }, { name: '三维可视化' }] },
    { title: '五里湖隧道', btns: [{ name: '系统组态图' }, { name: '三维可视化' }] },
    { title: '太湖大道隧道', btns: [{ name: '系统组态图' }, { name: '三维可视化' }] },
    { title: '通江立交' },
    { title: '红星路立交' },
    { title: '广通立交' },
    { title: '周新立交' },
    { title: '学府立交' },
    { title: '雪浪立交' },
    { title: '南泉立交' },
    { title: '隐秀立交' },
    { title: '太湖东地下人行通道' },
    { title: '太湖西地下人行通道' },
    { title: '望山路地下人行通道' },
    { title: '湖滨路人行地道' },
    { title: '联结路人行地道' },

]
InfoTop = [
    { label: '建筑群', value: '20', unit: '座' },
    { label: '单洞里程', value: '49', unit: 'km' },
    { label: '总面积', value: '53', unit: '万㎡' },
    { label: '变电所', value: '22', unit: '座' },
    { label: '排水泵房', value: '79', unit: '座' },
    { label: '消防泵房', value: '8', unit: '座' },
]

sensorCountData = [
    { title: '智能电表', value: '229', color: 'rgba(127, 255, 212, 0.5)', diameter: '10rem', left: '8rem', top: '1rem' },
    { title: '视频监控', value: '17', color: 'rgba(127, 197, 255, 0.5)', diameter: '6.5rem', left: '2rem', top: '0rem' },
    { title: '高压继保', value: '11', color: 'rgba(127, 255, 212, 0.5)', diameter: '5.5rem', left: '15rem', top: '6rem' },
    { title: '红外报警', value: '2', color: 'rgba(255, 127, 227, 0.5)', diameter: '4rem', left: '17.5rem', top: '1rem' },
    { title: '液位', value: '2', color: 'rgba(127, 255, 165, 0.5)', diameter: '4rem', left: '0.5rem', top: '6rem' },
    { title: '温湿度', value: '3', color: 'rgba(255, 232, 127, 0.5)', diameter: '4.5rem', left: '22rem', top: '1rem' },
    // { title: '变压器温控器', value: '4', color: 'rgba(255, 232, 127, 0.5)', diameter: '3rem', left: '16rem', top: '0rem' },
    // { title: '水浸', value: '10', color: 'rgba(150, 127, 255, 0.5)', diameter: '3rem', left: '6rem', top: '2rem' },
    { title: '网关', value: '15', color: 'rgba(150, 127, 255, 0.5)', diameter: '6rem', left: '20rem', top: '5rem' },
    { title: '巡检机器人', value: '2', color: 'rgb(255, 145, 0,0.8)', diameter: '4rem', left: '4rem', top: '8rem', highlight: 1 },
]

tunnelData = [
    { title: '蠡湖隧道', time: '2007年12月', length: '1180m', area: '**' },
    { title: '惠山隧道', time: '2008年09月', length: '1576m', area: '**', reverse: 1 },
    { title: '青祁隧道', time: '2008年09月', length: '1775m', area: '**' },
    { title: '金城隧道', time: '2008年09月', length: '1670m', area: '**', reverse: 1 },
    { title: '隐秀立交', time: '2008年09月', length: '425m', area: '**', icoUrl: '../img/main/lijiao.png' },
    { title: '通江立交', time: '2008年09月', length: '570m', area: '**', reverse: 1, icoUrl: '../img/main/lijiao.png' },
    { title: '太湖大道隧道', time: '2011年04月', length: '4020m', area: '**' },
    { title: '红星路立交', time: '2011年04月', length: '383m', area: '**', reverse: 1, icoUrl: '../img/main/lijiao.png' },
    { title: '广通立交', time: '2018年11月', length: '536m', area: '**', icoUrl: '../img/main/lijiao.png' },
    { title: '周新立交', time: '2019年05月', length: '610m', area: '**', reverse: 1, icoUrl: '../img/main/lijiao.png' },
    { title: '学府立交', time: '2019年05月', length: '2815m', area: '**', icoUrl: '../img/main/lijiao.png' },
    { title: '雪浪立交', time: '2019年05月', length: '591m', area: '**', reverse: 1, icoUrl: '../img/main/lijiao.png' },
    { title: '南泉立交', time: '2019年05月', length: '395m', area: '**', icoUrl: '../img/main/lijiao.png' },
    { title: '五里湖隧道', time: '2024年', length: '5960m', area: '**', reverse: 1 },
    { title: '界泾隧道', time: '2024年', length: '835m', area: '**' },
]

operationData = [
    { label: '日常巡检', value: '140次', icoUrl: '../img/main/mile-icon.png' },
    { label: '缺陷上报', value: '12次', icoUrl: '../img/main/faults.png' },
    { label: '缺陷维修', value: '12次', icoUrl: '../img/main/tools.png' },
    { label: '任务工单', value: '5次', icoUrl: '../img/main/paperClip.png' },
    { label: '专项保养', value: '24次', icoUrl: '../img/main/paper.png' },
]

carData = [
    { title: '巡视车', count: '3辆', value1: '60次', value2: '1200km' },
    { title: '面包车', count: '1辆', value1: '60次', value2: '1200km' },
    { title: '举升车(小)', count: '1辆', value1: '15次', value2: '700km' },
    { title: '举升车(大)', count: '1辆', value1: '16次', value2: '630km' },
    { title: '叉车', count: '1辆', value1: '1次', value2: '30km' },
    { title: '其他车辆', count: '1辆', value1: '1次', value2: '40km' },

]
// operationScoreData = [
//     { label: '工单响应及时率', value: '98%' },
//     { label: '本年度总工时', value: '1220h' },
// ]

deviceHealthData = [
    { label: '系统综合评分', value: '**分', color: 'rgb(0, 255, 0)' },

]



personStatisticsData = [
    { name: '人员01', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 98 },
    { name: '人员02', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 96 },
    { name: '人员03', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 96 },
    { name: '人员04', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 92 },
    { name: '人员05', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 90 },
    { name: '人员06', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 88 },
    { name: '人员07', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 88 },
    { name: '人员08', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 80 },
    { name: '人员09', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 78 },
    { name: '人员10', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 78 },
    { name: '人员11', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 78 },
    { name: '人员12', state: { value: '在岗', 'color': 'aqua' }, num: 25, score: 78 },

]
carStatisticsData = [
    { type: '巡视车', num: '苏B11GP5', state: { value: '姚**', 'color': 'aqua' }, mileage: '2600km' },
    { type: '巡视车', num: '苏B959V2', state: { value: '袁**', 'color': 'aqua' }, mileage: '1620km' },
    { type: '面包车', num: '苏B72037', state: { value: '吴**', 'color': 'aqua' }, mileage: '970km' },
    { type: '面包车', num: '苏B11GP5', state: '待命', mileage: '2612km' },
    { type: '巡视车', num: '苏BQ9232', state: '待命', mileage: '1600km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '200km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '20km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '10km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '0km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '0km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '0km' },
    { type: '巡视车', num: '苏B11GP5', state: '待命', mileage: '0km' },


]




toolStatisticsData = [
    { name: '液压钳', person: '张**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '7天前' },
    { name: '电缆剪', person: '王**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '3天前' },
    { name: '万用表', person: '刘**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '8件', time: '3天前' },
    { name: '网络测试仪', person: '张**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '3件', time: '今天11:30' },
    { name: '网络测试仪', person: '张**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '今天13:20' },
    { name: '网络测试仪', person: '吴**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '今天14:20' },
    { name: '万用表', person: '吴**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '今天15:20' },
    { name: '万用表', person: '吴**', state: { value: '未归还', 'color': 'rgb(255, 54, 54)' }, num: '2件', time: '今天16:20' },

]








personIntelligenceData = [
    { name: '钱**', value1: '455单', value2: '1925.7h', num: '62单', score: '170.0h' },
    { name: '孙**', value1: '452单', value2: '1925.5h', num: '59单', score: '165.5h' },
    { name: '张**', value1: '451单', value2: '1927.6h', num: '55单', score: '165.2h' },
    { name: '王**', value1: '430单', value2: '1888.0h', num: '55单', score: '163.5h' },
    { name: '吴**', value1: '426单', value2: '1888.0h', num: '55单', score: '160.0h' },
    { name: '刘**', value1: '425单', value2: '1735.0h', num: '53单', score: '155.5h' },
    { name: '邓**', value1: '421单', value2: '1712.0h', num: '53单', score: '155.2h' },
    { name: '陈**', value1: '420单', value2: '1700.8h', num: '53单', score: '153.0h' },
    { name: '庄**', value1: '420单', value2: '1635.2h', num: '53单', score: '153.0h' },
    { name: '朱**', value1: '415单', value2: '1635.2h', num: '53单', score: '152.1h' },
    { name: '李**', value1: '395单', value2: '1535.5h', num: '52单', score: '152,0h' },

]

spareIntelligenceData = [
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },
    { name: '***', value1: { value: '5件', 'color': 'orange' }, time1: '2024-11-11 09:20', time2: '2024-08-11 07:20' },

]

intelligenceDeviceCount = [
    { label: '维修次数/总数', value1: 59, value2: 422, left: '3rem', top: '4rem' },
    { label: '‌南方泵业', value1: 45, value2: 204, smallSize: 1, left: '10rem', top: '6.5rem' },
    { label: '海德隆', value1: 6, value2: 18, smallSize: 1, left: '5rem', top: '10rem' },
    { label: '浙江金盾', value1: 8, value2: 200, smallSize: 1, left: '10rem', top: '14rem' },

]



fileData = [
    { label: '', link: '', left: '2.5rem', top: '7rem', pos: 'top-left' },
    { label: '', link: '', left: '14rem', top: '4rem', pos: 'top' },
    { label: '', link: '', left: '25.5rem', top: '7rem', pos: 'top-right' },
    { label: '全维度考核评分报表', link: '', left: '2rem', top: '26rem', pos: 'bottom-left' },
    { label: '设备重要参数报警报表', link: '', left: '26rem', top: '26rem', pos: 'bottom-right' },

]

aiCountData = [
    { label: '◈ AI预警准确率  -', value: '99.8%', left: '12rem', top: '0.5rem' },
    { label: '◈ AI预警累计次数  -', value: '129次', left: '0rem', top: '3.5rem' },
    { label: '◈ AI预警已处理数  -', value: '117次 ', left: '22rem', top: '3.5rem' },

]

aiWarningData = [
    { device: 'JJ_401A', value: { value: 'A相电流', color: 'orange' }, info: 'A相电流过电流', time: '2024-11-18 10:04' },
    { device: 'QQ_421A', value: { value: 'A相电流', color: 'orange' }, info: 'A相电流过电流', time: '2024-11-15 14:20' },
    { device: 'QQ_422A', value: { value: 'A相电流', color: 'orange' }, info: 'A相电流过电流', time: '2024-11-15 10:20' },
    { device: 'NSB-1', value: { value: '液位,水泵状态', color: 'orange' }, info: '液位超限且水泵未启动', time: '2024-11-15 09:22' },
    { device: 'NSB-1', value: { value: '液位,水泵状态', color: 'orange' }, info: '液位超限且水泵未启动', time: '2024-11-15 09:22' },
    { device: 'JJ_411A', value: { value: 'A相电流', color: 'orange' }, info: 'A相电流过电流', time: '2024-11-15 14:20' },
    { device: 'JJ_412B', value: { value: 'A相电流', color: 'orange' }, info: 'A相电流过电流', time: '2024-11-15 14:20' },

]

importantDeviceData = [
    { label: '灯具', value: '+22', unit: '%', alarm: 1 },
    { label: '水泵', value: '+18', unit: '%', alarm: 1 },
    { label: '配电柜', value: '+16', unit: '%', alarm: 1 },
    { label: '伸缩缝', value: '+16', unit: '%', alarm: 1 },
    { label: '射流风机', value: '+12', unit: '%', alarm: 1 },
    { label: '装饰', value: '+3', unit: '%' },
    { label: '路面', value: '+3', unit: '%' },
    { label: '消防泵', value: '+1', unit: '%' },
]