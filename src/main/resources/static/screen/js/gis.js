
//各隧道坐标及名称
//pLabel:gis地图上文字标签位置
//menu:右侧菜单按钮是否需要添加二级菜单
//alarm:是否报警 alarm: 1,
var point = [
    { position: [120.294677, 31.536103], name: '隧道管理中心' },
    { position: [120.277196, 31.542794], name: '青祁隧道', link: 'qqsd.html', menu: 1, pic: [{ url: 'img/tunnel/qqsd.jpg', scan: 0 },], data: [{ label: '双洞长度(m)', value: '1900' }, { label: '限高(m)', value: '4.5' }, { label: '通车日期', value: '2008.09' }, { label: '运行时间', value: '14年' }] },
    { position: [120.281701, 31.528271], name: '金城隧道', menu: 1, pic: [{ url: 'img/tunnel/jcsd.jpg', scan: 0 },], data: [{ label: 'ZX段长度(m)', value: '506' }, { label: 'NE段长度(m)', value: '755' }, { label: 'EN段长度(m)', value: '634' }, { label: '限高(m)', value: '4.5' }, { label: '通车日期', value: '2008.09' }, { label: '运行时间', value: '14年' }] },
    { position: [120.273104, 31.577817], name: '惠山隧道', menu: 1, pic: [{ url: 'img/tunnel/hssd.jpg', scan: 0 },], data: [{ label: '左线长度(m)', value: '1576' }, { label: '右线长度(m)', value: '1555' }, { label: '限高(m)', value: '4.8' }, { label: '通车日期', value: '2008.09' }, { label: '运行时间', value: '14年' }] },
    { position: [120.309064, 31.554723], name: '太湖大道隧道', menu: 1, pic: [{ url: 'img/tunnel/thddsd.jpg', scan: 0 },], data: [{ label: '双洞长度(m)', value: '4045' }, { label: '限高(m)', value: '4.2' }, { label: '通车日期', value: '2011.04' }, { label: '运行时间', value: '近12年' }] },
    { position: [120.292175, 31.531522], name: '蠡湖隧道', menu: 1, pic: [{ url: 'img/tunnel/lhsd.jpg', scan: 0 },], pLabel: [-75, -13], data: [{ label: '双洞长度(m)', value: '1180' }, { label: '限高(m)', value: '4.5' }, { label: '通车日期', value: '2007.12' }, { label: '运行时间', value: '近15年' }] },
    { position: [120.278705, 31.533742], name: '隐秀立交' },
    { position: [120.315028, 31.593597], name: '通江立交' },
    { position: [120.295585, 31.547291], name: '红星路立交' },
    { position: [120.277219, 31.546568], name: '太湖东地下人行通道' },
    { position: [120.276397, 31.546803], name: '太湖西地下人行通道', pLabel: [-135, -13] },
    { position: [120.277785, 31.538605], name: '望山路地下人行通道', pLabel: [-135, -13] },
    { position: [120.292418, 31.531353], name: '湖滨路人行地道' },
    { position: [120.291681, 31.564388], name: '联结路人行地道' },
    { position: [120.282959, 31.513188], name: '周新立交' },
    { position: [120.283929, 31.500835], name: '学府立交' },
    { position: [120.287185, 31.459549], name: '雪浪立交' },
    { position: [120.294803, 31.447547], name: '南泉立交' },
    { position: [120.317801, 31.605173], name: '广通立交' },
    { position: [120.334858, 31.509197], name: '五里湖隧道' },
    { position: [120.286045, 31.71475], name: '界泾隧道' },

]
//人员实时坐标
var personPoint = [
    { position: [120.28, 31.57], name: '周华' },
    { position: [120.276, 31.57], name: '祁友安' },
    { position: [120.277, 31.51], name: '徐寿根' },
    { position: [120.269, 31.592], name: '盛欧闻' },
]
//各隧道分段 
//有的隧道是曲线或多段线。所以需要多个坐标绘制
var markerPath = [
    { name: '青祁隧道', path: [[120.277134, 31.542929], [120.276487, 31.556123]] },
    {
        name: '惠山隧道', path: [
            [120.268941, 31.590441], [120.268438, 31.589549], [120.267971, 31.588226],
            [120.267899, 31.586996], [120.267971, 31.585704], [120.268007, 31.585242],
            [120.269408, 31.582597], [120.270486, 31.581335], [120.27196, 31.579859], [120.272822, 31.578075]
        ]
    },
    { name: '蠡湖隧道', path: [[120.292175, 31.531522], [120.291494, 31.529302], [120.291413, 31.528809], [120.291808, 31.526047], [120.293074, 31.52355]] },
    { name: '金城隧道', path: [[120.278171, 31.531238], [120.278383, 31.529121], [120.278895, 31.528332], [120.280135, 31.52814], [120.281701, 31.528271]] },
    { name: '金城隧道2', path: [[120.278171, 31.531238], [120.278315, 31.527136]] },
    { name: '太湖大道隧道', path: [[120.309064, 31.554723], [120.313412, 31.558753], [120.319485, 31.563491], [120.323186, 31.565522], [120.336876, 31.573706], [120.340675, 31.576671]] },
    { name: '太湖大道隧道2', path: [[120.309064, 31.554723], [120.307861, 31.554484], [120.307048, 31.554634], [120.306679, 31.554973]] },
    { name: '太湖大道隧道3', path: [[120.309064, 31.554723], [120.308526, 31.553891], [120.308499, 31.553157], [120.308498, 31.553149]] },
    { name: '五里湖隧道', path: [[120.334858, 31.509197], [120.319534, 31.505845], [120.284204, 31.499064], [120.277982, 31.497894]], style: { strokeColor: 'aqua', strokeWeight: 8, strokeOpacity: 0.3 } },

];
//车辆测试
// var carPoint = [
//     { lng: 120.240295, lat: 31.562512, velocity: 0.0, no: '苏EZ803S', state: 0, angle: 0, address: '江苏省无锡市滨湖区梁溪路982号巨帆新能源充电站(梁湖家园快充站)南231米' },
//     { lng: 120.295435, lat: 31.536039, velocity: 0.0, no: '苏B19J8P', state: 1, angle: 63, angleaddress: '江苏省无锡市滨湖区隐秀路57号无锡市市政设施管理中心内' },
//     { lng: 120.294976, lat: 31.535912, velocity: 0.0, no: '苏B11GP5', state: 0, angle: 0, address: '江苏省无锡市滨湖区隐秀路59号无锡市市政设施管理中心内,无锡市隧道控制中心附近38米' },
//     { lng: 120.281555, lat: 31.53431, velocity: 0.0, no: '苏BQ3P51', state: 0, angle: 0, address: '江苏省无锡市滨湖区经贸路24号无锡市建筑垃圾工程渣土处置协会东北141米' },
//     { lng: 120.281441, lat: 31.535217, velocity: 0.0, no: '苏B703W9', state: 0, angle: 8, address: '江苏省无锡市滨湖区经贸路20号无锡市建筑垃圾工程渣土处置协会北250米' },

// ]
let carMarkerCom = []
let carLabelCom = []
let carNeed = ['苏B11GP5','苏B959V2','苏B19J8P','苏B27D5K','苏B72037','苏B22MU8','苏BQ9232','苏BKH950','苏B79F0J','苏BGR802','苏B78673']
getCarGps()
setInterval(() => {
    getCarGps()
}, 2000)
function getCarGps() {
    $.ajax({
        url: 'https://tunnel.memanager.cn/vehicle-gps/status?map=2',
        type: 'GET',
        // async: false, // 设置为同步请求
        success: function (res) {
            carMarkerCom.forEach(marker => {
                map.removeOverlay(marker);
            });
            carMarkerCom = []
            carLabelCom.forEach(label => {
                map.removeOverlay(label);
            });
            carLabelCom = []
            let carPoint = []
            let data = res.data
            data.forEach(item => {
                if(carNeed.includes(item.no)){
                    let p = new BMapGL.Point(item.lng, item.lat)
                    carPoint.push({ no: item.no, position: p, angle: item.angle, state: item.state })    
                }
            });
            carPoint.forEach(item => {
                createCarMarker(item)
            });
        }
    });

}
//================================================================
//=========================== GIS地图 ============================
//================================================================

var map = new BMapGL.Map('map-container', {
    showVectorStreetLayer: true, // 设置是否加载POI
    showVectorLine: true   //设置是否加载路网数据，注意:路网数据的加载依赖必需加载POI。
}); // 创建Map实例
// map.setMapType(BMAP_PERSPECTIVE_MAP);
map.centerAndZoom(new BMapGL.Point(120.309292, 31.500295), 14); // 初始化地图,设置中心点坐标（无锡）和地图级别
map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
map.setMapType(BMAP_SATELLITE_MAP);      // 设置地图类型为普通卫星地图模式
// 禁用地图旋转功能
map.disableRotate();
// 禁用底图文字标注
map.setHeading(20);
map.setTilt(65);
//返回地图初始位置角度
function mapInitial() {
    map.centerAndZoom(new BMapGL.Point(120.297292, 31.513295), 14); // 初始化地图,设置中心点坐标（无锡）和地图级别
}
// map.setMinZoom(13);
//天空颜色
map.setDisplayOptions({
    skyColors: ['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.5)']
})

//自定义地图样式
map.setMapStyleV2({
    styleId: '2173ba9a3d33c9bf99be313ced5ea622'
});

// 创建图标
var myIcon = new BMapGL.Icon("../img/main/mapMarker.svg", new BMapGL.Size(20, 30));
var myIconAlarm = new BMapGL.Icon("../img/main/mapMarkerAlarm.svg", new BMapGL.Size(20, 30));
var personIcon = new BMapGL.Icon("../img/main/person.svg", new BMapGL.Size(30, 30));

//标注文字样式
var markerLabelStyle2 = {
    opacity: 0,
    display: 'none'
}
var markerLabelStyle = {
    color: 'white',
    borderRadius: '5px',
    borderColor: 'aqua',
    padding: '2px 6px',
    fontSize: '12px',
    fontFamily: '微软雅黑',
    fontWeight: '600',
    background: 'rgba(0,0,0,0.5)',
    boxShadow: 'aqua 0 0 5px',
    opacity: 1,
    display: 'block'
}
var markerLabelAlarm = {
    color: 'white',
    borderRadius: '5px',
    borderColor: 'aqua',
    padding: '2px 6px',
    fontSize: '12px',
    fontFamily: '微软雅黑',
    fontWeight: '600',
    background: 'red',
}
var personLabelStyle = {
    color: 'aqua',
    borderRadius: '5px',
    borderColor: 'transparent',
    padding: '3px 10px',
    fontSize: '12px',
    fontFamily: '微软雅黑',
    fontWeight: '600',
    background: 'rgba(0,0,0,0.5)'
}
var labelMouseOver = {
    background: 'rgba(0,255,255,0.5)'
}
var carLabelStyle = {
    color: 'white',
    // borderRadius: '5px',
    borderColor: 'aqua',
    padding: '2px 6px',
    fontSize: '12px',
    fontFamily: '微软雅黑',
    fontWeight: '600',
    background: 'rgba(0,0,0,0.2)',
    boxShadow: 'aqua 0 0 5px',
    opacity: 0,
    display: 'block'
}
let btntemp = 'hide'
$('#btn-cldw').on('click', function () {
    if (btntemp == 'show') {
        carLabelStyle.opacity = 0
        btntemp = 'hide'
        console.log(btntemp, carLabelStyle.opacity);
        getCarGps()
    } else {
        carLabelStyle.opacity = 1
        btntemp = 'show'
        console.log(btntemp, carLabelStyle.opacity);
        getCarGps()
    }
})
$('#btn-sdbz').on('click', function () {
    mapLabels.forEach(function (label) {
        if (map.getOverlays().indexOf(label) !== -1) {
            map.removeOverlay(label); // 如果标签在地图上，则移除
        } else {
            map.addOverlay(label); // 如果标签不在地图上，则添加
        }
    });
    sendMessage({ func: 'getUnityFunction', param: { objectName: 'LayerRenderer', objectFunc: 'SwitchBiaoShi' } })
})
//创建标注  图表+文字 传入坐标+名称
function createMapMarker(option) {
    // 创建图标标注
    var point = option.point//坐标位置
    var name = option.name//标题名称
    var link = option.link//点击跳转链接
    var pLabel = option.pLabel//坐标偏转
    var alarm = option.alarm//报警状态
    var marker
    // 创建文本标注对象
    var positionLabel
    pLabel ? positionLabel = pLabel : positionLabel = [12, -13]
    var opts = {
        position: point, // 指定文本标注所在的地理位置
        offset: new BMapGL.Size(positionLabel[0], positionLabel[1]) // 设置文本偏移量
    };
    var label = new BMapGL.Label(name, opts);
    // 自定义文本标注样式
    if (alarm) {
        marker = new BMapGL.Marker(point, { icon: myIconAlarm });//报警标注
        map.addOverlay(marker);
        label.setStyle(markerLabelAlarm);
        label.addEventListener('mouseout', function () {
            label.setStyle(markerLabelAlarm);
        }
        )
    } else {
        marker = new BMapGL.Marker(point, { icon: myIcon });//正常状态标注
        map.addOverlay(marker);
        label.setStyle(markerLabelStyle);
        label.addEventListener('mouseout', function () {
            label.setStyle(markerLabelStyle);
        }
        )
    }
    //默认初始不添加标签
    // map.addOverlay(label);

    //标注监听事件
    label.addEventListener('click', function () {
        if (link)
            window.open(link)
    }
    )
    marker.addEventListener('click', function () {
        if (link)
            window.open(link)
    }
    )
    label.addEventListener('mouseover', function () {
        label.setStyle(labelMouseOver);
    }
    )

    return label
}


//重新创建标注div实现icon旋转
function createRotatedIcon() {
    // 创建一个包含图片的div元素
    var div = document.createElement('div');
    let imgUrl
    if (this.properties.state) {
        imgUrl = "../img/main/carOnline.png"
        div.style.zIndex = 99;
    } else {
        imgUrl = "../img/main/carOffline.png"
        div.style.zIndex = -1;
    }

    div.style.width = '25px';
    div.style.height = '25px';
    div.style.position = 'absolute';
    div.style.overflow = 'hidden';
    div.style.filter = 'drop-shadow(aqua 0 0 2px)'; // 应用旋转
    div.style.transform = 'rotate(' + this.properties.angle + 'deg)'; // 应用旋转
    div.title = this.properties.title; // 应用旋转

    // 创建一个img元素，并将其添加到div中
    var img = document.createElement('img');
    img.src = imgUrl;
    img.style.width = '100%';
    img.style.height = '100%';
    div.appendChild(img);
    return div;
}
//创建车辆标注  图标+文字 传入坐标+名称
function createCarMarker(option) {
    // 创建图标标注
    let point = option.position//坐标位置
    let no = option.no//标题名称
    let link = option.link//点击跳转链接

    // 创建文本标注对象
    // var positionLabel
    var opts = {
        position: point, // 指定文本标注所在的地理位置
        offset: new BMapGL.Size(12, -12) // 设置文本偏移量
    };
    var label = new BMapGL.Label(option.no, opts);
    // 根据车辆状态选择图标
    // let icon = option.status === 'online' ? carOnlineIcon : carOfflineIcon;
    let marker = new BMapGL.CustomOverlay(createRotatedIcon, {
        point: point,
        properties: {
            angle: option.angle,
            title: option.no,
            state: option.state

        }

    });
    map.addOverlay(marker);
    carMarkerCom.push(marker);
    map.addOverlay(label);
    carLabelCom.push(label);

    label.setStyle(carLabelStyle);
    label.addEventListener('mouseout', function () {
        label.setStyle(carLabelStyle);
    }
    )
    //默认初始不添加标签
    // map.addOverlay(label);

    //标注监听事件
    label.addEventListener('click', function () {
        if (link)
            window.open(link)
    }
    )
    marker.addEventListener('click', function () {
        if (link)
            window.open(link)
    }
    )
    label.addEventListener('mouseover', function () {
        label.setStyle(labelMouseOver);
    }
    )

    // 返回一个对象，包含 marker 和 label 的引用以及一个更新位置的方法
    return {
        marker: marker,
        label: label,
    };
}
//创建人员位置标注  图表+文字 传入坐标+名称
function createPersonMarker(point, name) {
    // 创建图标标注
    var marker = new BMapGL.Marker(point, { icon: personIcon });
    map.addOverlay(marker);
    // 创建文本标注对象
    var opts = {
        position: point, // 指定文本标注所在的地理位置
        offset: new BMapGL.Size(15, -10) // 设置文本偏移量
    };
    var label = new BMapGL.Label(name, opts);
    // 自定义文本标注样式
    label.setStyle(personLabelStyle);
    map.addOverlay(label);

}
//设置标注
//point为各隧道对象数组
let mapLabels = []
let mapLabelsCar = []
point.forEach(index => {
    let option = $.extend({}, index);
    let p = new BMapGL.Point(option.position[0], option.position[1])
    mapLabels.push(createMapMarker({ point: p, name: option.name, link: option.link, pLabel: option.pLabel, alarm: option.alarm }))
});



//触发动画
//设置聚焦效果
function positionViewAnimation(option) {
    var startPoint = new BMapGL.Point(map.getCenter().lng, map.getCenter().lat)
    var startZoom = map.getZoom()
    var targetPoint = new BMapGL.Point(option.position[0], option.position[1])
    // 定义关键帧
    var keyFrames = [
        {
            center: startPoint,
            zoom: startZoom,
            tilt: 70,
            heading: 0,
            percentage: 0
        },
        {
            center: targetPoint,
            zoom: 19,
            tilt: 70,
            heading: 0,
            percentage: 1
        },
    ];
    var opts = {
        duration: 1000,
        delay: 0,
        // interation: 'INFINITE'
    };

    // 声明动画对象
    var animation = new BMapGL.ViewAnimation(keyFrames, opts);
    // 监听事件
    animation.addEventListener('animationstart', function (e) { console.log('start') });
    animation.addEventListener('animationiterations', function (e) { console.log('onanimationiterations') });
    animation.addEventListener('animationend', function (e) { console.log('end') });
    map.startViewAnimation(animation)

}
function gisCruise(path) {
    var point = [];
    for (var i = 0; i < path.length; i++) {
        point.push(new BMapGL.Point(path[i].position[0], path[i].position[1]));
    }
    var pl = new BMapGL.Polyline(point, { strokeColor: 'aqua', strokeWeight: 2, strokeOpacity: 0.5 });
    trackAni = new BMapGLLib.TrackAnimation(map, pl, {
        overallView: true,
        tilt: 30,
        duration: 10000,
        delay: 300,
        // strokeColor:"aqua"
    });
    trackAni.start();
}

//设置分段线标注
//style:{ strokeColor: 颜色, strokeWeight: 线粗, strokeOpacity: 透明度 }
function createLine(path, style) {
    var point = [];
    for (var i = 0; i < path.length; i++) {
        point.push(new BMapGL.Point(path[i][0], path[i][1]));
    }
    style ? style = style : style = { strokeColor: 'aqua', strokeWeight: 8, strokeOpacity: 0.8 }
    var polyline = new BMapGL.Polyline(point, style);
    map.addOverlay(polyline);
}
markerPath.forEach(index => {
    var option = $.extend({}, index);
    createLine(option.path, option.style)
});

// 清除覆盖物
function removeOverlay() {
    map.clearOverlays();
}

//地图特效
// 2. 创建MapVGL图层管理器
var view = new mapvgl.View({
    map: map
});

// 3. 创建可视化图层，并添加到图层管理器中
function createFlyLine(path) {
    var layer = new mapvgl.LineTripLayer({
        trailLength: 5,
        color: 'rgb(0, 255, 255)',
        data: [{
            geometry: {
                type: 'LineString',
                coordinates: path
            }
        }]
    });
    view.addLayer(layer);
}

markerPath.forEach(index => {
    var option = $.extend({}, index);
    createFlyLine(option.path)
});


//报警雷达扩散效果
function createGisAlarmAniamtion(position) {
    var size1 = 150;
    function CustomSymbol(_size, _anchor) {
        BMapGL.Symbol.call(this, _size, _anchor);
        this.width = _size.width;
        this.height = _size.height;
        // 需要再addOverlay之前设置true，需要保证纹理大小不变化
        this.isReDraw = true;
    }
    CustomSymbol.prototype = new BMapGL.Symbol();
    CustomSymbol.prototype.constructor = CustomSymbol;
    CustomSymbol.prototype.add = function () {
        const canvas = document.createElement('canvas');
        canvas.width = this.width * 1;
        canvas.height = this.height * 1;
        this.context = canvas.getContext('2d');
        this.isReDraw = true;

    }
    CustomSymbol.prototype.render = function (map) {
        const duration = 1500;
        const t = (performance.now() % duration) / duration;

        // 可以通过修改 0.3 0.7
        const radius = (this.width / 2) * 0.1;
        const outerRadius = (this.width / 2) * 0.5 * t + radius;
        const context = this.context;
        context.save();
        // 2倍图
        context.scale(1, 1);
        context.clearRect(0, 0, this.width, this.height);

        // 扩散圆
        context.beginPath();
        context.arc(
            this.width / 2,
            this.height / 2,
            outerRadius,
            0,
            Math.PI * 2
        );
        context.fillStyle = `rgba(255,0,0, ${1 - t})`;
        context.fill();

        // 中间圆
        context.beginPath();
        context.arc(
            this.width / 2,
            this.height / 2,
            radius,
            0,
            Math.PI * 2
        );
        context.fillStyle = 'rgba(255,0,0, 1)';
        context.strokeStyle = 'rgba(255,0,0, .1)';
        // 圆描边宽度动态展示
        context.lineWidth = 2 + 2 * (1 - t);
        context.fill();
        context.stroke();

        context.restore();

        // 更新图像的像素数据
        this.data = context.getImageData(
            0,
            0,
            this.context.canvas.width,
            this.context.canvas.height
        );
        return true;
    }
    var custom = new CustomSymbol(new BMapGL.Size(size1, size1), new BMapGL.Size(size1 / 2, size1 / 2));
    // var point = new BMapGL.Point(116.404, 39.925);
    // map.centerAndZoom(point, 15);
    // map.enableScrollWheelZoom(true);

    //标记位置
    var markerAlarm = new BMapGL.Marker(new BMapGL.Point(120.294677, 31.536103), { icon: custom, enableDragging: false });
    // markerAlarm.id='markerAlarm'
    map.addOverlay(markerAlarm);
    function start() {
        custom.isReDraw = true;
        canvasOverlay.isReDraw = true;

    }
    //停止动画
    function stop() {
        custom.isReDraw = false;
        canvasOverlay.isReDraw = false;
    }

    // 自定义canvas
    const canvas = document.createElement('canvas');
    canvas.width = size1 * 2;
    canvas.height = size1 * 2;

    function getTextureCanvas() {
        const duration = 1000;
        const t = (performance.now() % duration) / duration;

        const radius = (size1 / 2) * 0.3;
        const outerRadius = (size1 / 2) * 0.7 * t + radius;
        const context = canvas.getContext('2d');
        context.save();
        // 2倍图
        context.scale(2, 2);
        // Draw the outer circle.
        context.clearRect(0, 0, size1, size1);
        context.beginPath();
        context.arc(
            size1 / 2,
            size1 / 2,
            outerRadius,
            0,
            Math.PI * 2
        );
        context.fillStyle = `rgba(255, 200, 200, ${1 - t})`;
        context.fill();

        // Draw the inner circle.
        context.beginPath();
        context.arc(
            size1 / 2,
            size1 / 2,
            radius,
            0,
            Math.PI * 2
        );
        context.fillStyle = 'rgba(255, 100, 100, 1)';
        context.strokeStyle = 'white';
        context.lineWidth = 2 + 4 * (1 - t);
        context.fill();
        context.stroke();
        context.restore();

    }
    getTextureCanvas();

}

